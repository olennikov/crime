export const environment = {
  production: false,
  baseUrl: 'http://localhost:4200/api',
  websocketBaseUrl: `http://localhost:4200`,
  fileDownloadUrl: `http://localhost:8080/api/files/`
};
