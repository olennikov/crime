export const environment = {
  production: true,
  baseUrl: 'http://192.168.33.2:8081/api',
  websocketBaseUrl: `http://192.168.33.2:8081/api`,
  fileDownloadUrl: `http://192.168.33.2:8081/api/files/`
};
