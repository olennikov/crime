export const environment = {
  production: true,
  baseUrl: 'http://ib.cirkul-m.ru:8081/api',
  websocketBaseUrl: `http://ib.cirkul-m.ru:8081/api`,
  fileDownloadUrl: `http://ib.cirkul-m.ru:8081/api/files/`
};
