import {Component} from '@angular/core';
import {MenuItem} from '../../../shared/models/menu/menu-item';
import {ExpertiseApplication} from '../../../shared/models/domain/expertise-application';
import {
  ExpertiseApplicationStatus,
  ExpertiseApplicationStatusDisplayValue
} from '../../../shared/models/domain/expertise-application-status';
import {TableHeaderColumn} from '../../../shared/components/table/TableHeaderColumn';
import {ExpertiseApplicationService} from '../../services/expertise-application.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Util} from '../../../shared/services/Util';

@Component({
  selector: 'app-application-list',
  templateUrl: './application-list.component.html',
  styleUrls: ['./application-list.component.scss']
})
export class ApplicationListComponent {

  dataSource: ExpertiseApplication[] = [];

  tableColumns: TableHeaderColumn[] = [
    new TableHeaderColumn('id', 'ID'),
    new TableHeaderColumn('name', 'Наименование'),
    new TableHeaderColumn('description', 'Описание'),
    new TableHeaderColumn('receiveDate', 'Дата создания', (receiveDate: Date) => Util.formatDate(receiveDate)),
    new TableHeaderColumn('status', 'Статус', (status: ExpertiseApplicationStatus) => ExpertiseApplicationStatusDisplayValue.get(status))
  ];

  applicationsStatus: MenuItem[] = [
    {displayValue: 'Все', routerLink: 'ALL'},
    {displayValue: 'На рассмотрении', routerLink: ExpertiseApplicationStatus.NEW},
    {displayValue: 'Активные', routerLink: ExpertiseApplicationStatus.IN_WORK},
    {displayValue: 'Завершенные', routerLink: ExpertiseApplicationStatus.ACCEPT},
    {displayValue: 'Отклоненные', routerLink: ExpertiseApplicationStatus.REJECT}
  ];

  constructor(private expertiseApplicationService: ExpertiseApplicationService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
  }

  selectedItemChange(menuItem: MenuItem) {
    this.expertiseApplicationService.getExpertiseApplications(menuItem.routerLink)
      .subscribe(data => this.dataSource = data);
  }

  rowClick(expertiseApplication: ExpertiseApplication) {
    this.router.navigate([`./${expertiseApplication.id}`], {relativeTo: this.activatedRoute});
  }
}
