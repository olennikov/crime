import {Component, OnInit, ViewChild} from '@angular/core';
import {ExpertiseApplicationService} from '../../services/expertise-application.service';
import {ExpertiseApplication} from '../../../shared/models/domain/expertise-application';
import {ActivatedRoute, Router} from '@angular/router';
import {FileUploaderComponent} from '../../../shared/components/file/file-uploader/file-uploader.component';
import {Urls} from '../../../core/urls/url';
import {ChatService} from '../../../core/services/chat/chat.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UserFioPipe} from '../../../shared/pipes/user-fio.pipe';
import {
  ExpertiseApplicationStatus,
  ExpertiseApplicationStatusDisplayValue
} from '../../../shared/models/domain/expertise-application-status';
import {File} from '../../../shared/models/domain/file';
import {PopupService} from '../../../shared/services/popup/popup.service';
import {InfoPopupBuilder} from '../../../shared/components/popup/info-popup/InfoPopup';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-application-item',
  templateUrl: './application-item.component.html',
  styleUrls: ['./application-item.component.scss']
})
export class ApplicationItemComponent implements OnInit {

  @ViewChild(FileUploaderComponent, {static: false})
  uploader: FileUploaderComponent;

  expertiseApplication: ExpertiseApplication = new ExpertiseApplication(null, '', '', null, null, null, '', '', []);

  isNew = true;
  isUserChatParticipant: boolean = false;

  applicationFormGroup: FormGroup;

  constructor(private route: Router,
              private activatedRoute: ActivatedRoute,
              private expertiseApplicationService: ExpertiseApplicationService,
              private chatService: ChatService,
              private formBuilder: FormBuilder,
              private popupService: PopupService) {
    this.applicationFormGroup = formBuilder.group({
      id: null,
      name: null,
      description: null,
      status: null,
      receiveDate: null,
      leaderUser: null,
      rejectComment: null,
      resultDescription: null
    });
  }

  get expertiseApplicationStatus() {
    return ExpertiseApplicationStatus;
  }

  ngOnInit() {
    let idParam: any = this.activatedRoute.snapshot.paramMap.get('id');
    if (!isNaN(idParam)) {
      this.isNew = false;
      this.load(Number.parseFloat(idParam));
    } else {
      this.isNew = true;
    }
  }

  load(id: number): void {
    this.expertiseApplicationService.getExpertiseApplicationById(id)
      .subscribe(data => {
        this.expertiseApplication = data;
        this.applicationFormGroup.get('id').setValue(data.id);
        this.applicationFormGroup.get('name').setValue(data.name);
        this.applicationFormGroup.get('name').setValue(data.name);
        this.applicationFormGroup.get('description').setValue(data.description);
        this.applicationFormGroup.get('status').setValue(ExpertiseApplicationStatusDisplayValue.get(data.status));
        this.applicationFormGroup.get('receiveDate').setValue(data.receiveDate ? new Date(data.receiveDate) : null);
        this.applicationFormGroup.get('leaderUser').setValue(UserFioPipe.formatUser(data.leaderUser));
        this.applicationFormGroup.get('rejectComment').setValue(data.rejectComment);
        this.applicationFormGroup.get('resultDescription').setValue(data.resultDescription);
        this.isChatParticipant();
      });
  }

  save(): void {
    this.expertiseApplication.name = this.applicationFormGroup.value.name;
    this.expertiseApplication.description = this.applicationFormGroup.value.description;
    if (this.isNew) {
      this.expertiseApplicationService.createExpertiseApplication(this.expertiseApplication)
        .subscribe(this.subsriptionSave);
    } else {
      this.expertiseApplicationService.updateExpertiseApplication(this.expertiseApplication)
        .subscribe(this.subsriptionSave);
    }
  }

  uploadFiles(id: number) {
    const url = Urls.uploadExpertiseApplication(id);
    this.uploader.uploadAll(url);
  }


  deleteFile(file: File) {
    this.expertiseApplicationService.deleteFile(this.expertiseApplication.id, file.id)
      .subscribe(data => {
        this.popupService.openInfoPopup(InfoPopupBuilder.builder()
          .text('Файл успешно удален')
          .positiveHandler(() => this.expertiseApplication.expertiseFiles.splice(this.expertiseApplication.expertiseFiles.indexOf(file), 1))
          .build());
      });
  }

  successSave() {
    this.route.navigate(['../'], {relativeTo: this.activatedRoute});
  }

  isChatParticipant() {
    return this.chatService.isChatParticipant(this.expertiseApplication.id)
      .subscribe(data => {
        this.isUserChatParticipant = data;
      }, error => {
        this.isUserChatParticipant = false;
        console.error(error);
      });
  }

  private subsriptionSave = expertiseApplication => {
    if (this.uploader.hasItem()) {
      this.uploadFiles(expertiseApplication.id);
    } else {
      this.successSave();
    }
  };
}
