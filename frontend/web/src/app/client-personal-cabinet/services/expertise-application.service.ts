import {Injectable} from '@angular/core';
import {HttpService} from '../../core/services/http/http.service';
import {Observable} from 'rxjs';
import {ExpertiseApplication} from '../../shared/models/domain/expertise-application';
import {Urls} from '../../core/urls/url';

@Injectable({
  providedIn: 'root'
})
export class ExpertiseApplicationService {

  constructor(private httpService: HttpService) {
  }

  public getExpertiseApplications(status: string): Observable<ExpertiseApplication[]> {
    return this.httpService.getObservable(Urls.getExpertiseApplicationsByClient(status));
  }

  public getExpertiseApplicationById(id: number): Observable<ExpertiseApplication> {
    return this.httpService.getObservable(Urls.getExpertiseApplicationByClient(id));
  }

  public updateExpertiseApplication(expertiseApplication: ExpertiseApplication): Observable<ExpertiseApplication> {
    return this.httpService.putObservable(Urls.saveExpertiseApplicationByClient(), expertiseApplication);
  }

  public createExpertiseApplication(expertiseApplication: ExpertiseApplication): Observable<ExpertiseApplication> {
    return this.httpService.postObservable(Urls.saveExpertiseApplicationByClient(), expertiseApplication);
  }

  public deleteFile(expertiseApplicationId, fileId: number) {
    return this.httpService.deleteObservable(Urls.deleteExpertiseApplicationFile(expertiseApplicationId, fileId));
  }
}
