import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ClientPersonalCabinetComponent} from './components/client-personal-cabinet.component';
import {SharedModule} from '../shared/shared.module';
import {RouterModule, Routes} from '@angular/router';
import {ApplicationListComponent} from './components/application-list/application-list.component';
import {ApplicationItemComponent} from './components/application-item/application-item.component';
import {MatCardModule, MatDatepickerModule, MatFormFieldModule, MatInputModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    component: ClientPersonalCabinetComponent,
    children: [
      {path: '', redirectTo: 'applications', pathMatch: 'full'},
      {path: 'applications', component: ApplicationListComponent},
      {path: 'applications/:id', component: ApplicationItemComponent}
    ]
  }
];

@NgModule({
  declarations: [ClientPersonalCabinetComponent, ApplicationListComponent, ApplicationItemComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    RouterModule.forChild(routes),
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    MatDatepickerModule
  ],
  exports: []
})
export class ClientPersonalCabinetModule {
}
