import {BrowserModule} from '@angular/platform-browser';
import {NgModule, LOCALE_ID} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';


import {RegistrationService} from './services/registration/registration.service';
import {AuthorisationService} from './services/authorisation/authorisation.service';
import {ProfileService} from './services/profile-service/profile-service.service';


import {AppComponent} from './app.component';
import {ApplicationPublicHeader} from './components/header/header.component';
import {CreateUserRegistrationComponent} from './components/create-user-registration/create-user-registration.component';
import {LabInfoComponent} from './components/lab-info/lab-info.component';
import {RegistrationComponent} from './components/registration/registration.component';
import {AuthorisationComponent} from './components/authorisation/authorisation.component';
import {SystemRoutingComponent} from './components/system/system.routing.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {AppRoutingModule} from './app-routing/app-routing.module';
import {RouterModule} from '@angular/router';
import {CoreModule} from './core/core.module';
import {SharedModule} from './shared/shared.module';
import {_MatMenuDirectivesModule, MatFormFieldModule, MatIconModule, MatMenuModule} from '@angular/material';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {UserMenuComponent} from './components/user-menu/user-menu.component';
import {ForbiddenComponent} from './forbidden/forbidden.component';
import { registerLocaleData } from '@angular/common';
import localeRu from '@angular/common/locales/ru';

registerLocaleData(localeRu, 'ru');

import {ProfileComponent} from './components/profile/profile.component';
import {EditProfileComponent} from './components/edit-profile/edit-profile.component';
import {EditPasswordComponent} from './components/edit-password/edit-password.component';

@NgModule({
  exports: [],
  declarations: [
    AppComponent,
    ApplicationPublicHeader,
    LabInfoComponent,
    RegistrationComponent,
    AuthorisationComponent,
    PageNotFoundComponent,
    SystemRoutingComponent,
    CreateUserRegistrationComponent,
    UserMenuComponent,
    ForbiddenComponent,
    ProfileComponent,
    EditProfileComponent,
    EditPasswordComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    RouterModule,
    AppRoutingModule,
    CoreModule,
    SharedModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    _MatMenuDirectivesModule,
    MatMenuModule,
    MatIconModule
  ],
  providers: [
    RegistrationService,
    AuthorisationService,
    ProfileService,
    {provide: LOCALE_ID, useValue: 'ru'}
  ],
  entryComponents:[
    EditPasswordComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
