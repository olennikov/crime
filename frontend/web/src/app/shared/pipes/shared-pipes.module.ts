import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserFioPipe} from './user-fio.pipe';

const pipes = [
  UserFioPipe
];


@NgModule({
  imports: [CommonModule],
  declarations: [...pipes],
  exports: [...pipes]
})
export class SharedPipesModule {
}
