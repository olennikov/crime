import {Pipe, PipeTransform} from '@angular/core';
import {UserInfo} from '../models/domain/user-info';

@Pipe({
  name: 'userFio'
})
export class UserFioPipe implements PipeTransform {

  public static formatUser(userInfo: UserInfo): string {
    return userInfo ? `${userInfo.lastName || ''}  ${userInfo.firstName || ''} ${userInfo.secondName || ''}` : '';
  }

  transform(userInfo: UserInfo, ...args: any[]): any {
    return UserFioPipe.formatUser(userInfo);
  }
}
