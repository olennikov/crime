import { Injectable } from '@angular/core';
import {HttpService} from '../../../core/services/http/http.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpService) {}
}
