export class PaginationResponse<ItemType>{
    count: number;
    records: ItemType[];
 }
