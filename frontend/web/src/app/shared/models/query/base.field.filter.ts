import {QueryOperator} from './base.field.operator';

export class BaseFieldFilter {

  constructor(public fieldName: string,
              public operator: QueryOperator,
              public value?: string,
              public type?: string) {
  }

}
