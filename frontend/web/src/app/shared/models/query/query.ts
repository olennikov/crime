import {BaseFieldFilter} from './base.field.filter';
import {BaseFieldOrder} from './base.field.order';

export class Query {

  constructor(public limit: number,
              public offset: number,
              public filter: BaseFieldFilter[] = [],
              public orders: BaseFieldOrder[] = [],
              public findString: string = '',
              public dtoName: string = '') {

  }
}
