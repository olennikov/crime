import { OrderDirection } from './base.field.order-direction';

export class BaseFieldOrder{
  constructor(public field: string,
              public direction: OrderDirection){}
}
