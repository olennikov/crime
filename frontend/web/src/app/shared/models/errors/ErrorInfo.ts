export class ErrorInfo {
  constructor(public name: string,
              public description: string) {

  }
}
