export enum ExpertiseApplicationStatus {
  NEW = 'NEW',
  IN_WORK = 'IN_WORK',
  ACCEPT = 'ACCEPT',
  REJECT = 'REJECT',
  CLOSED = 'CLOSED'
}

export const ExpertiseApplicationStatusDisplayValue: Map<ExpertiseApplicationStatus, String> = new Map<ExpertiseApplicationStatus, String>([
  [ExpertiseApplicationStatus.ACCEPT, 'Одобрено'],
  [ExpertiseApplicationStatus.IN_WORK, 'В работе'],
  [ExpertiseApplicationStatus.NEW, 'Новое'],
  [ExpertiseApplicationStatus.REJECT, 'Отказано'],
  [ExpertiseApplicationStatus.CLOSED, 'Закрыта']
]);
