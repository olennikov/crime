import {File} from './file';

export class SubTask {
  constructor(public id: number = null,
              public name: string = '',
              public description: string = '',
              public comment: string = '',
              public deadlineDate: Date = null,
              public files: File[] = [],
              public taskId: number = null,
              public isClosed: boolean = false) {
  }
}
