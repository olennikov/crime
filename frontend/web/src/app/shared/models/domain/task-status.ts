export enum TaskStatus {
  NEW = 'NEW',
  IN_WORK = 'IN_WORK',
  CLOSED = 'CLOSED'
}

export const TaskStatusDisplayValue: Map<TaskStatus, string> = new Map<TaskStatus, string>([
  [TaskStatus.NEW, 'Новая'],
  [TaskStatus.IN_WORK, 'В работе'],
  [TaskStatus.CLOSED, 'Закрыта']
]);
