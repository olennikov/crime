export class UserInfo {
  constructor(public id: number,
              public lastName: string,
              public firstName: string,
              public secondName: string,
              public phone: string) {

  }
}
