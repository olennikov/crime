import {ExpertiseApplicationStatus} from './expertise-application-status';
import {UserInfo} from './user-info';
import {File} from './file';

export class ExpertiseApplication {
  constructor(public id: number,
              public name: string,
              public description: string,
              public receiveDate: Date,
              public status: ExpertiseApplicationStatus,
              public leaderUser: UserInfo,
              public rejectComment: string,
              public resultDescription: string,
              public expertiseFiles: File[] = []) {

  }
}
