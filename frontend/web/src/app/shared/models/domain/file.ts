import {UserInfo} from './user-info';

export class File {
  constructor(public id: number,
              public filename: string,
              public creator: UserInfo,
              public dateCreate: Date) {

  }
}
