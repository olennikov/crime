export enum ExpertiseStatus {
  NEW = 'NEW',
  IN_WORK = 'IN_WORK',
  DONE = 'DONE'
}

export const ExpertiseStatusDisplayValue: Map<ExpertiseStatus, string> = new Map<ExpertiseStatus, string>([
  [ExpertiseStatus.NEW, 'Новая'],
  [ExpertiseStatus.IN_WORK, 'В работе'],
  [ExpertiseStatus.DONE, 'Закрыта']
]);
