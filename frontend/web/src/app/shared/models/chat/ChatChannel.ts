import {SocketChannel} from '../../../core/services/websocket/models/socket-channel';

export class ChatChannel implements SocketChannel<any> {
  address: string;

  constructor(chatId: number, userId: number) {
    this.address = `/chat/${chatId}/user/${userId}`;
  }
}
