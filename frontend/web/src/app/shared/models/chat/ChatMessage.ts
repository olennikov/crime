import {Chat} from './Chat';
import {UserInfo} from '../domain/user-info';

export class ChatMessage {
  public isOwnMessage: boolean;

  constructor(public id: number,
              public chat: Chat,
              public timestamp: string,
              public text: string,
              public writer: UserInfo) {
  }
}
