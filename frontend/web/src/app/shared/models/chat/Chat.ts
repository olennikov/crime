import {ExpertiseApplication} from '../domain/expertise-application';
import {UserInfo} from '../domain/user-info';

export class Chat {
  constructor(public id: number,
              public expertise: ExpertiseApplication,
              public users: UserInfo[]) {
  }
}
