export interface MenuItem {
  displayValue: string;
  routerLink: string;
}
