export interface TreeNode {
  displayValue: string;
  routerLink?: string;
  children?: TreeNode[];
}
