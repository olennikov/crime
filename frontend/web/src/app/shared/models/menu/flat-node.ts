export interface FlatNode {
  expandable: boolean;
  name: string;
  level: number;
  routerLink?: string;
}
