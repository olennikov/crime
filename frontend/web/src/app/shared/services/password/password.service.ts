import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import {UUID} from 'angular2-uuid';

@Injectable({
  providedIn: 'root'
})
export class PasswordService {

  constructor() { }

  public generateRoundAuthorisationKey (password: string, salt: string, roundSalt: string): string {
    let saltPassword = this.generateAuthorisationKey(password, salt);
    return CryptoJS.SHA256(saltPassword+roundSalt).toString();
  }

  public generateAuthorisationKey(password: string, salt: string): string{
    return CryptoJS.SHA256(password+salt).toString();
  }

  public generateUUID (): string {
    return UUID.UUID();
  }

}
