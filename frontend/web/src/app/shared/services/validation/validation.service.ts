import {AbstractControl, ValidatorFn, FormGroup} from '@angular/forms';

export class ValidationService {

  constructor() {
  }

  static getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
    const config = {
      'required': 'Поле обязательно для заполнения',
      'email': 'Поле должно содержать email адрес',
      'digitLength': `Поле должно содержать ${validatorValue} цифр`,
      'minlength': `Минимальное количество символов ${validatorValue}`
    };
    return config[validatorName];
  }

  static equalFieldsValues (firstFieldName: string, secondFiedlName: string): ValidatorFn{
      return (group: FormGroup)=>{
         if (!(group.get(firstFieldName).value === group.get(secondFiedlName).value)){
            return  {notEquivalent: true};
         } else {
            return null;
         }
      }
  }

  static digitNumberValidator(count): ValidatorFn {
    return (control: AbstractControl) => {
      const regex = new RegExp(`^[0-9]{${count}}$`);
      if (!control.value || (control.value && control.value.match(regex))) {
        return null;
      } else {
        return {'digitLength': count};
      }
    };
  }
}
