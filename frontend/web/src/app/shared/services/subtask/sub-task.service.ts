import {Injectable} from '@angular/core';
import {HttpService} from '../../../core/services/http/http.service';
import {Observable} from 'rxjs';
import {SubTask} from '../../models/domain/subtask';
import {Urls} from '../../../core/urls/url';

@Injectable({
  providedIn: 'root'
})
export class SubTaskService {

  constructor(private httpService: HttpService) {
  }

  public loadSubtasksByTaskId(taskId: number): Observable<SubTask[]> {
    return this.httpService.getObservable(Urls.getSubtaskByTaskId(taskId));
  }

  public loadSubtasksById(id: number): Observable<SubTask> {
    return this.httpService.getObservable(Urls.getSubtaskById(id));
  }

  public closeSubTask(subTask: SubTask): Observable<SubTask> {
    return this.httpService.postObservable(Urls.closeSubTask(), subTask);
  }

  public createSubtasks(subtask: SubTask): Observable<SubTask> {
    return this.httpService.postObservable(Urls.saveOrDeleteSubtask());
  }

  public updateSubtasks(subtask: SubTask): Observable<SubTask> {
    return this.httpService.putObservable(Urls.saveOrDeleteSubtask());
  }

  public deleteSubTask(subtask: SubTask): Observable<void> {
    return this.httpService.deleteObservable(Urls.saveOrDeleteSubtask());
  }

  public deleteFile(subTaskId: number, fileId: number) {
    return this.httpService.deleteObservable(Urls.deleteSubFile(subTaskId, fileId));
  }
}
