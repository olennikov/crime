import {Injectable} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {InfoPopup} from '../../components/popup/info-popup/InfoPopup';
import {InfoPopupComponent} from '../../components/popup/info-popup/info-popup.component';

@Injectable({
  providedIn: 'root'
})
export class PopupService {

  constructor(public dialog: MatDialog) {
  }

  openPopup(component, data, callback?: Function) {
    let dialogRef = this.dialog.open(component, data);
    if (callback) {
      dialogRef.afterClosed().subscribe(data => callback.apply(data));
    }
  }

  openInfoPopup(data: InfoPopup) {
    const matDialogConfig: MatDialogConfig<InfoPopup> = new MatDialogConfig<InfoPopup>();
    matDialogConfig.data = data;
    matDialogConfig.restoreFocus = true;
    matDialogConfig.disableClose = true;
    this.dialog.open(InfoPopupComponent, matDialogConfig);
  }
}
