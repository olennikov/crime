import {DatePipe} from '@angular/common';

export class Util {
  private static readonly datePipe: DatePipe = new DatePipe('ru');

  public static formatDate(date: Date, format: string = 'dd.MM.yyyy hh:mm'): string {
    return this.datePipe.transform(date, format);
  }
}
