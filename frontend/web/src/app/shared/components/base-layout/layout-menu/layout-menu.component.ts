import {Component} from '@angular/core';

@Component({
  selector: 'app-layout-menu',
  templateUrl: './layout-menu.component.html',
  styleUrls: ['./layout-menu.component.scss']
})
export class LayoutMenuComponent {
  isNarrowAsideBar: boolean = false;

  /** переключатель ширины панели */
  public onWidthAsideBarToggled(): void {
    this.isNarrowAsideBar = !this.isNarrowAsideBar;
  }
}
