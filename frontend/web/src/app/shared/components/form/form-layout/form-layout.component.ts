import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-form-layout',
  templateUrl: './form-layout.component.html',
  styleUrls: ['./form-layout.component.scss']
})
export class FormLayoutComponent {

  @Output()
  saveHandler: EventEmitter<any> = new EventEmitter<any>();

  save() {
    this.saveHandler.emit();
  }
}
