import {Component, Input} from '@angular/core';
import {AbstractControl} from '@angular/forms';
import {ValidationService} from '../../../services/validation/validation.service';

@Component({
  selector: '[app-form-error-message]',
  template: `
      <mat-error role="alert" *ngIf="errorMessage">{{errorMessage}}</mat-error>`
})
export class FormErrorMessageComponent {
  @Input() control: AbstractControl;

  constructor() {
  }

  get errorMessage() {
    for (let propertyName in this.control.errors) {
      if (this.control.errors.hasOwnProperty(propertyName) && this.control.touched) {
        return ValidationService.getValidatorErrorMessage(propertyName, this.control.errors[propertyName]);
      }
    }

    return null;
  }

}
