import {Component, EventEmitter, Input, Output} from '@angular/core';
import {File} from '../../../models/domain/file';
import {environment} from '../../../../../environments/environment';
import {PopupService} from '../../../services/popup/popup.service';
import {InfoPopupBuilder} from '../../popup/info-popup/InfoPopup';

@Component({
  selector: 'app-file-list',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.scss']
})
export class FileListComponent {

  fileDownloadUrl = environment.fileDownloadUrl;

  @Input()
  canDelete: boolean = true;

  constructor(private popupService: PopupService) {

  }

  @Input()
  header: string;

  @Input()
  files: File[];

  @Output()
  deleteFile: EventEmitter<File> = new EventEmitter<File>();

  delete(file: File) {
    this.popupService.openInfoPopup(InfoPopupBuilder.builder()
      .positiveButtonText('Да')
      .positiveHandler(() => this.deleteFile.emit(file))
      .negativeButtonText('Нет')
      .showNegativeButton(true)
      .text(`Вы уверены, что хотите удалить файл "${file.filename}"?`)
      .build());
  }
}
