import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FileItem, FileUploader, FileUploaderOptions} from 'ng2-file-upload';
import {PopupService} from '../../../services/popup/popup.service';
import {ErrorPopupComponent} from '../../popup/error-popup/error-popup.component';
import {ErrorInfo} from '../../../models/errors/ErrorInfo';


@Component({
  selector: 'app-file-uploader',
  templateUrl: './file-uploader.component.html',
  styleUrls: ['./file-uploader.component.scss']
})
export class FileUploaderComponent implements OnInit {

  @Input()
  url: string;

  @Output()
  successUpload: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  successUploadAll: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  failUpload: EventEmitter<any> = new EventEmitter<any>();

  uploader: FileUploader;
  fileUploaderOptions: FileUploaderOptions;

  constructor(private popupService: PopupService) {
    this.fileUploaderOptions = {
      url: this.url,
      disableMultipart: false, // 'DisableMultipart' must be 'true' for formatDataFunction to be called.
      formatDataFunctionIsAsync: false,
      formatDataFunction: async (item) => {
        return new Promise((resolve, reject) => {
          resolve({
            name: item._file.name,
            length: item._file.size,
            contentType: item._file.type,
            date: new Date()
          });
        });
      }
    };
  }

  ngOnInit(): void {
    this.uploader = new FileUploader(this.fileUploaderOptions);
    this.uploader.onSuccessItem = (item, response) => this.successUpload.emit(response);
    this.uploader.onErrorItem = (item, response) => {
      this.resetFileState(item);
      this.popupService.openPopup(ErrorPopupComponent, {data: new ErrorInfo(`Произошла ошибка при загрузке файла "${item.file.name}"`,
          response && response != ""? JSON.parse(response).message: "")});
      this.failUpload.emit(response);
    };
    this.uploader.onCompleteAll = () => this.successUploadAll.emit(true);
  }

  public uploadAll(url?: string): void {
    if (url) {
      this.fileUploaderOptions.url = url;
      this.uploader.setOptions(this.fileUploaderOptions);
    }
    this.uploader.uploadAll();
  }

  public hasItem(): boolean {
    return this.uploader.queue.filter(x => !x.isUploaded || !x.isSuccess).length > 0;
  }

  private resetFileState(item: FileItem) {
    item.isSuccess = false;
    item.isUploaded = false;
    item.isUploading = false;
    item.isError = true;
  }
}
