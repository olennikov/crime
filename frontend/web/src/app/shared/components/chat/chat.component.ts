import {AfterViewChecked, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {ChatMessage} from '../../models/chat/ChatMessage';
import {ChatService} from '../../../core/services/chat/chat.service';
import {Chat} from '../../models/chat/Chat';
import {WebsocketService} from '../../../core/services/websocket/websocket.service';
import {ChatChannel} from '../../models/chat/ChatChannel';
import {UserInfo} from '../../models/domain/user-info';

@Component({
  selector: 'chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, AfterViewChecked {

  constructor(private chatService: ChatService,
              private webSocket: WebsocketService) {
  }

  @Input()
  expertiseApplicationId: number;

  @ViewChild('message_container', {static: false})
  private myScrollContainer: ElementRef;

  currentMessageText: string;
  messages: ChatMessage[] = [];
  chat: Chat;
  currentUser: UserInfo;
  currentSkip = this.chatService.SKIP;
  shouldBeScroll = true;

  ngOnInit() {
    this.initializeUser();
    this.initializeMessages();
    this.initializeChat();
  }

  private initializeMessages() {
    this.chatService.getMessagesByExpertise(this.expertiseApplicationId)
      .subscribe(data => {
        this.messages = this.processMessages(data);
      }, error => {
        console.error(error);
      });
  }

  private initializeChat() {
    this.chatService.getChatExpertise(this.expertiseApplicationId)
      .subscribe((data: Chat) => {
        this.chat = data;
        this.webSocket
          .selectChanel(new ChatChannel(this.chat.id, this.currentUser.id))
          .subscribe((data: ChatMessage) => {
            this.messages.push(...this.processMessages([data]));
            if (this.isScrollBottom()) {
              this.shouldBeScroll = true;
            }
          }, readMessageError => console.error('Error get message from ws', readMessageError));
      }, error => {
        console.error('Error intialize chat', error);
      });
  }

  private initializeUser() {
    this.chatService.getCurrentUser()
      .subscribe((data: UserInfo) => {
        this.currentUser = data;
      }, error => {
        console.log(error);
      });
  }

  sendMessage(message: string) {
    if (message) {
      this.chatService.sendMessage(message, this.chat, this.currentUser).subscribe((data: ChatMessage) => {
        this.currentMessageText = '';
      });
    }
  }

  private processMessages(messages: ChatMessage[]): ChatMessage[] {
    return messages.map(message => {
      message.isOwnMessage = message.writer.id === this.currentUser.id;
      return message;
    });
  }

  scrollUpMessages() {
    this.shouldBeScroll = false;
    this.currentSkip += this.chatService.TAKE;
    this.chatService.getMessagesByExpertise(this.expertiseApplicationId, this.currentSkip)
      .subscribe(data => {
        this.messages = this.processMessages(data).concat(this.messages);
      }, error => {
        console.log(error);
      });
  }

  ngAfterViewChecked(): void {
    if (this.shouldBeScroll) {
      this.scrollToBottom();
      if (this.isScrollBottom()) {
        this.shouldBeScroll = false;
      }
    }
  }

  /**
   * Находится ли скролл чата в самом низу
   */
  isScrollBottom(): boolean {
    return this.myScrollContainer.nativeElement.scrollTop > 0 &&
      this.myScrollContainer.nativeElement.scrollHeight -
      (this.myScrollContainer.nativeElement.scrollTop + this.myScrollContainer.nativeElement.offsetHeight) === 0;
  }

  scrollToBottom(): void {
    try {
      if (this.shouldBeScroll) {
        this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
      }
    } catch (err) {
      console.log(err);
    }
  }

  handleKeypress(event) {
    // Если нажат enter и не нажат shift
    if (!event.shiftKey && event.which === 13) {
      event.preventDefault();
      this.sendMessage(this.currentMessageText);
    }
  }
}
