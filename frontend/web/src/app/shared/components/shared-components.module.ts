import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BaseLayoutComponent} from './base-layout/base-layout.component';
import {LayoutHeaderComponent} from './base-layout/layout-header/layout-header.component';
import {LayoutMenuComponent} from './base-layout/layout-menu/layout-menu.component';
import {LogoutComponent} from './logout/logout.component';
import {VerticalMenuComponent} from './menu/vertical-menu/vertical-menu.component';
import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatSelectModule,
  MatSliderModule,
  MatTableModule,
  MatTabsModule,
  MatTreeModule
} from '@angular/material';
import {RouterModule} from '@angular/router';
import {HorizontalMenuComponent} from './menu/horizontal-menu/horizontal-menu.component';
import {TableComponent} from './table/standard-table/table.component';
import {ButtonPrimaryComponent} from './button/button-primary/button-primary.component';
import {ButtonWarnComponent} from './button/button-warn/button-warn.component';
import {ButtonBasicComponent} from './button/button-basic/button-basic.component';
import {FileUploaderComponent} from './file/file-uploader/file-uploader.component';
import {FileUploadModule} from 'ng2-file-upload';
import {ButtonBackComponent} from './button/button-back/button-back.component';
import {FormHeaderComponent} from './form/form-header/form-header.component';
import {FormLayoutComponent} from './form/form-layout/form-layout.component';
import {FileListComponent} from './file/file-list/file-list.component';
import {SharedPipesModule} from '../pipes/shared-pipes.module';
import {DatepickerComponent} from './datepicker/datepicker.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CdkTreeModule} from '@angular/cdk/tree';
import {TableWithPaginationComponent} from './table/table-with-pagination/table-with-pagination.component';
import {ChatComponent} from './chat/chat.component';
import {ChatMessageComponent} from './chat-message/chat-message.component';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {FormErrorMessageComponent} from './form/form-error-message/form-error-message.component';
import {ErrorPopupComponent} from './popup/error-popup/error-popup.component';
import {InfoPopupComponent} from './popup/info-popup/info-popup.component';
import {RusMatPaginatorIntlProvider} from './utils/RusMatPaginatorIntl';

const components = [
  BaseLayoutComponent,
  LayoutHeaderComponent,
  LayoutMenuComponent,
  VerticalMenuComponent,
  HorizontalMenuComponent,
  TableComponent,
  ButtonPrimaryComponent,
  ButtonWarnComponent,
  ButtonBasicComponent,
  LogoutComponent,
  FileUploaderComponent,
  ButtonBackComponent,
  FormHeaderComponent,
  FormLayoutComponent,
  ChatComponent,
  ChatMessageComponent,
  FileListComponent,
  DatepickerComponent,
  TableWithPaginationComponent,
  FormErrorMessageComponent,
  ErrorPopupComponent
];

@NgModule({
  declarations: [...components, InfoPopupComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatButtonToggleModule,
    RouterModule,
    MatTabsModule,
    MatTableModule,
    MatSliderModule,
    MatNativeDateModule,
    FileUploadModule,
    MatCardModule,
    MatInputModule,
    SharedPipesModule,
    MatFormFieldModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    FormsModule,
    CdkTreeModule,
    MatTreeModule,
    MatIconModule,
    MatPaginatorModule,
    MatExpansionModule,
    MatPaginatorModule,
    MatInputModule,
    MatIconModule,
    MatExpansionModule,
    InfiniteScrollModule,
    MatProgressBarModule,
    MatCheckboxModule,
    MatSelectModule
  ],
  exports: [...components,
    MatButtonModule,
    MatExpansionModule
  ],
  entryComponents: [ErrorPopupComponent, InfoPopupComponent],
  providers: [RusMatPaginatorIntlProvider]
})
export class SharedComponentsModule {
}
