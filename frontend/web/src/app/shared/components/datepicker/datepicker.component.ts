import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss']
})
export class DatepickerComponent implements OnInit, OnChanges {


  @Input()
  disabled: boolean;

  @Input()
  date: Date;

  @Input()
  fieldName: string = '';

  @Input()
  fieldLabel: string = '';

  @Input()
  formGroup: FormGroup;

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['date'] && changes['date'].currentValue) {
      this.date = new Date(changes['date'].currentValue.toString());
    }
  }
}
