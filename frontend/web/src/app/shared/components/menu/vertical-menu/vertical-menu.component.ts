import {Component, Input, OnInit} from '@angular/core';
import {TreeNode} from '../../../models/menu/tree-node';
import {FlatTreeControl} from '@angular/cdk/tree';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material';
import {FlatNode} from '../../../models/menu/flat-node';


@Component({
  selector: 'app-vertical-menu',
  templateUrl: './vertical-menu.component.html',
  styleUrls: ['./vertical-menu.component.scss']
})
export class VerticalMenuComponent implements OnInit {

  @Input()
  menuItems: TreeNode[] = [];
  dataSource: MatTreeFlatDataSource<TreeNode, FlatNode>;
  treeControl = new FlatTreeControl<FlatNode>(
    node => node.level, node => node.expandable);

  constructor() {
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  }

  ngOnInit(): void {
    this.dataSource.data = this.menuItems;
    this.treeControl.expandAll();
  }

  hasChild = (_: number, node: FlatNode) => node.expandable;

  private transformer = (node: TreeNode, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.displayValue,
      level: level,
      routerLink: node.routerLink
    };
  };

  treeFlattener = new MatTreeFlattener(
    this.transformer, node => node.level, node => node.expandable, node => node.children);
}


