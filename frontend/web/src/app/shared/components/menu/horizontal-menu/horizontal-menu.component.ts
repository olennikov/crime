import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MenuItem} from '../../../models/menu/menu-item';

@Component({
  selector: 'app-horizontal-menu',
  templateUrl: './horizontal-menu.component.html',
  styleUrls: ['./horizontal-menu.component.scss']
})
export class HorizontalMenuComponent implements OnInit {

  @Input()
  menuItems: MenuItem[] = [];

  @Output()
  selectedItemChange: EventEmitter<MenuItem> = new EventEmitter<MenuItem>();

  constructor() {
  }

  ngOnInit() {
    this.selectedTabIndexChange(0);
  }

  selectedTabIndexChange(tabNumber: number): void {
    if (this.selectedItemChange !== null) {
      this.selectedItemChange.emit(this.menuItems[tabNumber]);
    }
  }
}
