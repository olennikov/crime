import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {TableHeaderColumn} from '../TableHeaderColumn';
import {SelectionModel} from '@angular/cdk/collections';
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, OnChanges {

  @Input()
  displayedColumns: TableHeaderColumn[];

  @Input()
  dataSource: MatTableDataSource<any>;

  @Input()
  selectable: boolean = false;

  @Output()
  rowClick: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  selectedRowsChanged: EventEmitter<any[]> = new EventEmitter<any[]>();

  columns: string [] = [];
  selection = new SelectionModel<any>(true, []);

  ngOnInit() {
    let formattedDisplayColumns = this.displayedColumns.map(x => x.key);
    this.columns = this.selectable ? ['select', ...formattedDisplayColumns] : formattedDisplayColumns;
    this.selection.changed.subscribe(data => {
      this.selectedRowsChanged.emit(this.selection.selected);
    });
  }

  click(rowIndex: number) {
    this.rowClick.emit(this.dataSource.data[rowIndex]);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['dataSource'].currentValue != null) {
      this.selection.clear();
      let ds = changes['dataSource'].currentValue;
      const transformColumn = this.displayedColumns.filter(x => x.transform != null);
      ds.map(item => {
        transformColumn.map(transformColumn => {
          item[transformColumn.key] = transformColumn.transform(item[transformColumn.key]);
        });
      });
      this.dataSource = new MatTableDataSource((ds));
    }
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
}
