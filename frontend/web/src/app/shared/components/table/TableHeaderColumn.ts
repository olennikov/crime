export class TableHeaderColumn {
  constructor(public key: string,
              public display: string,
              public transform?: (x: any) => {}) {
  }
}
