export class PaginationInfo {
  constructor(public size: number,
              public number: number,
              public sort?: string) {

  }
}
