import {Component, EventEmitter, Input, Output} from '@angular/core';
import {TableHeaderColumn} from '../TableHeaderColumn';
import {PageEvent} from '@angular/material';
import {PaginationInfo} from './pagination-info';

@Component({
  selector: 'app-table-with-pagination',
  templateUrl: './table-with-pagination.component.html',
  styleUrls: ['./table-with-pagination.component.scss']
})
export class TableWithPaginationComponent {

  @Input()
  displayedColumns: TableHeaderColumn[];

  @Input()
  dataSource: any[] = [];

  @Input()
  selectable: boolean = false;

  @Input()
  pageSize: number = 10;

  @Input()
  pageSizeOptions: number[] = [5, 10, 25, 100];

  @Input()
  countRecord: number = 0;

  @Output()
  rowClick: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  loadInfo: EventEmitter<PaginationInfo> = new EventEmitter<PaginationInfo>();

  @Output()
  selectedRowsChanged: EventEmitter<any[]> = new EventEmitter<any[]>();

  click(rowItem: any) {
    this.rowClick.emit(rowItem);
  }

  loadMore(pageEvent: PageEvent) {
    const paginationInfo: PaginationInfo = new PaginationInfo(pageEvent.pageSize, pageEvent.pageIndex);
    this.loadInfo.emit(paginationInfo);
  }
}
