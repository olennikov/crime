import { Component, OnInit } from '@angular/core';
import {ButtonBaseComponent} from '../button-base/button-base.component';

@Component({
  selector: 'app-button-basic',
  templateUrl: './button-basic.component.html',
  styleUrls: ['./button-basic.component.scss']
})
export class ButtonBasicComponent extends ButtonBaseComponent implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
