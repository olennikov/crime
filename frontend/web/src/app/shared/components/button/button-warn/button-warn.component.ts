import { Component, OnInit } from '@angular/core';
import {ButtonBaseComponent} from '../button-base/button-base.component';

@Component({
  selector: 'app-button-warn',
  templateUrl: './button-warn.component.html',
  styleUrls: ['./button-warn.component.scss']
})
export class ButtonWarnComponent extends ButtonBaseComponent implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
