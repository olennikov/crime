import {Component, Input, OnInit} from '@angular/core';
import {ButtonBaseComponent} from '../button-base/button-base.component';

@Component({
  selector: 'app-button-primary',
  templateUrl: './button-primary.component.html',
  styleUrls: ['./button-primary.component.scss']
})
export class ButtonPrimaryComponent extends ButtonBaseComponent implements OnInit {

  @Input() label: string = '';

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
