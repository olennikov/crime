import { Component, OnInit, Input} from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-button-back',
  templateUrl: './button-back.component.html',
  styleUrls: ['./button-back.component.scss']
})
export class ButtonBackComponent {

  @Input() url: string = null;
  @Input() buttonName: string = 'Назад';

  constructor (private router:Router){
  }

  goBack() {
    console.log(this.url);
    if (this.url==null){
        window.history.back();
    } else {
        this.router.navigate([this.url]);
    }
  }

}
