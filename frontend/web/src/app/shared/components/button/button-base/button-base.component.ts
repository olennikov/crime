import {EventEmitter, Input, Output} from '@angular/core';

export class ButtonBaseComponent {
  /**Содержание лейбла для кнопки */
  @Input()
  label: string;

  /** состояние активности кнопки  */
  @Input()
  disabled: boolean = false;

  /** субмитет ли кнопка форму */
  @Input()
  isSubmitButton: boolean = false;

  /** всплывающая надпись при наведении курсором  */
  @Input()
  title = '';

  @Output()
  clickHandler: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
  }

  /** тип кнопки */
  get typeButton() {
    return this.isSubmitButton ? 'submit' : 'button';
  }

  click = () => this.clickHandler.emit();
}
