import {MatPaginatorIntl} from '@angular/material';

export class RusMatPaginatorIntl extends MatPaginatorIntl {
  constructor() {
    super();
    this.itemsPerPageLabel = 'Количество элементов на странице:';
    this.firstPageLabel = 'Первая страница';
    this.lastPageLabel = 'Последняя страница';
    this.nextPageLabel = 'Следующая страница';
    this.previousPageLabel = 'Предыдущая страница';
  }


  getRangeLabel: (page: number, pageSize: number, length: number) => string = (page: number, pageSize: number, length: number) => {
    if (length == 0 || pageSize == 0) {
      return `0 из ${length}`;
    }

    length = Math.max(length, 0);

    const startIndex = page * pageSize;

    // If the start index exceeds the list length, do not try and fix the end index to the end.
    const endIndex = startIndex < length ?
      Math.min(startIndex + pageSize, length) :
      startIndex + pageSize;

    return `${startIndex + 1} - ${endIndex} из ${length}`;
  };
}

export const RusMatPaginatorIntlProvider = {
  provide: MatPaginatorIntl, useClass: RusMatPaginatorIntl
};

