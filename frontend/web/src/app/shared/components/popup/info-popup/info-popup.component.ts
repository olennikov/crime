import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {InfoPopup} from './InfoPopup';

@Component({
  selector: 'app-info-popup',
  templateUrl: './info-popup.component.html',
  styleUrls: ['./info-popup.component.scss']
})
export class InfoPopupComponent {

  constructor(public dialogRef: MatDialogRef<InfoPopup>,
              @Inject(MAT_DIALOG_DATA) public data: InfoPopup) {
  }

  positiveClick() {
    this.dialogRef.close();
    if(this.data.positiveHandler){
      this.data.positiveHandler();
    }
  }

  negativeClick() {
    this.dialogRef.close();
    if(this.data.negativeHandler){
      this.data.negativeHandler();
    }
  }
}
