export class InfoPopup {
  public title: string = null;
  public text: string = null;
  public positiveButtonText: string = 'Ок';
  public negativeButtonText: string = 'Закрыть';
  public showNegativeButton: boolean = false;
  public showPositiveButton: boolean = true;
  public positiveHandler: () => void = null;
  public negativeHandler: () => void = null;
}

export class InfoPopupBuilder {
  private readonly infoPopup: InfoPopup;

  private constructor() {
    this.infoPopup = new InfoPopup();
  }

  public static builder() {
    return new InfoPopupBuilder();
  }

  public title(title: string): InfoPopupBuilder {
    this.infoPopup.title = title;
    return this;
  }

  public text(text: string): InfoPopupBuilder {
    this.infoPopup.text = text;
    return this;
  }

  public positiveButtonText(positiveButtonText: string): InfoPopupBuilder {
    this.infoPopup.positiveButtonText = positiveButtonText;
    return this;
  }

  public negativeButtonText(negativeButtonText: string): InfoPopupBuilder {
    this.infoPopup.negativeButtonText = negativeButtonText;
    return this;
  }

  public showNegativeButton(showNegativeButton: boolean): InfoPopupBuilder {
    this.infoPopup.showNegativeButton = showNegativeButton;
    return this;
  }

  public showPositiveButton(showPositiveButton: boolean): InfoPopupBuilder {
    this.infoPopup.showPositiveButton = showPositiveButton;
    return this;
  }

  public positiveHandler(positiveHandler: () => void): InfoPopupBuilder {
    this.infoPopup.positiveHandler = positiveHandler;
    return this;
  }

  public negativeHandler(negativeHandler: () => void): InfoPopupBuilder {
    this.infoPopup.negativeHandler = negativeHandler;
    return this;
  }

  public build(): InfoPopup {
    return this.infoPopup;
  }
}
