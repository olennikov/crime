import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ErrorInfo} from '../../../models/errors/ErrorInfo';

@Component({
  selector: 'app-error-popup',
  templateUrl: './error-popup.component.html',
  styleUrls: ['./error-popup.component.scss']
})
export class ErrorPopupComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ErrorPopupComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ErrorInfo) {
  }

  ngOnInit() {
  }

}
