import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedComponentsModule} from './components/shared-components.module';
import {SharedPipesModule} from './pipes/shared-pipes.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  exports: [SharedComponentsModule, SharedPipesModule]
})
export class SharedModule { }
