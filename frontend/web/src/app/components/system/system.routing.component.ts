import {urlsNavigate} from './url.redirect';
import {UserModel} from '../../services/authorisation/model/user.model';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {Component, OnInit} from '@angular/core';
import {AuthorisationService} from '../../services/authorisation/authorisation.service';


@Component({
  selector: 'system-module',
  templateUrl: './system.routing.component.html',
  styleUrls: ['./system.routing.component.scss']
})
export class SystemRoutingComponent implements OnInit {

  constructor(private router: Router,
              private authorisationService: AuthorisationService,) {

  }

  ngOnInit() {
    this.getUserInfo();
  }

  getUserInfo() {
    let observable: Observable<UserModel> = this.authorisationService.getUserInfo();
    observable.subscribe((data: UserModel) => {
        this.selectApplication(data);
      },
      message => {
        this.router.navigate([urlsNavigate.get('GUEST')]);
      });
  }

  selectApplication(data: UserModel) {
    let role: string = data.roles[0];
    this.router.navigate([urlsNavigate.get(role)]);
  }

}
