export const urlsNavigate = new Map([
  ['CUSTOMER', '/client'],
  ['DIRECTOR', '/director'],
  ['EMPLOYEE', '/employee'],
  ['GUEST', '/authorisation']
]);
