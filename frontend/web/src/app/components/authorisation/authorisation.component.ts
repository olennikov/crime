import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {Component} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {RoundLoginModel} from '../../services/authorisation/model/round.salt.model';
import {UserAuthorisation} from '../../services/authorisation/model/user.authorisation.model';
import {PasswordService} from '../../shared/services/password/password.service';
import {AuthorisationService} from '../../services/authorisation/authorisation.service';
import {PopupService} from '../../shared/services/popup/popup.service';

@Component({
  selector: 'authorisation',
  templateUrl: './authorisation.component.html',
  styleUrls: ['./authorisation.component.scss']
})
export class AuthorisationComponent {

  authorisationForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private authorisationService: AuthorisationService,
              private router: Router,
              private popupService: PopupService,
              private passwordService: PasswordService) {
    this.authorisationForm = this.formBuilder.group({
      login: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required)
    });
  }

  submit() {
    let observable: Observable<RoundLoginModel> = this.authorisationService.getRoundSalt(this.authorisationForm.value.login);
    observable.subscribe((data: RoundLoginModel) => {
      this.generatePasswordAndLogin(data, this.authorisationForm.value.login, this.authorisationForm.value.password);
    });
  }


  generatePasswordAndLogin(data: RoundLoginModel, login: string, password: string) {
    let roundPassword: string = this.passwordService.generateRoundAuthorisationKey(password,data.userSalt,data.roundSalt);
    let authorisation: UserAuthorisation = new UserAuthorisation(login, roundPassword, data.roundSalt);
    let observable: Observable<string> = this.authorisationService.loginUser(authorisation);
    observable.subscribe(data => {
      this.router.navigate(['/system']);
    });
  }
}

