import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ProfileService} from '../../services/profile-service/profile-service.service';
import {Router} from '@angular/router';
import {ValidationService} from '../../shared/services/validation/validation.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {EditPasswordComponent} from '../edit-password/edit-password.component';
import {PopupService} from '../../shared/services/popup/popup.service';
import {InfoPopupBuilder} from '../../shared/components/popup/info-popup/InfoPopup';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent {

  public editProfileFormGroup:FormGroup;

  constructor(private formBuilder: FormBuilder,
              private profileService: ProfileService,
              private router: Router,
              private popupService: PopupService) {
      this.editProfileFormGroup = formBuilder.group({
          lastName: new FormControl(null, [Validators.required]),
          firstName: new FormControl(null, [Validators.required]),
          secondName: new FormControl(null),
          email: new FormControl(null,[Validators.email]),
          phone: new FormControl(null,[Validators.required])
      })
      this.profileService.loadUserProfile().subscribe(data=>{
        this.editProfileFormGroup.patchValue(data);
      })
  }

  editProfile(){
    this.profileService.editUserProfile(this.editProfileFormGroup.value).subscribe(data=>{
        this.popupService.openInfoPopup(InfoPopupBuilder.builder()
            .text('Данные профиля пользователя успешно обновлены')
            .build());
    })
  }

  editPassword(){
    this.popupService.openPopup(EditPasswordComponent,{
      width: '50%'
    })
  }

}

