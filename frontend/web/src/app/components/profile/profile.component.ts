import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ProfileService} from '../../services/profile-service/profile-service.service';
import {Router} from '@angular/router';



@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent{

  profileFormGroup: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private profileService: ProfileService,
              private router: Router) {
    this.profileFormGroup = this.formBuilder.group({
          lastName: new FormControl(null),
          firstName: new FormControl(null),
          secondName: new FormControl(null),
          email: new FormControl(null),
          phone: new FormControl(null),
          organisationName: new FormControl(null),
          organisationPhone: new FormControl(null)
       });
      this.profileService.loadUserProfile().subscribe(data=>{
        this.profileFormGroup.patchValue(data);
      });

  }


  edit(){
    this.router.navigate(['/edit-profile'])
  }
}
