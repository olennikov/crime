import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ValidationService} from '../../shared/services/validation/validation.service';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ProfileService} from '../../services/profile-service/profile-service.service';
import {ChangePassword} from '../../services/profile-service/model/change-password';
import {PasswordService} from '../../shared/services/password/password.service';
import {PopupService} from '../../shared/services/popup/popup.service';
import {InfoPopupBuilder} from '../../shared/components/popup/info-popup/InfoPopup';

@Component({
  selector: 'app-edit-password',
  templateUrl: './edit-password.component.html',
  styleUrls: ['./edit-password.component.scss']
})
export class EditPasswordComponent{

  public editPasswordFormGroup: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private profileService: ProfileService,
              private passwordService: PasswordService,
              private popupService: PopupService,
              public dialogRef: MatDialogRef<EditPasswordComponent>) {
      this.editPasswordFormGroup = formBuilder.group({
          oldPassword: new FormControl(null,[Validators.required]),
          newPassword: new FormControl(null,[Validators.required, Validators.minLength(8)]),
          newRepeatPassword: new FormControl(null,[Validators.required, Validators.minLength(8)])
      },{validator: ValidationService.equalFieldsValues('newPassword','newRepeatPassword') })
  }


  close (){
    this.dialogRef.close();
  }

  editPassword(){
      this.profileService.roundSalt().subscribe(data=>{
          let changePassword: ChangePassword = new ChangePassword(null,null,null,null);
          changePassword.newSalt = this.passwordService.generateUUID();
          changePassword.newPassword = this.passwordService.generateAuthorisationKey(this.editPasswordFormGroup.get('newPassword').value, changePassword.newSalt)
          changePassword.roundSalt = data.roundSalt;
          changePassword.oldPassword = this.passwordService.generateRoundAuthorisationKey(this.editPasswordFormGroup.get('oldPassword').value, data.userSalt, data.roundSalt);
          this.profileService.editUserPassword(changePassword).subscribe(data=>{
            this.close();
            this.popupService.openInfoPopup(InfoPopupBuilder.builder()
            .text('Пароль успешно обновлён')
            .build());
          })
      })
  }

}
