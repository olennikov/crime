import {Observable} from 'rxjs';
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, NgForm, Validators} from '@angular/forms';
import {RegistrationService} from '../../services/registration/registration.service';
import {ValidationService} from '../../shared/services/validation/validation.service';
import {InfoPopupBuilder} from '../../shared/components/popup/info-popup/InfoPopup';
import {PopupService} from '../../shared/services/popup/popup.service';


@Component({
  selector: 'registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  registrationForm;

  constructor(private formBuilder: FormBuilder,
              private registrationService: RegistrationService,
              private popupService: PopupService) {
    this.registrationForm = this.formBuilder.group({
      lastName: new FormControl(null, [Validators.required]),
      firstName: new FormControl(null, [Validators.required]),
      secondName: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.required, Validators.email]),
      phone: new FormControl(null, [Validators.required]),
      organisationName: new FormControl(null, [Validators.required]),
      organisationPhone: new FormControl(null, [Validators.required]),
      organisationInn: new FormControl(null, [Validators.required, ValidationService.digitNumberValidator(13)]),
      organisationOgrn: new FormControl(null, [Validators.required, ValidationService.digitNumberValidator(15)])
    });
  }

  ngOnInit() {
  }

  submit(form: NgForm) {
    let observable: Observable<number> = this.registrationService.createRegistrationApplication(form.value);
    observable.subscribe(data => {
      this.createRegistrationCallback(data)
    });
  }

  createRegistrationCallback(data: number) {
    this.popupService.openInfoPopup(InfoPopupBuilder.builder()
      .text('Заявка на регистрацию зарегистрирована под номером ' + data + '.\r\nНа указанный Вами электронный адрес приёдт письмо о результате рассмотрения')
      .positiveHandler(() => this.registrationForm.reset())
      .build());
  }
}
