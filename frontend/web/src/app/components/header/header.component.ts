import {Component} from '@angular/core';

@Component({
  selector: 'app-public-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class ApplicationPublicHeader {
  headerButtons: ApplicationHeader[] = [
    new ApplicationHeader('Описание и услуги', 'main'),
    new ApplicationHeader('Заявка на регистрацию', 'registration'),
    new ApplicationHeader('Личный кабинет', 'system')
  ];
}


class ApplicationHeader {

  constructor(
    public title: string,
    public path: string) {
  }

}
