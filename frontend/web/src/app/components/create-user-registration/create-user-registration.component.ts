import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {RegistrationService} from '../../services/registration/registration.service';
import {RegistrationModel} from '../../services/registration/model/registration.model';
import {RegistrationUserModel} from '../../services/registration/model/registration.user.model';
import {Observable, of} from 'rxjs';
import {InfoPopupBuilder} from '../../shared/components/popup/info-popup/InfoPopup';
import {PopupService} from '../../shared/services/popup/popup.service';
import {PasswordService} from '../../shared/services/password/password.service';

@Component({
  selector: 'app-create-user-registration',
  templateUrl: './create-user-registration.component.html',
  styleUrls: ['./create-user-registration.component.scss']
})
export class CreateUserRegistrationComponent implements OnInit {

  title: string = 'Добавить';
  createUserForm: FormGroup;
  condition: string = 'load';
  registrationModel: RegistrationModel;

  constructor(private formBuilder: FormBuilder,
              private activateRoute: ActivatedRoute,
              private passwordService: PasswordService,
              private registrationService: RegistrationService,
              private popupService: PopupService) {
    this.createUserForm = this.formBuilder.group({
      login: new FormControl(null, Validators.compose([Validators.required])),
      password: new FormControl(null, Validators.compose([Validators.required])),
      repeatPassword: new FormControl(null, Validators.compose([Validators.required]))
    });
  }

  ngOnInit() {
    let disposableUrl: string = this.activateRoute.snapshot.queryParams['disposableUrl'];
    let observable: Observable<RegistrationModel> = this.registrationService.getAcceptedApplicationByDisposableUrl(disposableUrl);
    observable.subscribe((data: RegistrationModel) => {
        this.condition = 'registrate';
        this.registrationModel = data;
      },
      err => {
        this.condition = 'invalid';
        console.error(err);
        return of(null);
      });
  }

  submit() {
    if (this.createUserForm.value.password === this.createUserForm.value.repeatPassword) {
      let registrationUserModel: RegistrationUserModel = new RegistrationUserModel();
      registrationUserModel.userSalt = this.passwordService.generateUUID();
      registrationUserModel.userLogin = this.createUserForm.value.login;
      registrationUserModel.changePasswordUrl = this.activateRoute.snapshot.queryParams['disposableUrl'];
      registrationUserModel.userPassword = this.passwordService.generateAuthorisationKey(this.createUserForm.value.password, registrationUserModel.userSalt);
      let observable: Observable<number> = this.registrationService.createUser(registrationUserModel);
      observable.subscribe(data => {
          this.condition = 'success';
        },
        err => {
          this.condition = 'error';
          console.error(err);
          return of(null);
        });
    } else {
      this.popupService.openInfoPopup(InfoPopupBuilder.builder()
        .text('Пароли не совпадают')
        .positiveHandler(() => this.resetPasswordFields())
        .build());
    }
  }

  private resetPasswordFields() {
    this.createUserForm.value.password = '';
    this.createUserForm.value.repeatPassword = '';
  }
}
