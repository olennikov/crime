import {Component, OnInit} from '@angular/core';
import {SecurityContextHolderService} from '../../services/security-context-holder/security-context-holder.service';
import {UserModel} from '../../services/authorisation/model/user.model';
import {AuthorisationService} from '../../services/authorisation/authorisation.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss']
})
export class UserMenuComponent {

  user: UserModel = null;

  constructor(private securityContextHolderService: SecurityContextHolderService,
              private authorisationService: AuthorisationService,
              private router: Router) {
    securityContextHolderService.userContext.subscribe(userContext => {
      if (userContext && userContext.isLogged) {
        this.user = userContext.currentUser;
      } else {
        this.user = null;
      }
    });
  }

  logout(): void {
    this.authorisationService.logout();
  }

  profile(): void {
    this.router.navigate(['/profile'])
  }

}
