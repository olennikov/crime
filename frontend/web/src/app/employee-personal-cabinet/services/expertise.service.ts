import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Expertise} from '../models/expertise';
import {HttpService} from '../../core/services/http/http.service';
import {Urls} from '../../core/urls/url';
import {PaginationResponse} from '../../shared/models/query/pagination.response';
import {PaginationInfo} from '../../shared/components/table/table-with-pagination/pagination-info';
import {ExpertiseFiles} from '../models/expertise-files';

@Injectable({
  providedIn: 'root'
})
export class ExpertiseService {

  constructor(private httpService: HttpService) {
  }

  public getExpertises(paginationInfo: PaginationInfo, type: string): Observable<PaginationResponse<Expertise>> {
    paginationInfo.sort = 'id,asc';
    return this.httpService.getObservable(Urls.expertiseByStatus(paginationInfo, type));
  }

  public getExpertise(id: number): Observable<Expertise> {
    return this.httpService.getObservable(Urls.expertiseById(id));
  }

  public loadFileFromExpertise(id: number): Observable<ExpertiseFiles> {
    return this.httpService.getObservable(Urls.expertiseFiles(id));
  }
}
