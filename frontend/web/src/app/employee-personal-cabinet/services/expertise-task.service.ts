import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpService} from '../../core/services/http/http.service';
import {Urls} from '../../core/urls/url';
import {ExpertiseTask} from '../models/expertise-task';
import {SubTask} from '../../shared/models/domain/subtask';

@Injectable({
  providedIn: 'root'
})
export class ExpertiseTaskService {

  constructor(private httpService: HttpService) {
  }

  public taskById(id: number): Observable<ExpertiseTask> {
    return this.httpService.getObservable(Urls.getEmployeeTaskById(id));
  }

  public subTaskByTaskId(id: number): Observable<SubTask[]> {
    return this.httpService.getObservable(Urls.getSubtaskByTaskId(id));
  }

  public deleteFile(taskId: number, fileId: number) {
    return this.httpService.deleteObservable(Urls.deleteTaskFile(taskId, fileId));
  }

}
