import {Component, Input, OnInit} from '@angular/core';
import {ExpertiseService} from '../../services/expertise.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Expertise} from '../../models/expertise';
import {ExpertiseTask} from '../../models/expertise-task';
import {SubTask} from '../../../shared/models/domain/subtask';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UserFioPipe} from '../../../shared/pipes/user-fio.pipe';

@Component({
  selector: 'app-expertise-info',
  templateUrl: './expertise-info.component.html',
  styleUrls: ['./expertise-info.component.scss']
})
export class ExpertiseInfoComponent implements OnInit {

  @Input()
  expertiseId: number;

  expertise: Expertise = new Expertise();

  selectedTask: ExpertiseTask = new ExpertiseTask();

  selectedSubTask: SubTask = new SubTask();

  expertiseFormGroup: FormGroup;

  constructor(private expertiseService: ExpertiseService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private formBuilder: FormBuilder) {
    this.expertiseFormGroup = formBuilder.group({
      name: null,
      receiveDate: null,
      creator: null,
      leader: null,
      description: null
    });
  }

  ngOnInit() {
    try {
      const idParam: any = this.activatedRoute.parent.snapshot.paramMap.get('id');
      if (!isNaN(idParam)) {
        this.expertiseId = Number.parseFloat(idParam);
        this.loadExpertise();
      } else {
        throw new Error(`Receive not number id: ${idParam}`);
      }
    } catch (e) {
      console.error('Error parsing paramId. Redirecting to parent', e);
      this.router.navigate(['../'], {relativeTo: this.activatedRoute});
    }
  }

  loadExpertise() {
    this.expertiseService.getExpertise(this.expertiseId)
      .subscribe(data => {
        this.expertise = data;
        this.expertiseFormGroup.get('name').setValue(data.name);
        this.expertiseFormGroup.get('receiveDate').setValue(data.receiveDate ? new Date(data.receiveDate) : null);
        this.expertiseFormGroup.get('creator').setValue(UserFioPipe.formatUser(data.creator));
        this.expertiseFormGroup.get('leader').setValue(UserFioPipe.formatUser(data.leader));
        this.expertiseFormGroup.get('description').setValue(data.description);
      });
  }

  selectTask(selectedTask: ExpertiseTask): void {
    this.selectedTask = selectedTask;
    this.router.navigate(['./task', selectedTask.id], {relativeTo: this.activatedRoute});
  }
}
