import {Component, EventEmitter, Input, Output} from '@angular/core';
import {TableHeaderColumn} from '../../../shared/components/table/TableHeaderColumn';
import {UserFioPipe} from '../../../shared/pipes/user-fio.pipe';
import {UserInfo} from '../../../shared/models/domain/user-info';
import {PaginationInfo} from '../../../shared/components/table/table-with-pagination/pagination-info';
import {ExpertiseTask} from '../../models/expertise-task';

@Component({
  selector: 'app-expertise-task-list',
  templateUrl: './expertise-task-list.component.html',
  styleUrls: ['./expertise-task-list.component.scss']
})
export class ExpertiseTaskListComponent {

  pageSize: number = 10;
  countRecord: number;
  pageSizeOption: number[] = [5, 10, 25, 100];

  columns: TableHeaderColumn[] = [
    new TableHeaderColumn('id', 'ID'),
    new TableHeaderColumn('name', 'Имя'),
    new TableHeaderColumn('responsible', 'Ответственный', (u: UserInfo) => UserFioPipe.formatUser(u)),
    new TableHeaderColumn('progress', 'Выполнено задач')
  ];

  @Input()
  dataSource: ExpertiseTask[] = [];

  @Output()
  taskSelectEvent: EventEmitter<ExpertiseTask> = new EventEmitter<ExpertiseTask>();

  selectTask(selectedTask: ExpertiseTask) {
    this.taskSelectEvent.emit(selectedTask);
  }

  loadData(paginationInfo: PaginationInfo) {

  }
}
