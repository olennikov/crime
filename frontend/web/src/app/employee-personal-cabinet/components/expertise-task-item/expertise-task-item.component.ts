import {Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {ExpertiseTask} from '../../models/expertise-task';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UserFioPipe} from '../../../shared/pipes/user-fio.pipe';
import {ActivatedRoute, Router} from '@angular/router';
import {ExpertiseTaskService} from '../../services/expertise-task.service';
import {FileUploaderComponent} from '../../../shared/components/file/file-uploader/file-uploader.component';
import {FileListComponent} from '../../../shared/components/file/file-list/file-list.component';
import {PopupService} from '../../../shared/services/popup/popup.service';
import {InfoPopupBuilder} from '../../../shared/components/popup/info-popup/InfoPopup';
import {Urls} from '../../../core/urls/url';
import {File} from '../../../shared/models/domain/file';

@Component({
  selector: 'app-expertise-task-item',
  templateUrl: './expertise-task-item.component.html',
  styleUrls: ['./expertise-task-item.component.scss']
})
export class ExpertiseTaskItemComponent implements OnInit, OnChanges {

  @ViewChild(FileUploaderComponent, {static: false})
  uploader: FileUploaderComponent;
  @Input()
  task: ExpertiseTask = new ExpertiseTask();
  taskId: number;
  expertiseForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private expertiseTaskService: ExpertiseTaskService,
              private popupService: PopupService) {
    this.expertiseForm = formBuilder.group({
      id:null,
      name: '',
      deadlineDate: null,
      expiredDate: null,
      responsible: ''
    });
  }

  ngOnInit() {
    try {
      const idParam: any = this.activatedRoute.snapshot.paramMap.get('taskId');
      if (!isNaN(idParam)) {
        this.taskId = Number.parseFloat(idParam);
        this.loadTaskInfo();
      } else {
        throw new Error(`Receive not number taskId: ${idParam}`);
      }
    } catch (e) {
      console.error('Error parsing taskId. Redirecting to parent', e);
      this.router.navigate(['../'], {relativeTo: this.activatedRoute});
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  loadTaskInfo() {
    if (this.taskId) {
      this.expertiseTaskService.taskById(this.taskId).subscribe(data => {
        this.task = data;
        this.expertiseForm.get('id').setValue(this.task.id);
        this.expertiseForm.get('name').setValue(this.task.name);
        this.expertiseForm.get('deadlineDate').setValue(this.task.deadlineDate ? new Date(this.task.deadlineDate) : null);
        this.expertiseForm.get('expiredDate').setValue(this.task.expiredDate ? new Date(this.task.expiredDate) : null);
        this.expertiseForm.get('responsible').setValue(UserFioPipe.formatUser(this.task.responsible));
      });
    }
  }

    saveResult() {
        if (this.uploader.hasItem()) {
          this.uploader.uploadAll(Urls.uploadTaskFileUrl(this.expertiseForm.get('id').value));
        }
    }

    successSave() {
      this.popupService.openInfoPopup(InfoPopupBuilder.builder()
        .text('Данные задачи успешно обновлены')
        .build());
    }

      deleteFile(file: File) {
        this.expertiseTaskService.deleteFile(this.task.id, file.id)
          .subscribe(data => {
            this.popupService.openInfoPopup(InfoPopupBuilder.builder()
              .text('Файл успешно удален')
              .positiveHandler(() => this.task.taskFiles.splice(this.task.taskFiles.indexOf(file), 1))
              .build());
          });
      }
}
