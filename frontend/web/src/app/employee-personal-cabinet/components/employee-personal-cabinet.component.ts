import {Component, OnInit} from '@angular/core';
import {TreeNode} from '../../shared/models/menu/tree-node';

@Component({
  selector: 'app-employee-personal-cabinet',
  templateUrl: './employee-personal-cabinet.component.html',
  styleUrls: ['./employee-personal-cabinet.component.scss']
})
export class EmployeePersonalCabinetComponent implements OnInit {

  routes: TreeNode[] = [
    {
      displayValue: 'Test1', children: [
        {displayValue: 'Test1-1', routerLink: 'expertise/opened', children: []},
        {displayValue: 'Test1-2', routerLink: 'expertise/closed', children: []},
        {displayValue: 'Test1-3', routerLink: 'expertise/all', children: []}
      ]
    },
    {
      displayValue: 'Test2', routerLink: 'test', children: [
        {displayValue: 'Test1', routerLink: 'expertise1', children: []},
      ]
    },
    {
      displayValue: 'Test3', routerLink: 'test', children: [
        {displayValue: 'Test1', routerLink: 'expertise1', children: []},
      ]
    }
  ];

  constructor() {
  }

  ngOnInit() {
  }

}
