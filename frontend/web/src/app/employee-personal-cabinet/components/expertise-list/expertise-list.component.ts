import {Component} from '@angular/core';
import {TableHeaderColumn} from '../../../shared/components/table/TableHeaderColumn';
import {MenuItem} from '../../../shared/models/menu/menu-item';
import {ActivatedRoute, Router} from '@angular/router';
import {ExpertiseService} from '../../services/expertise.service';
import {ExpertiseStatus, ExpertiseStatusDisplayValue} from '../../../shared/models/domain/expertise-status';
import {Expertise} from '../../models/expertise';
import {PaginationInfo} from '../../../shared/components/table/table-with-pagination/pagination-info';
import {PaginationResponse} from '../../../shared/models/query/pagination.response';
import {UserFioPipe} from '../../../shared/pipes/user-fio.pipe';
import {UserInfo} from '../../../shared/models/domain/user-info';
import {Util} from '../../../shared/services/Util';

@Component({
  selector: 'app-expertise-list',
  templateUrl: './expertise-list.component.html',
  styleUrls: ['./expertise-list.component.scss']
})
export class ExpertiseListComponent {

  selectedTabIndex: MenuItem;
  pageSize: number = 10;
  countRecord: number = 0;

  dataSource: Expertise[] = [];

  tableColumns: TableHeaderColumn[] = [
    new TableHeaderColumn('id', 'ID'),
    new TableHeaderColumn('name', 'Наименование'),
    new TableHeaderColumn('receiveDate', 'Дата создания', (receiveDate: Date) => Util.formatDate(receiveDate)),
    new TableHeaderColumn('status', 'Статус', (status: ExpertiseStatus) => ExpertiseStatusDisplayValue.get(status)),
    new TableHeaderColumn('creator', 'Инициатор', (x: UserInfo) => UserFioPipe.formatUser(x)),
    new TableHeaderColumn('leader', 'Ответственный', (x: UserInfo) => UserFioPipe.formatUser(x)),
    new TableHeaderColumn('applicationId', 'Идентификатор заявления')
  ];

  expertiseStatus: MenuItem[] = [
    {displayValue: 'Активные', routerLink: ExpertiseStatus.IN_WORK},
    {displayValue: 'Законченные', routerLink: ExpertiseStatus.DONE},
    {displayValue: 'Все', routerLink: 'ALL'}
  ];

  constructor(private expertiseService: ExpertiseService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
  }

  rowClick(expertise: Expertise) {
    this.router.navigate([`./${expertise.id}`], {relativeTo: this.activatedRoute});
  }

  selectedItemChange(menuItem: MenuItem) {
    this.selectedTabIndex = menuItem;
    this.expertiseService.getExpertises(new PaginationInfo(this.pageSize, 0), this.selectedTabIndex.routerLink)
      .subscribe(this.proccessLoadResponse());
  }

  loadMore(paginationInfo: PaginationInfo) {
    this.pageSize = paginationInfo.size;
    this.expertiseService.getExpertises(paginationInfo, this.selectedTabIndex.routerLink)
      .subscribe(this.proccessLoadResponse());
  }

  private proccessLoadResponse() {
    return (response: PaginationResponse<Expertise>) => {
      this.countRecord = response.count;
      this.dataSource = response.records;
    };
  }
}
