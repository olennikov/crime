import {Component, OnInit} from '@angular/core';
import {MenuItem} from '../../../shared/models/menu/menu-item';

@Component({
  selector: 'app-expertise-item',
  templateUrl: './expertise-item.component.html',
  styleUrls: ['./expertise-item.component.scss']
})
export class ExpertiseItemComponent implements OnInit {

  menuItems: MenuItem[] = [
    {displayValue: 'Данные об экспертизе', routerLink: 'info'},
    {displayValue: 'Файлы', routerLink: 'files'},
  ];

  constructor() {
  }

  ngOnInit() {
  }

}
