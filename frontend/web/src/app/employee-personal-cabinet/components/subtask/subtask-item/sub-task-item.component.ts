import {Component, Input, OnChanges, SimpleChanges, ViewChild} from '@angular/core';
import {SubTask} from '../../../../shared/models/domain/subtask';
import {SubTaskService} from '../../../../shared/services/subtask/sub-task.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {FileUploaderComponent} from '../../../../shared/components/file/file-uploader/file-uploader.component';
import {InfoPopupBuilder} from '../../../../shared/components/popup/info-popup/InfoPopup';
import {PopupService} from '../../../../shared/services/popup/popup.service';
import {File} from '../../../../shared/models/domain/file';

@Component({
  selector: 'app-subtask-item',
  templateUrl: './sub-task-item.component.html',
  styleUrls: ['./sub-task-item.component.scss']
})
export class SubTaskItemComponent implements OnChanges {

  @Input()
  subTask: SubTask = new SubTask();

  @Input()
  id: number;


  @ViewChild(FileUploaderComponent, {static: false})
  uploader: FileUploaderComponent;

  subTaskFormGroup: FormGroup;

  constructor(private subTaskService: SubTaskService,
              private formBuilder: FormBuilder,
              private popupService: PopupService) {
    this.subTaskFormGroup = formBuilder.group({
      name: null,
      description: null,
      comment: null,
      deadlineDate: null
    });
  }

  successClose() {
    this.popupService.openInfoPopup(InfoPopupBuilder.builder()
      .text('Подзадача успешно закрыта')
      .positiveHandler(() => this.subTask.isClosed = true)
      .build());
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['subTask'] && changes['subTask'].currentValue) {
      this.subTask = changes['subTask'].currentValue;
      if (this.subTask && this.subTask.id) {
        this.subTaskService.loadSubtasksById(this.subTask.id)
          .subscribe(data => {
            this.subTaskFormGroup.get('name').setValue(data.name);
            this.subTaskFormGroup.get('description').setValue(data.description);
            this.subTaskFormGroup.get('comment').setValue(data.comment);
            this.subTaskFormGroup.get('deadlineDate').setValue(data.deadlineDate ? new Date(data.deadlineDate) : null);
          });
      }
    }
  }

  closeSubtask() {
    this.subTask.comment = this.subTaskFormGroup.get('comment').value;
    this.subTaskService.closeSubTask(this.subTask).subscribe(data => {
      if (this.uploader.hasItem()) {
        this.uploader.uploadAll(`api/subtasks/${data.id}/upload`);
      } else {
        this.successClose();
      }
    });
  }

  deleteFile(file: File) {
    this.subTaskService.deleteFile(this.subTask.id, file.id)
      .subscribe(data => {
        this.popupService.openInfoPopup(InfoPopupBuilder.builder()
          .text('Файл успешно удален')
          .positiveHandler(() => this.subTask.files.splice(this.subTask.files.indexOf(file), 1))
          .build());
      });
  }
}
