import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {SubTask} from '../../../../shared/models/domain/subtask';
import {ExpertiseTask} from '../../../models/expertise-task';
import {ExpertiseTaskService} from '../../../services/expertise-task.service';

@Component({
  selector: 'app-subtask-list',
  templateUrl: './sub-task-list.component.html',
  styleUrls: ['./sub-task-list.component.scss']
})
export class SubTaskListComponent implements OnChanges{

  @Input()
  subTasks: SubTask[];

  @Input()
  taskId: number;

  constructor(private expertiseTaskService: ExpertiseTaskService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes['taskId'] && changes['taskId'].currentValue) {
      this.taskId = changes['taskId'].currentValue;
      this.expertiseTaskService.subTaskByTaskId(this.taskId).subscribe(data => {
        this.subTasks = data;
      });
    }
  }
}
