import {Component, Input} from '@angular/core';
import {SubTask} from '../../../shared/models/domain/subtask';

@Component({
  selector: 'app-expertise-subtask-item',
  templateUrl: './expertise-subtask-item.component.html',
  styleUrls: ['./expertise-subtask-item.component.scss']
})
export class ExpertiseSubtaskItemComponent {

  @Input()
  subTask: SubTask = new SubTask();
}
