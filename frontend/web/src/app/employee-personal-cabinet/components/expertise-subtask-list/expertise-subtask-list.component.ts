import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {TableHeaderColumn} from '../../../shared/components/table/TableHeaderColumn';
import {ExpertiseTask} from '../../models/expertise-task';
import {PaginationInfo} from '../../../shared/components/table/table-with-pagination/pagination-info';
import {SubTask} from '../../../shared/models/domain/subtask';
import {SubTaskService} from '../../../shared/services/subtask/sub-task.service';

@Component({
  selector: 'app-expertise-subtask-list',
  templateUrl: './expertise-subtask-list.component.html',
  styleUrls: ['./expertise-subtask-list.component.scss']
})
export class ExpertiseSubtaskListComponent implements OnInit, OnChanges {

  pageSize: number = 10;
  countRecord: number;
  pageSizeOption: number[] = [5, 10, 25, 100];

  columns: TableHeaderColumn[] = [
    new TableHeaderColumn('id', 'ID'),
    new TableHeaderColumn('name', 'Имя')
  ];

  dataSource: SubTask[] = [];

  @Input()
  task: ExpertiseTask;

  @Output()
  selectedSubTask: EventEmitter<SubTask> = new EventEmitter<SubTask>();

  constructor(private service: SubTaskService) {
  }

  ngOnInit() {
  }

  selectTask(selectedTask: ExpertiseTask) {
    if (selectedTask && selectedTask.id) {
      this.service.loadSubtasksByTaskId(selectedTask.id)
        .subscribe(data => this.dataSource = data);
    }
  }

  selectSubTask(selectedSubTask: SubTask) {
    this.selectedSubTask.emit(selectedSubTask);
  }

  loadData(paginationInfo: PaginationInfo) {

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['task'] && changes['task'].currentValue) {
      this.task = changes['task'].currentValue;
      this.selectTask(this.task);
    }
  }


}
