import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ExpertiseService} from '../../services/expertise.service';
import {File} from '../../../shared/models/domain/file';

@Component({
  selector: 'app-expertise-file-list',
  templateUrl: './expertise-file-list.component.html',
  styleUrls: ['./expertise-file-list.component.scss']
})
export class ExpertiseFileListComponent implements OnInit {

  @Input()
  expertiseId: number;

  expertiseFiles: File[] = [];
  applicationFiles: File[] = [];

  constructor(private expertiseService: ExpertiseService,
              private activatedRoute: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    try {
      const idParam: any = this.activatedRoute.parent.snapshot.paramMap.get('id');
      if (!isNaN(idParam)) {
        this.expertiseId = Number.parseFloat(idParam);
        this.loadFiles();
      } else {
        throw new Error(`Receive not number id: ${idParam}`);
      }
    } catch (e) {
      console.error('Error parsing paramId. Redirecting to parent', e);
      this.router.navigate(['../'], {relativeTo: this.activatedRoute});
    }
  }

  loadFiles(): void {
    this.expertiseService.loadFileFromExpertise(this.expertiseId)
      .subscribe((data) => {
        this.applicationFiles = data.applicationFiles;
        this.expertiseFiles = data.expertiseFiles;
      });
  }
}
