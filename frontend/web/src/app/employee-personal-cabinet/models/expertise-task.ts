import {UserInfo} from '../../shared/models/domain/user-info';
import {File} from '../../shared/models/domain/file';

export class ExpertiseTask {
  constructor(public id: number = null,
              public name: string = null,
              public expiredDate: Date = null,
              public deadlineDate: Date = null,
              public isClosed: Boolean = null,
              public responsible: UserInfo = null,
              public taskFiles: File[] = []) {
  }
}
