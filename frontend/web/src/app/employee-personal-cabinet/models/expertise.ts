import {UserInfo} from '../../shared/models/domain/user-info';
import {ExpertiseTask} from './expertise-task';

export class Expertise {
  constructor(public id: number = null,
              public receiveDate: Date = null,
              public name: string = null,
              public description: string = null,
              public applicationId: number = null,
              public creator: UserInfo = null,
              public leader: UserInfo = null,
              public tasks: ExpertiseTask[] = []) {

  }
}
