import {File} from '../../shared/models/domain/file';

export class ExpertiseFiles {
  constructor(public applicationFiles: File[],
              public expertiseFiles: File[]) {

  }
}
