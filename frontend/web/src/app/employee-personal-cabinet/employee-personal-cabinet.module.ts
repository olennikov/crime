import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {EmployeePersonalCabinetComponent} from './components/employee-personal-cabinet.component';
import {ExpertiseListComponent} from './components/expertise-list/expertise-list.component';
import {ExpertiseItemComponent} from './components/expertise-item/expertise-item.component';
import {SharedModule} from '../shared/shared.module';
import {ExpertiseFileListComponent} from './components/expertise-file-list/expertise-file-list.component';
import {ExpertiseInfoComponent} from './components/expertise-info/expertise-info.component';
import {ExpertiseTaskListComponent} from './components/expertise-task-list/expertise-task-list.component';
import {ExpertiseSubtaskListComponent} from './components/expertise-subtask-list/expertise-subtask-list.component';
import {ExpertiseSubtaskItemComponent} from './components/expertise-subtask-item/expertise-subtask-item.component';
import {ExpertiseTaskItemComponent} from './components/expertise-task-item/expertise-task-item.component';
import {MatFormFieldModule, MatInputModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SubTaskItemComponent} from './components/subtask/subtask-item/sub-task-item.component';
import {SubTaskListComponent} from './components/subtask/subtask-list/sub-task-list.component';

const routes: Routes = [
  {
    path: '',
    component: EmployeePersonalCabinetComponent,
    children: [
      {path: '', redirectTo: 'expertise', pathMatch: 'full'},
      {path: 'expertise', component: ExpertiseListComponent},
      {
        path: 'expertise/:id', component: ExpertiseItemComponent, children: [
          {path: '', redirectTo: 'info', pathMatch: 'full'},
          {path: 'files', component: ExpertiseFileListComponent},
          {path: 'info/task/:taskId', component: ExpertiseTaskItemComponent},
          {path: 'info', component: ExpertiseInfoComponent}
        ]
      }
    ]
  }
];

@NgModule({
  declarations: [EmployeePersonalCabinetComponent,
    ExpertiseListComponent,
    ExpertiseItemComponent,
    ExpertiseFileListComponent,
    ExpertiseInfoComponent,
    ExpertiseTaskListComponent,
    ExpertiseSubtaskListComponent,
    ExpertiseSubtaskItemComponent,
    ExpertiseTaskItemComponent,
    SubTaskItemComponent,
    SubTaskListComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    RouterModule.forChild(routes),
    MatInputModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class EmployeePersonalCabinetModule {
}
