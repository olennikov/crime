import {environment} from '../../../environments/environment';
import {PaginationInfo} from '../../shared/components/table/table-with-pagination/pagination-info';

export const baseUrl = environment.baseUrl;

export class Urls {

  public static loginPageUrl(): string {
    return 'authorisation';
  }

  public static loginUrl(): string {
    return `${baseUrl}/auth/login`;
  }

  public static logoutUrl(): string {
    return `${baseUrl}/auth/logout`;
  }

  public static roundSaltUrl(userEmail: string): string {
    return `${baseUrl}/auth/roundsalt?userEmail=${userEmail}`;
  }

  public static userInfoUrl(): string {
    return `${baseUrl}/auth/userInfo`;
  }

  public static registrationUrl(): string {
    return `${baseUrl}/registration`;
  }

  public static getExpertiseApplicationsByClient(status: string): string {
    return `${baseUrl}/client/expertise-applications/filter/${status}`;
  }

  public static getExpertiseApplicationByClient(id: number): string {
    return `${baseUrl}/client/expertise-applications/${id}`;
  }

  public static saveExpertiseApplicationByClient(): string {
    return `${baseUrl}/client/expertise-applications/`;
  }

  public static uploadExpertiseApplication(id: number): string {
    return `${baseUrl}/client/expertise-applications/${id}/file-upload/`;
  }

  public static deleteExpertiseApplicationFile(expertiseApplication, fileId: number): string {
    return `${baseUrl}/files/${fileId}/expertise-application/${expertiseApplication}`;
  }

  public static deleteSubFile(subTaskId: number, fileId: number): string {
    return `${baseUrl}/files/${fileId}/subtasks/${subTaskId}`;
  }

  public static registrationModelUrl(disposableUrl: string): string {
    return `${baseUrl}/registration/get/${disposableUrl}`;
  }

  public static registrationGenerateUserUrl(): string {
    return `${baseUrl}/registration/generate-user`;
  }

  public static registrationApplicationList(): string {
    return `${baseUrl}/registration/registration-application-list`;
  }

  public static registrationApplicatopnByIdUrl(id: number): string {
    return `${baseUrl}/registration/getById/${id}`;
  }

  public static registrationAcceptedUrl(): string {
    return `${baseUrl}/registration/accept`;
  }

  public static registrationRejectedUrl(): string {
    return `${baseUrl}/registration/reject`;
  }

  public static expertiseApplicationListDirectorUrl(): string {
    return `${baseUrl}/director/expertise-applications/director-application-list`;
  }

  public static expertiseApplicationByIdUrl(id: number): string {
    return `${baseUrl}/director/expertise-applications/${id}`;
  }

  public static expertiseApplicationReject(): string {
    return `${baseUrl}/director/expertise-applications/reject`;
  }

  public static expertiseByStatus(paginationInfo: PaginationInfo, status: string): string {
    return `${baseUrl}/expertise/list?size=${paginationInfo.size}&page=${paginationInfo.number}&sort=${paginationInfo.sort}&status=${status}`;
  }

  public static expertiseFiles(id: number): string {
    return `${baseUrl}/expertise/${id}/files`;
  }

  public static expertiseById(id: number): string {
    return `${baseUrl}/expertise/${id}/`;
  }

  public static getEmployeeTaskById(taskId: number): string {
    return `${baseUrl}/employee/tasks/${taskId}`;
  }

  public static closeSubTask(): string {
    return `${baseUrl}/subtasks/close`;
  }

  public static getSubtaskByTaskId(taskId: number): string {
    return `${baseUrl}/subtasks/task/${taskId}`;
  }

  public static getSubtaskById(id: number): string {
    return `${baseUrl}/subtasks/${id}`;
  }

  public static saveOrDeleteSubtask(): string {
    return `${baseUrl}/subtasks`;
  }

  public static getChatMessagesList(expertiseId: number) {
    return `${baseUrl}/chat/messages/${expertiseId}`;
  }

  public static sendChatMessage() {
    return `${baseUrl}/chat/message`;
  }

  public static getChatExpertise(expertiseId: number) {
    return `${baseUrl}/chat/${expertiseId}`;
  }

  public static isChatParticipant(expertiseId: number) {
    return `${baseUrl}/chat/isChatParticipant/${expertiseId}`;
  }

  public static getTaskByExpertise(expertiseId: number): string {
    return `${baseUrl}/director/task/by-expertise/${expertiseId}`;
  }

  public static getDirectorTaskById(taskId: number): string {
    return `${baseUrl}/director/task/${taskId}`;
  }

  public static saveDirectorSubTaskUrl(): string {
    return `${baseUrl}/director/task/save-subtask`;
  }

  public static getDirectorExpertiseByApplication(applicationId: number): string {
    return `${baseUrl}/director/expertise/get-expertise/${applicationId}`;
  }

  public static getEmployeeUsers(): string {
    return `${baseUrl}/director/expertise/get-users`;
  }

  public static expertiseCreate(): string {
    return `${baseUrl}/director/expertise`;
  }

  public static acceptExpertiseApplication(): string {
    return `${baseUrl}/director/expertise-applications/accept`;
  }

  public static updateExpertiseApplicationresultUrl(): string {
    return `${baseUrl}/director/expertise-applications/update-result`;
  }

  public static getExpertiseApplicationResult(applicationId: number): string {
    return `${baseUrl}/director/expertise-applications/get-result/${applicationId}`;
  }

  public static uploadResultFileUrl(applicationId: number): string {
    return `${baseUrl}/director/expertise-applications/upload-result-file/${applicationId}`;
  }

  public static inWorkExpertiseApplication(): string {
    return `${baseUrl}/director/expertise-applications/in-work`;
  }

  public static InWorkExpertiseUrl(applicationId: number): string {
    return `${baseUrl}/director/expertise/setinwork/${applicationId}`;
  }

  public static closeExpertiseApplicationUrl(applicationId: number): string {
    return `${baseUrl}/director/expertise-applications/close-application/${applicationId}`;
  }

  public static clientsStatistic(): string {
    return `${baseUrl}/users/clients`;
  }

  public static employeesStatistic(): string {
    return `${baseUrl}/users/employees`;
  }

  public static directorsStatistic(): string {
    return `${baseUrl}/users/directors`;
  }

  public static blockUsers(): string {
    return `${baseUrl}/users/block`;
  }

  public static unblockUsers(): string {
    return `${baseUrl}/users/unblock`;
  }

  public static createUser(): string {
    return `${baseUrl}/users`;
  }

  public static getProfileUser(): string {
    return `${baseUrl}/profile/profile`;
  }

  public static editProfileUser(): string{
    return `${baseUrl}/profile/edit-profile`;
  }

  public static getRoundSaltContext(): string{
    return `${baseUrl}/profile/context-round-salt`;
  }

  public static editUserPassword(): string{
    return `${baseUrl}/profile/edit-password`;
  }

  public static createTaskUrl(): string {
    return `${baseUrl}/director/task/save-task`;
  }

  public static uploadTaskFileUrl(taskId: number): string {
    return `${baseUrl}/employee/tasks/${taskId}/upload-file`;
  }

    public static deleteTaskFile(subTaskId: number, fileId: number): string {
      return `${baseUrl}/files/${fileId}/task/${subTaskId}`;
    }

}
