import { SocketChannel } from '../models/socket-channel';

export class SocketChannels {
  public static chat: SocketChannel<String> = {
    address: '/user/chat'
  };
}
