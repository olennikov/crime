import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {Subject} from 'rxjs';
import {CompatClient, Frame, Message, Stomp} from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import {SocketChannel} from './models/socket-channel';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  /** Точка входа для подключения SockJs */
  private ENTRY_POINT_URL = `/socket/sockJs`;
  /** Время через которое клиент и сервер обмениваются пингами для подтверждения соединения */
  private HEARTBEAT = {
    incoming: 5000,
    outgoing: 5000
  };
  /** Задержка между попытками повторного подключения */
  private RECONNECT_DELAY = 5000;

  /** SockJs полностью повторяет интерфейс WebSocket */
  private sock: WebSocket;
  /** StompClient - клиент реализующий протокол Stomp */
  private stompClient: CompatClient;

  /** id пользователя после авторизации будет заменен на реальный id */
  private userId = 1;
  /** Флаг для включения отладочного вывода */
  private debug = false;

  /** Мапа для сопоставления адреса с уже существующей подпискойno
   * Нужна для восстановления подписок при реконнекте и кеширования подписок
   */
  private subjectsMap: { [key: string]: Subject<any> } = {};

  constructor() {
    this.userId = 2;
    this.initializeWebSocketConnection();
    //TODO: после того как будет реализвана авторизация/аутентификация необходимо сохранить userId
    // Подключение по websocket должно быть привязано к пользователю для корректной работы
    // поэтому выбираем текущего пользователя.
    //  this.userId = 10001;

  }

  /**
   * Выбор канала для подписки
   * Данный метод необходимо вызвать, чтобы подписаться на прослушивание сокета
   * @param address адрес канала (Например: /user/notifications)
   * Если канал начинается с /user, то по нему будут
   * отправляться персональные уведомления для текущего пользователя
   * иначе - все сообщения независимо от пользователя
   * @returns subject для подписки на соообщения
   */
  public selectChanel<T>(chanel: SocketChannel<T>): Subject<T> {
    const address = chanel.address;
    if (this.subjectsMap[address]) {
      return this.subjectsMap[address];
    }

    const result = new Subject<T>();
    this.subjectsMap[address] = result;
    if (this.stompClient && this.stompClient.connected) {
      this.stompClient.subscribe(address, message => {
        result.next(this.convertMessage(message));
      });
    }

    return result;
  }

  /**
   * Метод для отправки сообщений через webSocket
   * @param dest - адрес назначения (например /app/hello)
   * @param message - сообщение для отправки
   */
  public sendMessage(dest: string, message: any): void {
    const stringMessage = JSON.stringify(message);
    this.stompClient.send(dest, {}, stringMessage);
  }

  private initializeWebSocketConnection(): void {
    this.tryConnect();
  }

  private tryConnect(): void {
    // инициализируем Stomp через созданный ранее канал
    this.stompClient = Stomp.over(() => new SockJS(this.getConnectionUrl(`${this.userId}`)));
    this.stompClient.reconnectDelay = this.RECONNECT_DELAY;

    // Настраиваем stomp
    this.stompClient.heartbeat = this.HEARTBEAT;
    if (!this.debug) {
      this.stompClient.debug = () => {
      };
    }

    // Коннектимся
    this.stompClient.connect({}, this.onConnect.bind(this), this.onError.bind(this));
  }

  /** Обработчик успешного подключения
   * Пока что только инициализирует подписки
   */
  private onConnect(frame: Frame): void {
    this.initSubscriptions();
  }

  /** Обработчик ошибок при передаче
   * Ошибкой может быть только потеря связи, поэтому в случае ошибки планируется реконнект через заданное время.
   */
  private onError(error: string): void {
    console.log(`Scheduled reconnect after ${this.RECONNECT_DELAY}ms`);
  }

  /** Функция для инициализации подписок
   * Если вызывается при первом подключении то подписок нет и нечего инициализированно не будет
   * Если вызывается после реконнекта то будут восстановлены все старые подписки
   */
  private initSubscriptions(): void {
    Object.keys(this.subjectsMap).forEach(dest => {
      this.stompClient.subscribe(dest, message => {
        this.subjectsMap[dest].next(this.convertMessage(message));
      });
    });
  }

  /** Функция для приведения сообщений к одному формату */
  private convertMessage(message: Message): any {
    return JSON.parse(message.body);
  }

  /** получить url для соединения */
  private getConnectionUrl(username: string): string {
    return `${environment.websocketBaseUrl}${this.ENTRY_POINT_URL}`;
  }
}
