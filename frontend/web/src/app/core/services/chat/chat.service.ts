import {Injectable} from '@angular/core';
import {HttpService} from '../http/http.service';
import {Observable} from 'rxjs';
import {ChatMessage} from '../../../shared/models/chat/ChatMessage';
import {Urls} from '../../urls/url';
import {Chat} from '../../../shared/models/chat/Chat';
import {UserInfo} from '../../../shared/models/domain/user-info';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  SKIP = 0;
  TAKE = 25;

  constructor(private httpService: HttpService) {
  }

  /**
   * Получение сообщений чата экспертизы
   * @param expertiseId идентификатор экспертизы
   * @param skip сколько сообщений нужно пропустить
   * @param take сколько сообщений нужно выгрузить
   */
  public getMessagesByExpertise(expertiseId: number, skip?: number, take?: number): Observable<ChatMessage[]> {
    return this.httpService.getObservable(Urls.getChatMessagesList(expertiseId), {
      params: this.httpService.getRequestParams({
        skip: skip ? skip : this.SKIP,
        take: take ? take : this.TAKE
      })
    });
  }

  /**
   * Отправка сообщения в чат
   * @param message сообщение, которое необходимо отправить
   * @param chat чат к которому прикреплено данное сообщение
   * @param user пользователь, написавший комментарий
   */
  public sendMessage(message: string, chat: Chat, user: UserInfo): Observable<ChatMessage> {
    const chatMessage: ChatMessage = new ChatMessage(null, chat, null, message, user);
    return this.httpService.postObservable(Urls.sendChatMessage(), chatMessage);
  }

  /**
   * Получение идентификатора залогинненого пользователя
   */
  public getCurrentUser(): Observable<UserInfo> {
    return this.httpService.getObservable(Urls.userInfoUrl());
  }

  /**
   * Получение чата экспертизы
   * @param expertiseId идентификатор экспертизы
   */
  public getChatExpertise(expertiseId: number) {
    return this.httpService.getObservable(Urls.getChatExpertise(expertiseId));
  }

  /**
   * Является ли текущий пользователь участником чата
   * @param expertiseId идентификатор экспертизы
   */
  public isChatParticipant(expertiseId: number): Observable<boolean> {
    return this.httpService.getObservable(Urls.isChatParticipant(expertiseId));
  }
}
