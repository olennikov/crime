import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpService} from './services/http/http.service';
import {CookieProviderService} from './services/cookie/cookie.service';
import {HttpClientModule} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import {GlobalHttpErrorInterceptorProvider} from './http-interceptor/HttpErrorInterceptor';


@NgModule({
  providers: [
    CookieService,
    HttpService,
    CookieProviderService,
    GlobalHttpErrorInterceptorProvider
  ],
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
  ]
})
export class CoreModule {
}
