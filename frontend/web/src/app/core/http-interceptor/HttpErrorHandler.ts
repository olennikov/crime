import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {FORBIDDEN, INTERNAL_SERVER_ERROR, NOT_ACCEPTABLE, NOT_FOUND, UNAUTHORIZED} from 'http-status-codes';
import {Urls} from '../urls/url';
import {PopupService} from '../../shared/services/popup/popup.service';
import {ErrorPopupComponent} from '../../shared/components/popup/error-popup/error-popup.component';
import {ErrorInfo} from '../../shared/models/errors/ErrorInfo';

@Injectable({
  providedIn: 'root'
})
export class HttpErrorHandler {
  private errorMessage: string = 'Произошла ошибка при обработке запроса';

  constructor(
    private router: Router,
    private popupService: PopupService
  ) {
  }

  /** Обработчик ошибок */
  public handleError(httpErrorResponse: HttpErrorResponse): void {
    if (!environment.production) {
      console.error(httpErrorResponse);
    }

    switch (httpErrorResponse.status) {
      case UNAUTHORIZED: {
        this.redirectToLoginPage();
        break;
      }
      case FORBIDDEN: {
        this.router.navigateByUrl('/forbidden');
        break;
      }
      case NOT_FOUND: {
        this.popupService.openPopup(ErrorPopupComponent, {data: new ErrorInfo("Произошла ошибка при отправке запроса", `${httpErrorResponse.url} ${httpErrorResponse.status} ${httpErrorResponse.statusText}`)});
        break;
      }
      case NOT_ACCEPTABLE: {
        this.popupService.openPopup(ErrorPopupComponent, {data: new ErrorInfo(this.errorMessage, httpErrorResponse.error.message)});
        break;
      }
      case INTERNAL_SERVER_ERROR: {
        this.popupService.openPopup(ErrorPopupComponent, {data: new ErrorInfo(this.errorMessage, httpErrorResponse.error.message)});
        break;
      }
      //Пропал интернет:
      case 0: {
        this.popupService.openPopup(ErrorPopupComponent, {
          data: new ErrorInfo(
            this.errorMessage,
            'Не удалось сохранить внесенные изменения из-за проблем с интернет-соединением')
        });
        break;
      }
    }
  }

  /** Редирект на страницу логина */
  private redirectToLoginPage(): void {
    window.location.href = Urls.loginPageUrl();
  }
}
