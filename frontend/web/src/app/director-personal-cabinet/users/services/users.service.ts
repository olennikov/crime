import {Injectable} from '@angular/core';
import {HttpService} from '../../../core/services/http/http.service';
import {PaginationInfo} from '../../../shared/components/table/table-with-pagination/pagination-info';
import {Urls} from '../../../core/urls/url';
import {Observable} from 'rxjs';
import {ClientInfo} from '../models/ClientInfo';
import {HttpParams} from '@angular/common/http';
import {EmployeeInfo} from '../models/EmployeeInfo';
import {DirectorInfo} from '../models/DirectorInfo';
import {PaginationResponse} from '../../../shared/models/query/pagination.response';
import {CreateUser} from '../models/CreateUser';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private httpService: HttpService) {

  }

  public getClientStatistic(paginationInfo: PaginationInfo): Observable<PaginationResponse<ClientInfo>> {
    return this.httpService.getObservable(Urls.clientsStatistic(), {
      params: this.transformPaginationToParams(paginationInfo)
    });
  }

  public getEmployeesStatistic(paginationInfo: PaginationInfo): Observable<PaginationResponse<EmployeeInfo>> {
    return this.httpService.getObservable(Urls.employeesStatistic(), {
      params: this.transformPaginationToParams(paginationInfo)
    });
  }

  public getDirectorsStatistic(paginationInfo: PaginationInfo): Observable<PaginationResponse<DirectorInfo>> {
    return this.httpService.getObservable(Urls.directorsStatistic(), {
      params: this.transformPaginationToParams(paginationInfo)
    });
  }

  public blockUsers(ids: number[]) {
    return this.httpService.postObservable(Urls.blockUsers(), ids);
  }

  public unblockUsers(ids: number[]) {
    return this.httpService.postObservable(Urls.unblockUsers(), ids);
  }

  public createUser(user: CreateUser) {
    return this.httpService.postObservable(Urls.createUser(), user);
  }

  private transformPaginationToParams(paginationInfo: PaginationInfo): HttpParams {
    return this.httpService.getRequestParams({
      size: paginationInfo.size,
      page: paginationInfo.number
    });
  }
}
