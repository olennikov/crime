import {UserInfo} from './UserInfo';

export class EmployeeInfo extends UserInfo {
  email: string;
  isBlock: boolean;
  taskCount: number;
  closedTaskCount: number;
  subTaskCount: number;
  closedSubTaskCount: number;
}
