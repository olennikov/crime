import {UserInfo} from './UserInfo';

export class DirectorInfo extends UserInfo {
  email: string;
  isBlock: boolean;
  expertiseCount: number;
  closedExpertiseCount: number;
}
