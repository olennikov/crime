import {UserInfo} from './UserInfo';

export class ClientInfo extends UserInfo {
  email: string;
  isBlock: boolean;
  applicationCount: number;
}
