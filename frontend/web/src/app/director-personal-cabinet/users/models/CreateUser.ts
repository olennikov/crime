export class CreateUser {
  constructor(public lastName: string,
              public firstName: string,
              public secondName: string,
              public email: string,
              public phone: string,
              public role: string) {

  }
}
