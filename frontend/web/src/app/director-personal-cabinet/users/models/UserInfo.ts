export abstract class UserInfo {
  id: number;
  firstName: string;
  secondName: string;
  lastName: string;
  phone: string;
}
