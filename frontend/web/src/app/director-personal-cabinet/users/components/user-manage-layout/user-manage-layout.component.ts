import {ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {PaginationInfo} from '../../../../shared/components/table/table-with-pagination/pagination-info';
import {TableHeaderColumn} from '../../../../shared/components/table/TableHeaderColumn';
import {UsersService} from '../../services/users.service';
import {UserInfo} from '../../models/UserInfo';
import {Observable} from 'rxjs';
import {PaginationResponse} from '../../../../shared/models/query/pagination.response';
import {PopupService} from '../../../../shared/services/popup/popup.service';
import {InfoPopupBuilder} from '../../../../shared/components/popup/info-popup/InfoPopup';

@Component({
  selector: 'app-user-manage-layout',
  templateUrl: './user-manage-layout.component.html',
  styleUrls: ['./user-manage-layout.component.scss']
})
export class UserManageLayoutComponent implements OnInit {

  countRecord = 0;
  dataSource: UserInfo[] = [];

  @Input()
  columns: TableHeaderColumn[];

  @Input()
  addedBtn: boolean = true;

  @Input()
  blockBtn: boolean = true;

  @Input()
  unblockBtn: boolean = true;

  @Input()
  loadDataFn: (paginationInfo: PaginationInfo) => Observable<PaginationResponse<UserInfo>>;

  @Input()
  addFn: () => void;

  private selectedUsers: UserInfo[] = [];
  private lastPaginationInfo: PaginationInfo = null;

  constructor(private userService: UsersService,
              private popupService: PopupService,
              private cdr: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    const paginationInfo: PaginationInfo = new PaginationInfo(10, 0);
    this.loadData(paginationInfo);
  }

  public get selectedUserIsEmpty(): boolean {
    return this.selectedUsers.length == 0;
  }

  loadData(paginationInfo: PaginationInfo) {
    this.lastPaginationInfo = paginationInfo;
    this.loadDataFn(paginationInfo)
      .subscribe(data => {
        this.countRecord = data.count;
        this.dataSource = data.records;
      });
  }

  selectedRowsChanged(selectedUsers: UserInfo[]) {
    this.selectedUsers = selectedUsers;
    this.cdr.detectChanges();
  }

  blockUsers() {
    this.userService.blockUsers(this.selectedUsers.map(x => x.id))
      .subscribe(data => {
        this.popupService.openInfoPopup(InfoPopupBuilder.builder()
          .text('Пользователи успешно заблокированы')
          .positiveHandler(() => this.loadData(this.lastPaginationInfo))
          .build());
      });
  }

  unblockUsers() {
    this.userService.unblockUsers(this.selectedUsers.map(x => x.id))
      .subscribe(data => {
        this.popupService.openInfoPopup(InfoPopupBuilder.builder()
          .text('Пользователи успешно разблокированы')
          .positiveHandler(() => this.loadData(this.lastPaginationInfo))
          .build());
      });
  }

  addUsers() {
    if (this.addedBtn && this.addFn) {
      this.addFn();
    }
  }
}
