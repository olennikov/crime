import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UsersService} from '../../services/users.service';
import {CreateUser} from '../../models/CreateUser';
import {ActivatedRoute, Router} from '@angular/router';
import {InfoPopupBuilder} from '../../../../shared/components/popup/info-popup/InfoPopup';
import {PopupService} from '../../../../shared/services/popup/popup.service';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss']
})
export class UserCreateComponent implements OnInit {

  registrationForm: FormGroup;
  private allowedUserTypes = ['DIRECTOR', 'EMPLOYEE'];
  private readonly userRole: string;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private form: FormBuilder,
              private userService: UsersService,
              private popupService: PopupService) {
    this.userRole = this.activatedRoute.snapshot.paramMap.get('type').toUpperCase();
    if (!this.allowedUserTypes.includes(this.userRole)) {
      this.navigateToBack();
    }
  }

  ngOnInit() {
    this.registrationForm = this.form.group({
      lastName: new FormControl('', [Validators.required]),
      firstName: new FormControl('', [Validators.required]),
      secondName: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      phone: new FormControl('', [Validators.required])
    });
  }

  saveUser() {
    this.userService.createUser(this.mapFormToUser())
      .subscribe(data => {
        this.popupService.openInfoPopup(InfoPopupBuilder.builder()
          .text('На указанный email был отправлен пароль для доступа к системе')
          .positiveHandler(() => this.navigateToBack())
          .build());
      });
  }

  private navigateToBack() {
    this.router.navigate(['./'], {relativeTo: this.activatedRoute.parent});
  }

  private mapFormToUser(): CreateUser {
    return new CreateUser(this.registrationForm.get('lastName').value,
      this.registrationForm.get('firstName').value,
      this.registrationForm.get('secondName').value,
      this.registrationForm.get('email').value,
      this.registrationForm.get('phone').value,
      this.userRole);
  }
}
