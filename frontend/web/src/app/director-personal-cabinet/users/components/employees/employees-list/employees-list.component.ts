import {Component} from '@angular/core';
import {TableHeaderColumn} from '../../../../../shared/components/table/TableHeaderColumn';
import {UsersService} from '../../../services/users.service';
import {PaginationInfo} from '../../../../../shared/components/table/table-with-pagination/pagination-info';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-employees-list',
  templateUrl: './employees-list.component.html',
  styleUrls: ['./employees-list.component.scss']
})
export class EmployeesListComponent {

  columns: TableHeaderColumn[] = [
    new TableHeaderColumn('id', 'ID'),
    new TableHeaderColumn('lastName', 'Фамилия'),
    new TableHeaderColumn('firstName', 'Имя'),
    new TableHeaderColumn('secondName', 'Отчество'),
    new TableHeaderColumn('phone', 'Номер телефона'),
    new TableHeaderColumn('email', 'email'),
    new TableHeaderColumn('isBlock', 'Заблокирован'),
    new TableHeaderColumn('taskCount', 'Кол-во задач'),
    new TableHeaderColumn('closedTaskCount', 'Кол-во закрытых задач'),
    new TableHeaderColumn('subTaskCount', 'Кол-во подзадач'),
    new TableHeaderColumn('closedSubTaskCount', 'Кол-во закрытых подзадач')
  ];

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private userService: UsersService) {
  }

  loadDataFn = (paginationInfo: PaginationInfo) => this.userService.getEmployeesStatistic(paginationInfo);

  addEmployeeRedirect = () => this.router.navigate(['./create/employee'], {relativeTo: this.activatedRoute.parent});
}
