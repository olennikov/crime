import {Component} from '@angular/core';
import {TableHeaderColumn} from '../../../../../shared/components/table/TableHeaderColumn';
import {PaginationInfo} from '../../../../../shared/components/table/table-with-pagination/pagination-info';
import {UsersService} from '../../../services/users.service';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.scss']
})
export class ClientsListComponent {

  columns: TableHeaderColumn[] = [
    new TableHeaderColumn('id', 'ID'),
    new TableHeaderColumn('lastName', 'Фамилия'),
    new TableHeaderColumn('firstName', 'Имя'),
    new TableHeaderColumn('secondName', 'Отчество'),
    new TableHeaderColumn('phone', 'Номер телефона'),
    new TableHeaderColumn('email', 'email'),
    new TableHeaderColumn('isBlock', 'Заблокирован'),
    new TableHeaderColumn('applicationCount', 'Кол-во заявлений')
  ];

  constructor(private userService: UsersService) {
  }

  loadDataFn = (paginationInfo: PaginationInfo) => this.userService.getClientStatistic(paginationInfo);
}
