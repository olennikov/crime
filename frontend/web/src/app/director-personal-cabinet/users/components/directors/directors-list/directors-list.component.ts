import {Component} from '@angular/core';
import {TableHeaderColumn} from '../../../../../shared/components/table/TableHeaderColumn';
import {UsersService} from '../../../services/users.service';
import {PaginationInfo} from '../../../../../shared/components/table/table-with-pagination/pagination-info';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-directors-list',
  templateUrl: './directors-list.component.html',
  styleUrls: ['./directors-list.component.scss']
})
export class DirectorsListComponent {

  columns: TableHeaderColumn[] = [
    new TableHeaderColumn('id', 'ID'),
    new TableHeaderColumn('lastName', 'Фамилия'),
    new TableHeaderColumn('firstName', 'Имя'),
    new TableHeaderColumn('secondName', 'Отчество'),
    new TableHeaderColumn('phone', 'Номер телефона'),
    new TableHeaderColumn('email', 'email'),
    new TableHeaderColumn('isBlock', 'Заблокирован'),
    new TableHeaderColumn('expertiseCount', 'Кол-во экспертиз'),
    new TableHeaderColumn('closedExpertiseCount', 'Кол-во закрытых экспертиз')
  ];

  constructor(private userService: UsersService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
  }

  loadDataFn = (paginationInfo: PaginationInfo) => this.userService.getDirectorsStatistic(paginationInfo);

  addDirectorRedirect = () => this.router.navigate(['./create/director'], {relativeTo: this.activatedRoute.parent});
}
