import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ClientsListComponent} from './components/clients/clients-list/clients-list.component';
import {EmployeesListComponent} from './components/employees/employees-list/employees-list.component';
import {DirectorsListComponent} from './components/directors/directors-list/directors-list.component';
import {SharedModule} from '../../shared/shared.module';
import {UserManageLayoutComponent} from './components/user-manage-layout/user-manage-layout.component';
import {UserCreateComponent} from './components/user-create/user-create.component';
import {MatInputModule, MatSelectModule} from '@angular/material';
import {ReactiveFormsModule} from '@angular/forms';

const routes: Routes = [
  {path: '', redirectTo: 'leaders', pathMatch: 'full'},
  {path: 'leaders', component: DirectorsListComponent},
  {path: 'employees', component: EmployeesListComponent},
  {path: 'clients', component: ClientsListComponent},
  {path: 'create/:type', component: UserCreateComponent},
];

@NgModule({
  declarations: [ClientsListComponent, EmployeesListComponent, DirectorsListComponent, UserManageLayoutComponent, UserManageLayoutComponent, UserCreateComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSelectModule,
  ]
})
export class UsersModule {
}
