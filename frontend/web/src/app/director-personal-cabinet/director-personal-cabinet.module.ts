import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {RouterModule, Routes} from '@angular/router';
import {DirectorPersonalCabinetComponent} from './components/director-personal-cabinet.component';
import {ApplicationDataComponent} from './components/application-data/application-data.component';
import {RegistrationApplicationDataService} from './services/registration/registration-application-data.service';
import {ApplicaionExpertiseDataComponent} from './components/applicaion-expertise-data/applicaion-expertise-data.component';
import {ExpertiseApplicationFormComponent} from './components/expertise-application-form/expertise-application-form.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {RegistrationApplicationFormComponent} from './components/registration-application-form/registration-application-form.component';
import {ExpertiseApplicationRejectFormComponent} from './components/expertise-application-reject-form/expertise-application-reject-form.component';
import {ExpertiseApplicationAcceptFormComponent} from './components/expertise-application-accept-form/expertise-application-accept-form.component';
import {RegistrationApplicationRejectComponent} from './components/registration-application-reject/registration-application-reject.component';
import {CreateExpertiseObjectComponent} from './components/create-expertise-object/create-expertise-object.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatSelectModule} from '@angular/material/select';
import {ResultExpertiseApplicationComponent} from './components/result-expertise-application/result-expertise-application.component';
import { ExpertiseTaskComponent } from './components/expertise-task/expertise-task.component';
import { ExpertiseSubtaskComponent } from './components/expertise-subtask/expertise-subtask.component';

const routes: Routes = [
  {
    path: '',
    component: DirectorPersonalCabinetComponent,
    children: [
      {path: '', redirectTo: 'registrationApplication', pathMatch: 'full'},
      {path: 'registrationApplication', component: ApplicationDataComponent},
      {path: 'registrationApplication/:id', component: RegistrationApplicationFormComponent},
      {path: 'registrationApplication/reject/:id', component: RegistrationApplicationRejectComponent},
      {path: 'expertiseApplication', component: ApplicaionExpertiseDataComponent},
      {path: 'expertiseApplication/:id', component: ExpertiseApplicationFormComponent},
      {path: 'expertiseApplication/reject/:id', component: ExpertiseApplicationRejectFormComponent},
      {path: 'expertiseApplication/create-expertise/:id', component: CreateExpertiseObjectComponent},
      {path: 'expertiseApplication/accept/:id', component: ExpertiseApplicationAcceptFormComponent},
      {path: 'expertiseApplication/result/:id', component: ResultExpertiseApplicationComponent},
      {path: 'expertiseApplication/task/:expertiseId/:id', component: ExpertiseTaskComponent},
            {path: 'expertiseApplication/task/:expertiseId', component: ExpertiseTaskComponent},
      {path: 'users', loadChildren: './users/users.module#UsersModule'}
    ]
  }
];

@NgModule({
  declarations: [DirectorPersonalCabinetComponent, ApplicationDataComponent, ApplicaionExpertiseDataComponent, ExpertiseApplicationFormComponent, RegistrationApplicationFormComponent, ExpertiseApplicationRejectFormComponent, ExpertiseApplicationAcceptFormComponent, RegistrationApplicationRejectComponent, CreateExpertiseObjectComponent, ResultExpertiseApplicationComponent, ExpertiseTaskComponent, ExpertiseSubtaskComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    ReactiveFormsModule,
    MatInputModule,
    MatDatepickerModule,
    RouterModule.forChild(routes),
    MatSelectModule,
    MatAutocompleteModule
  ],
  providers: [RegistrationApplicationDataService]
})
export class DirectorPersonalCabinetModule {
}
