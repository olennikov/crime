import {Component, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ExpertiseApplicationDataService} from '../../services/expertise_application/expertise-application-data.service';
import {ExpertiseApplicationResult} from '../../services/expertise_application/models/expertise-application-result.model';
import {FileUploaderComponent} from '../../../shared/components/file/file-uploader/file-uploader.component';
import {FileListComponent} from '../../../shared/components/file/file-list/file-list.component';
import {Urls} from '../../../../app/core/urls/url';
import {Observable} from 'rxjs';
import {PopupService} from '../../../shared/services/popup/popup.service';
import {InfoPopupBuilder} from '../../../shared/components/popup/info-popup/InfoPopup';
import {File} from '../../../shared/models/domain/file';

@Component({
  selector: 'app-result-expertise-application',
  templateUrl: './result-expertise-application.component.html',
  styleUrls: ['./result-expertise-application.component.scss']
})
export class ResultExpertiseApplicationComponent {

  @ViewChild(FileUploaderComponent, {static: false})
  uploader: FileUploaderComponent;

  observable: Observable<ExpertiseApplicationResult>;

  applicationId: number = 0;
  resultForm: FormGroup;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private activatedRoute: ActivatedRoute,
              private expertiseApplicationService: ExpertiseApplicationDataService,
              private popupService: PopupService) {
    this.resultForm = this.formBuilder.group({
      id: new FormControl(null),
      resultDescription: new FormControl(null),
      applicationStatus: new FormControl(null)
    });
    this.activatedRoute.params.subscribe(params => {
      this.applicationId = params['id'];
      this.loadFiles();
    });
  }

  loadFiles() {
    this.observable = this.expertiseApplicationService.getApplicationResult(this.applicationId);
    this.observable.subscribe(data => {
      this.resultForm.patchValue(data);
    });
  }

  closeApplication() {
    this.expertiseApplicationService
      .closeExpertiseApplication(this.applicationId)
      .subscribe(data => {
        this.popupService.openInfoPopup(InfoPopupBuilder.builder()
          .text(`Заявка с идентификатором ${this.applicationId} успешно закрыта`)
          .build());
      });
  }

  close() {
    this.router.navigate(['director','expertiseApplication']);
  }

  previous() {
    this.router.navigate(['director','expertiseApplication', 'create-expertise', `${this.applicationId}`]);
  }

  saveResult() {
    let observable: Observable<number> = this.expertiseApplicationService.updateExpertiseResult(this.resultForm.value);
    observable.subscribe(data => {
      if (this.uploader.hasItem()) {
        this.uploader.uploadAll(Urls.uploadResultFileUrl(this.applicationId));
      } else {
        this.successSave();
      }
    });
  }

  successSave() {
    this.popupService.openInfoPopup(InfoPopupBuilder.builder()
      .text('Данные результата успешно обновлены')
      .build());
  }

  deleteFile(file: File) {
    this.expertiseApplicationService.deleteFile(this.applicationId, file.id)
      .subscribe(data => {
        this.popupService.openInfoPopup(InfoPopupBuilder.builder()
          .text("Файл успешно удален")
          .positiveHandler(() => this.loadFiles())
          .build())
      })
  }
}
