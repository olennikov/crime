import { Component, OnInit, Input } from '@angular/core';
import {FormBuilder, FormGroup, FormControl} from '@angular/forms';
import { SubTaskModel } from '../../services/task/models/subtask.model';
import {TaskDataService} from '../../services/task/task-data.service';
import {InfoPopupBuilder} from '../../../shared/components/popup/info-popup/InfoPopup';
import {PopupService} from '../../../shared/services/popup/popup.service';

@Component({
  selector: 'app-expertise-subtask',
  templateUrl: './expertise-subtask.component.html',
  styleUrls: ['./expertise-subtask.component.scss']
})
export class ExpertiseSubtaskComponent implements OnInit {

  public subtaskForm: FormGroup;

  @Input()
  public subtaskData: SubTaskModel;
  @Input()
  private taskId: number;

  constructor(private formBuilder: FormBuilder,
              private taskService: TaskDataService,
              private popupService: PopupService) {
    this.subtaskForm = this.formBuilder.group({
      id: new FormControl(null),
      name: new FormControl(null),
      isClosed: new FormControl(null),
      description: new FormControl(null),
      deadlineDate: new FormControl(null),
      comment: new FormControl(null)
    });
  }

  save (){
    let subTaskData = this.subtaskForm.value;
    subTaskData.taskId = this.subtaskData.taskId;
    this.taskService.createSubTask(subTaskData).subscribe(data=>{
          this.subtaskForm.controls['id'].setValue(data.id);
          this.subtaskData.id = data.id;
          this.subtaskData.name = data.name;
          this.popupService.openInfoPopup(InfoPopupBuilder.builder()
                        .text(`Подзадача сохранена с идентификатором ${data.id}`)
                        .build());
    });
  }

  ngOnInit() {
    this.subtaskData.deadlineDate = new Date(this.subtaskData.deadlineDate)
    this.subtaskForm.patchValue(this.subtaskData);
  }

}
