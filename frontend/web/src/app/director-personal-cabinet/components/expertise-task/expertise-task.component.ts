import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormControl} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {MatExpansionModule} from '@angular/material/expansion';
import { TaskDataService } from '../../services/task/task-data.service';
import { UserService } from '../../services/user/user.service';
import { SubTaskModel } from '../../services/task/models/subtask.model';
import { TaskDataView } from '../../services/task/models/task-view.model';
import { TaskModel } from '../../models/task.model';
import {TaskStatus, TaskStatusDisplayValue} from '../../../shared/models/domain/task-status';
import {UserFioPipe} from '../../../shared/pipes/user-fio.pipe';
import {InfoPopupBuilder} from '../../../shared/components/popup/info-popup/InfoPopup';
import {PopupService} from '../../../shared/services/popup/popup.service';
import { UserInfo } from '../../../shared/models/domain/user-info';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-expertise-task',
  templateUrl: './expertise-task.component.html',
  styleUrls: ['./expertise-task.component.scss']
})
export class ExpertiseTaskComponent implements OnInit {

  public createExpertiseTaskForm: FormGroup = null;

  public taskStatuses = (()=>{
      var displayArray = [];
      TaskStatusDisplayValue.forEach((value,key)=>displayArray.push({'key': key, 'value':value}))
      return displayArray;
  })();

  public subTasksData: SubTaskModel [] = [];
  public taskModel: TaskDataView = new TaskDataView();
  public users: any[];
  private expertiseId: number = 0;

  constructor(private formBuilder: FormBuilder,
              private taskService: TaskDataService,
              private activateRoute: ActivatedRoute,
              private popupService: PopupService,
              private userService: UserService) {
      this.createExpertiseTaskForm = this.formBuilder.group({
          id: new FormControl(null),
          taskStatus: new FormControl(null),
          responsibleUserId: new FormControl(null),
          name: new FormControl(null),
          description: new FormControl(null),
          deadlineDate: new FormControl (null)
      });
      this.activateRoute.params.subscribe(params=>{
         this.expertiseId = params['expertiseId'];
         if (params['id']!=null){
            this.taskService.getTaskById(params['id']).subscribe(data=>{
                 if (data!=null){
                    data.deadlineDate = new Date(data.deadlineDate);
                    this.taskModel = data;
                    this.createExpertiseTaskForm.patchValue(data)
                    this.subTasksData = this.taskModel.subTaskArray;
                 }
            })
         }
      });
  }

  ngOnInit() {
       let observable: Observable<any[]> = this.userService.getEmployeeUsers();
       observable.pipe(map((data:UserInfo[])=>{
           return data.map(UserFioPipe.formatUser)
       }));
       observable.subscribe(data=>{
           this.users = data;
       });
  }

  public addNewModel() {
    this.subTasksData.push(new SubTaskModel(null,null,null,new Date(),this.createExpertiseTaskForm.controls['id'].value,null,null,[]));
  }

  public save () {
    let taskModel: TaskModel = this.createExpertiseTaskForm.value;
    taskModel.expertiseId = this.expertiseId;
    this.taskService.createTask(taskModel).subscribe(data=>{
        this.createExpertiseTaskForm.controls['id'].setValue(data);
        this.createExpertiseTaskForm.controls['taskStatus'].setValue(TaskStatus.NEW);
        this.popupService.openInfoPopup(InfoPopupBuilder.builder()
                .text(`Задача для экспертизы ${this.expertiseId} сохранена с номером ${data}`)
                .build());
    });
  }

}
