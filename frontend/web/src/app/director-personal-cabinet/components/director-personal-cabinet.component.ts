import {Component, OnInit} from '@angular/core';
import {TreeNode} from '../../shared/models/menu/tree-node';

@Component({
  selector: 'app-director-personal-cabinet',
  templateUrl: './director-personal-cabinet.component.html',
  styleUrls: ['./director-personal-cabinet.component.scss']
})
export class DirectorPersonalCabinetComponent implements OnInit {

  registrationRoute: TreeNode[] = [
    {displayValue: 'Заявки на регистрацию', routerLink: 'registrationApplication'},
    {displayValue: 'Заявки экспертиз', routerLink: 'expertiseApplication'},
    {
      displayValue: 'Управление пользователями', children: [
        {displayValue: 'Сотрудники', routerLink: 'users/employees'},
        {displayValue: 'Руководители', routerLink: 'users/leaders'},
        {displayValue: 'Клиенты', routerLink: 'users/clients'}
      ]
    },
  ];

  constructor() {
  }

  ngOnInit() {
  }

}
