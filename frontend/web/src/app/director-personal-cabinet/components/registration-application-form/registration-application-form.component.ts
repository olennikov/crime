import {Component} from '@angular/core';
import {Observable} from 'rxjs';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {RegistrationApplicationModel} from '../../services/registration/models/registration-application.model';
import {RegistrationApplicationDataService} from '../../services/registration/registration-application-data.service';
import {InfoPopupBuilder} from '../../../shared/components/popup/info-popup/InfoPopup';
import {PopupService} from '../../../shared/services/popup/popup.service';

@Component({
  selector: 'app-registration-application-form',
  templateUrl: './registration-application-form.component.html',
  styleUrls: ['./registration-application-form.component.scss']
})
export class RegistrationApplicationFormComponent {

  registrationFormGroup: FormGroup;
  applicationId: number = 0;
  status: string = 'REJECT';

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private registrationApplicationService: RegistrationApplicationDataService,
              private popupService: PopupService) {
    this.registrationFormGroup = this.formBuilder.group({
      id: new FormControl(null),
      status: new FormControl(null),
      lastName: new FormControl(null),
      firstName: new FormControl(null),
      secondName: new FormControl(null),
      phone: new FormControl(null),
      email: new FormControl(null),
      organisationName: new FormControl(null),
      organisationPhone: new FormControl(null),
      organisationInn: new FormControl(null),
      organisationOgrn: new FormControl(null),
      rejectReason: new FormControl(null)
    });
    activatedRoute.params.subscribe(params => {
      this.registrationFormGroup.patchValue({id: params['id']});
      this.applicationId = params['id'];
      let observable: Observable<RegistrationApplicationModel> = this.registrationApplicationService.getRegistrationApplicationById(params['id']);
      observable.subscribe(data => {
        this.status = data.status;
        this.registrationFormGroup.patchValue(data);
      });
    });
  }

  accepted() {
    let observable: Observable<number> = this.registrationApplicationService.acceptRegistrationRequest(this.applicationId);
    observable.subscribe(data => {
      this.popupService.openInfoPopup(InfoPopupBuilder.builder()
        .text(`Заявка на регистрацию ${this.applicationId} одобрена.\r\nПользователю отправлена ссылка для регистрации`)
        .positiveHandler(() => this.router.navigate(['director', 'registrationApplication']))
        .build());
    });
  }

  reject() {
    this.router.navigate(['director', 'registrationApplication', 'reject', `${this.applicationId}`]);
  }
}
