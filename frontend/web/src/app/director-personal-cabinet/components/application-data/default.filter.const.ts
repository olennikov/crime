import {QueryOperator} from '../../../shared/models/query/base.field.operator';
import {OrderDirection} from '../../../shared/models/query/base.field.order-direction';
import {BaseFieldOrder} from '../../../shared/models/query/base.field.order';
import {BaseFieldFilter} from '../../../shared/models/query/base.field.filter';


export const defaultFilters = new Map([
  ['CREATED', [new BaseFieldFilter('status', QueryOperator.EQUALS, 'CREATED')]],
  ['DENIED', [new BaseFieldFilter('status', QueryOperator.EQUALS, 'DENIED')]],
  ['ACCEPTED', [new BaseFieldFilter('status', QueryOperator.EQUALS, 'ACCEPTED')]],
  ['NEW', [new BaseFieldFilter('status', QueryOperator.EQUALS, 'NEW')]],
]);

export const defaultOrder: BaseFieldOrder[] = [
  new BaseFieldOrder('id', OrderDirection.DESC)
];
