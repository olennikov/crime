import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Component} from '@angular/core';
import {TableHeaderColumn} from '../../../shared/components/table/TableHeaderColumn';
import {RegistrationApplicationView} from '../../services/registration/models/registrationapplicationview.model';
import {RegistrationApplicationDataService} from '../../services/registration/registration-application-data.service';
import {MenuItem} from '../../../shared/models/menu/menu-item';
import {RegistrationApplicationStatus} from '../../models/registration.application.statuses';
import {Query} from '../../../shared/models/query/query';
import {defaultFilters, defaultOrder} from './default.filter.const';
import {Router} from '@angular/router';
import {PaginationInfo} from '../../../shared/components/table/table-with-pagination/pagination-info';

@Component({
  selector: 'app-application-data',
  templateUrl: './application-data.component.html',
  styleUrls: ['./application-data.component.scss']
})
export class ApplicationDataComponent {

  dataSource: Observable<RegistrationApplicationView[]> = new Observable<RegistrationApplicationView[]>();
  registrationApplicationStatus: MenuItem[] = [
    {displayValue: 'Новые', routerLink: RegistrationApplicationStatus.NEW},
    {displayValue: 'Отказанные', routerLink: RegistrationApplicationStatus.DENIED},
    {displayValue: 'Ожидают завершения активации', routerLink: RegistrationApplicationStatus.ACCEPTED},
    {displayValue: 'Завершённые', routerLink: RegistrationApplicationStatus.CREATED},
  ];

  tableColumns: TableHeaderColumn[] = [
    new TableHeaderColumn('id', 'Идентификатор'),
    new TableHeaderColumn('receiveDate', 'Дата создания'),
    new TableHeaderColumn('fullName', 'ФИО'),
    new TableHeaderColumn('phone', 'Контактный телефон пользователя'),
    new TableHeaderColumn('organisationName', 'Наименование организации'),
    new TableHeaderColumn('organisationPhone', 'Контактный телефон организации')
  ];

  pageSize: number = 10;
  itemCount = 0;
  selectedMenuItem: MenuItem = this.registrationApplicationStatus[0];

  constructor(private registrationApplicationService: RegistrationApplicationDataService,
              private router: Router) {
  }

  changeSelectedExpertise(menuItem: MenuItem) {
    this.selectedMenuItem = menuItem;
    let query: Query = new Query(this.pageSize, 0,
      defaultFilters.get(this.selectedMenuItem.routerLink), defaultOrder);
    this.loadData(query);
  }

  selectedRow(dataRow: RegistrationApplicationView) {
    if (dataRow !== null) {
      this.router.navigate(['director', 'registrationApplication', `${dataRow.id}`]);
    }
  }

  loadInfo(paginationInfo: PaginationInfo) {
    this.pageSize = paginationInfo.size;
    let query: Query = new Query(paginationInfo.size, paginationInfo.size * paginationInfo.number, defaultFilters.get(this.selectedMenuItem.routerLink), defaultOrder);
    this.pageSize = paginationInfo.size;
    this.loadData(query);
  }

  loadData(query: Query) {
    this.dataSource = this.registrationApplicationService.getRegistrationApplicationList(query).pipe(
      map(data => {
        this.itemCount = data.count;
        return data.records;
      })
    );
  }
}
