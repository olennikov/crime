import {Component} from '@angular/core';
import {ExpertiseApplicationStatus} from '../../models/expertise.application.statuses';
import {TableHeaderColumn} from '../../../shared/components/table/TableHeaderColumn';
import {Router} from '@angular/router';
import {MenuItem} from '../../../shared/models/menu/menu-item';
import {ExpertiseApplicationDataService} from '../../services/expertise_application/expertise-application-data.service';
import {Query} from '../../../shared/models/query/query';
import {ExpertiseApplicationModel} from '../../services/expertise_application/models/expertise.application.model';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {defaultFilter, defaultOrder} from './default.filter.const';
import {PaginationInfo} from '../../../shared/components/table/table-with-pagination/pagination-info';


@Component({
  selector: 'app-applicaion-expertise-data',
  templateUrl: './applicaion-expertise-data.component.html',
  styleUrls: ['./applicaion-expertise-data.component.scss']
})
export class ApplicaionExpertiseDataComponent {

  pageSize: number = 10;
  itemCount = 0;

  dataSource: Observable<ExpertiseApplicationModel[]> = new Observable<ExpertiseApplicationModel[]>();

  tableColumns: TableHeaderColumn[] = [
    new TableHeaderColumn('id', 'Идентификатор'),
    new TableHeaderColumn('receiveDate', 'Дата подачи'),
    new TableHeaderColumn('expertiseName', 'Название'),
    new TableHeaderColumn('creationUserName', 'Заказчик'),
  ];

  expertiseApplicationStatus: MenuItem[] = [
    {displayValue: 'Новые', routerLink: ExpertiseApplicationStatus.NEW},
    {displayValue: 'Принятые', routerLink: ExpertiseApplicationStatus.ACCEPT},
    {displayValue: 'В работе', routerLink: ExpertiseApplicationStatus.IN_WORK},
    {displayValue: 'Отказано в проведении', routerLink: ExpertiseApplicationStatus.REJECT},
    {displayValue: 'Завершено', routerLink: ExpertiseApplicationStatus.CLOSED},
  ];

  selectedMenuItem = this.expertiseApplicationStatus[0];

  constructor(private expertiseApplicationService: ExpertiseApplicationDataService,
              private router: Router) {
  }

  selectedRow(appItem: ExpertiseApplicationModel) {
    if (appItem !== null) {
      this.router.navigate(['director', 'expertiseApplication', `${appItem.id}`]);
    }
  }

  changeStatus(menuItem: MenuItem) {
    this.selectedMenuItem = menuItem;
    let query: Query = new Query(this.pageSize, 0, defaultFilter.get(this.selectedMenuItem.routerLink), defaultOrder);
    this.loadData(query);
  }

  loadInfo(paginationInfo: PaginationInfo) {
    this.pageSize = paginationInfo.size;
    let query: Query = new Query(paginationInfo.size, paginationInfo.size * paginationInfo.number, defaultFilter.get(this.selectedMenuItem.routerLink), defaultOrder);
    this.pageSize = paginationInfo.size;
    this.loadData(query);
  }

  loadData(query: Query) {
    this.dataSource = this.expertiseApplicationService.getExpertiseApplicationList(query)
      .pipe(map(data => {
        this.itemCount = data.count;
        return data.records;
      }));
  }
}
