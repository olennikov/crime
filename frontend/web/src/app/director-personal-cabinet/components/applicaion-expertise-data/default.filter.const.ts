import {QueryOperator} from '../../../shared/models/query/base.field.operator';
import {OrderDirection} from '../../../shared/models/query/base.field.order-direction';
import {BaseFieldOrder} from '../../../shared/models/query/base.field.order';
import {BaseFieldFilter} from '../../../shared/models/query/base.field.filter';

export const defaultFilter = new Map([
  ['NEW', [new BaseFieldFilter('status', QueryOperator.EQUALS, 'NEW')]],
  ['IN_WORK', [new BaseFieldFilter('status', QueryOperator.EQUALS, 'IN_WORK')]],
  ['ACCEPT', [new BaseFieldFilter('status', QueryOperator.EQUALS, 'ACCEPT')]],
  ['REJECT', [new BaseFieldFilter('status', QueryOperator.EQUALS, 'REJECT')]],
  ['CLOSED', [new BaseFieldFilter('status', QueryOperator.EQUALS, 'CLOSED')]],
]);

export const defaultOrder: BaseFieldOrder[] = [
  new BaseFieldOrder('id', OrderDirection.DESC)
];
