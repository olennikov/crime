import {Component} from '@angular/core';
import {Observable} from 'rxjs';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {RegistrationApplicationDataService} from '../../services/registration/registration-application-data.service';
import {RejectRegistration} from '../../services/registration/models/registration-reject.model';
import {ActivatedRoute, Router} from '@angular/router';
import {InfoPopupBuilder} from '../../../shared/components/popup/info-popup/InfoPopup';
import {PopupService} from '../../../shared/services/popup/popup.service';

@Component({
  selector: 'app-registration-application-reject',
  templateUrl: './registration-application-reject.component.html',
  styleUrls: ['./registration-application-reject.component.scss']
})
export class RegistrationApplicationRejectComponent {

  registrationRejectForm: FormGroup;
  applicationId: number = 0;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private registrationApplicationService: RegistrationApplicationDataService,
              private popupService: PopupService) {
    this.activatedRoute.params.subscribe(params => {
      this.applicationId = params['id'];
      this.registrationRejectForm = this.formBuilder.group({
        id: new FormControl(params['id'], Validators.required),
        rejectReason: new FormControl(null, Validators.required)
      });
    });
  }

  reject() {
    let registrationRejectData: RejectRegistration = new RejectRegistration(this.registrationRejectForm.value.id, this.registrationRejectForm.value.rejectReason);
    let observable: Observable<number> = this.registrationApplicationService.rejectRegistrationRequest(registrationRejectData);
    observable.subscribe(data => {
        this.popupService.openInfoPopup(InfoPopupBuilder.builder()
          .text(`Заявка на регистрацию с номером ${this.applicationId} успешно отклонена`)
          .positiveHandler(() => this.router.navigate(['director', 'registrationApplication']))
          .build());
      },
      error => {
        this.popupService.openInfoPopup(InfoPopupBuilder.builder()
          .text('Произошла ошибка при отказе заявки')
          .build());
      });
  }
}
