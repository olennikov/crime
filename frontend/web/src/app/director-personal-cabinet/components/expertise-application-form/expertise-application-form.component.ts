import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ExpertiseApplicationDataService} from '../../services/expertise_application/expertise-application-data.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ExpertiseApplication} from '../../../shared/models/domain/expertise-application';
import {File} from '../../../shared/models/domain/file';
import {PopupService} from '../../../shared/services/popup/popup.service';
import {InfoPopupBuilder} from '../../../shared/components/popup/info-popup/InfoPopup';

@Component({
  selector: 'app-expertise-application-form',
  templateUrl: './expertise-application-form.component.html',
  styleUrls: ['./expertise-application-form.component.scss']
})
export class ExpertiseApplicationFormComponent implements OnInit {

  applicationForm: FormGroup;
  applicationId: number = 0;

  statusDescriptions = [
    {key: 'NEW', value: 'Ожидает обработки'},
    {key: 'ACCEPT', value: 'Принято'},
    {key: 'IN_WORK', value: 'Производится экспертиза'},
    {key: 'REJECT', value: 'Отказ по проведению экспертизы'},
    {key: 'CLOSED', value: 'Выполнена'}
  ];


  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private activateRoute: ActivatedRoute,
              private expertiseApplicationService: ExpertiseApplicationDataService,
              private popupService: PopupService) {
    this.applicationForm = this.formBuilder.group({
      name: new FormControl(null),
      description: new FormControl(null),
      receiveDate: new FormControl(null),
      status: new FormControl(null),
      expertiseFiles: new FormControl([]),
    });
  }

  rejectClick() {
    this.router.navigate(['director', 'expertiseApplication', 'reject', `${this.applicationId}`]);
  }

  acceptClick() {
    let observable: Observable<string> = this.expertiseApplicationService.acceptExpertiseApplication(this.applicationId);
    observable.subscribe(data => {
      this.applicationForm.patchValue({status: data});
    });
  }

  inWorkClick() {
    this.expertiseApplicationService.inWorkExpertiseApplication(this.applicationId).subscribe(data => {
      this.applicationForm.patchValue({status: data});
      this.nextStep();
    });
  }

  nextStep() {
    this.router.navigate(['director', 'expertiseApplication', 'create-expertise', `${this.applicationId}`]);
  }

  ngOnInit() {
    this.activateRoute.params.subscribe(params => {
      let id: number = params['id'];
      this.applicationId = id;
      let observable: Observable<ExpertiseApplication> = this.expertiseApplicationService.getExpertiseApplicattionById(id);
      observable.subscribe((data: ExpertiseApplication) => {
        this.applicationForm.patchValue({
          receiveDate: new Date(data.receiveDate),
          name: data.name,
          description: data.description,
          status: data.status,
          expertiseFiles: data.expertiseFiles
        });
      });
    });
  }

  deleteFile(file: File): void {
    this.expertiseApplicationService.deleteFile(this.applicationId, file.id)
      .subscribe(data => {
        this.popupService.openInfoPopup(InfoPopupBuilder.builder()
          .positiveHandler(() => {
            let files = <File[]>this.applicationForm.get('expertiseFiles').value;
            const indexOfFile = files.indexOf(file);
            files.splice(indexOfFile, 1);
          })
          .text('Файл успешно удален')
          .build());
      });
  }
}
