import {Component} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ExpertiseApplicationDataService} from '../../services/expertise_application/expertise-application-data.service';
import {Observable} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {InfoPopupBuilder} from '../../../shared/components/popup/info-popup/InfoPopup';
import {PopupService} from '../../../shared/services/popup/popup.service';

@Component({
  selector: 'app-expertise-application-reject-form',
  templateUrl: './expertise-application-reject-form.component.html',
  styleUrls: ['./expertise-application-reject-form.component.scss']
})
export class ExpertiseApplicationRejectFormComponent {

  rejectForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private activateRoute: ActivatedRoute,
              private router: Router,
              private expertiseApplicationService: ExpertiseApplicationDataService,
              private popupService: PopupService) {
    this.activateRoute.params.subscribe(params => {
      this.rejectForm = this.formBuilder.group({
        id: new FormControl(params['id'], Validators.required),
        rejectReason: new FormControl(null, Validators.required),
      });
    });

  }

  reject() {
    let observable: Observable<number> = this.expertiseApplicationService.rejectExpertiseApplication(this.rejectForm.value);
    observable.subscribe(data => {
        this.popupService.openInfoPopup(InfoPopupBuilder.builder()
          .text(`По заявлению на проведение экспертизы ${data} произведён отказ`)
          .positiveHandler(() => this.router.navigate(['director', 'expertiseApplication']))
          .build());
      },
      error => {
        this.popupService.openInfoPopup(InfoPopupBuilder.builder()
          .text('Произошла ошибка при проведении отказа')
          .build());
      });
  }
}
