import { Component, OnInit, ViewChild, ComponentFactoryResolver, ViewContainerRef} from '@angular/core';
import { Observable } from 'rxjs';
import { FormArray, FormGroup, FormBuilder, ReactiveFormsModule, NgForm, FormControl, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ExpertiseModel } from '../../services/expertise/models/expertise.model';
import { ExpertiseDataService } from '../../services/expertise/expertise-data.service';
import {InfoPopupBuilder} from '../../../shared/components/popup/info-popup/InfoPopup';
import {PopupService} from '../../../shared/services/popup/popup.service';
import {TableWithPaginationComponent} from '../../../shared/components/table/table-with-pagination/table-with-pagination.component';
import {TableHeaderColumn} from '../../../shared/components/table/TableHeaderColumn';
import {ExpertiseStatus, ExpertiseStatusDisplayValue} from '../../../shared/models/domain/expertise-status';
import {TaskStatus, TaskStatusDisplayValue} from '../../../shared/models/domain/task-status';
import { TaskModel } from '../../models/task.model'

@Component({
  selector: 'app-create-expertise-object',
  templateUrl: './create-expertise-object.component.html',
  styleUrls: ['./create-expertise-object.component.scss']
})
export class CreateExpertiseObjectComponent implements OnInit {

  statusDescription = (()=>{
    var statusArray = [];
    ExpertiseStatusDisplayValue.forEach((value,key)=>{
      var statusKeyValuePair = {'key': key, 'value': value};
      statusArray.push(statusKeyValuePair);
    })
    return statusArray;
  })();

  taskStatuses = (()=>{
                       var displayArray = [];
                       TaskStatusDisplayValue.forEach((value,key)=>displayArray.push({'key': key, 'value':value}))
                       return displayArray;
                   })();

  createExpertiseForm: FormGroup;
  applicationId: number = 0;
  expertiseId: number = 0;
  countRecord: number = 0;

  dataSource: TaskModel[] = [];
  columns: TableHeaderColumn [] = [
      new TableHeaderColumn("id","Идентификатор"),
      new TableHeaderColumn("name","Название"),
      new TableHeaderColumn("taskStatus", "Статус",(status: string) => this.taskStatuses.find((item)=>item.key==status).value),
      new TableHeaderColumn("progress", "Выполненные подзадачи")
  ];

  constructor(private formBuilder: FormBuilder,
              private componentFactory: ComponentFactoryResolver,
              private expertiseService: ExpertiseDataService,
              private activateRoute: ActivatedRoute,
              private router: Router,
              private popupService: PopupService) {
    this.createExpertiseForm = this.formBuilder.group({
      id: new FormControl(null),
      status: new FormControl(null),
      name: new FormControl(null),
      description: new FormControl(null),
    })
    activateRoute.params.subscribe(params=>{
      this.applicationId = params['id'];
      let observable: Observable<ExpertiseModel> = this.expertiseService.getExpertiseByApplicationId(this.applicationId);
      observable.subscribe(data=>{
        if (data!=null){
          this.expertiseId = data.id;
          this.createExpertiseForm.patchValue(data);
          this.dataSource = data.taskArray;
          this.countRecord = data.taskArray.length;
        }
      })
    });
  }

   addTaskClick(){
    this.router.navigate(['director','expertiseApplication','task',this.createExpertiseForm.controls['id'].value]);
  }

  loadTask(selectedTask: TaskModel){
    this.router.navigate(['director','expertiseApplication','task',this.createExpertiseForm.controls['id'].value,selectedTask.id]);
  }


  save () {
    let expertiseObject: ExpertiseModel = this.createExpertiseForm.value;
    expertiseObject.expertiseApplicationId = this.applicationId;
    let observable: Observable<number> = this.expertiseService.createExpertise(expertiseObject);
    observable.subscribe(data=>{
      this.createExpertiseForm.controls['id'].setValue(data);
      this.createExpertiseForm.controls['status'].setValue(ExpertiseStatus.NEW);
      this.popupService.openInfoPopup(InfoPopupBuilder.builder()
        .text(`Объект экспертизы сохранён с номером ${data}`)
        .build());
    });
  }


  setInWork (){
    this.expertiseService.setInworkExpertise(this.expertiseId).subscribe(data=>{
      this.createExpertiseForm.patchValue({status: data})
    })
  }

  previous (){
    this.router.navigate(['director','expertiseApplication',`${this.applicationId}`])
  }

  next () {
    this.router.navigate(['director','expertiseApplication','result',`${this.applicationId}`])
  }


  ngOnInit() {

  }
}
