export class TaskModel{
  constructor(public id: number,
              public name: string,
              public deadlineDate: Date,
              public taskStatus: string,
              public description: string,
              public expertiseId: number,
              public responsibleUserId: number){}

}
