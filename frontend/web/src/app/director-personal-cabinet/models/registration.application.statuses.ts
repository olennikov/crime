export enum RegistrationApplicationStatus{
  NEW = 'NEW',
  ACCEPTED = 'ACCEPTED',
  CREATED = 'CREATED',
  DENIED = 'DENIED'
}
