export enum ExpertiseApplicationStatus{
  NEW = 'NEW',
  IN_WORK = 'IN_WORK',
  ACCEPT = 'ACCEPT',
  REJECT = 'REJECT',
  CLOSED = 'CLOSED'
}
