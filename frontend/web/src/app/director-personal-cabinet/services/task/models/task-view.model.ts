import { SubTaskModel } from './subtask.model';
import {File} from '../../../../shared/models/domain/file';

export class TaskDataView{
  constructor (public id: number = null,
               public name: string = null,
               public description: string = null,
               public deadlineDate: Date = null,
               public subTaskArray: SubTaskModel[] = [],
               public taskFiles: File[] = []){}
}
