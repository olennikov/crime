import {File} from '../../../../shared/models/domain/file';

export class SubTaskModel{
  constructor(public id: number,
              public name: string,
              public description: string,
              public deadlineDate: Date,
              public taskId: number,
              public comment: string,
              public isClosed: boolean,
              public files: File[]){}

}
