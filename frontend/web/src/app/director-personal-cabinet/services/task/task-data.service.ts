import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from '../../../core/services/http/http.service';
import { TaskModel } from '../../models/task.model';
import { TaskDataView } from './models/task-view.model';
import { Urls } from '../../../../app/core/urls/url';
import {SubTaskModel} from './models/subtask.model';



@Injectable({
  providedIn: 'root'
})
export class TaskDataService {

  constructor(private httpService: HttpService) { }

  getTaskByExpertise (expertiseId: number): Observable<TaskModel[]>{
      return this.httpService.getObservable(Urls.getTaskByExpertise(expertiseId));
  }

  getTaskById (id: number): Observable<TaskDataView> {
    return this.httpService.getObservable(Urls.getDirectorTaskById(id));
  }

  saveSubTask (view: TaskDataView): Observable<number>{
    return this.httpService.putObservable(Urls.saveDirectorSubTaskUrl(), view);
  }

  createTask (taskModel: TaskModel): Observable<number>{
    return this.httpService.postObservable(Urls.createTaskUrl(),taskModel);
  }

  createSubTask (subTaskModel: SubTaskModel) : Observable<SubTaskModel>{
    return this.httpService.postObservable(Urls.saveOrDeleteSubtask(),subTaskModel);
  }
}
