import {File} from '../../../../shared/models/domain/file';
import {ExpertiseApplicationStatus} from '../../../../shared/models/domain/expertise-application-status';

export class ExpertiseApplicationResult{
  constructor (public id: number,
               public resultDescription: string,
               public applicationStatus: ExpertiseApplicationStatus,
               public fileDtoList: File []){}

}
