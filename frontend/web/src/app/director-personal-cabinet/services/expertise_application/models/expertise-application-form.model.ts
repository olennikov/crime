export class ExpertiseApplicationFormModel{
  constructor(  public id: number,
                public name: string,
                public description: string,
                public receiveDate: Date,
                public status: string,
                ){}
}
