export class ExpertiseApplicationModel {

  constructor(public id: number,
              public receiveDate: Date,
              public expertiseName: string,
              public creationUserName: string){}

}
