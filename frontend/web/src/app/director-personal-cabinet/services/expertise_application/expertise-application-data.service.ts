import {Injectable} from '@angular/core';
import {ExpertiseApplicationModel} from './models/expertise.application.model';
import {HttpService} from '../../../core/services/http/http.service';
import {Observable} from 'rxjs';
import {Query} from '../../../shared/models/query/query';
import {PaginationResponse} from '../../../shared/models/query/pagination.response';
import {Urls} from '../../../../app/core/urls/url';
import {ExpertiseApplicationReject} from './models/expertise-application-reject.model';
import {ExpertiseApplicationResult} from './models/expertise-application-result.model';
import {HttpHeaders} from '@angular/common/http';
import {ExpertiseApplication} from '../../../shared/models/domain/expertise-application';

@Injectable({
  providedIn: 'root'
})
export class ExpertiseApplicationDataService {

  constructor(private httpService: HttpService) {
  }

  getExpertiseApplicationList(query: Query): Observable<PaginationResponse<ExpertiseApplicationModel>> {
    return this.httpService.postObservable(Urls.expertiseApplicationListDirectorUrl(), query);
  }

  getExpertiseApplicattionById(id: number): Observable<ExpertiseApplication> {
    return this.httpService.getObservable(Urls.expertiseApplicationByIdUrl(id));
  }

  rejectExpertiseApplication(reject: ExpertiseApplicationReject): Observable<number> {
    return this.httpService.postObservable(Urls.expertiseApplicationReject(), reject);
  }

  acceptExpertiseApplication(applicationId: number): Observable<string> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.httpService.postObservable(Urls.acceptExpertiseApplication(), applicationId, {headers: headers});
  }

  inWorkExpertiseApplication(applicationId: number): Observable<string> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.httpService.postObservable(Urls.inWorkExpertiseApplication(), applicationId, {headers: headers});
  }

  updateExpertiseResult(applicationResult: ExpertiseApplicationResult): Observable<number> {
    return this.httpService.putObservable(Urls.updateExpertiseApplicationresultUrl(), applicationResult);
  }

  getApplicationResult(applicationId: number): Observable<ExpertiseApplicationResult> {
    return this.httpService.getObservable(Urls.getExpertiseApplicationResult(applicationId));
  }

  closeExpertiseApplication (applicationId: number): Observable<string>{
    return this.httpService.postObservable(Urls.closeExpertiseApplicationUrl(applicationId))
  }

  deleteFile(applicationId, fileId: number) {
    return this.httpService.deleteObservable(Urls.deleteExpertiseApplicationFile(applicationId, fileId));
  }

}
