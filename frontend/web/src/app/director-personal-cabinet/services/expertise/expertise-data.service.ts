import {Injectable} from '@angular/core';
import {HttpService} from '../../../core/services/http/http.service';
import {Urls} from '../../../../app/core/urls/url';
import {Observable} from 'rxjs';
import {ExpertiseModel} from './models/expertise.model';
import {UserInfo} from '../../../shared/models/domain/user-info';

@Injectable({
  providedIn: 'root'
})
export class ExpertiseDataService {

  constructor(private httpService: HttpService) {
  }

  createExpertise(expertise: ExpertiseModel): Observable<number> {
    return this.httpService.putObservable(Urls.expertiseCreate(), expertise);
  }



  getExpertiseByApplicationId(applicationId: number): Observable<ExpertiseModel> {
    return this.httpService.getObservable(Urls.getDirectorExpertiseByApplication(applicationId));
  }

  setInworkExpertise(expertiseId: number): Observable<string> {
    return this.httpService.postObservable(Urls.InWorkExpertiseUrl(expertiseId));
  }

}
