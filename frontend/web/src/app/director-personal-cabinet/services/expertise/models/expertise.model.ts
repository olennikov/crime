import { TaskModel } from '../../../models/task.model'

export class ExpertiseModel {

  constructor(public id: number,
              public name: string,
              public description: string,
              public expertiseApplicationId: number,
              public status: string,
              public taskArray: TaskModel[]){}
}
