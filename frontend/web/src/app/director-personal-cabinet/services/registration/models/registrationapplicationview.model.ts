export class RegistrationApplicationView{
  id: number;
  receiveDate: Date;
  fullName: string;
  phone: String;
  organisationName: string;
  organisationPhone: string;
}
