export class RegistrationApplicationModel{
  constructor(
    public id: number,
    public status: string,
    public lastName: string,
    public firstName: string,
    public secondName: string,
    public phone: string,
    public email: string,
    public organisationName: string,
    public organisationPhone: string,
    public organisationInn: string,
    public organisationOgrn: string,
    public rejectReason: string){}

}
