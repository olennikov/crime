export class RejectRegistration{
  constructor(public applicationId: number,
              public rejectComment: string){}

}
