import {Injectable} from '@angular/core';
import {HttpService} from '../../../core/services/http/http.service';
import {Observable} from 'rxjs';
import {Query} from '../../../shared/models/query/query';
import {PaginationResponse} from '../../../shared/models/query/pagination.response';
import {Urls} from '../../../../app/core/urls/url';
import {RegistrationApplicationView} from './models/registrationapplicationview.model';
import {RegistrationApplicationModel} from './models/registration-application.model';
import {RejectRegistration} from './models/registration-reject.model';
import {HttpHeaders} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class RegistrationApplicationDataService {

  constructor(private httpService: HttpService) {
  }

  getRegistrationApplicationList(query: Query): Observable<PaginationResponse<RegistrationApplicationView>> {
    return this.httpService.postObservable(Urls.registrationApplicationList(), query);;
  }

  getRegistrationApplicationById(id: number): Observable<RegistrationApplicationModel> {
    let observable: Observable<RegistrationApplicationModel> = this.httpService.getObservable(Urls.registrationApplicatopnByIdUrl(id));
    return observable;
  }

  acceptRegistrationRequest(id: number): Observable<number> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    let observable: Observable<number> = this.httpService.postObservable(Urls.registrationAcceptedUrl(), id, {headers: headers});
    return observable;
  }

  rejectRegistrationRequest(reject: RejectRegistration): Observable<number> {
    let observable: Observable<number> = this.httpService.postObservable(Urls.registrationRejectedUrl(), reject);
    return observable;
  }
}
