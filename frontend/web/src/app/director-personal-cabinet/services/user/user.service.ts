import { Injectable } from '@angular/core';
import {UserInfo} from '../../../shared/models/domain/user-info';
import {HttpService} from '../../../core/services/http/http.service';
import {Urls} from '../../../../app/core/urls/url';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpService: HttpService) { }

    getEmployeeUsers(): Observable<UserInfo[]> {
      return this.httpService.getObservable(Urls.getEmployeeUsers());
    }
}
