import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './guards/auth.guard';
import {PageNotFoundComponent} from '../page-not-found/page-not-found.component';
import {LabInfoComponent} from '../components/lab-info/lab-info.component';
import {RegistrationComponent} from '../components/registration/registration.component';
import {AuthorisationComponent} from '../components/authorisation/authorisation.component';
import {ProfileComponent} from '../components/profile/profile.component';
import {EditProfileComponent} from '../components/edit-profile/edit-profile.component';
import {SystemRoutingComponent} from '../components/system/system.routing.component';
import {CreateUserRegistrationComponent} from '../components/create-user-registration/create-user-registration.component';
import {Urls} from '../core/urls/url';
import {ForbiddenComponent} from '../forbidden/forbidden.component';

const routes: Routes = [
  {path: '', redirectTo: 'main', pathMatch: 'full'},
  {path: 'main', component: LabInfoComponent},
  {path: 'registration', component: RegistrationComponent},
  {path: 'system', component: SystemRoutingComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'edit-profile', component: EditProfileComponent},
  {path: Urls.loginPageUrl(), component: AuthorisationComponent},
  {path: 'createuser', component: CreateUserRegistrationComponent},
  {
    path: 'director',
    loadChildren: 'app/director-personal-cabinet/director-personal-cabinet.module#DirectorPersonalCabinetModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'employee',
    loadChildren: 'app/employee-personal-cabinet/employee-personal-cabinet.module#EmployeePersonalCabinetModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'client',
    loadChildren: 'app/client-personal-cabinet/client-personal-cabinet.module#ClientPersonalCabinetModule',
    canActivate: [AuthGuard]
  },
  {path: 'forbidden', component: ForbiddenComponent},
  {path: '**', component: PageNotFoundComponent}
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload', preloadingStrategy: PreloadAllModules})
  ],
  providers: [AuthGuard],
  declarations: []
})
export class AppRoutingModule {
}
