import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable, of} from 'rxjs';
import {AuthorisationService} from '../../services/authorisation/authorisation.service';
import {UserModel} from '../../services/authorisation/model/user.model';
import {urlsNavigate} from '../../components/system/url.redirect';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authorisationService: AuthorisationService) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let observable: Observable<UserModel> = this.authorisationService.getUserInfo();
    return observable.pipe(
      map((data: UserModel) => {
        return this.acceptComponent(data, next);
      }),
      catchError(err => {
        console.error(err);
        return of(false);
      })
    );
  }

  acceptComponent(userModel: UserModel, route: ActivatedRouteSnapshot): boolean {
    let appPath: string = urlsNavigate.get(userModel.roles[0]);
    return appPath.replace('/', '') === route.routeConfig.path;
  }

}
