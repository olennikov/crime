export class RegistrationUserModel {
  userLogin: string;
  userPassword: string;
  userSalt: string;
  changePasswordUrl: string;
}
