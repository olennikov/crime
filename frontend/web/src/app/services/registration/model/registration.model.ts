export class RegistrationModel {
  lastName: string;
  firstName: string;
  secondName: string;
  phone: string;
  email: string;
  organisationName: string;
  organisationPhone: string;
  organisationInn: string;
  organisationOgrn: string;
}
