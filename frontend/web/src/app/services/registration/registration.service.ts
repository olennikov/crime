import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpService} from '../../core/services/http/http.service';
import {Urls} from '../../core/urls/url';
import {RegistrationModel} from './model/registration.model';
import {RegistrationUserModel} from './model/registration.user.model';

@Injectable()
export class RegistrationService {

  constructor(private httpService: HttpService) {

  }

  createRegistrationApplication(model: RegistrationModel): Observable<number> {
    return this.httpService.putObservable(Urls.registrationUrl(), model);
  }

  getAcceptedApplicationByDisposableUrl(disposableUrl: string): Observable<RegistrationModel> {
    return this.httpService.getObservable(Urls.registrationModelUrl(disposableUrl));
  }

  createUser(registrationUserModel: RegistrationUserModel): Observable<number> {
    return this.httpService.postObservable(Urls.registrationGenerateUserUrl(), registrationUserModel);
  }
}

