import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpService} from '../../core/services/http/http.service';
import {Urls} from '../../core/urls/url';
import {RoundLoginModel} from './model/round.salt.model';
import {UserAuthorisation} from './model/user.authorisation.model';
import {UserModel} from './model/user.model';
import {Router} from '@angular/router';

@Injectable()
export class AuthorisationService {

  loginSubject: BehaviorSubject<UserModel> = new BehaviorSubject<UserModel>(null);
  logoutSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);

  constructor(private httpService: HttpService,
              private router: Router) {
  }

  getRoundSalt(userLogin: string): Observable<RoundLoginModel> {
    return this.httpService.getObservable(Urls.roundSaltUrl(userLogin));
  }

  loginUser(authUser: UserAuthorisation): Observable<string> {
    return this.httpService.postObservable(Urls.loginUrl(), authUser);
  }

  logout() {
    this.httpService.postObservable(Urls.logoutUrl())
      .subscribe(data => {
        this.logoutSubject.next(true);
        this.router.navigateByUrl('/authorisation').then(r => console.debug('logout success'));
      });
  }

  getUserInfo(): Observable<UserModel> {
    let observable: Observable<UserModel> = this.httpService.getObservable(Urls.userInfoUrl());
    observable.subscribe(data => this.loginSubject.next(data));
    return observable;
  }
}
