export class UserAuthorisation {
  constructor(public login: string,
              public password: string,
              public roundSalt: string) {

  }
}
