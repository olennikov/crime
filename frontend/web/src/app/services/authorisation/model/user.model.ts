import {UserInfo} from '../../../shared/models/domain/user-info';

export class UserModel extends UserInfo {
  name: string;
  roles: string[];
}
