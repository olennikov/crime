export class ChangePassword{
  constructor(public newSalt: string,
              public newPassword: string,
              public oldPassword: string,
              public roundSalt: string){}

}
