export class UserProfile {
  constructor(public lastName: string,
              public firstName: string,
              public secondName: string,
              public email: string,
              public phone: string,
              public organisationName: string,
              public organisationPhone: string) {

  }
}
