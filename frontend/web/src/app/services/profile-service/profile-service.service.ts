import { Injectable } from '@angular/core';
import {HttpService} from '../../core/services/http/http.service';
import {Observable} from 'rxjs';
import {Urls} from '../../core/urls/url';
import {UserProfile} from './model/user-profile';
import {ChangePassword} from './model/change-password';
import {RoundSalt} from './model/round-salt';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private httpService: HttpService) { }

  public loadUserProfile (): Observable<UserProfile> {
      return this.httpService.getObservable(Urls.getProfileUser());
  }

  public editUserProfile (userProfile: UserProfile): Observable<number>{
      return this.httpService.postObservable(Urls.editProfileUser(),userProfile)
  }

  public roundSalt(): Observable<RoundSalt> {
      return this.httpService.getObservable(Urls.getRoundSaltContext());
  }

  public editUserPassword(changePassword: ChangePassword): Observable<number>{
      return this.httpService.postObservable(Urls.editUserPassword(),changePassword);
  }
}
