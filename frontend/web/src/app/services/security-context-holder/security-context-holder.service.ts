import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {UserModel} from '../authorisation/model/user.model';
import {UserContext} from './user-context';
import {AuthorisationService} from '../authorisation/authorisation.service';

@Injectable({
  providedIn: 'root'
})
export class SecurityContextHolderService {

  userContext: BehaviorSubject<UserContext> = new BehaviorSubject<UserContext>(null);

  constructor(private authorisationService: AuthorisationService) {
    authorisationService.loginSubject.subscribe(data => {
      this.successfullyAuthentication(data);
    });

    authorisationService.logoutSubject.subscribe(isLogout => {
      if (isLogout) {
        this.clearContext();
      }
    });
  }

  successfullyAuthentication(userModel: UserModel) {
    this.userContext.next(new UserContext(true, userModel));
  }

  clearContext() {
    this.userContext.next(new UserContext(false, null));
  }
}


