import {UserModel} from '../authorisation/model/user.model';

export class UserContext {
  constructor(public isLogged: boolean,
              public currentUser: UserModel) {

  }
}
