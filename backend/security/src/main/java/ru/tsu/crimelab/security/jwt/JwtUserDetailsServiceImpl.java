package ru.tsu.crimelab.security.jwt;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.tsu.crimelab.security.CrimeLabUser;
import ru.tsu.crimelab.security.jwt.session.SessionStorage;
import ru.tsu.crimelab.security.jwt.utils.Constants;
import ru.tsu.crimelab.security.jwt.utils.UserUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.springframework.util.StringUtils.isEmpty;
import static ru.tsu.crimelab.security.jwt.utils.Constants.SESSION_ID;
import static ru.tsu.crimelab.security.jwt.utils.Constants.USER_INFO;

/**
 * Created by PetrushinDA on 13.10.2019
 * Сервис отвечает за загрузку информации о пользователе из JWT токена
 */
@RequiredArgsConstructor
public class JwtUserDetailsServiceImpl implements JwtUserDetailsService {

    private final SessionStorage sessionStorage;

    private static List<GrantedAuthority> makeAuthorities(List<String> roles) {
        return new ArrayList<>(toGrantedAuthorities(roles, "ROLE_"));
    }

    private static List<GrantedAuthority> toGrantedAuthorities(List<String> list, String prefix) {
        return list.stream()
                .map(i -> new CrimelabGrantedAuthority(prefix + i))
                .collect(Collectors.toList());
    }

    @Override
    public String getSupportedIssuer() {
        return Constants.ISSUER;
    }

    @Override
    public JwtUserDetails loadUserByPayload(JwtPayload payload) throws UsernameNotFoundException {
        String id = payload.getId();
        String sessionId = payload.getDetails().get(SESSION_ID);
        boolean nonExpired = sessionStorage.nonExpired(id, sessionId);
        String serializedUserInfo = payload.getDetails().getOrDefault(USER_INFO, "");

        CrimeLabUser crimeLabUser = UserUtils.fromJsonString(serializedUserInfo, CrimeLabUser.class);

        if (isEmpty(serializedUserInfo)) {
            return null;
        }
        return new JwtUserDetails(
                id,
                sessionId,
                payload.getSubject(),
                payload.getIssuer(),
                payload.getDetails(),
                nonExpired,
                makeAuthorities(crimeLabUser.getRoles()),
                !Objects.requireNonNullElse(crimeLabUser.getIsBlock(), true)
        );
    }

    static class CrimelabGrantedAuthority implements GrantedAuthority {
        private final String name;

        CrimelabGrantedAuthority(String name) {
            this.name = name;
        }

        @Override
        public String getAuthority() {
            return name;
        }
    }
}

