package ru.tsu.crimelab.security.jwt.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.lang.NonNull;
import ru.tsu.crimelab.security.CrimeLabUser;

/**
 * Created by PetrushinDA on 16.10.2019
 */
public class UserUtils {
    private static final ObjectMapper mapper = new ObjectMapper();

    @SneakyThrows
    public static <T> T fromJsonString(@NonNull String json, Class<T> clazz) {
        return mapper.readValue(json, clazz);
    }

    @SneakyThrows
    public static String toStringUser(CrimeLabUser crimeLabUser) {
        return mapper.writeValueAsString(crimeLabUser);
    }
}
