package ru.tsu.crimelab.security.jwt.utils;

/**
 * Created by PetrushinDA on 13.10.2019
 */
public class Constants {
    public static final String ISSUER = "CrimeLab_ISSUER";
    public static final String USER_INFO = "USER_INFO";
    public static final String SESSION_ID = "SESSION_ID";
}
