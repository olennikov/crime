package ru.tsu.crimelab.security.jwt;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import ru.tsu.crimelab.security.jwt.exception.JwtServiceException;
import ru.tsu.crimelab.security.jwt.session.SessionStorage;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.Map;

/**
 * Created by PetrushinDA on 13.10.2019
 * Собственная реализация AuthenticationProvider, которую необходимо подключить для корректной работы аутентификации
 */
@Slf4j
public class JwtAuthenticationProvider implements AuthenticationProvider {
    private final static AccountStatusUserDetailsChecker CHECKER = new AccountStatusUserDetailsChecker();
    private final Map<String, JwtUserDetailsServiceImpl> userDetailsServiceMap;
    private final JwtUserDetailsServiceImpl defaultUserDetailsService;
    private final JwtService jwtService;
    private final Duration expiration;
    private final Clock clock;


    public JwtAuthenticationProvider(Map<String, JwtUserDetailsServiceImpl> userDetailsServiceMap,
                                     Duration expiration,
                                     JwtService jwtService,
                                     SessionStorage sessionStorage) {
        this(userDetailsServiceMap, new JwtUserDetailsServiceImpl(sessionStorage), jwtService, expiration, Clock.systemUTC());
    }

    JwtAuthenticationProvider(Map<String, JwtUserDetailsServiceImpl> userDetailsServiceMap,
                              JwtUserDetailsServiceImpl defaultUserDetailsService,
                              JwtService jwtService,
                              Duration expiration,
                              Clock clock) {
        this.userDetailsServiceMap = userDetailsServiceMap;
        this.defaultUserDetailsService = defaultUserDetailsService;
        this.jwtService = jwtService;
        this.expiration = expiration;
        this.clock = clock;
    }

    /**
     * Метод аутентификации пользователя
     *
     * @param authentication текущий объект Authentication
     * @return объект Authentication аутентифицируемого пользователя
     * @throws AuthenticationException возникшая ошибка аутентификации
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            Assert.isInstanceOf(JwtPreAuthenticationToken.class, authentication,
                    "Authentication must be instance of JwtPreAuthenticationToken");

            JwtPayload payload = getPayload(authentication);
            ensureThatJwtIsNotExpired(payload);

            JwtUserDetailsServiceImpl userDetailsService = getJwtUserDetailsService(payload.getIssuer());
            JwtUserDetails userDetails = userDetailsService.loadUserByPayload(payload);

            if (userDetails == null) {
                return null;
            }

            CHECKER.check(userDetails);

            return new JwtAuthenticationToken(userDetails, jwtService.encodeJwt(payload.renew(clock.instant())));
        } catch (AuthenticationException e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    /**
     * Получение payload из объекта Authentication
     *
     * @param authentication объект Authentication для текущего пользователя
     * @return объект JwtPayload
     */
    private JwtPayload getPayload(Authentication authentication) {
        try {
            String jwt = getJwt(authentication);
            return jwtService.decodeJwt(jwt);
        } catch (JwtServiceException e) {
            throw new BadCredentialsException(e.getMessage());
        }
    }

    /**
     * Получение Jwt токен из объекта Authentication
     *
     * @param authentication объект Authentication для текущего пользоватле
     * @return Jwt токен
     */
    private String getJwt(Authentication authentication) {
        String jwt = (String) authentication.getCredentials();
        if (StringUtils.isEmpty(jwt)) {
            throw new BadCredentialsException("JWT string is empty");
        }
        return jwt;
    }

    /**
     * Проверка истек ли JWT
     *
     * @param payload payload JWT token
     */
    private void ensureThatJwtIsNotExpired(JwtPayload payload) {
        Instant issuedAt = payload.getIssuedAt();
        Instant expiredAt = issuedAt.plus(expiration);

        Instant now = clock.instant();
        if (now.isAfter(expiredAt)) {
            throw new CredentialsExpiredException("User credentials have expired");
        }
    }

    /**
     * Получение JwtUserDetailsServiceImpl по имени issuerName
     *
     * @param issuerName подписатель
     * @return экземпляр JwtUserDetailsServiceImpl из userDetailsServiceMap
     */
    private JwtUserDetailsServiceImpl getJwtUserDetailsService(String issuerName) {
        JwtUserDetailsServiceImpl userDetailsService =
                userDetailsServiceMap.getOrDefault(issuerName, defaultUserDetailsService);

        if (userDetailsService == null) {
            throw new InternalAuthenticationServiceException("Failed to detect JwtOverThingUserDetailsService");
        }

        return userDetailsService;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return JwtPreAuthenticationToken.class.isAssignableFrom(authentication);
    }
}

