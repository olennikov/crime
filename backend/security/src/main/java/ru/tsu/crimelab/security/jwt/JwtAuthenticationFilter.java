package ru.tsu.crimelab.security.jwt;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.Assert;
import org.springframework.web.util.WebUtils;
import ru.tsu.crimelab.security.jwt.config.JwtAuthenticationConfig;

import javax.servlet.FilterChain;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.html.Option;

import java.util.Optional;

import static ru.tsu.crimelab.security.jwt.utils.CookieUtil.renewCookie;

/**
 * Created by PetrushinDA on 13.10.2019
 * Реализация фильтра обработки запросов HTTP-аутентификации
 */
@Slf4j
public class JwtAuthenticationFilter extends AbstractAuthenticationProcessingFilter {
    private final static String PATH_MATCHER = "/**"; //Шаблон запросов, на которые необходимо проверять аутентификацию
    private final JwtAuthenticationConfig authenticationConfig;

    public JwtAuthenticationFilter(JwtAuthenticationConfig authenticationConfig) {
        super(new AntPathRequestMatcher(PATH_MATCHER));
        this.authenticationConfig = authenticationConfig;
    }

    @Override
    protected boolean requiresAuthentication(HttpServletRequest request, HttpServletResponse response) {
        String authenticationToken = Optional.ofNullable(WebUtils.getCookie(request, authenticationConfig.getName()))
                .map(Cookie::getValue)
                .or(() -> Optional.ofNullable(request.getHeader(authenticationConfig.getName())))
                .orElse(null);
        return authenticationToken != null && !authenticationToken.trim().isEmpty() && super.requiresAuthentication(request, response);
    }

    /**
     * Проверка валидности пользователя(срабатывает, в случае если  URL запроса соответствует шаблону {@value #PATH_MATCHER}
     *
     * @param request  объект запроса
     * @param response объект ответа
     * @return объект Authentication
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        String jwtToken = Optional.ofNullable(WebUtils.getCookie(request, authenticationConfig.getName()))
                .map(Cookie::getValue)
                .or(() -> Optional.ofNullable(request.getHeader(authenticationConfig.getName())))
                .orElse(null);
        JwtPreAuthenticationToken token = new JwtPreAuthenticationToken(jwtToken);
        return getAuthenticationManager().authenticate(token);
    }


    /**
     * Метод успешной аутентификации (срабатывает, в случае если метод attemptAuthentication вернул объект Authentication)
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                            FilterChain chain, Authentication authResult) {
        try {
            Assert.isInstanceOf(JwtAuthenticationToken.class, authResult,
                    "JwtAuthenticationProvider must create JwtAuthenticationToken");

            String updatedJwt = ((JwtAuthenticationToken) authResult).getUpdatedJwt();

            Cookie cookie = WebUtils.getCookie(request, authenticationConfig.getName());
            log.debug("Successful authentication of principal '{}': update JWT {} -> {}",
                    authResult.getPrincipal(), cookie != null ? cookie.getValue() : null, updatedJwt);
            renewCookie(cookie, updatedJwt, response, authenticationConfig);

            SecurityContextHolder.getContext().setAuthentication(authResult);

            if (this.eventPublisher != null) {
                eventPublisher.publishEvent(new InteractiveAuthenticationSuccessEvent(authResult, this.getClass()));
            }


            chain.doFilter(request, response);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new AuthenticationServiceException(e.getMessage(), e);
        }
    }

    /**
     * Метод неуспешной аутентификации (срабатывает, в случае если метод attemptAuthentication <b>не</b> вернул объект Authentication)
     */
    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) {
        try {
            Cookie cookie = WebUtils.getCookie(request, authenticationConfig.getName());
            log.warn("Unsuccessful authentication: remove JWT {}",
                    cookie != null ? cookie.getValue() : null);
            if (cookie != null) {
                renewCookie(cookie, "", response, authenticationConfig);
            }

            super.unsuccessfulAuthentication(request, response, failed);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new AuthenticationServiceException(e.getMessage(), e);
        }
    }
}

