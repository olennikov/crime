package ru.tsu.crimelab.security.jwt;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.Instant;
import java.util.Map;

/**
 * Created by PetrushinDA on 13.10.2019
 * Payload Jwt token
 */
@Data
@Builder
@RequiredArgsConstructor
public class JwtPayload {
    private final String id;
    private final String subject;
    private final String issuer;
    private final Instant issuedAt;
    private final Map<String, String> details;

    public JwtPayload renew(Instant issuedAt) {
        return new JwtPayload(id, subject, issuer, issuedAt, details);
    }
}
