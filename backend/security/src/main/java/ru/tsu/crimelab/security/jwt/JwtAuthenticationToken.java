package ru.tsu.crimelab.security.jwt;

import lombok.Getter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.util.Assert;

/**
 * Created by PetrushinDA on 13.10.2019
 * Токен который выдается после успешной аутентификации
 */
public class JwtAuthenticationToken extends AbstractAuthenticationToken {
    @Getter
    private final String updatedJwt;

    public JwtAuthenticationToken(JwtUserDetails userDetails, String updatedJwt) {
        super(userDetails.getAuthorities());
        setDetails(userDetails);
        setAuthenticated(true);
        this.updatedJwt = updatedJwt;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return getDetails().getUsername();
    }

    @Override
    public JwtUserDetails getDetails() {
        return (JwtUserDetails) super.getDetails();
    }

    @Override
    public void setDetails(Object details) {
        Assert.isInstanceOf(JwtUserDetails.class, details, "Details must be instance of JwtUserDetails");
        super.setDetails(details);
    }
}
