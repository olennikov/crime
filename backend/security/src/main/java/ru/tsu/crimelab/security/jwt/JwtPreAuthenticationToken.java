package ru.tsu.crimelab.security.jwt;

import lombok.Getter;
import org.springframework.security.authentication.AbstractAuthenticationToken;

import static java.util.Collections.emptyList;

/**
 * Created by PetrushinDA on 13.10.2019
 * Токен который приходит перед аутентификацией в провайдере
 */
public class JwtPreAuthenticationToken extends AbstractAuthenticationToken {
    @Getter
    private final String jwt;

    public JwtPreAuthenticationToken(String jwt) {
        super(emptyList());
        this.jwt = jwt;
    }

    @Override
    public Object getCredentials() {
        return jwt;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }
}
