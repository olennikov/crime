package ru.tsu.crimelab.security.jwt;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.tsu.crimelab.security.CrimeLabUser;
import ru.tsu.crimelab.security.jwt.utils.UserUtils;

import static java.util.Optional.ofNullable;
import static ru.tsu.crimelab.security.jwt.utils.Constants.USER_INFO;

/**
 * Created by PetrushinDA on 10.11.2019
 */
@Service
public class SecurityService {
    public CrimeLabUser getCurrentEmployee() {
        var authentication = ofNullable((JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication())
                .orElseThrow(() -> new RuntimeException("Пользователь не определен"));
        return UserUtils.fromJsonString(authentication.getDetails().getDetails().get(USER_INFO), CrimeLabUser.class);
    }

    public Long getCurrentUserId() {
        return ofNullable(getCurrentEmployee())
                .map(CrimeLabUser::getId)
                .orElseThrow(() -> new RuntimeException("Пользователь не определен"));
    }
}
