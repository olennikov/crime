package ru.tsu.crimelab.security.jwt.exception;

/**
 * Created by PetrushinDA on 13.10.2019
 */
public class JwtServiceException extends RuntimeException {
    public JwtServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
