package ru.tsu.crimelab.security.jwt;

import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Map;

import static java.util.Collections.unmodifiableMap;

/**
 * Created by PetrushinDA on 13.10.2019
 * Расширение UserDetails
 */
public class JwtUserDetails implements UserDetails {

    private final String username;

    @Getter
    private final String id;

    @Getter
    private final String sessionId;

    @Getter
    private final String issuer;

    @Getter
    private final Map<String, String> details;

    private final boolean accountNonExpired;

    private final boolean accountNonLocked;

    private final boolean credentialsNonExpired;

    private final boolean enabled;

    private final Collection<? extends GrantedAuthority> authorities;

    public JwtUserDetails(String id, String sessionId, String username,
                          String issuer,
                          Map<String, String> details,
                          boolean credentialsNonExpired,
                          Collection<? extends GrantedAuthority> authorities,
                          boolean accountNonLocked) {
        this(id, sessionId, username, issuer, details, true, accountNonLocked, credentialsNonExpired, true, authorities);
    }

    JwtUserDetails(String id,
                   String sessionId,
                   String username,
                   String issuer,
                   Map<String, String> details,
                   boolean accountNonExpired,
                   boolean accountNonLocked,
                   boolean credentialsNonExpired,
                   boolean enabled,
                   Collection<? extends GrantedAuthority> authorities) {
        this.id = id;
        this.sessionId = sessionId;
        this.username = username;
        this.issuer = issuer;
        this.details = unmodifiableMap(details);
        this.accountNonExpired = accountNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.credentialsNonExpired = credentialsNonExpired;
        this.enabled = enabled;
        this.authorities = authorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
