package ru.tsu.crimelab.security.jwt;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Created by PetrushinDA on 13.10.2019
 * Сервис отвечает за загрузку информации о пользователе из JWT токена
 */
public interface JwtUserDetailsService {
    String getSupportedIssuer();

    /**
     * @param payload JWT payload
     * @return JWT user details, never null
     * @throws UsernameNotFoundException if user is not found or invalid credentials
     */
    JwtUserDetails loadUserByPayload(JwtPayload payload) throws UsernameNotFoundException;
}