package ru.tsu.crimelab.security.jwt;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by PetrushinDA on 13.10.2019
 */
public interface Principal extends Serializable {
    List<String> getPermissions();

    List<String> getRoles();

    String getId();

    Map<String, String> getDetails();
}