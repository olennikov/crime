package ru.tsu.crimelab.security.jwt.utils;

import ru.tsu.crimelab.security.jwt.config.JwtAuthenticationConfig;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import static org.springframework.util.StringUtils.isEmpty;

/**
 * Created by PetrushinDA on 14.10.2019
 * Утилити классы для работы с Cookie
 */
public class CookieUtil {
    /**
     * Метод для создания/обновления кук пользователя
     *
     * @param cookie     пересоздаваемая кука
     * @param updatedJwt значение токена
     * @param response объект респонса, в которой нужно положить куку
     */
    public static void renewCookie(Cookie cookie, String updatedJwt, HttpServletResponse response, JwtAuthenticationConfig cookieConfig) {
        if (cookie == null) {
            cookie = new Cookie(cookieConfig.getName(), updatedJwt);
        } else if (!isEmpty(updatedJwt)) {
            cookie.setValue(updatedJwt);
        }

        cookie.setMaxAge(isEmpty(updatedJwt) ? 0 : cookieConfig.getMaxAge());
        cookie.setPath("/");
        cookie.setHttpOnly(true);
        cookie.setSecure(cookieConfig.isSecure());

        response.addCookie(cookie);
    }

    /**
     * Метод для создания кук
     *
     * @param cookieConfig конфиг для создания
     * @param jwtToken     значение токена
     * @return созданный объект Cookie
     */
    public static Cookie createCookie(JwtAuthenticationConfig cookieConfig, String jwtToken) {
        Cookie cookie = new Cookie(cookieConfig.getName(), jwtToken);
        cookie.setHttpOnly(true);
        cookie.setMaxAge(cookieConfig.getMaxAge());
        cookie.setSecure(cookieConfig.isSecure());
        cookie.setPath("/");

        return cookie;
    }

    /**
     * Очистка кук
     */
    public static void clearCookie(HttpServletResponse response, JwtAuthenticationConfig cookieConfig) {
        response.addCookie(createCookie(cookieConfig, ""));
    }
}
