package ru.tsu.crimelab.security.jwt.session;

/**
 * Created by PetrushinDA on 16.10.2019
 */
public interface SessionStorage {
    boolean nonExpired(String userId, String session);

    boolean addRecord(String userId, String session);

    boolean removeRecord(String userId, String session);

    boolean removeAllForUser(String userId);
}
