package ru.tsu.crimelab.security.jwt.config;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by PetrushinDA on 13.10.2019
 * Конфиг для работы с Jwt Cookie
 */
@Data
@Component
@NoArgsConstructor
public class JwtAuthenticationConfig {
    @Value("${jwt.cookie.name}")
    private String name;
    @Value("${jwt.cookie.maxAge}")
    private int maxAge;
    @Value("${jwt.cookie.secure}")
    private boolean secure;
}
