package ru.tsu.crimelab.security.jwt;

import lombok.Value;

import java.util.List;
import java.util.Map;

/**
 * Created by PetrushinDA on 16.10.2019
 * Реализация Principal
 */
@Value
public class CrimelabPrincipal implements Principal {

    private final String id;
    private final List<String> permissions;
    private final List<String> roles;
    private final Map<String, String> details;

}
