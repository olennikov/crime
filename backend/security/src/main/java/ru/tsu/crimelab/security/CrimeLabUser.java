package ru.tsu.crimelab.security;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PetrushinDA on 12.10.2019
 * Пользователь внутри системы
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CrimeLabUser {
    /**
     * Идентификатор пользователя из справочника пользователей
     */
    private Long id;
    /**
     * Имя пользователя в системе (логин)
     */
    private String name;
    /**
     * Список ролей выданных пользователю
     */
    private List<String> roles = new ArrayList<>();
    /**
     * Имя пользователя
     */
    @NotNull
    private String firstName;
    /**
     * Фамилия пользователя
     */
    @NotNull
    private String lastName;
    /**
     * Отчество пользователя
     */
    @NotNull
    private String secondName;

    /**
     * Заблокирован ли пользователь
     */
    @NotNull
    private Boolean isBlock;
}
