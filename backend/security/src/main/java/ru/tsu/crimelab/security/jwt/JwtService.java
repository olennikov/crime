package ru.tsu.crimelab.security.jwt;

import io.jsonwebtoken.*;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.tsu.crimelab.security.jwt.exception.JwtServiceException;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.time.Instant;
import java.util.Date;
import java.util.Map;

import static java.util.stream.Collectors.toMap;

/**
 * Created by PetrushinDA on 13.10.2019
 * Сервис для работы с Jwt токеном
 */
@Data
@Service
public class JwtService {
    private static final SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS256;
    private static final String EXTRA_CLAIM_PREFIX = "extra:";

    private final Key secretKey;

    public JwtService(@Value("${jwt.secret}") String secret) {
        this.secretKey = new SecretKeySpec(secret.getBytes(), SIGNATURE_ALGORITHM.getJcaName());
    }

    private static Map<String, Object> toExtraClaimMap(Map<String, String> details) {
        return details.entrySet().stream().collect(toMap(e -> EXTRA_CLAIM_PREFIX + e.getKey(), Map.Entry::getValue));
    }

    public String encodeJwt(JwtPayload payload) {
        JwtBuilder builder = Jwts.builder();

        toExtraClaimMap(payload.getDetails()).forEach(builder::claim);

        return builder
                .setIssuedAt(Date.from(payload.getIssuedAt()))
                .setIssuer(payload.getIssuer())
                .setSubject(payload.getSubject())
                .setId(payload.getId())
                .signWith(SIGNATURE_ALGORITHM, secretKey)
                .compact();
    }

    public JwtPayload decodeJwt(String jwt) {
        Claims claims = parseClaims(jwt);

        Instant issuedAt = claims.getIssuedAt().toInstant();
        return new JwtPayload(claims.getId(), claims.getSubject(), claims.getIssuer(), issuedAt, getExtraClaimMap(claims));
    }

    private Map<String, String> getExtraClaimMap(Claims claims) {
        return claims.entrySet().stream()
                .filter(c -> c.getKey().startsWith(EXTRA_CLAIM_PREFIX))
                .collect(toMap(c -> c.getKey().substring(EXTRA_CLAIM_PREFIX.length()), c -> (String) c.getValue()));
    }

    private Claims parseClaims(String jwt) {
        try {
            return Jwts.parser()
                    .setSigningKey(secretKey)
                    .parseClaimsJws(jwt)
                    .getBody();
        } catch (SignatureException e) {
            throw new JwtServiceException("Failed to parse claims", e);
        }
    }
}

