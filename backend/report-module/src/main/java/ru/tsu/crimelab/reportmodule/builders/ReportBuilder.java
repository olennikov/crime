package ru.tsu.crimelab.reportmodule.builders;

import fr.opensagres.xdocreport.core.XDocReportException;
import ru.tsu.crimelab.reportmodule.reports.BaseReport;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Created by PetrushinDA on 07.01.2020
 * Построитель отчета.
 * ВАЖНО ЧТОБЫ В ШАБЛОНЕ ОТЧЕТА ИСПОЛЬЗОВАЛСЯ ОБЪЕКТ НАСЛЕДНИКА ОТ BaseReport!!!
 */
public interface ReportBuilder {
    <T> ByteArrayInputStream buildReport(BaseReport<T> report) throws IOException, XDocReportException;
}
