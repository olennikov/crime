package ru.tsu.crimelab.reportmodule.reports;

import lombok.Getter;
import ru.tsu.crimelab.reportmodule.models.Report;

/**
 * Created by PetrushinDA on 07.01.2020
 * Базовая сущность для формирования отчетов
 * Работа при формировании отчетов осушествляется непосредственно с наследниками данного класса.
 * Для реализации нового отчета, необходимо создать наследника данного класса, передав в качестве data сущность, используемую
 * в самом шаблоне
 */
@Getter
public abstract class BaseReport<T> {
    private T data;

    public BaseReport(T data) {
        this.data = data;
    }

    public abstract Report report();

    public abstract Class<T> dataClass();
}
