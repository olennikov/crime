package ru.tsu.crimelab.reportmodule.reports.data.models;

import lombok.Builder;
import lombok.Data;
import ru.tsu.crimelab.reportmodule.reports.TestReport;

/**
 * Created by PetrushinDA on 07.01.2020
 * Объект-сущность передаваемый в тестовый отчет {@link TestReport}
 */
@Data
@Builder
public class TestReportData {
    private String project;
    private String description;
}
