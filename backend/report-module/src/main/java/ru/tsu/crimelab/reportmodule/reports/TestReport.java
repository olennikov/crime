package ru.tsu.crimelab.reportmodule.reports;

import ru.tsu.crimelab.reportmodule.models.Report;
import ru.tsu.crimelab.reportmodule.reports.data.models.TestReportData;

/**
 * Created by PetrushinDA on 07.01.2020
 * Объект тестового отчета
 */
public class TestReport extends BaseReport<TestReportData> {

    public TestReport(TestReportData data) {
        super(data);
    }

    @Override
    public Report report() {
        return Report.TEST;
    }

    @Override
    public Class<TestReportData> dataClass() {
        return TestReportData.class;
    }
}
