package ru.tsu.crimelab.reportmodule.builders;

import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import fr.opensagres.xdocreport.template.formatter.FieldsMetadata;
import ru.tsu.crimelab.reportmodule.ReportStorage;
import ru.tsu.crimelab.reportmodule.reports.BaseReport;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by PetrushinDA on 07.01.2020
 * Реализация сборщика отчетов в формате DOCX
 */
public class DocxReportBuilder implements ReportBuilder {
    @Override
    public <T> ByteArrayInputStream buildReport(BaseReport<T> baseReport) throws IOException, XDocReportException {
        String reportFilePath = ReportStorage.getReportFileByType(baseReport.report());
        InputStream in = getClass().getClassLoader()
                .getResourceAsStream(reportFilePath);
        IXDocReport report = XDocReportRegistry.getRegistry().loadReport(in,
                TemplateEngineKind.Velocity);

        FieldsMetadata fieldsMetadata = report.createFieldsMetadata();
        fieldsMetadata.load("data", baseReport.dataClass());

        IContext context = report.createContext();
        context.put("data", baseReport.getData());

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        report.process(context, out);
        return new ByteArrayInputStream(out.toByteArray());
    }
}
