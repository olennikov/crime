package ru.tsu.crimelab.reportmodule;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.tsu.crimelab.reportmodule.builders.ReportBuilderFactory;
import ru.tsu.crimelab.reportmodule.models.ReportOutputType;
import ru.tsu.crimelab.reportmodule.reports.BaseReport;

import java.io.InputStream;

/**
 * Created by PetrushinDA on 07.01.2020
 * Сервис генерации отчетов
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ReportService {

    private final ReportBuilderFactory reportBuilderFactory;

    /**
     * Метод генерации отчета
     *
     * @param reportOutputType тип выходного файла
     * @param report           данные, передаваемые в отчет
     * @return поток отчета
     */
    public InputStream loadReport(ReportOutputType reportOutputType, BaseReport report) {
        try {
            return reportBuilderFactory.reportBuilderByOutputType(reportOutputType)
                    .buildReport(report);
        } catch (Exception e) {
            log.error("Возникла ошибка при формировании отчета", e);
            throw new RuntimeException(e);
        }
    }
}
