package ru.tsu.crimelab.reportmodule;

import ru.tsu.crimelab.reportmodule.models.Report;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by PetrushinDA on 07.01.2020
 * Хранилище зарегистрированных отчетов в системе.
 * Для регистрациии нового отчета необходимо добавить соответсвующий объект в переменную {@link ReportStorage#reports}
 */
public final class ReportStorage {
    //Перечень отчет-файл_шаблона
    private static final Map<Report, String> reports = new HashMap<>();

    static {
        reports.put(Report.TEST, "test.docx");
    }

    public static String getReportFileByType(Report report) {
        if (reports.containsKey(report)) {
            return reports.get(report);
        } else {
            throw new RuntimeException("Не удалось найти подходящий отчет");
        }
    }
}
