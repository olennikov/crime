package ru.tsu.crimelab.reportmodule.models;

/**
 * Created by PetrushinDA on 07.01.2020
 * Тип файла сформированного в качестве отчета
 */
public enum ReportOutputType {
    DOCX,
    PDF
}
