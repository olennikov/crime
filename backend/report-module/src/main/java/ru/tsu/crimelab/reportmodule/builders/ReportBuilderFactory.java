package ru.tsu.crimelab.reportmodule.builders;

import org.springframework.stereotype.Component;
import ru.tsu.crimelab.reportmodule.models.ReportOutputType;

/**
 * Created by PetrushinDA on 07.01.2020
 * Фабрика получения ReportBuilder в зависимости от типа выходного файла
 */
@Component
public class ReportBuilderFactory {
    public ReportBuilder reportBuilderByOutputType(ReportOutputType reportOutputType) {
        switch (reportOutputType) {
            case PDF:
                return new PdfReportBuilder();
            case DOCX:
                return new DocxReportBuilder();
        }
        throw new RuntimeException("Неизвестный тип отчета " + reportOutputType.name());
    }
}
