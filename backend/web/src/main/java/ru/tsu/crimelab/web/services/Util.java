package ru.tsu.crimelab.web.services;

import org.springframework.http.HttpHeaders;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.web.multipart.MultipartFile;
import ru.tsu.crimelab.dao.domain.ExpertiseApplication;

import java.io.UnsupportedEncodingException;
import java.net.URLConnection;
import java.util.Random;

import static java.lang.String.format;

/**
 * Created by PetrushinDA on 30.11.2019
 */
public class Util {
    /**
     * В документе RFC 2183 для значений параметра разрешено использовать только ASCII символы
     */
    @SuppressWarnings("CharsetObjectCanBeUsed")
    public static String getContentDispositionFilename(String fileName) {
        try {
            byte[] fileNameBytes = fileName.getBytes("utf-8");
            StringBuilder dispositionFileName = new StringBuilder();
            for (byte b : fileNameBytes) {
                dispositionFileName.append((char) (b & 0xff));
            }
            return dispositionFileName.toString();
        } catch (UnsupportedEncodingException ex) {
            return fileName;
        }
    }

    public static String buildFilePath(ExpertiseApplication expertiseApplication, MultipartFile multipartFile) {
        return format("%s/%s", expertiseApplication.getName(), multipartFile.getOriginalFilename());
    }

    public static HttpHeaders createFileDownloadHeader(String fileName) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        headers.add("Content-Disposition", "attachment;filename=" + Util.getContentDispositionFilename(fileName));
        headers.add("Content-Type", URLConnection.guessContentTypeFromName(fileName));
        return headers;
    }    
        
    public static String saltPassword(String password, String salt) {
        return DigestUtils.sha256Hex(password + salt);
    }

    public static String generatePassword() {
        int startIndex = '0', endIndex = 'z';
        int passwordLength = 10;
        Random random = new Random();
        char[] charsOfPassword = new char[passwordLength];
        for (int i = 0; i < passwordLength; i++) {
            charsOfPassword[i] = (char) (random.nextInt(endIndex - startIndex) + startIndex);
        }
        return new String(charsOfPassword);
    }
}
