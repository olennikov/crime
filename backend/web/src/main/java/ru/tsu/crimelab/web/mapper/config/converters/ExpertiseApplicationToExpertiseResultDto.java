package ru.tsu.crimelab.web.mapper.config.converters;

import lombok.RequiredArgsConstructor;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import ru.tsu.crimelab.dao.domain.ExpertiseApplication;
import ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationFileType;
import ru.tsu.crimelab.web.dto.director.ExpertiseApplicationResultDto;
import ru.tsu.crimelab.web.dto.file.FileDto;
import ru.tsu.crimelab.web.mapper.config.ConverterConfigurerSupport;

import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ExpertiseApplicationToExpertiseResultDto extends ConverterConfigurerSupport<ExpertiseApplication, ExpertiseApplicationResultDto> {

    private final ModelMapper modelMapper;

    @Override
    protected Converter<ExpertiseApplication, ExpertiseApplicationResultDto> converter() {
        return new AbstractConverter<ExpertiseApplication, ExpertiseApplicationResultDto>() {
            @Override
            protected ExpertiseApplicationResultDto convert(ExpertiseApplication expertiseApplication) {
                return ExpertiseApplicationResultDto.builder()
                        .id(expertiseApplication.getId())
                        .resultDescription(expertiseApplication.getResultDescription())
                        .applicationStatus(expertiseApplication.getStatus())
                        .fileDtoList(expertiseApplication
                                .getExpertiseFiles()
                                .stream()
                                .filter(x->x.getFileType().equals(ExpertiseApplicationFileType.RESULT_FILE))
                                .map(x->modelMapper.map(x.getFile(), FileDto.class))
                                .collect(Collectors.toList()))
                        .build();
            }
        };
    }
}
