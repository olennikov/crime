package ru.tsu.crimelab.web.dto.chat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.crimelab.web.dto.expertiseapplication.ExpertiseApplicationDto;
import ru.tsu.crimelab.web.dto.user.UserDto;

import java.util.List;

/**
 * DTO сущность для представления чата
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "ChatDto: чат", description = "Чат")
public class ChatDto {

    @ApiModelProperty(required = true, value = "Идентификатор чата")
    private Long id;

    @ApiModelProperty(required = true, value = "Экспертиза в рамках которой существует чат")
    private ExpertiseApplicationDto expertiseApplication;

    @ApiModelProperty(required = true, value = "Пользователи, участвующие в чате")
    private List<UserDto> users;
}
