package ru.tsu.crimelab.web.dto.login;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoginSaltDto {
    /**
     * Раундовая соль
     */
    private String roundSalt;

    /**
     * Статичная соль пользователя
     */
    private String userSalt;
}
