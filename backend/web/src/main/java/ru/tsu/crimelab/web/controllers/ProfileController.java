package ru.tsu.crimelab.web.controllers;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.tsu.crimelab.web.dto.login.EditPasswordDto;
import ru.tsu.crimelab.web.dto.login.LoginSaltDto;
import ru.tsu.crimelab.web.dto.user.UserProfileDto;
import ru.tsu.crimelab.web.services.ProfileService;

@RestController
@RequestMapping("profile")
@RequiredArgsConstructor
public class ProfileController {

    private final ProfileService profileService;

    @PostMapping("edit-profile")
    @ApiOperation("Метод для обновления данных профиля пользователя")
    public Long editUserProfile (@RequestBody UserProfileDto userProfileDto) {
        return profileService.editUserProfile(userProfileDto);
    }

    @PostMapping("edit-password")
    @ApiOperation("Метод для изменения пароля пользователя")
    public Long editUserPassword (@RequestBody EditPasswordDto editPasswordDto){
        return profileService.editUserPassword(editPasswordDto);
    }

    @GetMapping("context-round-salt")
    @ApiOperation("Метод для получения раундовой соли по контексту")
    public LoginSaltDto getRoundSaltByContext (){
        return profileService.getRoundSalt();
    }

    @GetMapping("profile")
    @ApiOperation("Получение данных профиля пользователя")
    public UserProfileDto getUserProfile (){
        return profileService.getUserProfile();
    }
}
