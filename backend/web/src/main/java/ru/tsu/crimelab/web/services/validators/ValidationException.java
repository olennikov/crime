package ru.tsu.crimelab.web.services.validators;

import java.util.Collection;

/**
 * Created by PetrushinDA on 15.01.2020
 */
public class ValidationException extends RuntimeException {
    public ValidationException() {
        super();
    }

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidationException(Throwable cause) {
        super(cause);
    }

    protected ValidationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public ValidationException(Collection<String> errors) {
        this(String.join(";" + System.lineSeparator(), errors));
    }
}
