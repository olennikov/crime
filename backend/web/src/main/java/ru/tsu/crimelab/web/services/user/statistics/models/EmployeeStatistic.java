package ru.tsu.crimelab.web.services.user.statistics.models;

import lombok.Builder;
import lombok.Data;

/**
 * Created by PetrushinDA on 03.01.2020
 * Данные статистики по сотрудникам лаборатории
 */
@Data
public class EmployeeStatistic extends UserStatistic {
    /**
     * Общее кол-во задач, назначенных на данного сотрудника
     */
    private Long taskCount;

    /**
     * Кол-во закрытых задач, назначенных на данного сотрудника
     */
    private Long closedTaskCount;

    /**
     * Общее кол-во подзадач, за которые отвественнен данный сотрудник
     */
    private Long subTaskCount;

    /**
     * Кол-во закрытых подзадач, за которые отвественнен данный сотрудник
     */
    private Long closedSubTaskCount;

    @Builder
    public EmployeeStatistic(Long id, String lastName, String firstName, String secondName, String email, Boolean isBlock, String phone, Long taskCount, Long closedTaskCount, Long subTaskCount, Long closedSubTaskCount) {
        super(id, lastName, firstName, secondName, email, isBlock, phone);
        this.taskCount = taskCount;
        this.closedTaskCount = closedTaskCount;
        this.subTaskCount = subTaskCount;
        this.closedSubTaskCount = closedSubTaskCount;
    }
}
