package ru.tsu.crimelab.web.dto.file;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.crimelab.web.dto.user.UserDto;
import ru.tsu.crimelab.dao.domain.File;

import java.util.Date;

/**
 * Created by PetrushinDA on 06.11.2019
 * ДТО файла
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@ApiModel(value = "FileDto: ДТО сущности файл", description = "ДТО сущности файл")
public class FileDto {

    @ApiModelProperty(required = true, value = "Идентификатор файла")
    private Long id;

    @ApiModelProperty(required = true, value = "Имя файла")
    private String filename;

    @ApiModelProperty(required = true, value = "Создатель файла")
    private UserDto creator;

    @ApiModelProperty(required = true, value = "Дата создания")
    private Date dateCreate;
}
