package ru.tsu.crimelab.web.services;

import org.springframework.lang.NonNull;
import org.springframework.web.multipart.MultipartFile;
import ru.tsu.crimelab.web.dto.substasks.SubTaskDto;

import java.util.List;

/**
 * Created by PetrushinDA on 17.11.2019
 * Сервис для работы с подзадачами
 */
public interface SubTaskService {

    /**
     * Метод выгрузки подзадачи по идентификатору
     *
     * @param id идентификатор подзадачи
     * @return объект подзадачи
     */
    SubTaskDto findById(Long id);

    /**
     * Метод выгрузки подзадач для задачи
     *
     * @param taskId идентификатор задачи
     * @return перчень подзадач
     */
    List<SubTaskDto> findByTaskId(Long taskId);

    /**
     * Метод обновления подзадачи
     *
     * @param subTaskDto обновляемый объект
     * @return обновленный объект
     */
    SubTaskDto updateSubTask(SubTaskDto subTaskDto);

    /**
     * Метод создания подзадачи
     *
     * @param subTaskDto создаваемый объект
     * @return созданный объект
     */
    SubTaskDto createSubTask(SubTaskDto subTaskDto);

    /**
     * Метод удаления подзадачи
     *
     * @param subTaskDto идентификатор удаляемой подзадачи
     */
    void deleteSubTask(SubTaskDto subTaskDto);

    /**
     * Метод закрытия подзадачи
     *
     * @param subTaskDto закрываемая подзадача
     * @return закртыая подзадача
     */
    SubTaskDto close(SubTaskDto subTaskDto);

    /**
     * Метод прикрепления файла к подзадаче
     *
     * @param subTaskId     идентификатор подзадачи
     * @param multipartFile прикрепляемый файл
     * @return обновленная подзадача
     */
    SubTaskDto uploadFile(Long subTaskId, MultipartFile multipartFile);

    /**
     * Метод удаления прикрепленного файла к подзадаче
     *
     * @param subTaskId идентификатор подзадачи
     * @param fileId    идентификатор удаляемого файла
     */
    void deleteFile(@NonNull Long subTaskId, @NonNull Long fileId);


    /**
     * Метод для получения количества открытых подзадач для задачи
     *
     * @param taskId идентиификатор задачи
     * @return количество открытых подзадач
     */
    Long getCountOpenedSubTask(Long taskId);
}
