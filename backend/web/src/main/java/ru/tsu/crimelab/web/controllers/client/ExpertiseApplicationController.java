package ru.tsu.crimelab.web.controllers.client;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.tsu.crimelab.web.dto.expertiseapplication.ExpertiseApplicationDto;
import ru.tsu.crimelab.web.dto.expertiseapplication.ExpertiseApplicationStatus;
import ru.tsu.crimelab.web.services.ExpertiseApplicationService;

import java.util.List;

/**
 * Created by PetrushinDA on 05.11.2019
 * Контроллер для работы с заявками на экспертизу клиентом
 */
@RestController("client-expertise-applications")
@RequestMapping("/client/expertise-applications")
@RequiredArgsConstructor
@PreAuthorize("hasRole(T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).customerRoleName)")
@Api(value = "Контроллер для работы с заявками на экспертизу клиентом")
public class ExpertiseApplicationController {

    private final ExpertiseApplicationService expertiseApplicationService;

    @ApiOperation(value = "Получение заявок на экспертизу клиентом")
    @GetMapping(value = "/filter/{type}")
    public List<ExpertiseApplicationDto> getExpertiseApplications(@PathVariable("type") ExpertiseApplicationStatus type) {
        return expertiseApplicationService.getByCurrentUserCreator(type);
    }

    @ApiOperation(value = "Получение заявки на экспертизу клиентом")
    @GetMapping(value = "/{id}")
    public ExpertiseApplicationDto getExpertiseApplication(@PathVariable("id") Long id) {
        return expertiseApplicationService.getExpertiseApplicationById(id);
    }

    @ApiOperation(value = "Обновление заявки на экспертизу клиентом")
    @PutMapping
    public ExpertiseApplicationDto updateExpertiseApplication(@RequestBody ExpertiseApplicationDto expertiseApplicationDto) {
        return expertiseApplicationService.updateExpertiseApplication(expertiseApplicationDto);
    }

    @ApiOperation(value = "Обновление заявки на экспертизу клиентом")
    @PostMapping
    public ExpertiseApplicationDto createExpertiseApplication(@RequestBody ExpertiseApplicationDto expertiseApplicationDto) {
        return expertiseApplicationService.createExpertiseApplication(expertiseApplicationDto);
    }

    @ApiOperation(value = "Прикрепление файла к заявке на экспертизу клиентом")
    @PostMapping(value = "/{id}/file-upload/")
    public ExpertiseApplicationDto attachFileToExpertiseApplication(@PathVariable("id") Long id, @RequestParam("file") MultipartFile file) {
        return expertiseApplicationService.uploadFile(id, file);
    }
}
