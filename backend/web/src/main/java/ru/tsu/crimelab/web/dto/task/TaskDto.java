package ru.tsu.crimelab.web.dto.task;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.crimelab.web.dto.file.FileDto;
import ru.tsu.crimelab.web.dto.user.UserDto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by PetrushinDA on 19.11.2019
 * DTO сущности task
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TaskDto {
    private Long id;
    private String name;
    private Date expiredDate;
    private Date deadlineDate;
    private UserDto responsible;
    private String progress;
    private Boolean isClosed;
    private List<FileDto> taskFiles = new ArrayList<>();
}
