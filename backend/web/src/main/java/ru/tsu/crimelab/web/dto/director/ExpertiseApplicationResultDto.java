package ru.tsu.crimelab.web.dto.director;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationStatus;
import ru.tsu.crimelab.web.dto.file.FileDto;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Builder(toBuilder = true)
@AllArgsConstructor
public class ExpertiseApplicationResultDto {
    private Long id;
    private String resultDescription;
    private ExpertiseApplicationStatus applicationStatus;
    private List<FileDto> fileDtoList  = new ArrayList<>();
}
