package ru.tsu.crimelab.web.services.file.access;

import org.springframework.stereotype.Service;
import ru.tsu.crimelab.dao.domain.User;

/**
 * Created by PetrushinDA on 07.12.2019
 * Стратегия запрещающая всем скачивать файлы
 */
@Service
public class DenyFileAccessStrategy implements FileCheckAccessStrategy {

    @Override
    public boolean canHandle(User user) {
        return false;
    }

    @Override
    public boolean canDownload(User user, Long fileId) {
        return false;
    }
}
