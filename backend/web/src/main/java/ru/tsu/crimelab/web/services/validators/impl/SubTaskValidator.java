package ru.tsu.crimelab.web.services.validators.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.tsu.crimelab.dao.domain.SubTask;
import ru.tsu.crimelab.dao.domain.enums.TaskStatus;
import ru.tsu.crimelab.dao.repository.SubTaskRepository;
import ru.tsu.crimelab.dao.repository.TaskRepository;
import ru.tsu.crimelab.web.services.validators.ActionType;
import ru.tsu.crimelab.web.services.validators.Validator;

import java.util.Collection;

import static java.util.List.of;
import static ru.tsu.crimelab.web.services.validators.ActionType.*;
import static ru.tsu.crimelab.web.services.validators.ValidatorUtils.baseValidateByAction;

/**
 * Created by PetrushinDA on 18.01.2020
 */
@Component
@RequiredArgsConstructor
public class SubTaskValidator extends Validator<SubTask> {

    private final TaskRepository taskRepository;
    private final SubTaskRepository subTaskRepository;

    @Override
    protected void validate(@NonNull SubTask subTask, @NonNull ActionType actionType, @NonNull Collection<String> errors) {
        baseValidateByAction(subTask::getId, actionType, errors);

        var task = subTask.getTask();
        if (task == null || task.getId() == null) {
            errors.add("Не передана информация о задаче для подзадачи");
        } else {
            var savedTask = taskRepository.getById(subTask.getTask().getId());
            if (savedTask.getTaskStatus().equals(TaskStatus.CLOSED)) {
                errors.add("Нельзя создавать/редактировать/удалять подзадачи у закрытой задачи");
                return;
            }

            if (of(UPDATE, DELETE).contains(actionType)) {
                var savedSubTask = subTaskRepository.getById(subTask.getId());

                if (savedSubTask.getIsClosed()) {
                    errors.add("Невозможно создать/отредактировать закрытую подзадачу");
                }
            }
        }
    }
}
