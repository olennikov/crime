package ru.tsu.crimelab.web.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.tsu.crimelab.security.jwt.JwtPreAuthenticationToken;
import ru.tsu.crimelab.security.jwt.config.JwtAuthenticationConfig;
import ru.tsu.crimelab.security.jwt.utils.CookieUtil;
import ru.tsu.crimelab.web.dto.login.EditPasswordDto;
import ru.tsu.crimelab.web.dto.login.LoginDto;
import ru.tsu.crimelab.web.dto.login.LoginSaltDto;
import ru.tsu.crimelab.web.dto.user.UserProfileDto;
import ru.tsu.crimelab.web.services.AuthService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * Created by PetrushinDA on 13.10.2019
 */
@RestController
@RequestMapping("/auth/")
@RequiredArgsConstructor
@Api(value = "Сервис для реализации авторизации в закрытой части системы", description = "Login Logout из системы")
public class LoginController {

    private final AuthService authService;

    private final JwtAuthenticationConfig authConfig;

    @PostMapping(value = "login")
    @ApiOperation(value = "Операция для получения токена входа")
    public void login(@ApiParam(name = "Сущность для входа (логин пароль)") @RequestBody LoginDto loginDto, HttpServletResponse response) {
        JwtPreAuthenticationToken jwtAuthenticationToken = Optional.ofNullable((JwtPreAuthenticationToken) authService.login(loginDto))
                .orElseThrow(() -> new AuthorizationServiceException("Некорректный логин-пароль"));
        SecurityContextHolder.getContext().setAuthentication(jwtAuthenticationToken);
        Cookie cookie = CookieUtil.createCookie(authConfig, jwtAuthenticationToken.getJwt());
        response.addCookie(cookie);
    }

    @GetMapping("roundsalt")
    @ApiOperation(value = "Операция для генерации раундовой соли")
    public LoginSaltDto getRoundSalt(@RequestParam("userEmail") String userEmail) {
        return authService.getSalt(userEmail);
    }

    @GetMapping("userInfo")
    @ApiOperation(value = "Получение данных о пользователе системы по его токену")
    public String getUserInfo() {
        return authService.getUserDetails(SecurityContextHolder.getContext());
    }



    @PostMapping("logout")
    @ApiOperation(value = "Операция для выхода из системы")
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        authService.logout(request, response);
    }

}
