package ru.tsu.crimelab.web.dto.registration;

import lombok.Data;

@Data
public class RegistrationUserDto {
    private String userLogin;
    private String userPassword;
    private String userSalt;
    private String changePasswordUrl;
}
