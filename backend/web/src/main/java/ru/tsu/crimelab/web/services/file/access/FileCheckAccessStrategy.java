package ru.tsu.crimelab.web.services.file.access;

import ru.tsu.crimelab.dao.domain.User;

/**
 * Created by PetrushinDA on 07.12.2019
 * Базовый интерфейс стратегии проверки прав на скачивание файлов
 */
public interface FileCheckAccessStrategy {
    /**
     * Проверка, может ли текущая реализация использоваться для проверки прав доступа к файлам
     *
     * @param user пользователь, для которого проверяется возможность выполнения стратегии
     * @return true, если может. Иначе false.
     */
    boolean canHandle(User user);

    /**
     * Проверка может ли пользователь скачивать файл
     *
     * @param user   пользователь, который пытается скачать
     * @param fileId идентфикатор скачиваемого файла
     * @return true, если может. Иначе false.
     */
    boolean canDownload(User user, Long fileId);
}
