package ru.tsu.crimelab.web.services.impl;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import ru.tsu.crimelab.dao.domain.File;
import ru.tsu.crimelab.dao.domain.SubTask;
import ru.tsu.crimelab.dao.domain.SubTaskFile;
import ru.tsu.crimelab.dao.repository.*;
import ru.tsu.crimelab.web.dto.substasks.SubTaskDto;
import ru.tsu.crimelab.web.services.FileService;
import ru.tsu.crimelab.web.services.SubTaskService;
import ru.tsu.crimelab.web.services.TaskService;
import ru.tsu.crimelab.web.services.Util;
import ru.tsu.crimelab.web.services.validators.impl.SubTaskFileValidator;
import ru.tsu.crimelab.web.services.validators.impl.SubTaskValidator;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static java.util.Optional.ofNullable;
import static ru.tsu.crimelab.web.services.validators.ActionType.*;

/**
 * Created by PetrushinDA on 17.11.2019
 * Сервис для работы с подзадачами
 */
@Service
@RequiredArgsConstructor
public class SubTaskServiceImpl implements SubTaskService {

    private final SubTaskRepository repository;

    private final TaskRepository taskRepository;

    private final SubTaskFileRepository subTaskFileRepository;

    private final ExpertiseApplicationRepository expertiseApplicationRepository;

    private final FileService fileService;

    private final TaskService taskService;

    private final ModelMapper modelMapper;

    private final SubTaskValidator validator;

    private final SubTaskFileValidator subTaskFileValidator;

    /**
     * Метод выгрузки подзадачи по идентификатору
     *
     * @param id идентификатор подзадачи
     * @return объект подзадачи
     */
    @Transactional(readOnly = true)
    @Override
    public SubTaskDto findById(Long id) {
        return repository.findById(id)
                .map(x -> modelMapper.map(x, SubTaskDto.class))
                .orElse(null);
    }

    /**
     * Метод выгрузки подзадач для задачи
     *
     * @param taskId идентификатор задачи
     * @return перчень подзадач
     */
    @Transactional(readOnly = true)
    @Override
    public List<SubTaskDto> findByTaskId(Long taskId) {
        return repository.findByTask_Id(taskId)
                .stream()
                .map(x -> modelMapper.map(x, SubTaskDto.class))
                .collect(Collectors.toList());
    }

    /**
     * Метод обновления подзадачи
     *
     * @param subTaskDto обновляемый объект
     * @return обновленный объект
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public SubTaskDto updateSubTask(SubTaskDto subTaskDto) {
        SubTask subTask = repository.findById(subTaskDto.getId())
                .orElseThrow(() -> new IllegalArgumentException("Подзадача не найдена"));

        subTask.setComment(subTaskDto.getComment());
        subTask.setDescription(subTaskDto.getDescription());
        subTask.setDeadlineDate(subTaskDto.getDeadlineDate());
        subTask.setName(subTaskDto.getName());

        validator.validate(subTask, UPDATE);
        return modelMapper.map(repository.save(subTask), SubTaskDto.class);
    }

    /**
     * Метод создания подзадачи
     *
     * @param subTaskDto создаваемый объект
     * @return созданный объект
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public SubTaskDto createSubTask(SubTaskDto subTaskDto) {
        if (subTaskDto.getTaskId() == null) {
            throw new IllegalArgumentException("Необходимо указать идентификатор задачи");
        }
        var task = taskRepository.findById(subTaskDto.getTaskId())
                .orElseThrow(() -> new IllegalArgumentException("Задача не найдена"));

        SubTask subTask = Optional.ofNullable(subTaskDto.getId())
                .map(repository::findById)
                .orElseGet(() -> Optional.of(new SubTask()))
                .orElseThrow(() -> new IllegalArgumentException("Не найдена подзадача с заданным идентификатором"));

        subTask.setComment(subTaskDto.getComment());
        subTask.setDescription(subTaskDto.getDescription());
        subTask.setDeadlineDate(subTaskDto.getDeadlineDate());
        subTask.setName(subTaskDto.getName());
        subTask.setIsClosed(false);
        subTask.setTask(task);

        validator.validate(subTask, CREATE);
        return modelMapper.map(repository.save(subTask), SubTaskDto.class);
    }


    /**
     * Метод удаления подзадачи
     *
     * @param subTaskDto идентификатор удаляемой подзадачи
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteSubTask(SubTaskDto subTaskDto) {
        repository.deleteById(subTaskDto.getId());
    }

    /**
     * Метод закрытия подзадачи
     *
     * @param subTaskDto закрываемая подзадача
     * @return закртыая подзадача
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public SubTaskDto close(SubTaskDto subTaskDto) {
        if (subTaskDto.getId() == null) {
            throw new IllegalArgumentException("Необходимо указать идентификатор подзадачи");
        }
        var subTask = repository.findById(subTaskDto.getId())
                .orElseThrow(() -> new IllegalArgumentException("Подзадача не найдена"));

        validator.validate(subTask, UPDATE);

        subTask.setIsClosed(true);
        subTask.setComment(subTaskDto.getComment());

        var savedSubTask = repository.save(subTask);
        if (getCountOpenedSubTask(subTaskDto.getTaskId()) == 0) {
            taskService.close(subTaskDto.getTaskId());
        }

        return modelMapper.map(savedSubTask, SubTaskDto.class);
    }

    /**
     * Метод прикрепления файла к подзадаче
     *
     * @param subTaskId     идентификатор подзадачи
     * @param multipartFile прикрепляемый файл
     * @return обновленная подзадача
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public SubTaskDto uploadFile(Long subTaskId, MultipartFile multipartFile) {
        var subTask = ofNullable(subTaskId)
                .map(x -> repository.findById(subTaskId))
                .orElseThrow(() -> new RuntimeException(format("Объект подзадачи с идентификатором %s не найден", subTaskId)))
                .orElseThrow(() -> new RuntimeException("Не передан идентификатор подзадачи"));

        var expertiseApplication = expertiseApplicationRepository.findBySubTaskId(subTask.getId())
                .orElseThrow(() -> new RuntimeException("Не найдено заявление на проведение экспертизы для данной подзадачи"));
        String fileName = Util.buildFilePath(expertiseApplication, multipartFile);
        File file = fileService.uploadFile(fileName, multipartFile);

        SubTaskFile subTaskFile = SubTaskFile.builder()
                .file(file)
                .subTask(subTask)
                .build();


        subTaskFileRepository.save(subTaskFile);
        return modelMapper.map(repository.getById(subTaskId), SubTaskDto.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteFile(@NonNull Long subTaskId, @NonNull Long fileId) {
        SubTaskFile subTaskFile = subTaskFileRepository.findByFile_Id(fileId)
                .orElseThrow(()-> new IllegalArgumentException("Данный файл не существует"));

        subTaskFileValidator.validate(subTaskFile, DELETE);
        subTaskFileRepository.delete(subTaskFile);
        fileService.deleteFile(subTaskFile.getFile());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public Long getCountOpenedSubTask(Long taskId) {
        return repository.countByTaskIdAndIsClosedFalse(taskId);
    }
}
