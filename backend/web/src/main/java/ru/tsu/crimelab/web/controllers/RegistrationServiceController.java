package ru.tsu.crimelab.web.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.tsu.crimelab.dao.api.Query;
import ru.tsu.crimelab.dao.api.repository.response.QueryResponse;
import ru.tsu.crimelab.web.dto.director.RegistrationDirectorModel;
import ru.tsu.crimelab.web.dto.registration.RegistrationApplicationModel;
import ru.tsu.crimelab.web.dto.registration.RegistrationApplicationView;
import ru.tsu.crimelab.web.dto.registration.RegistrationUserDto;
import ru.tsu.crimelab.web.dto.registration.RejectRegistrationDto;
import ru.tsu.crimelab.web.services.RegistrationService;

@RestController
@RequestMapping("registration")
@Api(value = "Сервис реализующий методы бизнес процесса \"Регистрация нового пользователя\"", description = "Сервис для регистрации нового пользователя-заказчика")
public class RegistrationServiceController {

    @Autowired
    RegistrationService registrationService;

    @PutMapping
    @ResponseBody
    @ApiOperation(value = "Сохранение заявления о регистрации в системе")
    public Long createApplicationRegistration(@ApiParam(value = "Сущность запроса для получения доступа к системе") @RequestBody RegistrationApplicationModel model) {
        return registrationService.createApplicationRegistration(model);
    }

    @ResponseBody
    @PostMapping("accept")
    @ApiOperation(value = "Создание пользователя в системе по полученной заявке на регистрацию")
    @PreAuthorize("hasRole(T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).directorRoleName)")
    public Long acceptedRegistration(@ApiParam(value = "Идентификатор заявки на регистрацию") @RequestBody Long applicationId) {
        return registrationService.acceptApplicationRegistration(applicationId);
    }

    @ResponseBody
    @PostMapping("reject")
    @ApiOperation(value = "Отклонение заявки на регистрацию")
    @PreAuthorize("hasRole(T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).directorRoleName)")
    public Long rejectRegistration(@ApiParam(name = "Сущность для отклонения заявки на регистрацию")@RequestBody RejectRegistrationDto rejectRegistrationDto) {
        return registrationService.rejectApplicationRegistration(rejectRegistrationDto);
    }

    @ResponseBody
    @GetMapping("get/{disposableUrl}")
    @ApiOperation(value = "Получение данных регистрации по одноразовой ссылке для создания пользователя")
    public RegistrationApplicationModel getRegistrationDataByToken (@ApiParam("Ссылка на объект регистрации пользователя")@PathVariable("disposableUrl") String disposableUrl){
        return registrationService.getAcceptableRegistrationByDisposableUri(disposableUrl);
    }

    @ResponseBody
    @PostMapping("generate-user")
    @ApiOperation(value = "Регистрация нового пользователя")
    public Long registrateUser(@RequestBody RegistrationUserDto registrationUserDto){
        return registrationService.closeApplicationRegistration(registrationUserDto);
    }

    @ResponseBody
    @PostMapping("registration-application-list")
    @ApiOperation(value = "Метод для получения списка заявок на регистрацию")
    @PreAuthorize("hasRole(T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).directorRoleName)")
    public QueryResponse<RegistrationApplicationView> getRegistrationList(@ApiParam("Объект запроса для получения сущностей")@RequestBody Query query){
        return registrationService.getRegistrationApplicationView(query);
    }

    @ResponseBody
    @GetMapping("getById/{id}")
    @ApiOperation(value = "Метод для запроса модели заявки регистрации")
    @PreAuthorize("hasRole(T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).directorRoleName)")
    public RegistrationDirectorModel getRegistrationApplicationModel (@ApiParam("Идентификатор")@PathVariable("id") Long id){
        return registrationService.getRegistrationById(id);
    }
}
