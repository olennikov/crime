package ru.tsu.crimelab.web.services.file.access;

import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.tsu.crimelab.dao.domain.User;

/**
 * Created by PetrushinDA on 07.12.2019
 * Фабрика для предоставления экземпляра стратегии проверки прав доступа для скачивания файла
 */
@Service
@RequiredArgsConstructor
public class FileCheckAccessStrategyFactory {
    private final ApplicationContext context;

    /**
     * Поиск необходимого экземпляра стратегии проверки прав доступа для скачивания файлов
     *
     * @param user пользователь, для которого осуществялется поиск стратегии
     * @return эксземпляр стратегии для проверки прав доступа
     */
    public FileCheckAccessStrategy getFileCheckAccessStrategy(User user) {
        return context.getBeansOfType(FileCheckAccessStrategy.class).values().stream()
                .filter(x -> x.canHandle(user))
                .findFirst()
                .orElse(new DenyFileAccessStrategy());
    }
}
