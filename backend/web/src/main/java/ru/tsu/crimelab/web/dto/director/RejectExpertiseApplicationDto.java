package ru.tsu.crimelab.web.dto.director;

import lombok.Data;

@Data
public class RejectExpertiseApplicationDto {
    private Long id;
    private String rejectReason;
}
