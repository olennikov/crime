package ru.tsu.crimelab.web.services.user.statistics.models;

import lombok.Builder;
import lombok.Data;

/**
 * Created by PetrushinDA on 03.01.2020
 * Данные статистики по клиентам лаборатории
 */
@Data
public class ClientStatistic extends UserStatistic {
    /**
     * Кол-во поданных заявлений на проведенеие экспертизы
     */
    private Long applicationCount;

    @Builder
    public ClientStatistic(Long id, String lastName, String firstName, String secondName, String email, Boolean isBlock, String phone, Long applicationCount) {
        super(id, lastName, firstName, secondName, email, isBlock, phone);
        this.applicationCount = applicationCount;
    }
}
