package ru.tsu.crimelab.web.dto.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.crimelab.dao.domain.enums.Role;

/**
 * Created by PetrushinDA on 07.01.2020
 * Сущность создаваемого пользователя
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "CreateUserDto: Создаваемый пользователь", description = "Создаваемый пользователь")
public class CreateUserDto {

    @ApiModelProperty(required = true, value = "Фамилия пользователя")
    private String lastName;

    @ApiModelProperty(required = true, value = "Имя пользователя")
    private String firstName;

    @ApiModelProperty(required = true, value = "Отчество пользователя")
    private String secondName;

    @ApiModelProperty(required = true, value = "Номер телефона пользователя")
    private String phone;

    @ApiModelProperty(required = true, value = "Email пользователя")
    private String email;

    @ApiModelProperty(required = true, value = "Роль пользователя")
    private Role role;
}
