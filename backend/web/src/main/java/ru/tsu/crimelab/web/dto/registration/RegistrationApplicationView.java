package ru.tsu.crimelab.web.dto.registration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.crimelab.dao.domain.RegistrationApplication;
import ru.tsu.crimelab.dto.api.Model;

import java.util.Date;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationApplicationView {
    private Long id;
    private Date receiveDate;
    private String fullName;
    private String phone;
    private String organisationName;
    private String organisationPhone;

    public static class RegistrationApplicationViewBuilder{
        public RegistrationApplicationViewBuilder registrationBuilder (RegistrationApplication registrationEntity){
            return id(registrationEntity.getId())
                    .fullName(String.format("%s %s %s",registrationEntity.getLastName(),registrationEntity.getFirstName(),registrationEntity.getSecondName()))
                    .organisationName(registrationEntity.getOrganisationName())
                    .organisationPhone(registrationEntity.getOrganisationPhone())
                    .phone(registrationEntity.getPhone())
                    .receiveDate(registrationEntity.getReceiveDate());
        }
    }
}
