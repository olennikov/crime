package ru.tsu.crimelab.web.dto.director;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.crimelab.dao.domain.RegistrationApplication;
import ru.tsu.crimelab.dao.domain.enums.RegistrationApplicationStatus;
import ru.tsu.crimelab.dto.api.Model;

@Data
@Model
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationDirectorModel {
    private Long id;
    private RegistrationApplicationStatus status;
    private String lastName;
    private String firstName;
    private String secondName;
    private String phone;
    private String email;
    private String organisationName;
    private String organisationPhone;
    private String organisationInn;
    private String organisationOgrn;
    private String rejectReason;
}
