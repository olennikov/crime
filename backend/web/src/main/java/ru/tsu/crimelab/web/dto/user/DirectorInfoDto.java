package ru.tsu.crimelab.web.dto.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * Created by PetrushinDA on 29.12.2019
 */
@Data
@NoArgsConstructor
@SuperBuilder
@ApiModel(value = "DirectorInfoDto: Информация о руководителе лаборатории", description = "Информация о руководителе лаборатории")
public class DirectorInfoDto extends UserDto {

    @ApiModelProperty(required = true, value = "email")
    private String email;

    @ApiModelProperty(required = true, value = "Общее количество экспертиз")
    private Long expertiseCount;

    @ApiModelProperty(required = true, value = "Количество закрытых экспертиз")
    private Long closedExpertiseCount;

    @ApiModelProperty(required = true, value = "Заблокирована ли учетная запись")
    private Boolean isBlock;
}
