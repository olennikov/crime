package ru.tsu.crimelab.web.services.validators.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import ru.tsu.crimelab.dao.domain.Expertise;
import ru.tsu.crimelab.dao.domain.enums.ExpertiseStatus;
import ru.tsu.crimelab.web.services.validators.ActionType;
import ru.tsu.crimelab.web.services.validators.Validator;

import java.util.Collection;

import static ru.tsu.crimelab.web.services.validators.ActionType.CREATE;
import static ru.tsu.crimelab.web.services.validators.ActionType.UPDATE;
import static ru.tsu.crimelab.web.services.validators.ValidatorUtils.baseValidateByAction;

/**
 * Created by PetrushinDA on 18.01.2020
 */
@Component
@RequiredArgsConstructor
public class ExpertiseValidator extends Validator<Expertise> {

    @Override
    protected void validate(@NonNull Expertise expertise, @NonNull ActionType actionType, @NonNull Collection<String> errors) {
        baseValidateByAction(expertise::getId, actionType, errors);

        if (actionType.equals(CREATE) || actionType.equals(UPDATE)) {
            if (expertise.getStatus().equals(ExpertiseStatus.DONE)) {
                errors.add("Изменение экспертизы невозможно");
            }
        }
    }
}
