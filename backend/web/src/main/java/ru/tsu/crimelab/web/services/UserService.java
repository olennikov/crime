package ru.tsu.crimelab.web.services;

import org.springframework.data.domain.Pageable;
import ru.tsu.crimelab.dao.api.repository.response.QueryResponse;
import ru.tsu.crimelab.dao.domain.User;
import ru.tsu.crimelab.web.dto.user.*;

import java.util.List;

/**
 * Created by PetrushinDA on 13.11.2019
 * Сервис для работы с пользователями
 */
public interface UserService {

    /**
     * Метод для получения текущего пользователя из контекста
     *
     * @return текущий пользователь
     */
    User getCurrentUserByContext();

    /**
     * Метод для получения идентификатора текущего пользователя из контекста
     *
     * @return идентификатор текущего пользователя
     */
    Long getCurrentUserIdByContext();

    /**
     * Метод загрузки списка клиентов лаборатории
     *
     * @param pageable параметры пагинации
     * @return список пользователей
     */
    QueryResponse<ClientInfoDto> loadClientsInfo(Pageable pageable);

    /**
     * Метод загрузки списка сотрудников лаборатории
     *
     * @param pageable параметры пагинации
     * @return список пользователей
     */
    QueryResponse<EmployeeInfoDto> loadEmployeesInfo(Pageable pageable);

    /**
     * Метод загрузки списка руководителей лаборатории
     *
     * @param pageable параметры пагинации
     * @return список пользователей
     */
    QueryResponse<DirectorInfoDto> loadDirectorsInfo(Pageable pageable);

    /**
     * Блокирование пользователей
     *
     * @param userIds идентификаторы пользователей, которые будут заблокированы
     */
    void blockUsers(List<Long> userIds);

    /**
     * Разблокирование пользователей
     *
     * @param userIds идентификаторы пользователей, которые будут разблокированы
     */
    void unblockUsers(List<Long> userIds);

    /**
     * Создание нового пользователя
     *
     * @param createUserDto создаваемый пользователь
     * @return созданный пользователь
     */
    UserDto createUser(CreateUserDto createUserDto);
}
