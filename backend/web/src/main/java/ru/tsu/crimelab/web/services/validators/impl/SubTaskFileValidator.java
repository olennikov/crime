package ru.tsu.crimelab.web.services.validators.impl;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.tsu.crimelab.dao.domain.SubTaskFile;
import ru.tsu.crimelab.dao.repository.SubTaskRepository;
import ru.tsu.crimelab.web.services.UserService;
import ru.tsu.crimelab.web.services.validators.ActionType;
import ru.tsu.crimelab.web.services.validators.Validator;

import java.util.Collection;

import static ru.tsu.crimelab.web.services.validators.ValidatorUtils.baseValidateByAction;

@Component
@RequiredArgsConstructor
public class SubTaskFileValidator extends Validator<SubTaskFile> {


    private final SubTaskRepository subTaskRepository;

    private final UserService userService;

    @Override
    protected void validate(@NonNull SubTaskFile subTaskFile, @NonNull ActionType actionType, @NonNull Collection<String> errors) {
        baseValidateByAction(subTaskFile::getId, actionType, errors);

        var subTask = subTaskFile.getSubTask();
        var currentUser = userService.getCurrentUserByContext();
        if (subTask == null || subTask.getId() == null) {
            errors.add("Не передана информация о подзадаче для файла " + subTaskFile.getFile().getFileName());
        } else {
            var savedSubTask = subTaskRepository.getById(subTask.getId());
            if (savedSubTask.getIsClosed()) {
                errors.add("Операция невозможна. Подзадача закрыта");
                if (actionType.equals(ActionType.DELETE) && !(subTaskFile.getFile().getCreatorUser().getId().equals(currentUser.getId()))) {
                    errors.add("Удалять файлы может только создатель файла");
                }
            }
        }
    }
}
