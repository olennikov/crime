package ru.tsu.crimelab.web.dto.file;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.InputStream;

/**
 * Created by PetrushinDA on 30.11.2019
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DownloadFileDto {
    private InputStream inputStream;
    private String fileName;
}
