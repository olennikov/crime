package ru.tsu.crimelab.web.services.user.statistics.models;

import lombok.Builder;
import lombok.Data;

/**
 * Created by PetrushinDA on 03.01.2020
 * Данные статистики по руководителям лаборатории
 */
@Data
public class DirectorStatistic extends UserStatistic {
    /**
     * Общее кол-во экспертиз, за которые ответственнен данный руководитель
     */
    private Long expertiseCount;

    /**
     * Кол-во закрытых экспертиз, за которые отвтственнен данный руководитель
     */
    private Long closedExpertiseCount;

    @Builder
    public DirectorStatistic(Long id, String lastName, String firstName, String secondName, String email, Boolean isBlock, String phone, Long expertiseCount, Long closedExpertiseCount) {
        super(id, lastName, firstName, secondName, email, isBlock, phone);
        this.expertiseCount = expertiseCount;
        this.closedExpertiseCount = closedExpertiseCount;
    }
}
