package ru.tsu.crimelab.web.services.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsu.crimelab.dao.api.Query;
import ru.tsu.crimelab.dao.api.repository.BaseRepository;
import ru.tsu.crimelab.dao.api.repository.response.QueryResponse;
import ru.tsu.crimelab.dao.domain.BaseEntity;
import ru.tsu.crimelab.dto.api.ModelNamedFactoryBean;
import ru.tsu.crimelab.web.services.StoreService;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Transactional
public class StoreServiceImpl implements StoreService {

    @Autowired
    private Map<String, BaseRepository> repositories;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ModelNamedFactoryBean modelFactoryBean;

    @Override
    public QueryResponse query(Query query, String repositoryName) {
        BaseRepository<BaseEntity<Serializable>,Serializable> baseRepository = getBaseRepository(repositoryName);
        QueryResponse<BaseEntity<Serializable>> entities = baseRepository.pagingQuery(query);
        if (query.getDtoName()!=null && modelFactoryBean.constainsModel(query.getDtoName())){
            List<?> dtoModels = entities.getRecords()
            .stream()
            .map(x->modelMapper.map(x,modelFactoryBean.getModel(query.getDtoName())))
            .collect(Collectors.toList());
            return new QueryResponse(entities.getCount(),dtoModels);
        }
        return entities;
    }

    @Override
    public Object create(String jsonObject, String repositoryName) {
        BaseRepository<BaseEntity<Serializable>,Serializable> baseRepository = getBaseRepository(repositoryName);
        BaseEntity<Serializable> entity = baseRepository.parseJsonEntity(jsonObject);
        baseRepository.save(entity);
        return entity;
    }


    @Override
    public BaseEntity<Serializable> read(Long id, String repositoryName) {
        BaseRepository<? extends BaseEntity<Serializable>,Serializable> baseRepository = getBaseRepository(repositoryName);
        BaseEntity<Serializable> entity = baseRepository.getById(id);
        return entity;
    }

    @Override
    public Object update(String jsonObject, String repositoryName) {
        BaseRepository<BaseEntity<Serializable>,Serializable> baseRepository = getBaseRepository(repositoryName);
        BaseEntity<Serializable> entity = baseRepository.parseJsonEntity(jsonObject);
        baseRepository.save(entity);
        return entity;
    }

    @Override
    public Long delete(Long id, String repositoryName) {
        BaseRepository<? extends BaseEntity<Serializable>, Serializable> baseRepository = getBaseRepository(repositoryName);
        baseRepository.deleteById(id);
        return id;
    }

    private BaseRepository<BaseEntity<Serializable>,Serializable> getBaseRepository (@NotNull String repositoryName){
        if (repositories.containsKey(repositoryName)){
            return (BaseRepository<BaseEntity<Serializable>,Serializable>)repositories.get(repositoryName);
        } else {
            throw new RuntimeException(String.format("Не удалось найти репозиторий с именем %s",repositoryName));
        }
    }
}
