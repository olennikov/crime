package ru.tsu.crimelab.web.dto.registration;

import lombok.*;
import ru.tsu.crimelab.dao.domain.RegistrationApplication;
import ru.tsu.crimelab.dto.api.Model;

@Data
@Model
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationApplicationModel {
    private String lastName;
    private String firstName;
    private String secondName;
    private String phone;
    private String email;
    private String organisationName;
    private String organisationPhone;
    private String organisationInn;
    private String organisationOgrn;
}
