package ru.tsu.crimelab.web.services;

import org.springframework.data.domain.Page;
import ru.tsu.crimelab.dao.api.Query;
import ru.tsu.crimelab.dao.api.repository.response.QueryResponse;
import ru.tsu.crimelab.dao.domain.BaseEntity;

import java.io.Serializable;

/**
 * Сервис связка между уровнем слоя данных и базового контроллера, реадизующего паатерн table
 */
public interface StoreService {

    /**
     * Метод для генерации запроса на основе Query
     * @param query - обънкт запроса на фронте
     * @return - возвращает список объектов выборки
     */
    QueryResponse<BaseEntity<Serializable>> query (Query query, String repositoryName);

    /**
     * Метод для сохранения объекта entity
     * @param jsonObject - объект Entity для сохранения в виде Json строки
     * @param reposiotryName - имя Bean репозитория для сохранения
     * @return - возвращает объект в виде entity или dto
     */
    Object create (String jsonObject, String reposiotryName);

    /**
     * Метод для чтения объекта по его идентификатору
     * @param id - идентификатор сущности
     * @param repositoryName - имя Bean репозитория
     * @return - возвращает объект сущности или dto по данному идентификатору
     */
    Object read (Long id, String repositoryName);

    /**
     * Метод для обновления объекта сущности
     * @param jsonObject - строка в виде Json объекта для обновления сущности
     * @param repositoryName - имя Bean репозитория
     * @return Возвращает обновлённый объект в виде dto или чистой сущности
     */
    Object update (String jsonObject, String repositoryName);

    /**
     * Метод для удаления объекта сущности по идентификатору
     * @param id - идентификатор удаляемой сущности
     * @param repositoryName имя bean репозитория
     * @return - возвращает идентификатор удаляемой записи
     */
    Long delete (Long id, String repositoryName);

}
