package ru.tsu.crimelab.web.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.tsu.crimelab.web.dto.substasks.SubTaskDto;
import ru.tsu.crimelab.web.services.SubTaskService;

import java.util.List;

/**
 * Created by PetrushinDA on 17.11.2019
 * Контроллер для работы с подазадачами
 */
@RestController
@RequestMapping("/subtasks")
@RequiredArgsConstructor
@Api(value = "Контроллер для работы с подзадачами")
public class SubTaskController {

    private final SubTaskService subtaskService;

    @PreAuthorize("hasAnyRole(T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).employeeRoleName)")
    @ApiOperation(value = "Получение подзадачи по идентифиактору")
    @GetMapping(value = "{id}")
    public SubTaskDto getSubtaskById(@PathVariable("id") Long id) {
        return subtaskService.findById(id);
    }

    @PreAuthorize("hasAnyRole(T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).employeeRoleName)")
    @ApiOperation(value = "Получение списка подзадач по идентификатору задачи")
    @GetMapping(value = "/task/{taskId}")
    public List<SubTaskDto> getSubTaskskByTaskId(@PathVariable("taskId") Long taskId) {
        return subtaskService.findByTaskId(taskId);
    }

    @PreAuthorize("hasAnyRole(T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).employeeRoleName)")
    @ApiOperation(value = "Обновление подзадачи")
    @PutMapping
    public SubTaskDto updateSubTask(@RequestBody SubTaskDto subTaskDto) {
        return subtaskService.updateSubTask(subTaskDto);
    }

    @PreAuthorize("hasAnyRole(T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).directorRoleName)")
    @ApiOperation(value = "Создание подзадачи")
    @PostMapping
    public SubTaskDto createSubTask(@RequestBody SubTaskDto subTaskDto) {
        return subtaskService.createSubTask(subTaskDto);
    }

    @PreAuthorize("hasAnyRole(T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).directorRoleName, " +
            "T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).employeeRoleName)")
    @ApiOperation(value = "Удаление подзадачи")
    @DeleteMapping
    public void deleteSubTask(@RequestBody SubTaskDto subTaskDto) {
        subtaskService.deleteSubTask(subTaskDto);
    }

    @PreAuthorize("hasAnyRole(T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).directorRoleName, " +
            "T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).employeeRoleName)")
    @ApiOperation(value = "Закрытие подзадачи")
    @PostMapping(value = "/close")
    public SubTaskDto closeSubTask(@RequestBody SubTaskDto subTaskDto) {
        return subtaskService.close(subTaskDto);
    }

    @PreAuthorize("hasAnyRole(T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).directorRoleName, " +
            "T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).employeeRoleName)")
    @ApiOperation(value = "Прикрепление файла к заявке на экспертизу клиентом")
    @PostMapping(value = "/{id}/upload")
    public SubTaskDto attachFileToSubTask(@PathVariable("id") Long id, @RequestParam("file") MultipartFile file) {
        return subtaskService.uploadFile(id, file);
    }
}
