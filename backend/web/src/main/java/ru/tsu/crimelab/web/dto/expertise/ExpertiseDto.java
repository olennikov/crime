package ru.tsu.crimelab.web.dto.expertise;

import lombok.Builder;
import lombok.Data;
import ru.tsu.crimelab.web.dto.user.UserDto;

import java.util.Date;

/**
 * Created by PetrushinDA on 17.11.2019
 */
@Data
@Builder
public class ExpertiseDto {
    public Long id;
    public Date receiveDate;
    public String name;
    public String description;
    public Long applicationId;
    public String status;
    public UserDto creator;
    public UserDto leader;
}
