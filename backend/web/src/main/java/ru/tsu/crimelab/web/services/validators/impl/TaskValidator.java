package ru.tsu.crimelab.web.services.validators.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import ru.tsu.crimelab.dao.domain.Task;
import ru.tsu.crimelab.dao.repository.ExpertiseRepository;
import ru.tsu.crimelab.dao.repository.TaskRepository;
import ru.tsu.crimelab.web.services.validators.ActionType;
import ru.tsu.crimelab.web.services.validators.Validator;

import java.util.Collection;

import static java.util.List.of;
import static ru.tsu.crimelab.dao.domain.enums.ExpertiseStatus.DONE;
import static ru.tsu.crimelab.dao.domain.enums.TaskStatus.CLOSED;
import static ru.tsu.crimelab.dao.domain.enums.TaskStatus.IN_WORK;
import static ru.tsu.crimelab.web.services.validators.ActionType.CREATE;
import static ru.tsu.crimelab.web.services.validators.ActionType.UPDATE;
import static ru.tsu.crimelab.web.services.validators.ValidatorUtils.baseValidateByAction;

/**
 * Created by PetrushinDA on 18.01.2020
 */
@Component
@RequiredArgsConstructor
public class TaskValidator extends Validator<Task> {

    private final TaskRepository taskRepository;

    private final ExpertiseRepository expertiseRepository;

    @Override
    protected void validate(@NonNull Task task, @NonNull ActionType actionType, @NonNull Collection<String> errors) {
        baseValidateByAction(task::getId, actionType, errors);

        if (of(UPDATE, CREATE).contains(actionType)) {
            var expertise = expertiseRepository.getById(task.getExpertise().getId());
            if (expertise == null) {
                errors.add("Не передана информация об экспертизе");
            } else {
                if (expertise.getStatus().equals(DONE)) {
                    errors.add("Невозможно производить манипуляции с задачами у закрытой экспертизы");
                }
            }
        }

        if (actionType.equals(UPDATE)) {
            var savedTask = taskRepository.getById(task.getId());
            if (savedTask == null) {
                errors.add("Задача не найдена");
            } else {
                if (task.getTaskStatus().equals(IN_WORK) && savedTask.getTaskStatus().equals(CLOSED)) {
                    errors.add("Невозможно редактировать закрытую задачу");
                }
            }
        }
    }
}
