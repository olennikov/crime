package ru.tsu.crimelab.web.controllers.director;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.tsu.crimelab.dao.api.Query;
import ru.tsu.crimelab.dao.api.repository.response.QueryResponse;
import ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationStatus;
import ru.tsu.crimelab.web.dto.director.ExpertiseApplicationResultDto;
import ru.tsu.crimelab.web.dto.director.RejectExpertiseApplicationDto;
import ru.tsu.crimelab.web.dto.expertiseapplication.ExpertiseApplicationDto;
import ru.tsu.crimelab.web.dto.expertiseapplication.ExpertiseApplicationView;
import ru.tsu.crimelab.web.services.ExpertiseApplicationService;


@RestController("director-expertise-applications")
@RequestMapping("/director/expertise-applications")
@RequiredArgsConstructor
@PreAuthorize("hasRole(T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).directorRoleName)")
@Api(value = "Контроллер для работы с заявками на экспертизу руководителем")
public class ExpertiseApplicationController {

    private final ExpertiseApplicationService expertiseApplicationService;

    @ApiOperation(value = "Получение списка ")
    @PostMapping(value = "/director-application-list")
    public QueryResponse<ExpertiseApplicationView> getExpertiseApplicationList(@RequestBody @ApiParam("Сущность запроса с фронта") Query query) {
        return expertiseApplicationService.getExpertiseApplicationListForDirector(query);
    }

    @GetMapping("{id}")
    @ApiOperation(value = "Получение объекта заявки экспертизы")
    public ExpertiseApplicationDto getExpertiseApplicationById(@ApiParam(value = "Идентификатор сущности") @PathVariable Long id) {
        return expertiseApplicationService.getExpertiseApplicationDtoById(id);
    }

    @PostMapping("reject")
    @ApiOperation(value = "Отказ по проведению экспертизы")
    public Long rejectExpertiseApplication(@RequestBody @ApiParam("Сущность для отказа в проведении экспертизы") RejectExpertiseApplicationDto rejectApplication) {
        return expertiseApplicationService.rejectExpertiseApplication(rejectApplication);
    }

    @PostMapping("accept")
    @ApiOperation("Метод для принятия заявки")
    public ExpertiseApplicationStatus acceptExpertiseApplication(@RequestBody Long applicationId) {
        return expertiseApplicationService.setAcceptApplication(applicationId);
    }

    @PostMapping("in-work")
    @ApiOperation("Метод взятия в работу")
    public ExpertiseApplicationStatus inWorkExpertiseAplication(@RequestBody Long applicationId) {
        return expertiseApplicationService.setInWorkApplication(applicationId);
    }

    @PutMapping("update-result")
    @ApiOperation("Метод для обновления результата")
    public Long updateResultApplication(@RequestBody ExpertiseApplicationResultDto expertiseApplicationResultDto) {
        return expertiseApplicationService.updateResultExpertiseApplication(expertiseApplicationResultDto);
    }

    @GetMapping("get-result/{id}")
    @ApiOperation("Метод для получения результата по экспертизе")
    public ExpertiseApplicationResultDto getResultApplication(@PathVariable("id") Long id) {
        return expertiseApplicationService.getExpertiseResult(id);
    }

    @PostMapping("upload-result-file/{id}")
    @ApiOperation("Операция для загрузки файлов результата")
    public Long uploadResultFile(@PathVariable("id") Long id, @RequestParam("file") MultipartFile file) {
        return expertiseApplicationService.saveResultFiles(id, file);
    }

    @PostMapping("close-application/{applicationId}")
    @ApiOperation("Операция для заякрытия заявки")
    public ExpertiseApplicationStatus closeExpertiseApplication(@PathVariable("applicationId") Long applicationId) {
        return expertiseApplicationService.closeExpertiseApplication(applicationId);
    }
}
