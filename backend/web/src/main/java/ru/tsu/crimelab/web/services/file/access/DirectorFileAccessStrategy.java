package ru.tsu.crimelab.web.services.file.access;

import org.springframework.stereotype.Service;
import ru.tsu.crimelab.dao.domain.User;
import ru.tsu.crimelab.dao.domain.enums.Role;

/**
 * Created by PetrushinDA on 07.12.2019
 * Стратегия проверки прав доступа для скачивания файлов директором
 */
@Service
public class DirectorFileAccessStrategy implements FileCheckAccessStrategy {
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canHandle(User user) {
        return user.getRole().equals(Role.DIRECTOR);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canDownload(User user, Long fileId) {
        return true;
    }
}
