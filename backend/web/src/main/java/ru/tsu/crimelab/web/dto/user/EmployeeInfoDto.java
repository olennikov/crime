package ru.tsu.crimelab.web.dto.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * Created by PetrushinDA on 29.12.2019
 */
@Data
@NoArgsConstructor
@SuperBuilder
@ApiModel(value = "EmployeeInfoDto: Информация о сотруднике лаборатории", description = "Информация о сотруднике лаборатории")
public class EmployeeInfoDto extends UserDto {

    @ApiModelProperty(required = true, value = "email")
    private String email;

    @ApiModelProperty(required = true, value = "Общее количество задач")
    private Long taskCount;

    @ApiModelProperty(required = true, value = "Количество закрытых задач")
    private Long closedTaskCount;

    @ApiModelProperty(required = true, value = "Общее количество подзадач")
    private Long subTaskCount;

    @ApiModelProperty(required = true, value = "Количество закрытых подзадач")
    private Long closedSubTaskCount;

    @ApiModelProperty(required = true, value = "Заблокирована ли учетная запись")
    private Boolean isBlock;
}
