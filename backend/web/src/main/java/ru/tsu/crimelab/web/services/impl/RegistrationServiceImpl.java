package ru.tsu.crimelab.web.services.impl;

import org.modelmapper.ModelMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsu.crimelab.appstarter.rabbitmq.consts.notification.Queues;
import ru.tsu.crimelab.dao.api.Query;
import ru.tsu.crimelab.dao.api.repository.response.QueryResponse;
import ru.tsu.crimelab.dao.domain.Organisation;
import ru.tsu.crimelab.dao.domain.RegistrationApplication;
import ru.tsu.crimelab.dao.domain.User;
import ru.tsu.crimelab.dao.domain.enums.RegistrationApplicationStatus;
import ru.tsu.crimelab.dao.domain.enums.Role;
import ru.tsu.crimelab.dao.repository.OrganisationRepository;
import ru.tsu.crimelab.dao.repository.RegistrationApplicationRepository;
import ru.tsu.crimelab.dao.repository.UserRepository;
import ru.tsu.crimelab.dto.model.EmailMessageDto;
import ru.tsu.crimelab.web.dto.director.RegistrationDirectorModel;
import ru.tsu.crimelab.web.dto.registration.RegistrationApplicationModel;
import ru.tsu.crimelab.web.dto.registration.RegistrationApplicationView;
import ru.tsu.crimelab.web.dto.registration.RegistrationUserDto;
import ru.tsu.crimelab.web.dto.registration.RejectRegistrationDto;
import ru.tsu.crimelab.web.services.RegistrationService;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class RegistrationServiceImpl implements RegistrationService {
    @Value("${registration.accepted.message.email}")
    private String acceptedMessage;
    @Value("${registration.denied.message.email}")
    private String rejectMessage;
    @Value("${registration.email.subjectmessage}")
    private String subjectMessage;
    @Value("${application.domain.name}")
    private String domainName;

    @Autowired
    private RegistrationApplicationRepository registrationRepostiory;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private OrganisationRepository organisationRepository;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private ModelMapper modelMapper;


    @Override
    public Long createApplicationRegistration(RegistrationApplicationModel model) {
        RegistrationApplication registrationApplication = modelMapper.map(model, RegistrationApplication.class);
        registrationApplication.setReceiveDate(new Date());
        registrationApplication.setStatus(RegistrationApplicationStatus.NEW);
        registrationRepostiory.save(registrationApplication);
        return registrationApplication.getId();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long rejectApplicationRegistration(RejectRegistrationDto model) {
        RegistrationApplication registrationApplication = getUnresolvedRegistrationApplication(model.getApplicationId());
        registrationApplication.setStatus(RegistrationApplicationStatus.DENIED);
        registrationApplication.setRejectComment(model.getRejectComment());
        registrationRepostiory.save(registrationApplication);
        rabbitTemplate.convertAndSend(Queues.EMAIL, createRejectedEmail(registrationApplication));
        return registrationApplication.getId();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long acceptApplicationRegistration(Long applicationId) {
        RegistrationApplication registrationApplication = getUnresolvedRegistrationApplication(applicationId);
        String disposableId = UUID.randomUUID().toString();
        String disposableUrl = domainName + String.format("?disposableUrl=%s", disposableId);
        registrationApplication.setStatus(RegistrationApplicationStatus.ACCEPTED);
        registrationApplication.setChangePasswordUrl(disposableId);
        registrationApplication.setChangePasswordReceiveDate(new Date());
        registrationRepostiory.save(registrationApplication);
        rabbitTemplate.convertAndSend(Queues.EMAIL, createAcceptedEmail(disposableUrl, registrationApplication));
        return registrationApplication.getId();
    }

    @Override
    @Transactional(readOnly = true)
    public RegistrationApplicationModel getAcceptableRegistrationByDisposableUri(String disposableUrl) {
        RegistrationApplication registrationApplication = registrationRepostiory.findOneByChangePasswordUrlAndStatus(disposableUrl, RegistrationApplicationStatus.ACCEPTED);
        if (registrationApplication != null) {
            return RegistrationApplicationModel.builder()
                    .lastName(registrationApplication.getLastName())
                    .firstName(registrationApplication.getFirstName())
                    .secondName(registrationApplication.getSecondName())
                    .email(registrationApplication.getEmail())
                    .phone(registrationApplication.getPhone())
                    .organisationInn(registrationApplication.getOrganisationInn())
                    .organisationName(registrationApplication.getOrganisationName())
                    .organisationOgrn(registrationApplication.getOrganisationOgrn())
                    .organisationPhone(registrationApplication.getOrganisationPhone())
                    .build();
        } else {
            throw new IllegalAccessError(String.format("Одобренной заявки на регистрацию %s не существует", disposableUrl));
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long closeApplicationRegistration(RegistrationUserDto registrationUserDto) {
        RegistrationApplication registrationApplication = registrationRepostiory
                .findOneByChangePasswordUrlAndStatus(registrationUserDto.getChangePasswordUrl(), RegistrationApplicationStatus.ACCEPTED);
        if (registrationApplication != null) {
            Organisation organisation = organisationRepository.findOneByInnOrOgrn(registrationApplication.getOrganisationInn(),
                    registrationApplication.getOrganisationOgrn());
            if (organisation == null) {
                organisation = Organisation.builder()
                        .inn(registrationApplication.getOrganisationInn())
                        .name(registrationApplication.getOrganisationName())
                        .ogrn(registrationApplication.getOrganisationOgrn())
                        .phone(registrationApplication.getOrganisationPhone())
                        .build();
                organisationRepository.save(organisation);
            }
            User user = User.builder()
                    .registrateBuilder(registrationApplication)
                    .role(Role.CUSTOMER)
                    .password(registrationUserDto.getUserPassword())
                    .salt(registrationUserDto.getUserSalt())
                    .organisation(organisation)
                    .isBlock(false)
                    .build();
            userRepository.save(user);
            registrationApplication.setChangePasswordUrl(null);
            registrationApplication.setChangePasswordReceiveDate(null);
            registrationApplication.setStatus(RegistrationApplicationStatus.CREATED);
            registrationRepostiory.save(registrationApplication);
            return registrationApplication.getId();
        } else {
            throw new IllegalAccessError(String.format("Не найден запрос на регистрацию с ссылкой %s", registrationUserDto.getChangePasswordUrl()));
        }
    }

    @Override
    @Transactional(readOnly = true)
    public QueryResponse<RegistrationApplicationView> getRegistrationApplicationView(Query query) {
        QueryResponse<RegistrationApplication> entityResponse = registrationRepostiory.pagingQuery(query);
        List<RegistrationApplicationView> registrationViewList = entityResponse.getRecords()
                .stream()
                .map(x -> RegistrationApplicationView.builder().registrationBuilder(x).build())
                .collect(Collectors.toList());
        QueryResponse<RegistrationApplicationView> queryResponse = new QueryResponse<>(entityResponse.getCount(), registrationViewList);
        return queryResponse;
    }

    @Override
    @Transactional(readOnly = true)
    public RegistrationDirectorModel getRegistrationById(Long id) {
        Optional<RegistrationApplication> optional = registrationRepostiory.findById(id);
        RegistrationApplication application = optional
                .orElseThrow(() -> new IllegalAccessError(String.format("Заявка на регистрацию с идентификатором %s не существует", id)));
        RegistrationDirectorModel model = RegistrationDirectorModel.builder().id(application.getId())
                .status(application.getStatus())
                .lastName(application.getLastName())
                .firstName(application.getFirstName())
                .secondName(application.getSecondName())
                .phone(application.getPhone())
                .email(application.getEmail())
                .organisationName(application.getOrganisationName())
                .organisationPhone(application.getOrganisationPhone())
                .organisationInn(application.getOrganisationInn())
                .organisationOgrn(application.getOrganisationOgrn())
                .rejectReason(application.getRejectComment())
                .build();
        return model;
    }


    private RegistrationApplication getUnresolvedRegistrationApplication(Long id) {
        RegistrationApplication registrationApplication = registrationRepostiory.getById(id);
        if (registrationApplication != null &&
                RegistrationApplicationStatus.NEW.equals(registrationApplication.getStatus())) {
            return registrationApplication;
        } else {
            throw new IllegalAccessError(String.format("Нельзя изменить статус у уже закрытого запроса на регистрацию (Идентификатор %s)", id));
        }
    }


    private EmailMessageDto createAcceptedEmail(String disposableUri, RegistrationApplication registrationApplication) {
        EmailMessageDto emailMessageDto = new EmailMessageDto();
        emailMessageDto.setEmail(registrationApplication.getEmail());
        emailMessageDto.setText(String.format(acceptedMessage, disposableUri));
        emailMessageDto.setSubject(subjectMessage);
        return emailMessageDto;
    }

    private EmailMessageDto createRejectedEmail(RegistrationApplication registrationApplication) {
        EmailMessageDto emailMessageDto = new EmailMessageDto();
        emailMessageDto.setSubject(subjectMessage);
        emailMessageDto.setText(rejectMessage + registrationApplication.getRejectComment());
        emailMessageDto.setEmail(registrationApplication.getEmail());
        return emailMessageDto;
    }
}
