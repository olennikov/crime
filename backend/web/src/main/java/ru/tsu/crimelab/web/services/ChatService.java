package ru.tsu.crimelab.web.services;

import org.springframework.lang.NonNull;
import ru.tsu.crimelab.dao.domain.Chat;
import ru.tsu.crimelab.dao.domain.ExpertiseApplication;
import ru.tsu.crimelab.dao.domain.User;
import ru.tsu.crimelab.web.dto.chat.ChatDto;
import ru.tsu.crimelab.web.dto.chat.MessageDto;

import java.util.List;
import java.util.Optional;

public interface ChatService {

    /**
     * Поиск чата по идентификатору
     *
     * @param id идентификатор чата
     * @return объект чата
     */
    Optional<Chat> getById(@NonNull Long id);

    /**
     * Загрузка сообщений для отображения
     *
     * @param expertiseApplicationId идентификатор экспертизы
     * @param skip                   количество пропущенных сообщений
     * @param take                   количество загружаемых сообщений
     * @return список загруженных сообщений для отображения
     */
    List<MessageDto> getMessages(Long expertiseApplicationId, Long skip, Long take);

    /**
     * Добавление сообщения в чат
     *
     * @param expertiseApplicationId идентификатор экспертизы
     * @param message                сообщение для добавления
     * @return добавленное сообщение
     */
    MessageDto addMessage(Long expertiseApplicationId, MessageDto message);

    /**
     * Получение чата для экспертизы
     *
     * @param expertiseApplicationId идентификатор заявки на экспертизу
     * @return чат, соответствующий экспертизе
     */
    ChatDto getChatByExpertiseApplication(Long expertiseApplicationId);

    /**
     * Создание чата для заявки
     *
     * @param expertiseApplication сущность заявки на экспертизу
     * @param leaderUser           сущность пользователя, ответственного за заявку
     * @return
     */
    Long createChat(ExpertiseApplication expertiseApplication, User leaderUser);

    /**
     * Проверка является ли текущий пользователь участником чата
     *
     * @param expertiseApplicationId идентификатор экспертизы
     * @return true, если пользователь является участником чата
     */
    boolean isChatParticipant(Long expertiseApplicationId);
}
