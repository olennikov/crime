package ru.tsu.crimelab.web.services.validators;

import org.springframework.lang.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by PetrushinDA on 15.01.2020
 */
public abstract class Validator<T> {

    protected abstract void validate(@NonNull T object, @NonNull ActionType actionType, @NonNull Collection<String> errors);

    public final void validate(@NonNull T object, @NonNull ActionType actionType) {
        List<String> errors = new ArrayList<>();

        validate(object, actionType, errors);

        if (!errors.isEmpty()) {
            throw new ValidationException(errors);
        }
    }
}
