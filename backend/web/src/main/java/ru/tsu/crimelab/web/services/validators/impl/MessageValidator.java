package ru.tsu.crimelab.web.services.validators.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import ru.tsu.crimelab.dao.domain.Message;
import ru.tsu.crimelab.dao.domain.User;
import ru.tsu.crimelab.dao.repository.ChatRepository;
import ru.tsu.crimelab.web.services.ChatService;
import ru.tsu.crimelab.web.services.UserService;
import ru.tsu.crimelab.web.services.validators.ActionType;
import ru.tsu.crimelab.web.services.validators.Validator;

import java.util.Collection;

import static java.util.stream.Collectors.toList;
import static ru.tsu.crimelab.web.services.validators.ActionType.DELETE;
import static ru.tsu.crimelab.web.services.validators.ValidatorUtils.baseValidateByAction;

/**
 * Created by PetrushinDA on 18.01.2020
 * Валидатор сообщений
 */
@Component
@RequiredArgsConstructor
public class MessageValidator extends Validator<Message> {

    private final ChatRepository chatService;

    private final UserService userService;

    @Override
    protected void validate(@NonNull Message message, @NonNull ActionType actionType, @NonNull Collection<String> errors) {
        baseValidateByAction(message::getId, actionType, errors);

        var chat = chatService.findById(message.getChat().getId()).orElseGet(() -> {
            errors.add("Чат для сообщения не найден");
            return null;
        });

        if (chat != null) {
            var participantIds = chat.getUsers().stream()
                    .map(User::getId)
                    .collect(toList());

            if (!participantIds.contains(userService.getCurrentUserIdByContext())) {
                errors.add("Вы не являетесь участником чата");
                return;
            }
        }

        if (actionType.equals(DELETE)) {
            errors.add("Операция удаления сообщения невозможна");
        }
    }
}
