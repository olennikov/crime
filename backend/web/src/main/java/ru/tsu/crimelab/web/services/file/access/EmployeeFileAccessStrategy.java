package ru.tsu.crimelab.web.services.file.access;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tsu.crimelab.dao.domain.ExpertiseApplicationFile;
import ru.tsu.crimelab.dao.domain.User;
import ru.tsu.crimelab.dao.domain.enums.Role;
import ru.tsu.crimelab.dao.repository.ExpertiseApplicationFileRepository;
import ru.tsu.crimelab.dao.repository.ExpertiseRepository;
import ru.tsu.crimelab.dao.repository.SubTaskFileRepository;
import ru.tsu.crimelab.dao.repository.TaskFileRepository;

/**
 * Created by PetrushinDA on 07.12.2019
 * Стратегия проверки прав доступа для скачивания файлов сотрудником
 */
@Service
@RequiredArgsConstructor
public class EmployeeFileAccessStrategy implements FileCheckAccessStrategy {

    private final TaskFileRepository taskFileRepository;
    private final SubTaskFileRepository subTaskFileRepository;
    private final ExpertiseApplicationFileRepository expertiseApplicationFileRepository;
    private final ExpertiseRepository expertiseRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canHandle(User user) {
        return user.getRole().equals(Role.EMPLOYEE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canDownload(User user, Long fileId) {
        return canDownloadFileSubTask(user, fileId)
                || canDownloadFileExpertiseApplication(user, fileId)
                || canDownloadTaskFile(user,fileId);
    }

    private boolean canDownloadFileSubTask(User user, Long fileId) {
        return subTaskFileRepository.findByFile_Id(fileId)
                .map(x -> x.getSubTask().getTask().getResponsibleUser().getId().equals(user.getId()))
                .orElse(false);
    }

    private boolean canDownloadTaskFile (User user, Long fileId){
        return taskFileRepository.findByFile_Id(fileId)
                .map(x -> x.getTask().getResponsibleUser().getId().equals(user.getId()))
                .orElse(false);
    }

    private boolean canDownloadFileExpertiseApplication(User user, Long fileId) {
        return expertiseApplicationFileRepository.findByFile_Id(fileId)
                .map(ExpertiseApplicationFile::getExpertiseApplication)
                .map(x -> expertiseRepository.findByExpertiseApplicationId(x.getId())
                        .map(expertise -> expertise.getExpertiseTasks().stream().anyMatch(y -> y.getResponsibleUser().getId().equals(user.getId())))
                        .orElse(false))
                .orElse(false);
    }
}
