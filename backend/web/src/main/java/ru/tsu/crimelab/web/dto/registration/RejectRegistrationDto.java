package ru.tsu.crimelab.web.dto.registration;

import lombok.Data;

@Data
public class RejectRegistrationDto {
    private Long applicationId;
    private String rejectComment;
}
