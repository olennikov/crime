package ru.tsu.crimelab.web.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.tsu.crimelab.dao.api.Query;
import ru.tsu.crimelab.dao.api.repository.response.QueryResponse;
import ru.tsu.crimelab.web.services.StoreService;

import java.io.Serializable;

@RestController
@RequestMapping("/table/{table}/")
@RequiredArgsConstructor
@Api(value = "Обобщённый доступ к данным системы", description = "Реализация паттерна api/table")
public class StoreController {

    private final StoreService storeService;

    @PostMapping(value = "query")
    @ApiOperation(value = "Запрос списка сущностей по объекту запроса, возвращает список объектов")
    public QueryResponse getRecordsByQuery(
            @ApiParam(value = "Название сервиса репозитория в системе", required = true) @PathVariable("table") String table,
            @ApiParam(value = "Объект запроса body") @RequestBody Query query) {
        return storeService.query(query, table);
    }

    @PostMapping
    @ApiOperation(value = "Запрос на создание сущности")
    public Object create(@ApiParam(value = "Название сервиса репозитория в системе", required = true) @PathVariable("table") String table,
                         @ApiParam(value = "Сущность в формате Json для сохранения", required = true) @RequestBody String json) {
        return storeService.create(json, table);
    }

    @ResponseBody
    @GetMapping(value = "{id}")
    @ApiOperation(value = "Запрос на получение сущности по её идентификатору")
    public Object read(@ApiParam(value = "Название сервиса репозитория в системе", required = true) @PathVariable("table") String table,
                       @ApiParam(value = "Идентификатор объекта", required = true) @PathVariable("id") Long id) {
        return storeService.read(id, table);
    }

    @ResponseBody
    @DeleteMapping
    @ApiOperation("Запрос на удаление объекта из системы")
    public Serializable delete(@ApiParam(value = "Название сервиса репозитория в системе", required = true) @PathVariable("table") String table,
                               @ApiParam(value = "Идентификатор объекта", required = true) @PathVariable("id") Long id) {
        return storeService.delete(id, table);
    }

    @ResponseBody
    @PutMapping
    @ApiOperation("Запрос на обновление объекта системы")
    public Object update(@ApiParam(value = "Название сервиса репозитория в системе", required = true) @PathVariable("table") String table,
                         @ApiParam(value = "Сущность в формате Json для обновления данных", required = true) @RequestBody String json) {
        return storeService.update(json, table);
    }


}
