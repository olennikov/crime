package ru.tsu.crimelab.web.services.validators.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import ru.tsu.crimelab.dao.domain.Chat;
import ru.tsu.crimelab.dao.repository.ExpertiseApplicationRepository;
import ru.tsu.crimelab.web.services.validators.ActionType;
import ru.tsu.crimelab.web.services.validators.Validator;

import java.util.Collection;

import static java.util.List.of;
import static ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationStatus.CLOSED;
import static ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationStatus.REJECT;
import static ru.tsu.crimelab.web.services.validators.ActionType.CREATE;
import static ru.tsu.crimelab.web.services.validators.ValidatorUtils.baseValidateByAction;

/**
 * Created by PetrushinDA on 19.01.2020
 */
@Component
@RequiredArgsConstructor
public class ChatValidator extends Validator<Chat> {

    private final ExpertiseApplicationRepository expertiseApplicationRepository;

    @Override
    protected void validate(@NonNull Chat chat, @NonNull ActionType actionType, @NonNull Collection<String> errors) {
        baseValidateByAction(chat::getId, actionType, errors);

        if (actionType.equals(CREATE)) {
            var expertiseApplication = chat.getExpertiseApplication();
            if (expertiseApplication == null || expertiseApplication.getId() == null) {
                errors.add("Не передана информация о заявки на проведение экспертизы");
            } else {
                var savedExpertiseApplicaiton = expertiseApplicationRepository.getById(chat.getExpertiseApplication().getId());
                if (of(CLOSED, REJECT).contains(savedExpertiseApplicaiton.getStatus())) {
                    errors.add("Невозможно создать чат для закрытой заявки на проведение экспертизы");
                }
            }
        }
    }
}
