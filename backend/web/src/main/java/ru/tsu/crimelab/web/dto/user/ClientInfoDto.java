package ru.tsu.crimelab.web.dto.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * Created by PetrushinDA on 29.12.2019
 */
@Data
@SuperBuilder
@NoArgsConstructor
@ApiModel(value = "ClientInfoDto: Информация о клиенте лаборатории", description = "Информация о клиенте лаборатории")
public class ClientInfoDto extends UserDto {

    @ApiModelProperty(required = true, value = "email пользователя")
    private String email;

    @ApiModelProperty(required = true, value = "Заблокирована ли учетная запись")
    private Boolean isBlock;

    @ApiModelProperty(required = true, value = "Количество поданных заявлений")
    private Long applicationCount;
}
