package ru.tsu.crimelab.web.services.impl;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import ru.tsu.crimelab.dao.domain.*;
import ru.tsu.crimelab.dao.domain.enums.ExpertiseStatus;
import ru.tsu.crimelab.dao.domain.enums.TaskStatus;
import ru.tsu.crimelab.dao.repository.*;
import ru.tsu.crimelab.web.dto.director.CreateSubTaskDto;
import ru.tsu.crimelab.web.dto.director.CreateTaskDto;
import ru.tsu.crimelab.web.dto.director.TaskDto;
import ru.tsu.crimelab.web.dto.file.FileDto;
import ru.tsu.crimelab.web.dto.substasks.SubTaskDto;
import ru.tsu.crimelab.web.services.FileService;
import ru.tsu.crimelab.web.services.TaskService;
import ru.tsu.crimelab.web.services.UserService;
import ru.tsu.crimelab.web.services.Util;
import ru.tsu.crimelab.web.services.validators.impl.TaskFileValidator;
import ru.tsu.crimelab.web.services.validators.impl.TaskValidator;

import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static ru.tsu.crimelab.web.services.validators.ActionType.DELETE;
import static ru.tsu.crimelab.web.services.validators.ActionType.UPDATE;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

    private final ExpertiseRepository expertiseRepository;

    private final UserRepository userRepository;

    private final TaskRepository taskRepository;

    private final FileService fileService;

    private final SubTaskRepository subTaskRepository;

    private final UserService userService;

    private final ModelMapper modelMapper;

    private final TaskValidator validator;

    private final TaskFileValidator taskFileValidator;

    private final TaskFileRepository taskFileRepository;

    @Override
    @Transactional(readOnly = true)
    public List<CreateTaskDto> getTasksByExpertise(Long expertiseId) {
        List<CreateTaskDto> dtos = taskRepository.getTasksByExpertise(expertiseId)
                .stream()
                .map(x -> CreateTaskDto.builder()
                        .id(x.getId())
                        .name(x.getName())
                        .description(x.getDescription())
                        .responsibleUserId(x.getResponsibleUser().getId())
                        .taskStatus(x.getTaskStatus())
                        .build())
                .collect(Collectors.toList());
        return dtos;
    }

    @Override
    @Transactional(readOnly = true)
    public TaskDto getTaskById(Long taskId) {
        Optional<Task> optional = taskRepository.findById(taskId);
        Task task = optional.orElseThrow(() -> new IllegalAccessError(format("Задача с идентификатором %s не существует", taskId)));

        TaskDto taskDto = TaskDto.builder()
                .id(task.getId())
                .name(task.getName())
                .description(task.getDescription())
                .taskStatus(task.getTaskStatus())
                .deadlineDate(task.getDeadlineDate())
                .taskFiles(task.getTaskFiles()
                        .stream()
                        .map(x->modelMapper.map(x.getFile(),FileDto.class))
                        .collect(Collectors.toList()))
                .responsibleUserId(task.getResponsibleUser() == null
                        ? null
                        : task.getResponsibleUser().getId())

                .subTaskArray(task.getSubTasks()
                        .stream().map(x -> CreateSubTaskDto.builder()
                                .id(x.getId())
                                .name(x.getName())
                                .description(x.getDescription())
                                .deadlineDate(x.getDeadlineDate())
                                .isClosed(x.getIsClosed())
                                .comment(x.getComment())
                                .files(x.getSubTaskFiles()
                                        .stream()
                                        .map(y -> modelMapper.map(y.getFile(), FileDto.class))
                                        .collect(Collectors.toList()))
                                .build())
                        .collect(Collectors.toList()))
                .build();
        return taskDto;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long saveSubtask(TaskDto taskDto) {
        Optional<Task> optional = taskRepository.findById(taskDto.getId());
        Task task = optional.orElseThrow(() -> new IllegalAccessError(format("Задача с идентификатором %s отсуствует", taskDto.getId())));
        List<SubTask> subTasks = taskDto.getSubTaskArray().stream()
                .map(x -> SubTask.builder()
                        .comment(x.getComment())
                        .description(x.getDescription())
                        .name(x.getName())
                        .task(task)
                        .build())
                .collect(Collectors.toList());

        subTaskRepository.saveAll(subTasks);
        return task.getId();
    }

    /**
     * Выгрузка задачи по идентификатору задачи, у которой в качестве ответственного установлен текущий пльзователь
     *
     * @param taskId идентификатор задачи
     * @return объект задачи
     */
    @Override
    @Transactional(readOnly = true)
    public ru.tsu.crimelab.web.dto.task.TaskDto getByIdAndCurrentUserAsResponsible(Long taskId) {
        var currentUserId = userService.getCurrentUserIdByContext();
        var task = taskRepository.getByIdAndResponsibleUser_Id(taskId, currentUserId);
        return Optional.ofNullable(task)
                .map(Optional::get)
                .map(x -> modelMapper.map(x, ru.tsu.crimelab.web.dto.task.TaskDto.class))
                .orElse(null);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void close(Long taskId) {
        var task = taskRepository.findById(taskId)
                .orElseThrow(() -> new IllegalArgumentException(format("Задача с идентификатором %s не найдена", taskId)));
        task.setTaskStatus(TaskStatus.CLOSED);

        validator.validate(task, UPDATE);
        taskRepository.save(task);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long createTask(CreateTaskDto createTaskDto) {
        Expertise expertise = Optional.ofNullable(createTaskDto.getExpertiseId())
                .map(expertiseRepository::findById)
                .orElseThrow(() -> new IllegalArgumentException("Указан пустой объект"))
                .orElseThrow(() -> new IllegalArgumentException("Объект экспертизы не существует"));
        User user = Optional.ofNullable(createTaskDto.getResponsibleUserId())
                .map(userRepository::findById)
                .orElseThrow(() -> new IllegalArgumentException("Не указан идентификатор ответственного пользователя"))
                .orElseThrow(() -> new IllegalArgumentException("Отсутствует пользователь с данным идентификатором"));
        if (!expertise.getStatus().equals(ExpertiseStatus.DONE)) {
            Task saveTask = Task.builder()
                    .deadlineDate(createTaskDto.getDeadlineDate())
                    .description(createTaskDto.getDescription())
                    .expertise(expertise)
                    .name(createTaskDto.getName())
                    .responsibleUser(user)
                    .taskStatus(TaskStatus.NEW)
                    .build();
            taskRepository.save(saveTask);
            return saveTask.getId();
        } else {
            throw new IllegalArgumentException("Нельзя создавать новые задачи у завершёных экспертиз");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ru.tsu.crimelab.web.dto.task.TaskDto attachFileToTask(Long taskId, MultipartFile file) {
        Task task = Optional.ofNullable(taskId)
                .map(taskRepository::findById)
                .orElseThrow(()->new IllegalArgumentException("Указан пустой объект"))
                .orElseThrow(()->new IllegalArgumentException(String.format("Объект задачи с идентификатором %s не существует",taskId)));

        String fileName = Util.buildFilePath(task.getExpertise().getExpertiseApplication(), file);
        ru.tsu.crimelab.dao.domain.File uploadedFile = fileService.uploadFile(fileName, file);

        TaskFile taskFile = TaskFile.builder()
                .file(uploadedFile)
                .task(task)
                .build();


        taskFileRepository.save(taskFile);
        return modelMapper.map(task, ru.tsu.crimelab.web.dto.task.TaskDto.class);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteFile(Long fileId, Long taskId) {
        TaskFile taskFile = taskFileRepository.findByFile_Id(fileId)
                .orElseThrow(()-> new IllegalArgumentException("Данный файл не существует"));

        taskFileValidator.validate(taskFile, DELETE);
        taskFileRepository.delete(taskFile);
        fileService.deleteFile(taskFile.getFile());
    }
}
