package ru.tsu.crimelab.web.services.validators;

import org.springframework.lang.NonNull;

import java.util.Collection;
import java.util.function.Supplier;

import static ru.tsu.crimelab.web.services.validators.ActionType.*;


/**
 * Created by PetrushinDA on 15.01.2020
 */
public class ValidatorUtils {
    private ValidatorUtils() {

    }

    public static <T> void baseValidateByAction(@NonNull Supplier<T> getId, @NonNull ActionType actionType, Collection<String> errors) {
        if (actionType.equals(CREATE) && getId.get() != null) {
            errors.add("При создании сущности идентификатор должен быть пустым");
        }
        if ((actionType.equals(UPDATE) || actionType.equals(DELETE)) && getId.get() == null) {
            errors.add("При обновлении/удалении идентификатор объекта не должен быть пустым");
        }
    }
}
