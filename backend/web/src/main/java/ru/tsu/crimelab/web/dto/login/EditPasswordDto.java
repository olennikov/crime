package ru.tsu.crimelab.web.dto.login;

import lombok.Data;

@Data
public class EditPasswordDto {
    private String newSalt;
    private String newPassword;
    private String oldPassword;
    private String roundSalt;
}
