package ru.tsu.crimelab.web.dto.director;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.crimelab.dao.domain.enums.TaskStatus;
import ru.tsu.crimelab.web.dto.file.FileDto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class TaskDto {
    private Long id;
    private String name;
    private String description;
    private TaskStatus taskStatus;
    private Long responsibleUserId;
    private Date deadlineDate;
    private List<CreateSubTaskDto> subTaskArray = new ArrayList();
    private List<FileDto> taskFiles = new ArrayList();
}
