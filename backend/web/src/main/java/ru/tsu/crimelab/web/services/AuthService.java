package ru.tsu.crimelab.web.services;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import ru.tsu.crimelab.web.dto.login.EditPasswordDto;
import ru.tsu.crimelab.web.dto.login.LoginDto;
import ru.tsu.crimelab.web.dto.login.LoginSaltDto;
import ru.tsu.crimelab.web.dto.user.UserProfileDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by PetrushinDA on 13.10.2019
 */
public interface AuthService {
    Authentication login(LoginDto loginDto);

    void logout(HttpServletRequest request, HttpServletResponse response);
    /**
     * Метод для отправки соли для генерации пары логина пароля.
     * @param userEmail - логин пользователя
     * @return - возвращает две соли - постоянную, для генерации хэша пароля и раундовую, для сокрытия хэша пароля
     */
    LoginSaltDto getSalt (String userEmail);
    /**
     * Метод для получения сведений о пользователе по его токену
     * @return сведения о пользователе в формате Json
     */
    String getUserDetails (SecurityContext securityContext);
}
