package ru.tsu.crimelab.web.mapper.config.converters;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.tsu.crimelab.dao.domain.SubTask;
import ru.tsu.crimelab.dao.domain.Task;
import ru.tsu.crimelab.web.dto.file.FileDto;
import ru.tsu.crimelab.web.dto.substasks.SubTaskDto;
import ru.tsu.crimelab.web.mapper.config.ConverterConfigurerSupport;

import java.util.Collections;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static java.util.Optional.ofNullable;

/**
 * Created by PetrushinDA on 17.11.2019
 * Конвертер Subtask в SubtaskDto
 */
@Component
public class SubTaskToSubTaskDtoConverter extends ConverterConfigurerSupport<SubTask, SubTaskDto> {

    @Override
    protected Converter<SubTask, SubTaskDto> converter() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void configure(ModelMapper modelMapper) {
        modelMapper.addConverter(converter(modelMapper));
    }

    protected Converter<SubTask, SubTaskDto> converter(ModelMapper modelMapper) {
        return new AbstractConverter<>() {
            @Override
            protected SubTaskDto convert(SubTask subTask) {
                return ofNullable(subTask)
                        .map(x -> SubTaskDto.builder()
                                .id(subTask.getId())
                                .comment(subTask.getComment())
                                .description(subTask.getDescription())
                                .deadlineDate(x.getDeadlineDate())
                                .name(x.getName())
                                .isClosed(x.getIsClosed())
                                .files((isNull(x.getTask()) || isNull(x.getSubTaskFiles())) ? Collections.emptySet() : x.getSubTaskFiles().stream()
                                        .map(file -> modelMapper.map(file.getFile(), FileDto.class))
                                        .collect(Collectors.toSet()))
                                .taskId(ofNullable(x.getTask())
                                        .map(Task::getId)
                                        .orElse(null))
                                .build())
                        .orElse(null);
            }
        };
    }
}
