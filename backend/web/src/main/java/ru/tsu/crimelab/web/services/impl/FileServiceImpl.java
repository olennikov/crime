package ru.tsu.crimelab.web.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import ru.tsu.crimelab.dao.domain.File;
import ru.tsu.crimelab.dao.repository.FileRepository;
import ru.tsu.crimelab.filestorage.BlobObjectWrapper;
import ru.tsu.crimelab.filestorage.service.storage.BlobStorageService;
import ru.tsu.crimelab.web.dto.file.DownloadFileDto;
import ru.tsu.crimelab.web.services.FileService;
import ru.tsu.crimelab.web.services.UserService;
import ru.tsu.crimelab.web.services.file.access.FileCheckAccessStrategyFactory;

import java.util.Date;

import static java.lang.String.format;

/**
 * Created by PetrushinDA on 13.11.2019
 * Сервис для работы с файлами
 */
@Service
@RequiredArgsConstructor
public class FileServiceImpl implements FileService {

    private final BlobStorageService blobStorageService;

    private final FileRepository repository;

    private final UserService userService;

    private final FileCheckAccessStrategyFactory fileCheckAccessStrategyFactory;

    /**
     * Метод загрузки нового файла
     *
     * @param filePath      полное имя сохраняемого файла
     * @param multipartFile объект файла
     * @return сохраненный в БД файл
     */
    @SneakyThrows
    @Override
    @Transactional(rollbackFor = Exception.class)
    public File uploadFile(String filePath, MultipartFile multipartFile) {
        var currentUser = userService.getCurrentUserByContext();
        var uploadFile = BlobObjectWrapper.builder()
                .contentType(multipartFile.getContentType())
                .objectName(filePath)
                .stream(multipartFile.getInputStream())
                .build();
        blobStorageService.putObject(uploadFile);

        File file = File.builder()
                .creatorUser(currentUser)
                .fileName(multipartFile.getOriginalFilename())
                .filePath(filePath)
                .fileSize(multipartFile.getSize())
                .mimeType(multipartFile.getContentType())
                .dateCreate(new Date())
                .build();
        return repository.save(file);
    }

    /**
     * Метод скачивания файла
     *
     * @param fileId идентифкатор файла
     * @return dto сущность для скачивания
     */
    @Override
    @Transactional(readOnly = true)
    public DownloadFileDto downloadFile(Long fileId) {
        var currentUser = userService.getCurrentUserByContext();
        var file = repository.findById(fileId)
                .orElseThrow(() -> new IllegalArgumentException("Указанный файл не найден"));

        var fileCheckAccessStrategy = fileCheckAccessStrategyFactory.getFileCheckAccessStrategy(currentUser);
        if (!fileCheckAccessStrategy.canDownload(currentUser, fileId)) {
            throw new IllegalArgumentException("У вас нет прав для скачивания данного файла");
        } else {
            return DownloadFileDto.builder()
                    .fileName(file.getFileName())
                    .inputStream(blobStorageService.getObject(file.getFilePath()))
                    .build();
        }
    }

    /**
     * Метод удаления файла из хранилища
     *
     * @param file идентификатор файла
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteFile(@NonNull File file) {
        repository.delete(file);
        blobStorageService.removeObject(file.getFilePath());
    }
}
