package ru.tsu.crimelab.web.controllers.employee;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.tsu.crimelab.web.dto.task.TaskDto;
import ru.tsu.crimelab.web.services.TaskService;

import javax.ws.rs.PathParam;

/**
 * Created by PetrushinDA on 04.12.2019
 */
@RestController("employee-task-controller")
@RequestMapping("employee/tasks")
@RequiredArgsConstructor
@PreAuthorize("hasRole(T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).employeeRoleName)")
@Api("Сервис работы с задачами из ЛК сотрудника")
public class TaskController {

    private final TaskService taskService;

    @GetMapping("{id}")
    @ApiOperation("Получение объекта задачи по идентификатору")
    public TaskDto getTaskById(@PathVariable("id") Long id) {
        return taskService.getByIdAndCurrentUserAsResponsible(id);
    }

    @PostMapping("{id}/upload-file")
    @ApiOperation("Прикрепление файла к задаче")
    public TaskDto attachFileToTask (@PathVariable ("id") Long id, @RequestParam("file") MultipartFile  file){
        return taskService.attachFileToTask(id,file);
    }


}
