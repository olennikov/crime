package ru.tsu.crimelab.web.dto.expertise;

import lombok.Builder;
import lombok.Data;
import ru.tsu.crimelab.web.dto.task.TaskDto;
import ru.tsu.crimelab.web.dto.user.UserDto;

import java.util.Date;
import java.util.List;

/**
 * Created by PetrushinDA on 17.11.2019
 */
@Data
@Builder
public class ExpertiseFullDto {
    private Long id;
    private Date receiveDate;
    private String name;
    private String description;
    private Long applicationId;
    private String status;
    private UserDto creator;
    private UserDto leader;
    private List<TaskDto> tasks;
}
