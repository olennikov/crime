package ru.tsu.crimelab.web.dto.director;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.crimelab.dao.domain.enums.TaskStatus;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true )
public class CreateTaskDto {
    private Long id;
    private String name;
    private String description;
    private TaskStatus taskStatus;
    private Long responsibleUserId;
    private Date deadlineDate;
    private Long expertiseId;
}
