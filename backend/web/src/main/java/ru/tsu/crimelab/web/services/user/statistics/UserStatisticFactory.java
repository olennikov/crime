package ru.tsu.crimelab.web.services.user.statistics;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.tsu.crimelab.dao.domain.enums.Role;

import static java.lang.String.format;

/**
 * Created by PetrushinDA on 01.01.2020
 * Фабрика для получения конкретного экземпляра сервиса загрузки статистики пользователей
 */
@Component
@RequiredArgsConstructor
public class UserStatisticFactory {

    private final ClientStatisticServiceImpl clientStatisticService;
    private final DirectorStatisticServiceImpl directorStatisticService;
    private final EmployeeStatisticServiceImpl employeeStatisticService;

    public UserStatisticService getUserStatisticServiceByRole(Role role) {
        switch (role) {
            case CUSTOMER:
                return clientStatisticService;
            case DIRECTOR:
                return directorStatisticService;
            case EMPLOYEE:
                return employeeStatisticService;
        }
        throw new RuntimeException(format("Не найден обработчик для сбора статистики пользователей с ролью \"%s\"", role.getRoleName()));
    }
}
