package ru.tsu.crimelab.web.dto.expertise;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.crimelab.web.dto.file.FileDto;

import java.util.Set;

/**
 * Created by PetrushinDA on 19.11.2019
 * Dto сущность для отображения файлов сязанных с экспертизой
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExpertiseStoreFiles {
    private Set<FileDto> applicationFiles;
    private Set<FileDto> expertiseFiles;
}
