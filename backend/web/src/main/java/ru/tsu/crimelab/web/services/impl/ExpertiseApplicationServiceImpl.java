package ru.tsu.crimelab.web.services.impl;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import ru.tsu.crimelab.dao.api.Query;
import ru.tsu.crimelab.dao.api.repository.response.QueryResponse;
import ru.tsu.crimelab.dao.domain.*;
import ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationFileType;
import ru.tsu.crimelab.dao.domain.enums.ExpertiseStatus;
import ru.tsu.crimelab.dao.repository.ExpertiseApplicationFileRepository;
import ru.tsu.crimelab.dao.repository.ExpertiseApplicationRepository;
import ru.tsu.crimelab.dao.repository.ExpertiseRepository;
import ru.tsu.crimelab.security.jwt.SecurityService;
import ru.tsu.crimelab.web.dto.director.ExpertiseApplicationResultDto;
import ru.tsu.crimelab.web.dto.director.RejectExpertiseApplicationDto;
import ru.tsu.crimelab.web.dto.expertiseapplication.ExpertiseApplicationDto;
import ru.tsu.crimelab.web.dto.expertiseapplication.ExpertiseApplicationStatus;
import ru.tsu.crimelab.web.dto.expertiseapplication.ExpertiseApplicationView;
import ru.tsu.crimelab.web.services.ChatService;
import ru.tsu.crimelab.web.services.ExpertiseApplicationService;
import ru.tsu.crimelab.web.services.FileService;
import ru.tsu.crimelab.web.services.UserService;
import ru.tsu.crimelab.web.services.validators.ActionType;
import ru.tsu.crimelab.web.services.validators.impl.ExpertiseApplicationFileValidator;
import ru.tsu.crimelab.web.services.validators.impl.ExpertiseApplicationValidator;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static java.util.Optional.ofNullable;
import static ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationStatus.NEW;
import static ru.tsu.crimelab.web.services.validators.ActionType.DELETE;
import static ru.tsu.crimelab.web.services.validators.ActionType.UPDATE;

/**
 * Created by PetrushinDA on 06.11.2019
 * Сервис для работы с заявками на экспертизу
 */
@Service
@RequiredArgsConstructor
public class ExpertiseApplicationServiceImpl implements ExpertiseApplicationService {

    private final ExpertiseApplicationRepository repository;

    private final SecurityService securitySevice;

    private final ModelMapper modelMapper;

    private final ExpertiseRepository expertiseRepositiry;

    private final ExpertiseApplicationFileRepository expertiseApplicationFileRepository;

    private final FileService fileService;

    private final UserService userService;

    private final ChatService chatService;

    private final ExpertiseApplicationValidator validator;

    private final ExpertiseApplicationFileValidator expertiseApplicationFileValidator;

    /**
     * Метод загрузки заявления на экспертизу по идентифиактору
     *
     * @param id идентификатор заявки
     * @return объект заявки на проведение экспертизы
     */
    @Transactional(readOnly = true)
    @Override
    public ExpertiseApplicationDto getExpertiseApplicationById(Long id) {
        return repository.findById(id)
                .map(x -> modelMapper.map(x, ExpertiseApplicationDto.class))
                .orElse(null);
    }


    /**
     * Метод обновления заявления на экспертизу по идентифиактору
     *
     * @param expertiseApplicationDto объект заявки
     * @return объект заявки на проведение экспертизы
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public ExpertiseApplicationDto updateExpertiseApplication(ExpertiseApplicationDto expertiseApplicationDto) {
        var expertiseApplication = repository.findById(expertiseApplicationDto.getId())
                .orElseThrow(() -> new RuntimeException("Объект заявки не найден"));

        expertiseApplication.setDescription(expertiseApplicationDto.getDescription());
        expertiseApplication.setName(expertiseApplicationDto.getName());

        validator.validate(expertiseApplication, UPDATE);
        return modelMapper.map(repository.save(expertiseApplication), ExpertiseApplicationDto.class);
    }

    /**
     * Метод создания заявления на экспертизу по идентифиактору
     *
     * @param expertiseApplicationDto объект заявки
     * @return объект заявки на проведение экспертизы
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public ExpertiseApplicationDto createExpertiseApplication(ExpertiseApplicationDto expertiseApplicationDto) {
        var currentUser = userService.getCurrentUserByContext();
        var expertiseApplication = ExpertiseApplication.builder()
                .name(expertiseApplicationDto.getName())
                .description(expertiseApplicationDto.getDescription())
                .status(NEW)
                .receiveDate(new Date())
                .creationUser(currentUser)
                .build();

        validator.validate(expertiseApplication, ActionType.CREATE);
        return modelMapper.map(repository.save(expertiseApplication), ExpertiseApplicationDto.class);
    }

    /**
     * Метод прикрепления файла
     *
     * @param expertiseApplicationId идентификатор заявления
     * @param multipartFile          объект файла
     * @return обновленный объект экспертизы
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public ExpertiseApplicationDto uploadFile(Long expertiseApplicationId, MultipartFile multipartFile) {
        var expertiseApplication = ofNullable(expertiseApplicationId)
                .map(x -> repository.findById(expertiseApplicationId))
                .orElseThrow(() -> new RuntimeException(format("Объект экспертизы с идентификатором %s не найден", expertiseApplicationId)))
                .orElseThrow(() -> new RuntimeException("Не передан идентификатор заявления"));


        String fileName = format("%s/%s", expertiseApplication.getName(), multipartFile.getOriginalFilename());
        File file = fileService.uploadFile(fileName, multipartFile);

        ExpertiseApplicationFile expertiseApplicationFile = ExpertiseApplicationFile.builder()
                .file(file)
                .expertiseApplication(expertiseApplication)
                .fileType(ExpertiseApplicationFileType.REQUEST_FILE)
                .build();

        expertiseApplicationFileRepository.save(expertiseApplicationFile);
        return modelMapper.map(expertiseApplication, ExpertiseApplicationDto.class);
    }

    /**
     * Метод удаления файла
     *
     * @param expertiseApplicationId идентификатор заявления
     * @param fileId                 идентификатор файла
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteFile(@NonNull Long expertiseApplicationId, @NonNull Long fileId) {
        ExpertiseApplicationFile expertiseApplicationFile = expertiseApplicationFileRepository.findByFile_Id(fileId)
                .orElseThrow(() -> new IllegalArgumentException(format("Информация о файле с идентификатором %s не найдена", fileId)));

        expertiseApplicationFileValidator.validate(expertiseApplicationFile, DELETE);
        expertiseApplicationFileRepository.delete(expertiseApplicationFile);
        fileService.deleteFile(expertiseApplicationFile.getFile());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationStatus setAcceptApplication(Long applicationId) {
        ExpertiseApplication expertiseApplication = Optional.ofNullable(applicationId)
                .map(repository::findById)
                .orElseThrow(() -> new IllegalAccessError(String.format("Заявки на проведение экспертизы %s не существует", applicationId)))
                .orElseThrow(() -> new RuntimeException("Пустой идентификатор"));
        User leaderUser = userService.getCurrentUserByContext();
        chatService.createChat(expertiseApplication, leaderUser);
        expertiseApplication.setStatus(ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationStatus.ACCEPT);
        expertiseApplication.setLeaderUser(leaderUser);

        validator.validate(expertiseApplication, UPDATE);
        repository.save(expertiseApplication);
        return expertiseApplication.getStatus();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long updateResultExpertiseApplication(ExpertiseApplicationResultDto resultDto) {
        ExpertiseApplication expertiseApplication = Optional.ofNullable(resultDto.getId())
                .map(repository::findById)
                .orElseThrow(() -> new IllegalAccessError(String.format("Данный объект отсутствует в системе %s", resultDto.getId())))
                .orElseThrow(() -> new RuntimeException("Пустой идентификатор"));

        expertiseApplication.setResultDescription(resultDto.getResultDescription());

        validator.validate(expertiseApplication, UPDATE);
        repository.save(expertiseApplication);
        return expertiseApplication.getId();
    }

    @Override
    @Transactional(readOnly = true)
    public ExpertiseApplicationResultDto getExpertiseResult(Long expertiseApplicationId) {
        ExpertiseApplication expertiseApplication = Optional.ofNullable(expertiseApplicationId)
                .map(repository::findById)
                .orElseThrow(() -> new IllegalAccessError(String.format("Данный объект отсутствует в системе %s", expertiseApplicationId)))
                .orElseThrow(() -> new RuntimeException("Пустой идентификатор"));
        return modelMapper.map(expertiseApplication, ExpertiseApplicationResultDto.class);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long saveResultFiles(Long applicationId, MultipartFile multipartFile) {
        ExpertiseApplication application = Optional.ofNullable(applicationId)
                .map(repository::findById)
                .orElseThrow(() -> new RuntimeException(format("Объект экспертизы с идентификатором %s не найден", applicationId)))
                .orElseThrow(() -> new RuntimeException("Не передан идентификатор заявления"));

        String fileName = format("%s/%s", application.getName(), multipartFile.getOriginalFilename());
        File file = fileService.uploadFile(fileName, multipartFile);

        ExpertiseApplicationFile expertiseApplicationFile = ExpertiseApplicationFile.builder()
                .file(file)
                .expertiseApplication(application)
                .fileType(ExpertiseApplicationFileType.RESULT_FILE)
                .build();

        expertiseApplicationFileRepository.save(expertiseApplicationFile);
        return expertiseApplicationFile.getId();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationStatus setInWorkApplication(Long applicationId) {
        ExpertiseApplication expertiseApplication = Optional.ofNullable(applicationId)
                .map(repository::findById)
                .orElseThrow(() -> new IllegalAccessError("Пустой идентификатор"))
                .orElseThrow(() -> new IllegalAccessError("Заявки с данным идентификатором не существует"));
        expertiseApplication.setStatus(ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationStatus.IN_WORK);

        validator.validate(expertiseApplication, UPDATE);
        repository.save(expertiseApplication);
        return ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationStatus.IN_WORK;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationStatus closeExpertiseApplication(Long applicationId) {
        ExpertiseApplication expertiseApplication = Optional.ofNullable(applicationId)
                .map(repository::findById)
                .orElseThrow(() -> new IllegalAccessError("Пустой идентификатор"))
                .orElseThrow(() -> new IllegalAccessError(String.format("Заявка на проведение экспертизы с идентификаторов %s не существует")));

        expertiseApplication.setStatus(ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationStatus.CLOSED);
        repository.save(expertiseApplication);

        Expertise expertise = expertiseRepositiry.findByExpertiseApplicationId(expertiseApplication.getId())
                .orElse(null);

        if (expertise != null) {
            expertise.setStatus(ExpertiseStatus.DONE);
            expertiseRepositiry.save(expertise);
        }

        return expertiseApplication.getStatus();
    }

    /**
     * Метод получения заявок, где создателем является текущий пользователь
     *
     * @return перечень заявок
     */
    @Transactional(readOnly = true)
    @Override
    public List<ExpertiseApplicationDto> getByCurrentUserCreator(ExpertiseApplicationStatus status) {
        Long userId = securitySevice.getCurrentUserId();
        var expertiseApplications = status == ExpertiseApplicationStatus.ALL ? repository.findExpertiseApplicationsByCreationUserId(userId) :
                repository.findExpertiseApplicationsByCreationUserIdAndStatusEquals(userId, ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationStatus.valueOf(status.name()));
        return expertiseApplications.stream()
                .map(x -> modelMapper.map(x, ExpertiseApplicationDto.class))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public QueryResponse<ExpertiseApplicationView> getExpertiseApplicationListForDirector(Query query) {
        QueryResponse<ExpertiseApplication> expertiseApplications = repository.pagingQuery(query);
        List<ExpertiseApplicationView> views = expertiseApplications.getRecords()
                .stream()
                .map(x -> ExpertiseApplicationView.builder().id(x.getId())
                        .receiveDate(x.getReceiveDate())
                        .expertiseName(x.getName())
                        .creationUserName(x.getCreationUser() != null
                                ? format("%s %s %s", x.getCreationUser().getLastName(),
                                x.getCreationUser().getFirstName(),
                                x.getCreationUser().getSecondName())
                                : "").build())
                .collect(Collectors.toList());
        return new QueryResponse<>(expertiseApplications.getCount(), views);
    }

    @Override
    @Transactional(readOnly = true)
    public ExpertiseApplicationDto getExpertiseApplicationDtoById(Long id) {
        return repository.findById(id)
                .map(x -> modelMapper.map(x, ExpertiseApplicationDto.class))
                .orElseThrow(() -> new IllegalArgumentException(format("Заявка с идентификатором %s не найдена", id)));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long rejectExpertiseApplication(RejectExpertiseApplicationDto reject) {
        Optional<ExpertiseApplication> optional = repository.findById(reject.getId());
        ExpertiseApplication expertiseApplication = optional
                .orElseThrow(() -> new IllegalAccessError(format("Заявки с идентификатором %s не существует", reject.getId())));
        expertiseApplication.setStatus(ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationStatus.REJECT);
        expertiseApplication.setRejectComment(reject.getRejectReason());

        validator.validate(expertiseApplication, UPDATE);
        repository.save(expertiseApplication);
        return expertiseApplication.getId();
    }


}
