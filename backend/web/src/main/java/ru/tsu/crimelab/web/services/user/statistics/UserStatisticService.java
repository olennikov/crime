package ru.tsu.crimelab.web.services.user.statistics;

import org.springframework.data.domain.Pageable;
import ru.tsu.crimelab.web.services.user.statistics.models.UserStatistic;

import java.util.Collection;

/**
 * Created by PetrushinDA on 01.01.2020
 */
public interface UserStatisticService {
    Collection<? extends UserStatistic> load(Pageable pageable);
}
