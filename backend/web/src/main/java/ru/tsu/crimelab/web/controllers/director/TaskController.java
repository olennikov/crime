package ru.tsu.crimelab.web.controllers.director;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.tsu.crimelab.web.dto.director.CreateTaskDto;
import ru.tsu.crimelab.web.dto.director.TaskDto;
import ru.tsu.crimelab.web.services.TaskService;

import java.util.List;

@RestController("director-task-controller")
@RequestMapping("director/task")
@RequiredArgsConstructor
@Api("Сервис работы с задачами из ЛК руководителя")
@PreAuthorize("hasRole(T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).directorRoleName)")
public class TaskController {

    private final TaskService taskService;

    @GetMapping("by-expertise/{expertiseId}")
    @ApiOperation("Список задач для данной экспертизы")
    public List<CreateTaskDto> getTasksByExpertiseId(@PathVariable("expertiseId") Long expertiseId) {
        return taskService.getTasksByExpertise(expertiseId);
    }

    @GetMapping("/{id}")
    @ApiOperation("Получение объекта задачи по идентификатору")
    public TaskDto getTaskById(@PathVariable("id") Long id) {
        return taskService.getTaskById(id);
    }

    @PutMapping("save-subtask")
    @ApiOperation("Сохранение подзадач")
    public Long saveSubTasks(@RequestBody TaskDto taskDto) {
        return taskService.saveSubtask(taskDto);
    }

    @PostMapping("save-task")
    @ApiOperation("Сохранение задачи")
    public Long createTask (@RequestBody CreateTaskDto createTaskDto){
        return taskService.createTask(createTaskDto);
    }
}

