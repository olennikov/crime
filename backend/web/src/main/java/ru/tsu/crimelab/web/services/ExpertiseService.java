package ru.tsu.crimelab.web.services;

import org.springframework.data.domain.Pageable;
import ru.tsu.crimelab.dao.api.repository.response.QueryResponse;
import ru.tsu.crimelab.dao.domain.enums.ExpertiseStatus;
import ru.tsu.crimelab.web.dto.director.CreateExpertiseDto;
import ru.tsu.crimelab.web.dto.director.EmployeeUserDto;
import ru.tsu.crimelab.web.dto.expertise.ExpertiseDto;
import ru.tsu.crimelab.web.dto.expertise.ExpertiseFullDto;
import ru.tsu.crimelab.web.dto.expertise.ExpertiseStatusDto;
import ru.tsu.crimelab.web.dto.expertise.ExpertiseStoreFiles;

import java.util.List;

/**
 * Created by PetrushinDA on 17.11.2019
 * Сервис для работы с экспертизами
 */
public interface ExpertiseService {

    /**
     * Метод для получения списка экспертиз
     *
     * @return список экспертиз
     */
    QueryResponse<ExpertiseDto> getExpertiseList(Pageable pageable, ExpertiseStatusDto status);

    /**
     * Получить перечень файлов связанных с экспертизой
     *
     * @param id идентификатор экспертизы
     * @return перечень файлов
     */
    ExpertiseStoreFiles getExpertiseStoreFiles(Long id);

    /**
     * Получить объект экспертизы
     *
     * @param id идентификатор экспертизы
     * @return объект экспертизы
     */
    ExpertiseFullDto getExpertise(Long id);

    /**
     * Метод для создания объекта экспертизы
     *
     * @param createExpertiseDto - сущность для сохранения пользователя
     * @return - возвращает идентификатор сохранённой сущности
     */
    Long createExpertise(CreateExpertiseDto createExpertiseDto);

    /**
     * Метод возращающий пользователей на которых может быть поставлена задача
     *
     * @return списко чущностей пользователя
     */
    List<EmployeeUserDto> getEmployeeUsers();

    /**
     * Метод для получения объекта экспертизы по идентификатору заявки
     *
     * @param applicationId - идентификатор заявки
     * @return
     */
    CreateExpertiseDto getExpertiseByApplicationId(Long applicationId);

    /**
     * Метод для преобразования объекта экспертизы
     *
     * @param expertiseId идегтификатор экспертизы
     * @return статус
     */
    ExpertiseStatus setInWork(Long expertiseId);
}
