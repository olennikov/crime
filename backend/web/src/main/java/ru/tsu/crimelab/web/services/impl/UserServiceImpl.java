package ru.tsu.crimelab.web.services.impl;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsu.crimelab.appstarter.rabbitmq.consts.notification.Queues;
import ru.tsu.crimelab.appstarter.security.BlockUserEvent;
import ru.tsu.crimelab.dao.api.repository.response.QueryResponse;
import ru.tsu.crimelab.dao.domain.User;
import ru.tsu.crimelab.dao.repository.UserRepository;
import ru.tsu.crimelab.dto.model.EmailMessageDto;
import ru.tsu.crimelab.security.jwt.SecurityService;
import ru.tsu.crimelab.web.dto.user.*;
import ru.tsu.crimelab.web.services.UserService;
import ru.tsu.crimelab.web.services.Util;
import ru.tsu.crimelab.web.services.user.statistics.UserStatisticFactory;

import java.util.List;
import java.util.UUID;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static ru.tsu.crimelab.dao.domain.enums.Role.*;

/**
 * Created by PetrushinDA on 13.11.2019
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    private final SecurityService securitySevice;

    private final ModelMapper modelMapper;

    private final UserStatisticFactory userStatisticFactory;

    private final RabbitTemplate rabbitTemplate;

    private final ApplicationEventPublisher applicationEventPublisher;

    @Value("${create.user.email.message}")
    private String sendPasswordMessage;

    /**
     * {@inheritDoc}
     */
    @Override
    public User getCurrentUserByContext() {
        return repository.getById(securitySevice.getCurrentUserId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long getCurrentUserIdByContext() {
        return securitySevice.getCurrentUserId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public QueryResponse<ClientInfoDto> loadClientsInfo(Pageable pageable) {
        var records = (userStatisticFactory.getUserStatisticServiceByRole(CUSTOMER))
                .load(pageable).stream()
                .map(x -> modelMapper.map(x, ClientInfoDto.class))
                .collect(toList());
        var total = repository.countByRoleEquals(CUSTOMER);
        return new QueryResponse<>(total, records);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public QueryResponse<EmployeeInfoDto> loadEmployeesInfo(Pageable pageable) {
        var records = (userStatisticFactory.getUserStatisticServiceByRole(EMPLOYEE))
                .load(pageable).stream()
                .map(x -> modelMapper.map(x, EmployeeInfoDto.class))
                .collect(toList());
        var total = repository.countByRoleEquals(EMPLOYEE);
        return new QueryResponse<>(total, records);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public QueryResponse<DirectorInfoDto> loadDirectorsInfo(Pageable pageable) {
        var records = (userStatisticFactory.getUserStatisticServiceByRole(DIRECTOR))
                .load(pageable).stream()
                .map(x -> modelMapper.map(x, DirectorInfoDto.class))
                .collect(toList());
        var total = repository.countByRoleEquals(DIRECTOR);
        return new QueryResponse<>(total, records);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void blockUsers(List<Long> userIds) {
        var currentUserId = securitySevice.getCurrentUserId();
        if (userIds.contains(currentUserId)) {
            throw new IllegalArgumentException("Невозможно заблокировать свою учетную запись");
        }

        var notBlockingDirectors = repository.findByRoleAndIdNotIn(DIRECTOR, userIds);
        if (notBlockingDirectors.size() == 0) {
            throw new IllegalArgumentException("Невозможно заблокировать всех руководителей лаборатории");
        }

        userIds.forEach(id -> {
            var foundUser = repository.findById(id)
                    .orElseThrow(() -> new IllegalArgumentException(format("Пользователь с идентификатором %s не найден", id)));
            foundUser.setIsBlock(true);
            repository.save(foundUser);
            applicationEventPublisher.publishEvent(new BlockUserEvent(this, id));
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void unblockUsers(List<Long> userIds) {
        userIds.forEach(id -> {
            var foundUser = repository.findById(id)
                    .orElseThrow(() -> new IllegalArgumentException(format("Пользователь с идентификатором %s не найден", id)));
            foundUser.setIsBlock(false);
            repository.save(foundUser);
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public UserDto createUser(CreateUserDto createUserDto) {
        User user = User.builder()
                .lastName(createUserDto.getLastName())
                .firstName(createUserDto.getFirstName())
                .secondName(createUserDto.getSecondName())
                .email(createUserDto.getEmail())
                .role(createUserDto.getRole())
                .phone(createUserDto.getPhone())
                .isBlock(false)
                .build();

        var salt = UUID.randomUUID().toString();
        var password = Util.generatePassword();
        user.setSalt(salt);
        user.setPassword(Util.saltPassword(password, salt));
        var savedUser = repository.saveAndFlush(user);
        rabbitTemplate.convertAndSend(Queues.EMAIL, createPasswordEmail(password, user.getEmail()));
        return modelMapper.map(savedUser, UserDto.class);
    }

    private EmailMessageDto createPasswordEmail(String password, String email) {
        EmailMessageDto emailMessageDto = new EmailMessageDto();
        emailMessageDto.setEmail(email);
        emailMessageDto.setText(String.format(sendPasswordMessage, password));
        emailMessageDto.setSubject("Получение пароля для продолжения работы с системой");
        return emailMessageDto;
    }
}
