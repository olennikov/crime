package ru.tsu.crimelab.web.services.user.statistics.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by PetrushinDA on 01.01.2020
 * Базовая сущность статистики по пользователям системы
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class UserStatistic {
    private Long id;
    private String lastName;
    private String firstName;
    private String secondName;
    private String email;
    private Boolean isBlock;
    private String phone;
}
