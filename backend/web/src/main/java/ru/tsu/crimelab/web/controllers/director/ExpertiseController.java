package ru.tsu.crimelab.web.controllers.director;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.tsu.crimelab.dao.domain.enums.ExpertiseStatus;
import ru.tsu.crimelab.web.dto.director.CreateExpertiseDto;
import ru.tsu.crimelab.web.dto.director.EmployeeUserDto;
import ru.tsu.crimelab.web.services.ExpertiseService;

import java.util.List;

@RestController("directorExpertiseController")
@RequestMapping("/director/expertise")
@AllArgsConstructor
@PreAuthorize("hasRole(T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).directorRoleName)")
@Api(value = "Работа с экспертизами из ЛК руководителя")
public class ExpertiseController {

    private final ExpertiseService expertiseService;

    @PutMapping
    @ApiOperation(value = "Создание объекта экспертизы")
    public Long createExpertise(@RequestBody CreateExpertiseDto createExpertiseDto) {
        return expertiseService.createExpertise(createExpertiseDto);
    }

    @GetMapping("get-users")
    @ApiOperation("Получение списка пользователей исполнителей")
    public List<EmployeeUserDto> getUsersDto() {
        return expertiseService.getEmployeeUsers();
    }


    @GetMapping("get-expertise/{applicationId}")
    @ApiOperation("Метод для получения объекта экспертизы по идентификатору")
    public CreateExpertiseDto getExpertise(@PathVariable("applicationId") Long applicationId) {
        return expertiseService.getExpertiseByApplicationId(applicationId);
    }

    @PostMapping("setinwork/{applicationId}")
    @ApiOperation("Метод для перевода экспертизы в статус работа")
    public ExpertiseStatus setExpertiseInWork(@PathVariable("applicationId") Long expertiseId) {
        return expertiseService.setInWork(expertiseId);
    }
}
