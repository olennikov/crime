package ru.tsu.crimelab.web.services.impl;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsu.crimelab.dao.api.repository.response.QueryResponse;
import ru.tsu.crimelab.dao.domain.*;
import ru.tsu.crimelab.dao.domain.enums.ExpertiseStatus;
import ru.tsu.crimelab.dao.domain.enums.Role;
import ru.tsu.crimelab.dao.repository.ExpertiseApplicationRepository;
import ru.tsu.crimelab.dao.repository.ExpertiseRepository;
import ru.tsu.crimelab.dao.repository.UserRepository;
import ru.tsu.crimelab.web.dto.director.CreateExpertiseDto;
import ru.tsu.crimelab.web.dto.director.EmployeeUserDto;
import ru.tsu.crimelab.web.dto.director.TaskViewDto;
import ru.tsu.crimelab.web.dto.expertise.ExpertiseDto;
import ru.tsu.crimelab.web.dto.expertise.ExpertiseFullDto;
import ru.tsu.crimelab.web.dto.expertise.ExpertiseStatusDto;
import ru.tsu.crimelab.web.dto.expertise.ExpertiseStoreFiles;
import ru.tsu.crimelab.web.dto.file.FileDto;
import ru.tsu.crimelab.web.services.ExpertiseService;
import ru.tsu.crimelab.web.services.UserService;
import ru.tsu.crimelab.web.services.validators.impl.ExpertiseValidator;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static java.util.Optional.ofNullable;
import static ru.tsu.crimelab.web.services.validators.ActionType.CREATE;
import static ru.tsu.crimelab.web.services.validators.ActionType.UPDATE;

/**
 * Created by PetrushinDA on 17.11.2019
 * Сервис для работы с экспертизами
 */

@Service
@RequiredArgsConstructor
public class ExpertiseServiceImpl implements ExpertiseService {

    private final ExpertiseRepository repository;

    private final UserService userService;

    private final ModelMapper modelMapper;

    private final ExpertiseApplicationRepository expertiseApplicationRepository;

    private final UserRepository userRepository;

    private final ExpertiseValidator validator;

    /**
     * {@inheritDoc}
     */
    @Transactional(readOnly = true)
    @Override
    public QueryResponse<ExpertiseDto> getExpertiseList(Pageable pageable, ExpertiseStatusDto status) {
        Long userId = userService.getCurrentUserIdByContext();
        var pageResult = status.equals(ExpertiseStatusDto.ALL) ?
                repository.findAllWithoutNEWByResponsibleUser(pageable, userId) :
                repository.findByResponsibleUserAndStatus(pageable, userId, ExpertiseStatus.valueOf(status.name()));
        var records = pageResult.get()
                .map(x -> modelMapper.map(x, ExpertiseDto.class))
                .collect(Collectors.toList());
        return new QueryResponse<>(pageResult.getTotalElements(), records);
    }

    /**
     * Получить перечень файлов связанныъ с экспертизой
     *
     * @param id идентификатор экспертизы
     * @return перечень файлов
     */
    @Transactional(readOnly = true)
    @Override
    public ExpertiseStoreFiles getExpertiseStoreFiles(Long id) {
        var expertise = repository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException(format("Экспертиза с идентификатором %s не найдена", id)));

        var expertiseFile = ofNullable(expertise.getExpertiseTasks())
                .orElse(Set.of())
                .stream()
                .flatMap(t -> t.getTaskFiles().stream())
                .map(f -> modelMapper.map(f.getFile(), FileDto.class))
                .collect(Collectors.toSet());

        var expertiseApplicationFile = ofNullable(expertise.getExpertiseApplication())
                .map(ExpertiseApplication::getExpertiseFiles)
                .orElse(Set.of());

        return ExpertiseStoreFiles.builder()
                .applicationFiles(expertiseApplicationFile.stream()
                        .map(x -> modelMapper.map(x.getFile(), FileDto.class))
                        .collect(Collectors.toSet()))
                .expertiseFiles(expertiseFile.stream()
                        .map(x -> modelMapper.map(x, FileDto.class))
                        .collect(Collectors.toSet()))
                .build();
    }

    /**
     * Получить объект экспертизы
     *
     * @param id идентификатор экспертизы
     * @return объект экспертизы
     */
    @Transactional(readOnly = true)
    @Override
    public ExpertiseFullDto getExpertise(Long id) {
        var expertise = repository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException(format("Экспертиза с идентификатором %s не найдена", id)));
        return modelMapper.map(expertise, ExpertiseFullDto.class);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long createExpertise(CreateExpertiseDto createExpertiseDto) {
        Expertise.ExpertiseBuilder builder = Expertise.builder();
        ExpertiseApplication expertiseApplication = Optional.ofNullable(createExpertiseDto.getExpertiseApplicationId())
                .map(expertiseApplicationRepository::findById)
                .orElseThrow(() -> new IllegalArgumentException(format("Экспертиза с идентификатором %s не найдена", createExpertiseDto.getExpertiseApplicationId())))
                .orElseThrow(() -> new RuntimeException("Пустой идентификатор"));
        Expertise expertise = builder.name(createExpertiseDto.getName())
                .description(createExpertiseDto.getDescription())
                .expertiseApplication(expertiseApplication)
                .receiveDate(new Date())
                .status(ExpertiseStatus.NEW)
                .build();

        validator.validate(expertise, CREATE);
        repository.save(expertise);
        return expertise.getId();
    }

    @Override
    @Transactional(readOnly = true)
    public List<EmployeeUserDto> getEmployeeUsers() {
        List<User> users =
                userRepository.findByRole(Role.EMPLOYEE);
        return users
                .stream()
                .map(x -> modelMapper.map(x, EmployeeUserDto.class))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public CreateExpertiseDto getExpertiseByApplicationId(Long applicationId) {
        CreateExpertiseDto expertise = Optional.ofNullable(applicationId)
                .map(repository::findByExpertiseApplicationId)
                .orElseThrow(() -> new RuntimeException("Пустой идентификатор"))
                .map(x -> CreateExpertiseDto.builder()
                        .id(x.getId())
                        .description(x.getDescription())
                        .name(x.getName())
                        .status(x.getStatus())
                        .taskArray(x.getExpertiseTasks()
                                .stream()
                                .map(y->modelMapper.map(y, TaskViewDto.class))
                                .collect(Collectors.toList()))
                        .build())
                .orElse(null);
        return expertise;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ExpertiseStatus setInWork(Long expertiseId) {
        Expertise expertise = Optional.of(expertiseId)
                .map(repository::findById)
                .orElseThrow(() -> new IllegalAccessError("Пустой идентификатор"))
                .orElseThrow(() -> new IllegalAccessError(String.format("Объекта экспертизы с идентификатором %s не существует", expertiseId)));
        expertise.setStatus(ExpertiseStatus.IN_WORK);

        validator.validate(expertise, UPDATE);
        repository.save(expertise);
        return expertise.getStatus();
    }
}
