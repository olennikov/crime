package ru.tsu.crimelab.web.dto.director;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.crimelab.web.dto.file.FileDto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class CreateSubTaskDto {
    private Long id;
    private String name;
    private String description;
    private String comment;
    private Date deadlineDate;
    private Boolean isClosed;
    private List<FileDto> files = new ArrayList<>();
}
