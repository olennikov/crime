package ru.tsu.crimelab.web.services;

import org.springframework.lang.NonNull;
import org.springframework.web.multipart.MultipartFile;
import ru.tsu.crimelab.dao.domain.File;
import ru.tsu.crimelab.web.dto.file.DownloadFileDto;

/**
 * Created by PetrushinDA on 13.11.2019
 * Сервис для работы с файлами
 */
public interface FileService {
    /**
     * Метод загрузки нового файла
     *
     * @param fileName      полное имя сохраняемого файла
     * @param multipartFile объект файла
     * @return сохраненный в БД файл
     */
    File uploadFile(String fileName, MultipartFile multipartFile);

    /**
     * Метод скачивания файла
     *
     * @param fileId идентифкатор файла
     * @return dto сущность для скачивания
     */
    DownloadFileDto downloadFile(Long fileId);

    /**
     * Метод удаления файла из хранилища
     *
     * @param file файл
     */
    void deleteFile(@NonNull File file);
}
