package ru.tsu.crimelab.web.services.validators;

/**
 * Created by PetrushinDA on 15.01.2020
 */
public enum ActionType {
    CREATE,
    READ,
    UPDATE,
    DELETE
}
