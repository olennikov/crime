package ru.tsu.crimelab.web.services.user.statistics;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import ru.tsu.crimelab.web.services.user.statistics.models.ClientStatistic;

import javax.annotation.PostConstruct;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

/**
 * Created by PetrushinDA on 03.01.2020
 * Сервис для загрузки статистики по клиентам лаборатории
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class ClientStatisticServiceImpl implements UserStatisticService {

    private final static String FUNCTION_NAME = "client_statistics";
    private final static String SCHEMA_NAME = "public";
    private final static String CURSOR_NAME = "statistics";
    private final JdbcTemplate jdbcTemplate;
    private SimpleJdbcCall functionCall;

    @PostConstruct
    public void postConstruct() {
        functionCall = new SimpleJdbcCall(jdbcTemplate)
                .returningResultSet(CURSOR_NAME, new ClientStatisticRowMapper())
                .withCatalogName(SCHEMA_NAME)
                .withFunctionName(FUNCTION_NAME);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Collection<ClientStatistic> load(Pageable pageable) {
        return (List<ClientStatistic>) functionCall.execute(
                pageable.getPageSize(), pageable.getPageNumber() * pageable.getPageSize()).get(CURSOR_NAME);
    }

    private static class ClientStatisticRowMapper implements RowMapper<ClientStatistic> {

        @Override
        public ClientStatistic mapRow(ResultSet resultSet, int rowNum) throws SQLException {
            return ClientStatistic.builder()
                    .id(resultSet.getLong("id"))
                    .firstName(resultSet.getString("first_name"))
                    .lastName(resultSet.getString("last_name"))
                    .secondName(resultSet.getString("second_name"))
                    .phone(resultSet.getString("phone"))
                    .email(resultSet.getString("email"))
                    .isBlock(resultSet.getBoolean("is_block"))
                    .applicationCount(resultSet.getLong("application_count"))
                    .build();
        }
    }
}
