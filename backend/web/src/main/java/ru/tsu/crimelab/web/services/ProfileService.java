package ru.tsu.crimelab.web.services;

import ru.tsu.crimelab.web.dto.login.EditPasswordDto;
import ru.tsu.crimelab.web.dto.login.LoginSaltDto;
import ru.tsu.crimelab.web.dto.user.UserProfileDto;

public interface ProfileService {
    /**
     * Метод для получения данных о пользователе для отображения его профиля
     * @return сущность профиля пользователя
     */
    UserProfileDto getUserProfile ();
    /**
     * Метод для редактирования данных пользователя на основе заполненных данных
     * @param userProfileDto - сущность профиля пользователя
     * @return идентификатор пользователя, у которого изменился профиль
     */
    Long editUserProfile (UserProfileDto userProfileDto);
    /**
     * Метод для изменения пользовательского пароля
     * @param editPasswordDto - сущность для изменения пароля
     * @return илентификатор пользователя, у которого изменили пароль
     */
    Long editUserPassword (EditPasswordDto editPasswordDto);
    /**
     * Метод для генерации раундовой соли по контексту безопасности пользователя
     * @return сущность соли для передачи пользовательского пароля
     */
    LoginSaltDto getRoundSalt();
}
