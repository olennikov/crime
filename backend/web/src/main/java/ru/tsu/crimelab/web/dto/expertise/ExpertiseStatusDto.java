package ru.tsu.crimelab.web.dto.expertise;

/**
 * Created by PetrushinDA on 17.11.2019
 */
public enum ExpertiseStatusDto {
    IN_WORK,
    DONE,
    NEW,
    ALL
}
