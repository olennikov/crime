package ru.tsu.crimelab.web.controllers.director;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.tsu.crimelab.dao.api.repository.response.QueryResponse;
import ru.tsu.crimelab.web.dto.user.*;
import ru.tsu.crimelab.web.services.UserService;

import java.util.List;

/**
 * Created by PetrushinDA on 28.12.2019
 * Контроллер для работы с пользователями системы
 */
@RestController
@RequestMapping("users")
@RequiredArgsConstructor
@Api("Работа с пользователями системы")
@PreAuthorize("hasRole(T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).directorRoleName)")
public class UserController {

    private final UserService userService;

    @ApiOperation("Получение списка клиентов")
    @GetMapping("clients")
    public QueryResponse<ClientInfoDto> getClients(Pageable pageable) {
        return userService.loadClientsInfo(pageable);
    }

    @ApiOperation("Получение списка сотрудников лаборатории")
    @GetMapping("employees")
    public QueryResponse<EmployeeInfoDto> getEmployees(Pageable pageable) {
        return userService.loadEmployeesInfo(pageable);
    }

    @ApiOperation("Получение списка руководителей лаборатории")
    @GetMapping("directors")
    public QueryResponse<DirectorInfoDto> getDirectors(Pageable pageable) {
        return userService.loadDirectorsInfo(pageable);
    }

    @ApiOperation("Создание пользователя")
    @PostMapping
    public UserDto createUser(@RequestBody CreateUserDto createUserDto) {
        return userService.createUser(createUserDto);
    }

    @ApiOperation("Блокирование пользователей")
    @PostMapping(value = "block")
    public void blockUser(@ApiParam(value = "Идентификаторы пользователей") @RequestBody List<Long> ids) {
        userService.blockUsers(ids);
    }

    @ApiOperation("Разблокирование пользователей")
    @PostMapping(value = "unblock")
    public void unblockUser(@ApiParam(value = "Идентификаторы пользователей") @RequestBody List<Long> ids) {
        userService.unblockUsers(ids);
    }
}
