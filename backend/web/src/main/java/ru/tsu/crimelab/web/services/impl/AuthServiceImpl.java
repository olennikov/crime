package ru.tsu.crimelab.web.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.authentication.event.LogoutSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsu.crimelab.dao.domain.User;
import ru.tsu.crimelab.dao.repository.UserRepository;
import ru.tsu.crimelab.security.CrimeLabUser;
import ru.tsu.crimelab.security.jwt.JwtPayload;
import ru.tsu.crimelab.security.jwt.JwtPreAuthenticationToken;
import ru.tsu.crimelab.security.jwt.JwtService;
import ru.tsu.crimelab.security.jwt.JwtUserDetails;
import ru.tsu.crimelab.security.jwt.config.JwtAuthenticationConfig;
import ru.tsu.crimelab.security.jwt.utils.CookieUtil;
import ru.tsu.crimelab.security.jwt.utils.UserUtils;
import ru.tsu.crimelab.web.dto.login.EditPasswordDto;
import ru.tsu.crimelab.web.dto.login.LoginDto;
import ru.tsu.crimelab.web.dto.login.LoginSaltDto;
import ru.tsu.crimelab.web.dto.user.UserProfileDto;
import ru.tsu.crimelab.web.services.AuthService;
import ru.tsu.crimelab.web.services.Util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static java.util.Map.of;
import static ru.tsu.crimelab.security.jwt.utils.Constants.*;

/**
 * Created by PetrushinDA on 13.10.2019
 */
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final UserRepository userRepository;

    private final JwtService jwtService;

    private final JwtAuthenticationConfig cookieConfig;

    private final ApplicationEventPublisher applicationEventPublisher;

    @Override
    @Transactional(readOnly = true)
    public Authentication login(LoginDto loginDto) {
        Optional<User> userOptional = userRepository.findOneByEmail(loginDto.getLogin());
        User user = userOptional.orElseThrow(IllegalAccessError::new);
        if (user.getIsBlock()) {
            throw new IllegalAccessError("Данная учетная запись заблокирована. Обратитесь к администратору");
        }
        String localSaltPassword = Util.saltPassword(user.getPassword(), loginDto.getRoundSalt());
        if (localSaltPassword.equals(loginDto.getPassword())) {
            var sessionId = UUID.randomUUID().toString();
            CrimeLabUser crimeLabUser = CrimeLabUser.builder()
                    .id(user.getId())
                    .roles(Collections.singletonList(user.getRole().getRoleName()))
                    .name(user.getEmail())
                    .firstName(user.getFirstName())
                    .lastName(user.getLastName())
                    .secondName(user.getSecondName())
                    .isBlock(user.getIsBlock())
                    .build();
            JwtPayload jwtPayload = JwtPayload.builder()
                    .id(crimeLabUser.getId().toString())
                    .subject("subject")
                    .issuer(ISSUER)
                    .issuedAt(Instant.now())
                    .details(of(USER_INFO, UserUtils.toStringUser(crimeLabUser), SESSION_ID, sessionId))
                    .build();
            String jwt = jwtService.encodeJwt(jwtPayload);
            var jwtPreAuthenticationToken = new JwtPreAuthenticationToken(jwt);
            applicationEventPublisher.publishEvent(new AuthenticationSuccessEvent(jwtPreAuthenticationToken));
            return jwtPreAuthenticationToken;
        } else {
            throw new RuntimeException(String.format("Для пользователя %s указан неправильный пароль, ошибка авторизации", loginDto.getLogin()));
        }

    }

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        var securityContext = SecurityContextHolder.getContext();
        var auth = securityContext.getAuthentication();
        CookieUtil.clearCookie(response, cookieConfig);
        securityContext.setAuthentication(null);
        applicationEventPublisher.publishEvent(new LogoutSuccessEvent(auth));
    }

    @Override
    @Transactional(readOnly = true)
    public LoginSaltDto getSalt(String userEmail) {
        Optional<User> userOptional = userRepository.findOneByEmail(userEmail);
        User user = userOptional.orElseThrow(() -> new IllegalAccessError("Указанный пользователь не зарегистрирован"));
        LoginSaltDto loginSaltDto = new LoginSaltDto();
        loginSaltDto.setRoundSalt(UUID.randomUUID().toString());
        loginSaltDto.setUserSalt(user.getSalt());
        return loginSaltDto;
    }

    @Override
    public String getUserDetails(SecurityContext securityContext) {
        JwtUserDetails jwtUserDetails = (JwtUserDetails) securityContext.getAuthentication().getDetails();
        return jwtUserDetails.getDetails().get(USER_INFO);
    }


    private UserProfileDto convertProfile(User user) {
        return UserProfileDto.builder().firstName(user.getFirstName())
                .lastName(user.getLastName())
                .secondName(user.getSecondName())
                .phone(user.getPhone())
                .email(user.getEmail())
                .organisationName(user.getOrganisation() == null
                        ? null
                        : user.getOrganisation().getName())
                .organisationPhone(user.getOrganisation() == null
                        ? null
                        : user.getOrganisation().getPhone())
                .build();
    }
}
