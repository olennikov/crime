package ru.tsu.crimelab.web.mapper.config.converters;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.springframework.stereotype.Component;
import ru.tsu.crimelab.dao.domain.User;
import ru.tsu.crimelab.web.dto.user.UserDto;
import ru.tsu.crimelab.web.mapper.config.ConverterConfigurerSupport;

import static java.util.Optional.ofNullable;

/**
 * Created by PetrushinDA on 14.11.2019
 */
@Component
public class UserToUserDtoConverter extends ConverterConfigurerSupport<User, UserDto> {
    @Override
    protected Converter<User, UserDto> converter() {
        return new AbstractConverter<User, UserDto>() {
            @Override
            protected UserDto convert(User user) {
                return ofNullable(user)
                        .map(x -> UserDto.builder()
                                .firstName(x.getFirstName())
                                .lastName(x.getLastName())
                                .secondName(x.getSecondName())
                                .phone(x.getPhone())
                                .id(x.getId())
                                .build())
                        .orElse(null);
            }
        };
    }
}
