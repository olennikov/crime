package ru.tsu.crimelab.web.mapper.config.converters;

import lombok.RequiredArgsConstructor;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.tsu.crimelab.dao.domain.Expertise;
import ru.tsu.crimelab.web.dto.expertise.ExpertiseFullDto;
import ru.tsu.crimelab.web.dto.task.TaskDto;
import ru.tsu.crimelab.web.dto.user.UserDto;
import ru.tsu.crimelab.web.mapper.config.ConverterConfigurerSupport;

import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

/**
 * Created by PetrushinDA on 19.11.2019
 * Конвертер сущности Expertise в ExpertiseFull
 */
@Component
@RequiredArgsConstructor
public class ExpertiseToExpertiseFullDtoConverter extends ConverterConfigurerSupport<Expertise, ExpertiseFullDto> {

    @Override
    protected Converter<Expertise, ExpertiseFullDto> converter() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void configure(ModelMapper modelMapper) {
        modelMapper.addConverter(converter(modelMapper));
    }

    protected Converter<Expertise, ExpertiseFullDto> converter(ModelMapper modelMapper) {
        return new AbstractConverter<>() {
            @Override
            protected ExpertiseFullDto convert(Expertise expertise) {
                return ExpertiseFullDto.builder()
                        .id(expertise.getId())
                        .leader(ofNullable(expertise.getLeaderUser()).map(x -> modelMapper.map(x, UserDto.class)).orElse(null))
                        .creator(ofNullable(expertise.getLeaderUser()).map(x -> modelMapper.map(x, UserDto.class)).orElse(null))
                        .applicationId(expertise.getExpertiseApplication().getId())
                        .description(expertise.getDescription())
                        .name(expertise.getName())
                        .receiveDate(expertise.getReceiveDate())
                        .tasks(expertise.getExpertiseTasks().stream()
                                .map(x -> modelMapper.map(x, TaskDto.class))
                                .collect(Collectors.toList()))
                        .status(expertise.getStatus().name())
                        .build();
            }
        };
    }
}
