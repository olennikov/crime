package ru.tsu.crimelab.web.services.validators.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import ru.tsu.crimelab.dao.domain.TaskFile;
import ru.tsu.crimelab.dao.domain.enums.TaskStatus;
import ru.tsu.crimelab.dao.repository.TaskRepository;
import ru.tsu.crimelab.web.services.UserService;
import ru.tsu.crimelab.web.services.validators.ActionType;
import ru.tsu.crimelab.web.services.validators.Validator;

import java.util.Collection;

import static ru.tsu.crimelab.web.services.validators.ActionType.DELETE;
import static ru.tsu.crimelab.web.services.validators.ValidatorUtils.baseValidateByAction;

/**
 * Created by PetrushinDA on 19.01.2020
 */
@Component
@RequiredArgsConstructor
public class TaskFileValidator extends Validator<TaskFile> {

    private final TaskRepository taskRepository;

    private final UserService userService;

    @Override
    protected void validate(@NonNull TaskFile taskFile, @NonNull ActionType actionType, @NonNull Collection<String> errors) {
        baseValidateByAction(taskFile::getId, actionType, errors);

        var task = taskFile.getTask();
        var currentUser = userService.getCurrentUserByContext();
        if(task == null || task.getId() == null) {
            errors.add("Не передана информация о подзадаче для файла " + taskFile.getFile().getFileName());
        } else {
            var savedSubTask = taskRepository.getById(task.getId());
            if(savedSubTask.getTaskStatus().equals(TaskStatus.CLOSED)){
                errors.add("Операция невозможна. Задача закрыта");
            }

            if(actionType.equals(DELETE) && !(taskFile.getFile().getCreatorUser().getId().equals(currentUser.getId()))) {
                errors.add("Удалять файлы может только создатель файла");
            }
        }
    }
}
