package ru.tsu.crimelab.web.services;

import ru.tsu.crimelab.dao.api.Query;
import ru.tsu.crimelab.dao.api.repository.response.QueryResponse;
import ru.tsu.crimelab.web.dto.registration.RegistrationApplicationModel;
import ru.tsu.crimelab.web.dto.director.RegistrationDirectorModel;
import ru.tsu.crimelab.web.dto.registration.RegistrationApplicationView;
import ru.tsu.crimelab.web.dto.registration.RegistrationUserDto;
import ru.tsu.crimelab.web.dto.registration.RejectRegistrationDto;

/**
 * Сервис для регистрации заявок на получение доступа к системе заказчика
 */
public interface RegistrationService {
    /**
     * Метод создания заявки на регистрацию
     * @param model dto объект для создания заявки
     * @return - идентификатор созданной заявки
     */
    Long createApplicationRegistration (RegistrationApplicationModel model);

    /**
     * Метод для отказа в регистрации заказчика
     * @param model объект Dto для отказа по заявки
     * @return
     */
    Long rejectApplicationRegistration (RejectRegistrationDto model);

    /**
     * Метод для подтверждения заявки и создания нового пользователя
     * @return идентификатор созданного пользователя
     */
    Long acceptApplicationRegistration (Long applicationId);
    /**
     * Метод для поиска запроса регистрации по отправленной одноразовой ссылке
     * @param disposableUrl - параметр одноразовой ссылки отправленной при успешной регистрации
     * @return - модель регистрации пользователя
     */
    RegistrationApplicationModel getAcceptableRegistrationByDisposableUri (String disposableUrl);
    /**
     * Метод для закрытия процесса регистрации и создания нового пользователя
     * @param registrationUserDto  сущность для создания нового пользователя
     * @return идентификатор сущности заявки на регистрацию
     */
    Long closeApplicationRegistration (RegistrationUserDto registrationUserDto);
    /**
     * Метод для поиска данных регистрации
     * query объект запроса для поиска
     * @return список объектов найденных регистраций
     */
    QueryResponse<RegistrationApplicationView> getRegistrationApplicationView(Query query);
    /**
     * Метод для запроса сущности заявки на регистрацию по её идентификатору
     * @param id  идентификатор сущности
     * @return модель заявки на регистрацию
     */
    RegistrationDirectorModel getRegistrationById(Long id);
}
