package ru.tsu.crimelab.web.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tsu.crimelab.reportmodule.ReportService;
import ru.tsu.crimelab.reportmodule.models.ReportOutputType;
import ru.tsu.crimelab.reportmodule.reports.TestReport;
import ru.tsu.crimelab.reportmodule.reports.data.models.TestReportData;
import ru.tsu.crimelab.web.services.Util;

import java.io.IOException;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
@RequestMapping(value = "/test", produces = APPLICATION_JSON_UTF8_VALUE)
@RequiredArgsConstructor
public class TestController {

    private final ReportService reportService;

    @GetMapping
    public String test() {
        return "I'm alive";
    }

    @GetMapping("report-docx")
    public ResponseEntity<Resource> testReportDocx() throws IOException {
        var reportBytes = reportService.loadReport(ReportOutputType.DOCX, new TestReport(testReportData()));
        return ResponseEntity.ok()
                .headers(Util.createFileDownloadHeader("report-data.docx"))
                .body(new ByteArrayResource(reportBytes.readAllBytes()));
    }

    @GetMapping("report-pdf")
    public ResponseEntity<Resource> testReportPdf() throws IOException {
        var reportBytes = reportService.loadReport(ReportOutputType.PDF, new TestReport(testReportData()));
        return ResponseEntity.ok()
                .headers(Util.createFileDownloadHeader("report-data.pdf"))
                .body(new ByteArrayResource(reportBytes.readAllBytes()));
    }

    private TestReportData testReportData() {
        return TestReportData.builder()
                .project("Crimelab")
                .description("Test description")
                .build();
    }
}
