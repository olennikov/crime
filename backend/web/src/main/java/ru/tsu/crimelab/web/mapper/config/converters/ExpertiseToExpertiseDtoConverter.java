package ru.tsu.crimelab.web.mapper.config.converters;

import lombok.RequiredArgsConstructor;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.tsu.crimelab.dao.domain.Expertise;
import ru.tsu.crimelab.dao.domain.ExpertiseApplication;
import ru.tsu.crimelab.web.dto.expertise.ExpertiseDto;
import ru.tsu.crimelab.web.dto.user.UserDto;
import ru.tsu.crimelab.web.mapper.config.ConverterConfigurerSupport;

import static java.util.Optional.ofNullable;

/**
 * Created by PetrushinDA on 17.11.2019
 * Конвертер объекта Expertise в ExpertiseDto
 */
@Component
@RequiredArgsConstructor
public class ExpertiseToExpertiseDtoConverter extends ConverterConfigurerSupport<Expertise, ExpertiseDto> {

    private final ModelMapper modelMapper;

    @Override
    protected Converter<Expertise, ExpertiseDto> converter() {
        return new AbstractConverter<>() {
            @Override
            protected ExpertiseDto convert(Expertise expertise) {
                return ExpertiseDto.builder()
                        .id(expertise.getId())
                        .description(expertise.getDescription())
                        .name(expertise.getName())
                        .receiveDate(expertise.getReceiveDate())
                        .status(expertise.getStatus().name())
                        .applicationId(ofNullable(expertise.getExpertiseApplication())
                                .map(ExpertiseApplication::getId)
                                .orElse(null))
                        .creator(ofNullable(expertise.getExpertiseApplication()).filter(x -> x.getCreationUser() != null)
                                .map(ExpertiseApplication::getCreationUser)
                                .map(x -> modelMapper.map(x, UserDto.class))
                                .orElse(null))
                        .leader(ofNullable(expertise.getLeaderUser())
                                .map(x -> modelMapper.map(x, UserDto.class))
                                .orElse(null))
                        .build();
            }
        };
    }
}
