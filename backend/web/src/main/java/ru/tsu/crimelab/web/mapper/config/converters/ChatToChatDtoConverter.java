package ru.tsu.crimelab.web.mapper.config.converters;

import lombok.RequiredArgsConstructor;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.tsu.crimelab.dao.domain.Chat;
import ru.tsu.crimelab.web.dto.chat.ChatDto;
import ru.tsu.crimelab.web.dto.expertiseapplication.ExpertiseApplicationDto;
import ru.tsu.crimelab.web.dto.user.UserDto;
import ru.tsu.crimelab.web.mapper.config.ConverterConfigurerSupport;

import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

/**
 * Created by PetrushinDA on 14.11.2019
 */
@Component
@RequiredArgsConstructor
public class ChatToChatDtoConverter extends ConverterConfigurerSupport<Chat, ChatDto> {

    private final ModelMapper modelMapper;

    @Override
    protected Converter<Chat, ChatDto> converter() {
        return new AbstractConverter<>() {
            @Override
            protected ChatDto convert(Chat chat) {
                return ofNullable(chat)
                        .map(x -> ChatDto.builder()
                                .expertiseApplication(ofNullable(x.getExpertiseApplication())
                                        .map(y -> modelMapper.map(y, ExpertiseApplicationDto.class))
                                        .orElse(null))
                                .id(chat.getId())
                                .users(ofNullable(chat.getUsers())
                                        .orElse(Set.of())
                                        .stream()
                                        .map(u -> modelMapper.map(u, UserDto.class))
                                        .collect(Collectors.toList()))
                                .build())
                        .orElse(null);
            }
        };
    }
}
