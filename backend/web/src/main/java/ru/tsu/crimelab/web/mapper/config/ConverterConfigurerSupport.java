package ru.tsu.crimelab.web.mapper.config;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;

/**
 * Created by PetrushinDA on 14.11.2019
 * Обертка для создания полноценного конфига конвертера
 */
public abstract class ConverterConfigurerSupport<S, D> implements ModelMapperConfigurer {

    /**
     * Абстрактный метод реализации конвертера
     */
    protected abstract Converter<S, D> converter();

    /**
     * Общий метод для конфигурирования маппера.
     * Регистрирует в маппере конвертер который вернул метод converter()
     */
    @Override
    public void configure(ModelMapper modelMapper) {
        modelMapper.addConverter(converter());
    }
}
