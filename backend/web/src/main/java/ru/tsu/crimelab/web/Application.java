package ru.tsu.crimelab.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import ru.tsu.crimelab.dao.api.repository.BaseRepositoryImpl;



@SpringBootApplication(scanBasePackages ={"ru.tsu.crimelab"},exclude = { SecurityAutoConfiguration.class })
@EntityScan(basePackages = "ru.tsu.crimelab.dao.domain")
@EnableJpaRepositories(basePackages = "ru.tsu.crimelab.dao.repository", repositoryBaseClass = BaseRepositoryImpl.class)
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
