package ru.tsu.crimelab.web.dto.substasks;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.crimelab.web.dto.file.FileDto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by PetrushinDA on 17.11.2019
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SubTaskDto {
    private Long id;
    private String name;
    private String description;
    private String comment;
    private Date deadlineDate;
    private Set<FileDto> files = new HashSet<>();
    private Long taskId;
    private Boolean isClosed;
}
