package ru.tsu.crimelab.web.dto.expertiseapplication;

/**
 * Created by PetrushinDA on 10.11.2019
 */
public enum ExpertiseApplicationStatus {
    ALL,
    NEW,
    IN_WORK,
    ACCEPT,
    REJECT
}
