package ru.tsu.crimelab.web.services.file.access;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tsu.crimelab.dao.domain.User;
import ru.tsu.crimelab.dao.domain.enums.Role;
import ru.tsu.crimelab.dao.repository.ExpertiseApplicationFileRepository;

/**
 * Created by PetrushinDA on 07.12.2019
 * Стратегия проверки прав доступа для скачивания файлов клиентом
 */
@Service
@RequiredArgsConstructor
public class CustomerFileAccessStrategy implements FileCheckAccessStrategy {

    private final ExpertiseApplicationFileRepository expertiseApplicationFileRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canHandle(User user) {
        return user.getRole().equals(Role.CUSTOMER);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canDownload(User user, Long fileId) {
        return expertiseApplicationFileRepository.findByFile_Id(fileId)
                .map(x -> x.getExpertiseApplication().getCreationUser().getId().equals(user.getId()))
                .orElse(false);
    }
}
