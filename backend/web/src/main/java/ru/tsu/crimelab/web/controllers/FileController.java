package ru.tsu.crimelab.web.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.tsu.crimelab.web.services.*;

import java.io.IOException;

/**
 * Created by PetrushinDA on 30.11.2019
 * Сервис для работы с файлами
 */
@Slf4j
@RestController
@RequestMapping("/files")
@RequiredArgsConstructor
@Api(value = "Контроллер для работы с файлами")
public class FileController {

    private final FileService fileService;
    private final ExpertiseApplicationService expertiseApplicationService;
    private final SubTaskService subTaskService;
    private final TaskService taskService;

    @GetMapping("{id}")
    @ApiOperation(value = "Выгрузка файла")
    @PreAuthorize("hasAnyRole(T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).employeeRoleName, " +
            "T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).directorRoleName, " +
            "T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).customerRoleName)")
    public ResponseEntity<Resource> download(@PathVariable("id") Long fileId) {
        try {
            var file = fileService.downloadFile(fileId);
            String fileName = file.getFileName();

            return ResponseEntity.ok()
                    .headers(Util.createFileDownloadHeader(fileName))
                    .body(new ByteArrayResource(file.getInputStream().readAllBytes()));
        } catch (IOException e) {
            String errorMessage = "Возникла ошибка при скачивании файла";
            log.error(errorMessage, e);
            throw new RuntimeException(errorMessage, e);
        }
    }

    @PreAuthorize("hasAnyRole(T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).directorRoleName, " +
            "T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).customerRoleName)")
    @ApiOperation(value = "Удаление файла из заявки на проведение экспертизы")
    @DeleteMapping(value = "/{fileId}/expertise-application/{expertiseApplicationId}")
    public void deleteExpertiseApplicationFile(@PathVariable("expertiseApplicationId") Long expertiseApplicationId, @PathVariable("fileId") Long fileId) {
        expertiseApplicationService.deleteFile(expertiseApplicationId, fileId);
    }

    @PreAuthorize("hasAnyRole(T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).directorRoleName, " +
            "T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).employeeRoleName)")
    @ApiOperation(value = "Удаление файла из заявки подзадачи")
    @DeleteMapping(value = "/{fileId}/subtasks/{subTaskId}")
    public void deleteSubTaskFile(@PathVariable("subTaskId") Long subTaskId, @PathVariable("fileId") Long fileId) {
        subTaskService.deleteFile(subTaskId, fileId);
    }

    @PreAuthorize("hasAnyRole(T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).directorRoleName, " +
            "T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).employeeRoleName)")
    @ApiOperation(value = "Удаление файла из заявки задачи")
    @DeleteMapping("/{fileId}/task/{taskId}")
    public void deleteTaskFile (@PathVariable("fileId") Long fileId, @PathVariable("taskId") Long taskId){
        taskService.deleteFile(fileId,taskId);
    }
}
