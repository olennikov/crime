package ru.tsu.crimelab.web.mapper.config.converters;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.springframework.stereotype.Component;
import ru.tsu.crimelab.dao.domain.SubTask;
import ru.tsu.crimelab.dao.domain.Task;
import ru.tsu.crimelab.web.dto.director.TaskViewDto;
import ru.tsu.crimelab.web.mapper.config.ConverterConfigurerSupport;

@Component
public class TaskToTaskViewDto extends ConverterConfigurerSupport<Task, TaskViewDto> {
    @Override
    protected Converter<Task, TaskViewDto> converter() {
        return new AbstractConverter<Task, TaskViewDto>() {
            @Override
            protected TaskViewDto convert(Task task) {
                String progress = String.format("%s/%s",
                        task.getSubTasks()
                                .stream()
                                .filter(SubTask::getIsClosed)
                                .count(),
                        task.getSubTasks().size());
                return TaskViewDto.builder()
                        .id(task.getId())
                        .name(task.getName())
                        .taskStatus(task.getTaskStatus())
                        .progress(progress)
                        .build();
            }
        };
    }
}
