package ru.tsu.crimelab.web.mapper.config;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by PetrushinDA on 14.11.2019
 * Фабрика. Создает и настраивает маппер, делегируя настройку наследникам ModelMapperConfigurer
 */
@Component
public class ModelMapperFactoryBean implements FactoryBean<ModelMapper> {

    /**
     * Список конфигов маппера
     */
    @Autowired(required = false)
    private List<ModelMapperConfigurer> configurers;

    /**
     * Создает маппер. Проходит по всем конфигам, передает им текущий инстанц маппера для конфигурирования
     */
    @Override
    public ModelMapper getObject() throws Exception {
        final ModelMapper modelMapper = new ModelMapper();

        if (configurers != null)
            configurers.forEach(c -> c.configure(modelMapper));

        return modelMapper;
    }

    @Override
    public Class<?> getObjectType() {
        return ModelMapper.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

}
