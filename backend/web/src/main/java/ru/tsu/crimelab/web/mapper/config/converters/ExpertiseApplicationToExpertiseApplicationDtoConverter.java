package ru.tsu.crimelab.web.mapper.config.converters;

import lombok.RequiredArgsConstructor;
import org.hibernate.Hibernate;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.tsu.crimelab.dao.domain.ExpertiseApplication;
import ru.tsu.crimelab.dao.domain.File;
import ru.tsu.crimelab.web.dto.expertiseapplication.ExpertiseApplicationDto;
import ru.tsu.crimelab.web.dto.file.FileDto;
import ru.tsu.crimelab.web.dto.user.UserDto;
import ru.tsu.crimelab.web.mapper.config.ConverterConfigurerSupport;

import java.util.Collections;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

/**
 * Created by PetrushinDA on 14.11.2019
 */
@Component
@RequiredArgsConstructor
public class ExpertiseApplicationToExpertiseApplicationDtoConverter extends ConverterConfigurerSupport<ExpertiseApplication, ExpertiseApplicationDto> {

    @Override
    protected Converter<ExpertiseApplication, ExpertiseApplicationDto> converter() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void configure(ModelMapper modelMapper) {
        modelMapper.addConverter(converter(modelMapper));
    }

    protected Converter<ExpertiseApplication, ExpertiseApplicationDto> converter(ModelMapper modelMapper) {

        return new AbstractConverter<ExpertiseApplication, ExpertiseApplicationDto>() {
            @Override
            protected ExpertiseApplicationDto convert(ExpertiseApplication source) {
                return ofNullable(source)
                        .map(expertiseApplication -> ExpertiseApplicationDto.builder()
                                .id(expertiseApplication.getId())
                                .description(expertiseApplication.getDescription())
                                .name(expertiseApplication.getName())
                                .receiveDate(expertiseApplication.getReceiveDate())
                                .status(expertiseApplication.getStatus().name())
                                .rejectComment(expertiseApplication.getRejectComment())
                                .resultDescription(expertiseApplication.getResultDescription())
                                .leaderUser(ofNullable(expertiseApplication.getLeaderUser()).map(x -> modelMapper.map(expertiseApplication.getLeaderUser(), UserDto.class)).orElse(null))
                                .expertiseFiles(ofNullable(expertiseApplication.getExpertiseFiles())
                                        .orElse(Collections.emptySet())
                                        .stream()
                                        .map(x -> modelMapper.map(Hibernate.unproxy(x.getFile(), File.class), FileDto.class))
                                        .collect(Collectors.toSet()))
                                .build())
                        .orElse(null);
            }
        };
    }
}
