package ru.tsu.crimelab.web.services.validators.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import ru.tsu.crimelab.dao.domain.ExpertiseApplicationFile;
import ru.tsu.crimelab.dao.domain.enums.Role;
import ru.tsu.crimelab.dao.repository.ExpertiseApplicationRepository;
import ru.tsu.crimelab.web.services.UserService;
import ru.tsu.crimelab.web.services.validators.ActionType;
import ru.tsu.crimelab.web.services.validators.Validator;

import java.util.Collection;

import static java.util.List.of;
import static ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationStatus.CLOSED;
import static ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationStatus.REJECT;
import static ru.tsu.crimelab.web.services.validators.ActionType.DELETE;
import static ru.tsu.crimelab.web.services.validators.ValidatorUtils.baseValidateByAction;

/**
 * Created by PetrushinDA on 19.01.2020
 */
@Component
@RequiredArgsConstructor
public class ExpertiseApplicationFileValidator extends Validator<ExpertiseApplicationFile> {

    private final ExpertiseApplicationRepository expertiseApplicationRepository;

    private final UserService userService;

    @Override
    protected void validate(@NonNull ExpertiseApplicationFile expertiseApplicationFile, @NonNull ActionType actionType, @NonNull Collection<String> errors) {
        baseValidateByAction(expertiseApplicationFile::getId, actionType, errors);

        var expertiseApplication = expertiseApplicationFile.getExpertiseApplication();
        var currentUser = userService.getCurrentUserByContext();
        if (expertiseApplication == null || expertiseApplication.getId() == null) {
            errors.add("Не передана информация о заявке на проведение экспертизы");
        } else {
            var savedExpertiseApplication = expertiseApplicationRepository.getById(expertiseApplication.getId());
            if (of(CLOSED, REJECT).contains(savedExpertiseApplication.getStatus())) {
                errors.add("Невозможно применить изменения файлов у закрытой заявки на проведение экспертизы");
            }
            if(actionType.equals(DELETE) && !expertiseApplicationFile.getFile().getCreatorUser().getId().equals(currentUser.getId())) {
                errors.add("Удалять файлы может только создатель файла");
            }
        }
    }
}
