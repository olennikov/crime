package ru.tsu.crimelab.web.mapper.config.converters;

import lombok.RequiredArgsConstructor;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.tsu.crimelab.dao.domain.File;
import ru.tsu.crimelab.web.dto.file.FileDto;
import ru.tsu.crimelab.web.dto.user.UserDto;
import ru.tsu.crimelab.web.mapper.config.ConverterConfigurerSupport;

import static java.util.Optional.ofNullable;

/**
 * Created by PetrushinDA on 14.11.2019
 */
@Component
@RequiredArgsConstructor
public class FileToFileDtoConverter extends ConverterConfigurerSupport<File, FileDto> {

    @Override
    protected Converter<File, FileDto> converter() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void configure(ModelMapper modelMapper) {
        modelMapper.addConverter(converter(modelMapper));
    }

    protected Converter<File, FileDto> converter(ModelMapper modelMapper) {
        return new AbstractConverter<File, FileDto>() {
            @Override
            protected FileDto convert(File source) {
                return ofNullable(source)
                        .map(file -> FileDto.builder()
                                .id(file.getId())
                                .filename(file.getFileName())
                                .creator(modelMapper.map(file.getCreatorUser(), UserDto.class))
                                .dateCreate(file.getDateCreate())
                                .build())
                        .orElse(null);
            }
        };
    }
}
