package ru.tsu.crimelab.web.services;

import org.springframework.web.multipart.MultipartFile;
import ru.tsu.crimelab.web.dto.director.CreateTaskDto;
import ru.tsu.crimelab.web.dto.director.TaskDto;

import java.io.File;
import java.util.List;

public interface TaskService {
    /**
     * Метод возвращающий все задачи поотправленному идентификатору экспертизы
     *
     * @param expertiseId идентификатор экспертизы
     * @return список задач экспертизы
     */
    List<CreateTaskDto> getTasksByExpertise(Long expertiseId);

    /**
     * Метод возвращает сущность задачи экспертизы по идентификатору
     *
     * @param taskId идентификатор задачи
     * @return сущность задачи экспертизы
     */
    TaskDto getTaskById(Long taskId);

    /**
     * Метод для сохранения данных формы генерации задачи
     *
     * @return
     */
    Long saveSubtask(TaskDto taskDto);

    /**
     * Выгрузка задачи по идентификатору задачи, у которой в качестве ответственного установлен текущий пльзователь
     *
     * @param taskId идентификатор задачи
     * @return объект задачи
     */
    ru.tsu.crimelab.web.dto.task.TaskDto getByIdAndCurrentUserAsResponsible(Long taskId);

    /**
     * Закрытие задачи
     *
     * @param taskId идентификатор задачи
     */
    void close(Long taskId);
    /**
     * Метод для создания объекта задачи экспертизы
     * @param createTaskDto - объект запроса фронта для создания объекта задачи экспертизы
     * @return - идентификатор созданной задачи
     */
    Long createTask (CreateTaskDto createTaskDto);

    /**
     * Метод для прикрепления файла к задаче
     * @param taskId - идентификатор задачи, к которой прикрепляется файл
     * @param file - объект файла, прикрепляемый к задаче
     * @return - сущность подзадачи к которой был прикреплён файл
     */
    ru.tsu.crimelab.web.dto.task.TaskDto attachFileToTask (Long taskId, MultipartFile file);

    /**
     * Метод для удаления файла от объекта задачи
     * @param fileId - идентификатор файла
     * @param taskId - идентификатор задачи
     */
    void deleteFile (Long fileId, Long taskId);
}
