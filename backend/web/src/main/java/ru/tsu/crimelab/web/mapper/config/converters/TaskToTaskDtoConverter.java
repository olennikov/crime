package ru.tsu.crimelab.web.mapper.config.converters;

import lombok.RequiredArgsConstructor;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.tsu.crimelab.dao.domain.Task;
import ru.tsu.crimelab.web.dto.file.FileDto;
import ru.tsu.crimelab.web.dto.task.TaskDto;
import ru.tsu.crimelab.web.dto.user.UserDto;
import ru.tsu.crimelab.web.mapper.config.ConverterConfigurerSupport;

import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

import static java.lang.String.format;
import static ru.tsu.crimelab.dao.domain.enums.TaskStatus.CLOSED;

/**
 * Created by PetrushinDA on 19.11.2019
 * Конвертер сущности Task в TaskDto
 */
@Component
@RequiredArgsConstructor
public class TaskToTaskDtoConverter extends ConverterConfigurerSupport<Task, TaskDto> {

    @Override
    protected Converter<Task, TaskDto> converter() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void configure(ModelMapper modelMapper) {
        modelMapper.addConverter(converter(modelMapper));
    }

    protected Converter<Task, TaskDto> converter(ModelMapper modelMapper) {
        return new AbstractConverter<>() {
            @Override
            protected TaskDto convert(Task task) {
                String progress = format("%s/%s", task.getSubTasks().stream().filter(x -> nonNull(x.getIsClosed()) && x.getIsClosed()).count(), task.getSubTasks().size());
                return TaskDto.builder()
                        .id(task.getId())
                        .deadlineDate(task.getDeadlineDate())
                        .expiredDate(task.getExpiredDate())
                        .name(task.getName())
                        .isClosed(CLOSED.equals(task.getTaskStatus()))
                        .progress(progress)
                        .responsible(modelMapper.map(task.getResponsibleUser(), UserDto.class))
                        .taskFiles(task.getTaskFiles()
                                .stream()
                                .map(x->modelMapper.map(x.getFile(), FileDto.class))
                                .collect(Collectors.toList()))
                        .build();
            }
        };
    }
}
