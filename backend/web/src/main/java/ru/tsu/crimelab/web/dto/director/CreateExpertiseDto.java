package ru.tsu.crimelab.web.dto.director;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.crimelab.dao.domain.enums.ExpertiseStatus;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class CreateExpertiseDto {
    private Long id;
    private String name;
    private String description;
    private ExpertiseStatus status;
    private Long expertiseApplicationId;
    private List<TaskViewDto> taskArray = new ArrayList<>();
}
