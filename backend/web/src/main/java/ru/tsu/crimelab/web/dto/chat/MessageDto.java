package ru.tsu.crimelab.web.dto.chat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import ru.tsu.crimelab.web.dto.user.UserDto;

import java.time.ZonedDateTime;

/**
 * DTO сущность для представления сообщения чата
 */
@Data
@ApiModel(value = "MessageDto: сообщение чата", description = "Сообщение чата")
public class MessageDto {

    @ApiModelProperty(required = true, value = "Идентификатор сообщения")
    private Long id;

    @ApiModelProperty(required = true, value = "Чат в котором находится данное сообщение")
    private ChatDto chat;

    @ApiModelProperty(required = false, value = "Дата и время отправки сообщения")
    private ZonedDateTime timestamp;

    @ApiModelProperty(required = true, value = "Текст сообщения")
    private String text;

    @ApiModelProperty(required = true, value = "Пользователь, написавший сообщение")
    private UserDto writer;
}
