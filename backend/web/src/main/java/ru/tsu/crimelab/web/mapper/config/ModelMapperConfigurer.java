package ru.tsu.crimelab.web.mapper.config;

import org.modelmapper.ModelMapper;

/**
 * Created by PetrushinDA on 14.11.2019
 * Позволяет настроить конкретное поведение маппера {@link org.modelmapper.ModelMapper}.
 * Все конфиги маппера должны быть наследниками данного интерфейса (фабрика собирает конфиг маппера имеено на основе данного интерфейса)
 * <p>
 * <pre>
 * <strong>Example:</strong>
 * &#064;Component
 * public class UserMapping implements ModelMapperConfigurer {
 *
 *      void configure(ModelMapper modelMapper) {
 *          modelMapper.getConfiguration()
 *              .setSourceNamingConvention(NamingConventions.NONE);
 *              .setDestinationNamingConvention(NamingConventions.NONE);
 *      }
 * }
 * </pre>
 */
public interface ModelMapperConfigurer {
    /**
     * Настройка экземпляра маппера
     *
     * @param modelMapper {@link ModelMapper} текущий инстанс маппера
     */
    void configure(ModelMapper modelMapper);
}
