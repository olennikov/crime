package ru.tsu.crimelab.web.services.validators.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import ru.tsu.crimelab.dao.domain.ExpertiseApplication;
import ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationStatus;
import ru.tsu.crimelab.dao.repository.ExpertiseApplicationRepository;
import ru.tsu.crimelab.web.services.validators.ActionType;
import ru.tsu.crimelab.web.services.validators.ValidationException;
import ru.tsu.crimelab.web.services.validators.Validator;

import java.util.Collection;
import java.util.List;

import static java.util.List.of;
import static java.util.Objects.isNull;
import static ru.tsu.crimelab.web.services.validators.ValidatorUtils.baseValidateByAction;

/**
 * Created by PetrushinDA on 15.01.2020
 */
@Service
@RequiredArgsConstructor
public class ExpertiseApplicationValidator extends Validator<ExpertiseApplication> {

    private final List<ExpertiseApplicationStatus> STOPPED_UPDATE_EXPERTISE_APPLICATION = of(ExpertiseApplicationStatus.CLOSED,
            ExpertiseApplicationStatus.REJECT);

    private final ExpertiseApplicationRepository expertiseApplicationRepository;

    @Override
    protected void validate(@NonNull ExpertiseApplication expertiseApplication, @NonNull ActionType actionType, @NonNull Collection<String> errors) {
        baseValidateByAction(expertiseApplication::getId, actionType, errors);

        if (isNull(expertiseApplication.getName()))
            errors.add("Не установлено наименование заявки на проведение экспертизы");

        if (isNull(expertiseApplication.getDescription()))
            errors.add("Не установлено описание заявки на проведение экспертизы");

        if (isNull(expertiseApplication.getReceiveDate()))
            errors.add("Не установлена дата создания заявки на проведение экспертизы");

        if (actionType.equals(ActionType.UPDATE)) {
            var savedExpertiseApplication = expertiseApplicationRepository.getById(expertiseApplication.getId());

            if (STOPPED_UPDATE_EXPERTISE_APPLICATION.contains(savedExpertiseApplication.getStatus()))
                errors.add("Операция невозможна. Данная заявка на проведение экспертизы закрыта");
        }

        if (!errors.isEmpty())
            throw new ValidationException(errors);
    }
}
