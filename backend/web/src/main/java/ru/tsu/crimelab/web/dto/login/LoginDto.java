package ru.tsu.crimelab.web.dto.login;

import lombok.Data;

/**
 * Created by PetrushinDA on 13.10.2019
 */
@Data
public class LoginDto {
    private String login;
    private String password;
    private String roundSalt;
}
