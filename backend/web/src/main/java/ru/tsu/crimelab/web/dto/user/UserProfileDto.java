package ru.tsu.crimelab.web.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserProfileDto {
    private String lastName;
    private String firstName;
    private String secondName;
    private String phone;
    private String email;
    private String organisationName;
    private String organisationPhone;
}
