package ru.tsu.crimelab.web.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.tsu.crimelab.dao.api.repository.response.QueryResponse;
import ru.tsu.crimelab.web.dto.expertise.ExpertiseDto;
import ru.tsu.crimelab.web.dto.expertise.ExpertiseFullDto;
import ru.tsu.crimelab.web.dto.expertise.ExpertiseStatusDto;
import ru.tsu.crimelab.web.dto.expertise.ExpertiseStoreFiles;
import ru.tsu.crimelab.web.services.ExpertiseService;

/**
 * Created by PetrushinDA on 17.11.2019
 * Контроллер для работы с заявками на экспертизу клиентом
 */
@RestController
@RequestMapping("/expertise")
@RequiredArgsConstructor
@PreAuthorize("hasAnyRole(T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).employeeRoleName, " +
        "T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).directorRoleName)")
@Api(value = "Контроллер для работы с экспертизами")
public class ExpertiseController {

    private final ExpertiseService expertiseService;

    @ResponseBody
    @ApiOperation(value = "Получение списка экспертиз")
    @GetMapping(value = "/list")
    public QueryResponse<ExpertiseDto> getExpertiseApplicationList(Pageable pageable, @RequestParam("status") ExpertiseStatusDto status) {
        return expertiseService.getExpertiseList(pageable, status);
    }

    @ResponseBody
    @ApiOperation(value = "Получение файлов экспертизы")
    @GetMapping(value = "/{id}/files")
    public ExpertiseStoreFiles getExpertiseStoreFiles(@PathVariable("id") Long expertiseId) {
        return expertiseService.getExpertiseStoreFiles(expertiseId);
    }

    @ResponseBody
    @ApiOperation(value = "Получение экспертизы")
    @GetMapping(value = "{id}")
    public ExpertiseFullDto getExpertise(@PathVariable("id") Long expertiseId) {
        return expertiseService.getExpertise(expertiseId);
    }
}
