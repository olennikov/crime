package ru.tsu.crimelab.web.services.impl;

import antlr.Utils;
import lombok.RequiredArgsConstructor;
import org.apache.commons.codec.digest.DigestUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsu.crimelab.dao.domain.User;
import ru.tsu.crimelab.dao.repository.UserRepository;
import ru.tsu.crimelab.security.jwt.SecurityService;
import ru.tsu.crimelab.web.dto.login.EditPasswordDto;
import ru.tsu.crimelab.web.dto.login.LoginSaltDto;
import ru.tsu.crimelab.web.dto.user.UserProfileDto;
import ru.tsu.crimelab.web.services.ProfileService;
import ru.tsu.crimelab.web.services.Util;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ProfileServiceImpl implements ProfileService {

    private final UserRepository userRepository;

    private final SecurityService securityService;

    private final ModelMapper modelMapper;

    @Override
    @Transactional(readOnly = true)
    public UserProfileDto getUserProfile() {
        return Optional.ofNullable(securityService.getCurrentEmployee())
                .map(x -> userRepository.findById(x.getId()))
                .orElseThrow(() -> new IllegalArgumentException("Пользователь не зарегистрирован"))
                .map(x->modelMapper.map(x,UserProfileDto.class))
                .orElseThrow(() -> new IllegalArgumentException("Не удалось найти пользователя с заданным идентификатором"));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long editUserProfile(UserProfileDto userProfileDto) {
        User user = Optional.ofNullable(securityService.getCurrentEmployee())
                .map(x -> userRepository.findById(x.getId()))
                .orElseThrow(() -> new IllegalArgumentException("Сессия не содержит данных о пользователе"))
                .orElseThrow(() -> new IllegalArgumentException("Пользователь с данным id отсутствует"));
        user.setFirstName(userProfileDto.getFirstName());
        user.setLastName(userProfileDto.getLastName());
        user.setSecondName(userProfileDto.getSecondName());
        user.setEmail(userProfileDto.getEmail());
        user.setPhone(userProfileDto.getPhone());
        userRepository.save(user);
        return user.getId();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long editUserPassword(EditPasswordDto editPasswordDto) {
        User user = Optional.ofNullable(securityService.getCurrentEmployee())
                .map(x -> userRepository.findById(x.getId()))
                .orElseThrow(() -> new IllegalArgumentException("Сессия не содержит данных о пользователе"))
                .orElseThrow(() -> new IllegalArgumentException("Пользователь с данным id отсутствует"));
        String hashedPassword = Util.saltPassword(user.getPassword(),  editPasswordDto.getRoundSalt());
        if (hashedPassword.equals(editPasswordDto.getOldPassword())) {
            user.setPassword(editPasswordDto.getNewPassword());
            user.setSalt(editPasswordDto.getNewSalt());
            userRepository.save(user);
            return user.getId();
        } else {
            throw new IllegalArgumentException("Текущий пароль указан неверно");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public LoginSaltDto getRoundSalt() {
        User user = Optional.ofNullable(securityService.getCurrentEmployee())
                .map(x -> userRepository.findById(x.getId()))
                .orElseThrow(() -> new IllegalArgumentException("Сессия не содержит данных о пользователе"))
                .orElseThrow(() -> new IllegalArgumentException("Пользователь с данным id отсутствует"));
        return LoginSaltDto.builder()
                .roundSalt(UUID.randomUUID().toString())
                .userSalt(user.getSalt())
                .build();
    }

}
