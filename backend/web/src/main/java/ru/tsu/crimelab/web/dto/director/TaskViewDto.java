package ru.tsu.crimelab.web.dto.director;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.crimelab.dao.domain.enums.TaskStatus;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaskViewDto {
    private Long id;
    private String name;
    private TaskStatus taskStatus;
    private String progress;
}
