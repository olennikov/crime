package ru.tsu.crimelab.web.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Sets;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsu.crimelab.appstarter.rabbitmq.consts.notification.Queues;
import ru.tsu.crimelab.dao.domain.Chat;
import ru.tsu.crimelab.dao.domain.ExpertiseApplication;
import ru.tsu.crimelab.dao.domain.Message;
import ru.tsu.crimelab.dao.domain.User;
import ru.tsu.crimelab.dao.repository.ChatRepository;
import ru.tsu.crimelab.dao.repository.MessageRepository;
import ru.tsu.crimelab.notification.messages.MessageInfo;
import ru.tsu.crimelab.web.dto.chat.ChatDto;
import ru.tsu.crimelab.web.dto.chat.MessageDto;
import ru.tsu.crimelab.web.services.ChatService;
import ru.tsu.crimelab.web.services.UserService;
import ru.tsu.crimelab.web.services.validators.impl.ChatValidator;
import ru.tsu.crimelab.web.services.validators.impl.MessageValidator;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;
import static ru.tsu.crimelab.web.services.validators.ActionType.CREATE;

@Slf4j
@Service
@RequiredArgsConstructor
public class ChatServiceImpl implements ChatService {


    private final ModelMapper modelMapper;

    private final ChatValidator chatValidator;

    private final MessageValidator messageValidator;

    private final MessageRepository messageRepository;

    private final ChatRepository chatRepository;

    private final UserService userService;

    private final ObjectMapper objectMapper;

    private final RabbitTemplate rabbitTemplate;

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Chat> getById(@NonNull Long id) {
        return chatRepository.findById(id);
    }

    /**
     * Загрузка сообщений для отображения
     *
     * @param expertiseApplicationId идентификатор экспертизы
     * @param skip                   количество пропущенных сообщений
     * @param take                   количество загружаемых сообщений
     * @return список сообщений
     */
    @Transactional(readOnly = true)
    @Override
    public List<MessageDto> getMessages(Long expertiseApplicationId, Long skip, Long take) {
        return messageRepository.getMessagesByExpertiseApplication(expertiseApplicationId, skip, take).stream()
                .map(x -> modelMapper.map(x, MessageDto.class))
                .collect(Collectors.toList());
    }

    /**
     * Добавление сообщения в чат
     *
     * @param expertiseApplicationId идентификатор заявки на првоедение экспертизы
     * @param messageDto
     * @return добавленное сообщение
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public MessageDto addMessage(Long expertiseApplicationId, MessageDto messageDto) {
        Chat chat = chatRepository.findFirstByExpertiseApplication_Id(expertiseApplicationId);
        if (!isChatParticipant(chat))
            return null;
        Message message = modelMapper.map(messageDto, Message.class);
        message.setTimestamp(ZonedDateTime.now());
        message.setChat(chat);

        messageValidator.validate(message, CREATE);
        Message savedMessage = messageRepository.save(message);
        MessageDto sendingMessage = modelMapper.map(savedMessage, MessageDto.class);

        chat.getUsers().stream()
                .map(User::getId)
                .collect(Collectors.toList())
                .forEach(userId -> {
                    try {
                        MessageInfo messageInfo = new MessageInfo();
                        messageInfo.setChatId(chat.getId());
                        messageInfo.setData(objectMapper.writeValueAsString(sendingMessage));
                        messageInfo.setUserId(userId);
                        rabbitTemplate.convertAndSend(Queues.SOCKET, messageInfo);
                    } catch (Exception e) {
                        log.error("Error send message ws", e);
                    }
                });
        return modelMapper.map(message, MessageDto.class);
    }

    /**
     * Получение чата для заявки на экспертизу
     *
     * @param expertiseApplicationId идентификатор заявки на экспертизу
     * @return чат
     */
    @Transactional(readOnly = true)
    public ChatDto getChatByExpertiseApplication(Long expertiseApplicationId) {
        return Optional.ofNullable(expertiseApplicationId)
                .map(x -> chatRepository.findFirstByExpertiseApplication_Id(x))
                .map(x -> modelMapper.map(x, ChatDto.class))
                .orElse(null);

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long createChat(ExpertiseApplication expertiseApplication, User leaderUser) {
        Chat chat = Chat.builder()
                .expertiseApplication(expertiseApplication)
                .users(Sets.newHashSet(leaderUser, expertiseApplication.getCreationUser()))
                .build();

        chatValidator.validate(chat, CREATE);
        chatRepository.save(chat);
        return chat.getId();
    }


    @Transactional(readOnly = true)
    @Override
    public boolean isChatParticipant(Long expertiseApplicationId) {
        Chat chat = chatRepository.findFirstByExpertiseApplication_Id(expertiseApplicationId);
        return nonNull(chat) && isChatParticipant(chat);
    }

    /**
     * Проверка является ли текущий пользователь участником чата
     *
     * @param chat сущность чата
     * @return true, если текущий пользователь является участником чата
     */
    private boolean isChatParticipant(@NonNull Chat chat) {
        var currentUser = userService.getCurrentUserByContext();
        return chat.getUsers().stream()
                .anyMatch(x -> x.getId().equals(currentUser.getId()));
    }
}
