package ru.tsu.crimelab.web.dto.expertiseapplication;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.crimelab.web.dto.file.FileDto;
import ru.tsu.crimelab.web.dto.user.UserDto;
import ru.tsu.crimelab.dao.domain.ExpertiseApplication;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by PetrushinDA on 06.11.2019
 * ДТО сущность для работы с заявками на экспертизу (клиент)
 */
@Data
@ApiModel(value = "ExpertiseApplicationDto: Заявка на экспертизу", description = "Заявка на экспертизу")
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class ExpertiseApplicationDto {

    @ApiModelProperty(required = true, value = "Идентификатор заявки")
    private Long id;

    @ApiModelProperty(required = true, value = "Имя экспертизы")
    private String name;

    @ApiModelProperty(required = true, value = "Описание экспертизы")
    private String description;

    @ApiModelProperty(required = true, value = "Дата подачи")
    private Date receiveDate;

    @ApiModelProperty(required = true, value = "Ответственный руководитель")
    private UserDto leaderUser;

    @ApiModelProperty(required = true, value = "Статус заявки на экспертизу")
    private String status;

    @ApiModelProperty(required = true, value = "Комментарий при отказе")
    private String rejectComment;

    @ApiModelProperty(required = true, value = "Заключение по экспертизе")
    private String resultDescription;

    @ApiModelProperty(required = false, value = "Файлы экспертизы")
    private Set<FileDto> expertiseFiles = new HashSet<>();
}
