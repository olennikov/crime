package ru.tsu.crimelab.web.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.tsu.crimelab.web.dto.chat.ChatDto;
import ru.tsu.crimelab.web.dto.chat.MessageDto;
import ru.tsu.crimelab.web.services.ChatService;

import java.util.List;

@RestController
@RequestMapping("/chat/")
@RequiredArgsConstructor
@PreAuthorize("hasAnyRole(T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).customerRoleName, " +
        "T(ru.tsu.crimelab.dao.domain.enums.RoleConstantName).directorRoleName)")
@Api(value = "Контроллер для работы с функционалом чатов в приложении")
public class ChatController {

    private final ChatService chatService;

    @ApiOperation(value = "Получение сообщений чата")
    @GetMapping(value = "messages/{expertise_application_id}")
    public List<MessageDto> getMessages(
            @ApiParam("Идентификатор экспертизы") @PathVariable(value = "expertise_application_id", required = true) Long expertiseApplicationId,
            @ApiParam("Количество пропущенных сообщений") @RequestParam(value = "skip", required = true) Long skip,
            @ApiParam("Количество сообщений для выгрузки") @RequestParam(value = "take", required = true) Long take) {
        return chatService.getMessages(expertiseApplicationId, skip, take);
    }

    @ApiOperation(value = "Добавление сообщения в чат")
    @PostMapping(value = "message")
    public MessageDto addMessage(
            @ApiParam("Сообщение для сохранения") @RequestBody MessageDto message) {
        MessageDto messageDto = chatService.addMessage(message.getChat().getExpertiseApplication().getId(), message);
        if (messageDto == null) {
            throw new RuntimeException("Пользователь не является участником чата, поэтому он не может отправлять сообщения");
        }
        return messageDto;
    }

    @ApiOperation(value = "Получение чата экспертизы")
    @GetMapping(value = "{expertise_application_id}")
    public ChatDto getChatExpertise(
            @ApiParam("Идентификатор экспертизы") @PathVariable(value = "expertise_application_id", required = true) Long expertiseApplicationId) {
        return chatService.getChatByExpertiseApplication(expertiseApplicationId);
    }

    @ApiOperation(value = "Является ли пользователь участником чата")
    @GetMapping(value = "isChatParticipant/{expertise_application_id}")
    public boolean isChatParticipant(@ApiParam("Идентификатор экспертизы") @PathVariable(value = "expertise_application_id") Long expertiseApplicationId) {
        return chatService.isChatParticipant(expertiseApplicationId);
    }
}
