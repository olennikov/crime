package ru.tsu.crimelab.web.dto.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;


/**
 * Created by PetrushinDA on 06.11.2019
 * ДТО сущность пользователя (клиент)
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "UserDto: Пользователь", description = "Пользователь")
public class UserDto {

    @ApiModelProperty(required = true, value = "Идентификатор пользователя")
    private Long id;

    @ApiModelProperty(required = true, value = "Фамилия пользователя")
    private String lastName;

    @ApiModelProperty(required = true, value = "Имя пользователя")
    private String firstName;

    @ApiModelProperty(required = true, value = "Отчество пользователя")
    private String secondName;

    @ApiModelProperty(required = true, value = "Номер телефона пользователя")
    private String phone;
}
