package ru.tsu.crimelab.web.mapper.config.converters;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.springframework.stereotype.Component;
import ru.tsu.crimelab.dao.domain.Organisation;
import ru.tsu.crimelab.dao.domain.User;
import ru.tsu.crimelab.web.dto.user.UserProfileDto;
import ru.tsu.crimelab.web.mapper.config.ConverterConfigurerSupport;

import java.util.Optional;

@Component
public class UserToUserProfileDtoConverter extends ConverterConfigurerSupport<User, UserProfileDto> {
    @Override
    protected Converter<User, UserProfileDto> converter() {
        return new AbstractConverter<User, UserProfileDto>() {
            @Override
            protected UserProfileDto convert(User user) {
                return UserProfileDto.builder()
                        .firstName(user.getFirstName())
                        .lastName(user.getLastName())
                        .secondName(user.getSecondName())
                        .phone(user.getPhone())
                        .email(user.getEmail())
                        .organisationName(Optional
                                .ofNullable(user.getOrganisation())
                                .map(Organisation::getName)
                                .orElse(null))
                        .organisationPhone(Optional
                                .ofNullable(user.getOrganisation())
                                .map(Organisation::getPhone)
                                .orElse(null))
                        .build();
            }
        };
    }
}
