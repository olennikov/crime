package ru.tsu.crimelab.web;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.http.fileupload.FileUploadBase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.util.NestedServletException;
import ru.tsu.crimelab.web.dto.ErrorResponse;

import java.util.Arrays;

import static java.lang.String.format;
import static org.apache.commons.lang3.exception.ExceptionUtils.getMessage;

/**
 * Created by PetrushinDA on 06.12.2019
 */
@ControllerAdvice
@Slf4j
public class ExceptionControllerAdvice {

    private static final int STACKTRACE_MAX_SIZE = 1000;
    @Value("${spring.servlet.multipart.maxFileSize}")
    String maxFileSize;

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorResponse exceptionInternalServerErrorHandler(Exception ex) {
        String message = getErrorMessageFromException(ex);
        log.error(message);
        return new ErrorResponse(message, StringUtils.left(formatStackTrace(ex), STACKTRACE_MAX_SIZE));
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public ErrorResponse exceptionHandlerForbidden(Exception ex) {
        String message = getMessage(ex);
        log.error(message);
        return new ErrorResponse(message, "");
    }

    @ExceptionHandler({MaxUploadSizeExceededException.class, FileUploadBase.SizeLimitExceededException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorResponse handleMaxSizeException(MaxUploadSizeExceededException ex) {
        log.error(ex.getMessage(), ex);
        return new ErrorResponse(format("Максимальный размер загружаемого файла составляет: %s", maxFileSize), formatStackTrace(ex));
    }

    private String getErrorMessageFromException(Exception exception) {
        return (exception instanceof NestedServletException) ? exception.getCause().getMessage() :
                exception.getMessage();
    }

    private String formatStackTrace(Throwable e) {
        StringBuilder sb = new StringBuilder();
        sb.append(e.getMessage());
        Arrays.stream(e.getStackTrace())
                .filter(element -> element.getClassName().startsWith("ru.crimelab"))
                .forEach(element -> {
                    sb.append(" \tat ");
                    sb.append(element.toString());
                    sb.append("\n");
                });

        return sb.toString();
    }
}
