package ru.tsu.crimelab.web.services;

import org.springframework.lang.NonNull;
import org.springframework.web.multipart.MultipartFile;
import ru.tsu.crimelab.dao.api.Query;
import ru.tsu.crimelab.dao.api.repository.response.QueryResponse;
import ru.tsu.crimelab.web.dto.director.ExpertiseApplicationResultDto;
import ru.tsu.crimelab.web.dto.director.RejectExpertiseApplicationDto;
import ru.tsu.crimelab.web.dto.expertiseapplication.ExpertiseApplicationDto;
import ru.tsu.crimelab.web.dto.expertiseapplication.ExpertiseApplicationStatus;
import ru.tsu.crimelab.web.dto.expertiseapplication.ExpertiseApplicationView;

import java.util.List;

/**
 * Created by PetrushinDA on 06.11.2019
 * Сервис для работы с заявками на экспертизу
 */
public interface ExpertiseApplicationService {

    /**
     * Метод получения заявок, где создателем является текущий пользователь
     *
     * @param status статус выгружаемых заявок
     * @return перечень заявок
     */
    List<ExpertiseApplicationDto> getByCurrentUserCreator(ExpertiseApplicationStatus status);

    /**
     * Метод для получения списка заявок на экспертизы руководителем
     *
     * @param query объект запроса от фронта (список фильтров, сортировок)
     * @return возвращает объект содержащий список
     */
    QueryResponse<ExpertiseApplicationView> getExpertiseApplicationListForDirector(Query query);

    /**
     * Метод возвращает сущность в формате dto по идентификатору
     *
     * @param id
     * @return
     */
    ExpertiseApplicationDto getExpertiseApplicationDtoById(Long id);

    /**
     * Метод для отказа в проведении экспертизы
     *
     * @param reject сущность описывающая причину отказа
     * @return идентификатор сущности по которой был зарегистрирован отказ
     */
    Long rejectExpertiseApplication(RejectExpertiseApplicationDto reject);

    /**
     * Метод загрузки заявления на экспертизу по идентифиактору
     *
     * @param id идентификатор заявки
     * @return объект заявки на проведение экспертизы
     */
    ExpertiseApplicationDto getExpertiseApplicationById(Long id);

    /**
     * Метод обновления заявления на экспертизу по идентифиактору
     *
     * @param expertiseApplicationDto объект заявки
     * @return объект заявки на проведение экспертизы
     */
    ExpertiseApplicationDto updateExpertiseApplication(ExpertiseApplicationDto expertiseApplicationDto);

    /**
     * Метод создания заявления на экспертизу по идентифиактору
     *
     * @param expertiseApplicationDto объект заявки
     * @return объект заявки на проведение экспертизы
     */
    ExpertiseApplicationDto createExpertiseApplication(ExpertiseApplicationDto expertiseApplicationDto);

    /**
     * Метод прикрепления файла
     *
     * @param expertiseApplicationId идентификатор заявления
     * @param multipartFile          объект файла
     * @return обновленный объект
     */
    ExpertiseApplicationDto uploadFile(Long expertiseApplicationId, MultipartFile multipartFile);

    /**
     * Метод удаления файла
     *
     * @param expertiseApplicationId идентификатор заявления
     * @param fileId                 идентификатор файла
     */
    void deleteFile(@NonNull Long expertiseApplicationId, @NonNull Long fileId);

    /**
     * Метод для принятия заявки в работу
     *
     * @param applicationId идентификатор заявки
     * @return успех или неуспех взятия в работу
     */
    ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationStatus setAcceptApplication(Long applicationId);

    /**
     * Метод для обновления сущности заявки для предоставления результата
     *
     * @param resultDto
     * @return
     */
    Long updateResultExpertiseApplication(ExpertiseApplicationResultDto resultDto);

    /**
     * Метод для запроса результата экспертизы
     *
     * @param expertiseApplicationId идентификатор заявки на экспертизу
     * @return
     */
    ExpertiseApplicationResultDto getExpertiseResult(Long expertiseApplicationId);

    /**
     * Метод для прикрепления файлов результата
     *
     * @param applicationId идентификатор
     * @param multipartFile
     * @return
     */
    Long saveResultFiles(Long applicationId, MultipartFile multipartFile);

    /**
     * Метод для переводи объекта заявки экспертизы в статус "в работе"
     *
     * @param applicationId идентификатор заявки
     * @return
     */
    ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationStatus setInWorkApplication(Long applicationId);

    /**
     * Метод для закрытия объекта заявки экспертизы
     *
     * @param applicationId идентификатор заявки экспертизы
     * @return объект статуса экспертизы
     */
    ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationStatus closeExpertiseApplication(Long applicationId);
}
