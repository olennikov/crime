package ru.tsu.crimelab.web.dto.director;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class EmployeeUserDto {
    private Long id;
    private String lastName;
    private String firstName;
    private String secondName;
}
