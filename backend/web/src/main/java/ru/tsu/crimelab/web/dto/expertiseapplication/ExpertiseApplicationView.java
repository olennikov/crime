package ru.tsu.crimelab.web.dto.expertiseapplication;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.crimelab.dao.domain.ExpertiseApplication;

import java.util.Date;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class ExpertiseApplicationView {
    private Long id;
    private Date receiveDate;
    private String expertiseName;
    private String creationUserName;
}
