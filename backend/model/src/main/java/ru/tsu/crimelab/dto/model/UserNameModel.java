package ru.tsu.crimelab.dto.model;

import lombok.Data;
import ru.tsu.crimelab.dto.api.Model;

@Data
@Model()
public class UserNameModel {
    private Long id;
    private String firstName;
    private String lastName;
    private String secondName;
}
