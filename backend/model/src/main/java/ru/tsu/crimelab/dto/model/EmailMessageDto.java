package ru.tsu.crimelab.dto.model;

import lombok.Data;

/**
 * Created by PetrushinDA on 26.10.2019
 */
@Data
public class EmailMessageDto {
    private String email;
    private String text;
    private String subject;
}
