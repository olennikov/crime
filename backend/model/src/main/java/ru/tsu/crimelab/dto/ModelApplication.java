package ru.tsu.crimelab.dto;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import ru.tsu.crimelab.dto.api.ModelNamedFactoryBean;

@Configuration
@Component
public class ModelApplication {


    @Value("${model.package.scan}")
    private String basePackageModelScan;

//    @Bean
//    public ModelMapper modelMapper (){
//        ModelMapper mapper = new ModelMapper();
//        mapper.getConfiguration()
//                .setMatchingStrategy(MatchingStrategies.STRICT)
//                .setFieldMatchingEnabled(true)
//                .setSkipNullEnabled(true)
//                .setFieldAccessLevel(PRIVATE);
//        return mapper;
//    }

    @Bean
    public ModelNamedFactoryBean modelNamed() {
        return new ModelNamedFactoryBean(basePackageModelScan);
    }
}
