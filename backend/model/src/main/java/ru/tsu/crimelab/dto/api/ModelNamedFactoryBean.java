package ru.tsu.crimelab.dto.api;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.stream.Collectors;

public class ModelNamedFactoryBean {

    private final String basePackage;
    private final Map<String, Class<?>> namedModels;

    public ModelNamedFactoryBean (String basePackage){
        this.basePackage = basePackage;
        this.namedModels = initialize();
    }

    private Map<String,Class<?>> initialize (){
        Reflections reflections = new Reflections(basePackage,new SubTypesScanner(), new TypeAnnotationsScanner());
        return reflections.getTypesAnnotatedWith(Model.class)
                .stream()
                .collect(Collectors.toMap(x->getModelName(x),x->x));
    }

    private String getModelName (Class<?> cls) {
        Model modelName = cls.getAnnotation(Model.class);
        if (StringUtils.isEmpty(modelName.name())){
            return cls.getSimpleName();
        } else {
            return modelName.name();
        }
    }

    public boolean constainsModel (String modelName){
        return namedModels.containsKey(modelName);
    }

    public Class<?> getModel (String modelName){
        return namedModels.get(modelName);
    }
}
