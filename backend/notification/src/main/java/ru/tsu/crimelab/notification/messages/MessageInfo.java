package ru.tsu.crimelab.notification.messages;

import lombok.Data;

/**
 * Created by PetrushinDA on 30.10.2019
 */
@Data
public class MessageInfo {
    private Long chatId;
    private Long userId;
    private String data;
}
