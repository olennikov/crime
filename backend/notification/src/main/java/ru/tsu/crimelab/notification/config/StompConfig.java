package ru.tsu.crimelab.notification.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.scheduling.concurrent.DefaultManagedTaskScheduler;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * Created by PetrushinDA on 30.10.2019
 * Конфигурация для протокола Stomp через SockJs
 */
@Configuration
@EnableWebSocketMessageBroker
public class StompConfig implements WebSocketMessageBrokerConfigurer {

    public static final String CHAT_BROKER_ADDRESS = "/chat";

    //25 - секнуд стандартное время для обмена пингами.
    @Value("${crimelab.notification.websocket.stomp.heartbeat.incoming:25000}")
    public long heartbeatIncoming;

    @Value("${crimelab.notification.websocket.stomp.heartbeat.outcoming:25000}")
    public long heartbeatOutcoming;

    /**
     * Настройка точки для подключения клиентов
     */
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        // Точка входа для подключения SockJs
        registry.addEndpoint("/socket/sockJs/")
                .setAllowedOrigins("*")
                .withSockJS();
    }

    /**
     * Настройка брокеров, на которых могут подписываться клинеты и префикса для отправки сообщений.
     */
    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.setApplicationDestinationPrefixes("/app")
                .enableSimpleBroker(CHAT_BROKER_ADDRESS)
                .setHeartbeatValue(new long[]{heartbeatIncoming, heartbeatOutcoming})
                .setTaskScheduler(new DefaultManagedTaskScheduler());
    }

}
