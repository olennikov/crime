package ru.tsu.crimelab.notification.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import ru.tsu.crimelab.appstarter.rabbitmq.consts.notification.Queues;
import ru.tsu.crimelab.notification.messages.MessageInfo;

/**
 * Created by PetrushinDA on 30.10.2019
 * Слушатель событий из очереди notifications.
 */
@Component
@Slf4j
public class NotificationsQueueListener {

    private final SimpMessagingTemplate template;

    @Autowired
    public NotificationsQueueListener(SimpMessagingTemplate template) {
        this.template = template;
    }

    /**
     * Обработчки сообщений.
     * Пока что просто отправляет уведомление на фронт.
     */
    @RabbitListener(queues = {Queues.SOCKET})
    public void processNewNotificationEvent(@Payload MessageInfo messageInfo) {
        doLog(Queues.SOCKET, messageInfo.toString());

        template.convertAndSend(String.format("/chat/%s/user/%s", messageInfo.getChatId(), messageInfo.getUserId()),
                messageInfo.getData());
    }

    private void doLog(String queueName, String message) {
        log.info("Получено сообщение из очереди {} контент: {}", queueName, message);
    }
}
