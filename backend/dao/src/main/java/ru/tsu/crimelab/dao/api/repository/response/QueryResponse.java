package ru.tsu.crimelab.dao.api.repository.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class QueryResponse<RecordType> {
    private Long count;
    private List<RecordType> records;
}
