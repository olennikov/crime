package ru.tsu.crimelab.dao.api.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Getter
@Data
public class Order {
    private String field;
    private OrderDirection direction;
}
