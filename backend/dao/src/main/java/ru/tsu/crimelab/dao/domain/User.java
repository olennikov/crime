package ru.tsu.crimelab.dao.domain;

import lombok.*;
import ru.tsu.crimelab.dao.domain.enums.Role;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = "users")
public class User implements BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "first_name")
    @NotNull
    private String firstName;

    @Column(name = "last_name")
    @NotNull
    private String lastName;

    @Column(name = "second_name")
    private String secondName;

    @Column(name = "phone")
    @NotNull
    private String phone;

    @Column(name = "email",unique = true)
    private String email;

    @NotNull
    @Column(name = "salt")
    private String salt;

    @Column(name = "user_password", length = 256)
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name="role")
    private Role role;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name="organisation_id")
    private Organisation organisation;

    @Column(name = "is_block")
    private Boolean isBlock;

    public static class UserBuilder{
        public UserBuilder registrateBuilder (RegistrationApplication registration){
            return firstName(registration.getFirstName())
                    .lastName(registration.getLastName())
                    .firstName(registration.getFirstName())
                    .secondName(registration.getSecondName())
                    .email(registration.getEmail())
                    .phone(registration.getPhone());
        }
    }
}
