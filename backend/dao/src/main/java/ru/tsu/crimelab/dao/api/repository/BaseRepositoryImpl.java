package ru.tsu.crimelab.dao.api.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.tsu.crimelab.dao.api.Query;
import ru.tsu.crimelab.dao.api.filter.filterparse.ValueParser;
import ru.tsu.crimelab.dao.api.filter.filterparse.ValueParserFactory;
import ru.tsu.crimelab.dao.api.order.Order;
import ru.tsu.crimelab.dao.api.repository.response.QueryResponse;
import ru.tsu.crimelab.dao.api.filter.fields.BaseField;
import ru.tsu.crimelab.dao.domain.BaseEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.SingularAttribute;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;


public class BaseRepositoryImpl <Entity extends BaseEntity<ID>, ID extends Serializable>
        extends SimpleJpaRepository<Entity,ID>
        implements BaseRepository<Entity,ID>{

    private static final ObjectMapper objectMapper = new ObjectMapper();

    private final EntityManager entityManager;
    private final Class<Entity> entityClass;
    private final EntityType<Entity> entityType;

    public BaseRepositoryImpl(JpaEntityInformation<Entity, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
        this.entityClass = entityInformation.getJavaType();
        this.entityType = entityManager.getMetamodel().entity(entityClass);
    }

    public BaseRepositoryImpl(Class<Entity> domainClass, EntityManager em) {
        super(domainClass, em);
        this.entityManager = em;
        this.entityClass = domainClass;
        this.entityType = em.getMetamodel().entity(entityClass);
    }

    @Override
    public Class<Entity> getEntityClass() {
        return entityClass;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Entity> query(Query query) {
        try{
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            List queryList = getPagingList(query,criteriaBuilder);
            return queryList;
        } catch (Exception ex){
            ex.printStackTrace();
            throw (ex);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Long count(Query query) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        Long count = getPagingCount(query,cb);
        return count;
    }

    @Override
    @Transactional(readOnly = true)
    public QueryResponse<Entity> pagingQuery(Query query) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        List pagingList = getPagingList(query,criteriaBuilder);
        Long count = getPagingCount(query, criteriaBuilder);
        return new QueryResponse(count,pagingList);
    }

    @Override
    public List<Predicate> createWhereList(Query query, Root<Entity> root, CriteriaBuilder criteriaBuilder) {
        List<Predicate> queryPredicates = query.getFilter()
                .stream()
                .map(x->createPredicate(x,root,criteriaBuilder))
                .collect(Collectors.toList());
        return queryPredicates;
    }

    @Override
    public List<javax.persistence.criteria.Order> createOrderList(Query query, Root<Entity> root, CriteriaBuilder criteriaBuilder) {
        List<javax.persistence.criteria.Order> orders = query.getOrders()
                .stream()
                .map(x->createOrder(x,root,criteriaBuilder))
                .collect(Collectors.toList());
        return orders;
    }

    @Override
    public Entity parseJsonEntity(String json) {
        try{
            Entity entity = objectMapper.readValue(json,entityClass);
            return entity;
        } catch (IOException e){
            e.printStackTrace();
            throw new RuntimeException(String.format("Не удалось преобразовать сущность %s",json));
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Entity getById(ID id) {
        Entity entity = entityManager.find(entityClass,id);
        return entity;
    }
    /**
     * Возвращает объект сортировки CriteriaBuilder
     * @param order - объект сортировки, полученный из внешней api
     * @param root - объект обхода сущности по metamodel
     * @param criteriaBuilder - объект генерации CriteriaQuery
     * @return - объект Order для CriteriaBuilder
     */
    private javax.persistence.criteria.Order createOrder (Order order, Root<Entity> root, CriteriaBuilder criteriaBuilder){
        Expression expression = getExpression(order.getField(),root);
        return order.getDirection().getDirection().createOrder(criteriaBuilder,expression);
    }
    /**
     * Преобразование объекта фильтра фронта в объект фильтра CriteriaBuilder
     * @param baseField - объект фильтра полученный с фронта
     * @param root - объект обхода сущности по metamodel
     * @param criteriaBuilder - объект генерации CriteriaQuery
     * @return - условие для выборки CriteriaBuilder
     */
    private Predicate createPredicate (BaseField baseField, Root<Entity> root, CriteriaBuilder criteriaBuilder){
        Expression expression = getExpression(baseField.getFieldName(),root);
        Predicate predicate = null;
        if (baseField.getValue().getClass().equals(expression.getJavaType())){
            return baseField.getOperator().getPredicateParser().generatePredicate(baseField.getValue(),expression,criteriaBuilder);
        } else if (StringUtils.isEmpty(baseField.getType())) {
            ValueParser valueParser = ValueParserFactory.getInstance().getValueParser(expression.getJavaType());
            return baseField.getOperator().getPredicateParser().generatePredicate(valueParser.parseValue(baseField.getValue().toString()),expression,criteriaBuilder);
        } else {
            throw new RuntimeException(String.format("Произошла ошибка при парсинге поля %s",baseField.getFieldName()));
        }
    }

    /**
     * Метод для получения Expression в метамодели класса
     * @param fieldName - имя поля в сущности
     * @return - выражение поля в CriteriaBuilder
     */
    private Expression getExpression (String fieldName, Root<Entity> root){
        SingularAttribute<Entity,?> attribute = entityType.getDeclaredSingularAttribute(fieldName);
        if (attribute!=null){
            return root.get(attribute);
        } else {
            throw new RuntimeException(String.format("Не удалось найти поле %s в схеме данных сущности %s",fieldName,entityType.getName()));
        }
    }

    /**
     * Метод для выборки данных из БД
     * @param query - объект для выборки данных полученный с фронта
     * @param cb - объект CriteriaBuilder для генерации запросов
     * @return
     */
    private List<Entity> getPagingList (Query query, CriteriaBuilder cb) {
        CriteriaQuery<Entity> criteriaQuery = cb.createQuery(entityClass);
        Root<Entity> root = criteriaQuery.from(entityClass);
        List<Predicate> predicates = createWhereList(query,root,cb);
        List<javax.persistence.criteria.Order> orders = createOrderList(query,root,cb);
        criteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        criteriaQuery.orderBy(orders.toArray(new javax.persistence.criteria.Order[orders.size()]));
        TypedQuery<Entity> typedQuery = entityManager.createQuery(criteriaQuery)
                .setFirstResult(query.getOffset())
                .setMaxResults(query.getLimit());
        List<Entity> resultList = typedQuery.getResultList();
        return resultList;
    }

    /**
     * Метод для подсчёта количества сущностей, удовлетворяющим условиях запроса
     * @param query - объект запроса, полученный с фронта
     * @param criteriaBuilder - объект для генерации запроса
     * @return
     */
    private Long getPagingCount (Query query, CriteriaBuilder criteriaBuilder){
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<Entity> root = criteriaQuery.from(entityClass);
        List<Predicate> predicates = createWhereList(query,root,criteriaBuilder);
        criteriaQuery.select(criteriaBuilder.count(root));
        criteriaQuery.where(predicates.toArray(new Predicate [predicates.size()]));
        TypedQuery<Long> typedQuery = entityManager.createQuery(criteriaQuery);
        Long count = typedQuery.getSingleResult();
        return count;
    }

}
