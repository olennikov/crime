package ru.tsu.crimelab.dao.domain.enums;

public enum ExpertiseApplicationFileType {
    RESULT_FILE,
    REQUEST_FILE;
}
