package ru.tsu.crimelab.dao.domain;

import lombok.*;
import ru.tsu.crimelab.dao.domain.enums.TaskStatus;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.FetchType.LAZY;

@Data
@Entity
@Builder
@Table(name = "task")
@NoArgsConstructor
@AllArgsConstructor
public class Task implements BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description", columnDefinition = "text")
    private String description;

    @Column(name = "expired_date")
    private Date expiredDate;

    @Column(name = "task_status")
    @Enumerated(EnumType.STRING)
    private TaskStatus taskStatus;

    @EqualsAndHashCode.Exclude
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "expertise_id")
    private Expertise expertise;

    @ManyToOne(fetch = LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "responsible_user")
    private User responsibleUser;

    @Column(name = "deadline_date")
    private Date deadlineDate;

    @OneToMany(cascade = CascadeType.ALL, fetch = LAZY, mappedBy = "task")
    private Set<SubTask> subTasks = new HashSet<>();

    @EqualsAndHashCode.Exclude
    @OneToMany(cascade = CascadeType.ALL, fetch = LAZY, mappedBy = "task")
    private Set<TaskFile> taskFiles = new HashSet<>();
}
