package ru.tsu.crimelab.dao.repository;

import ru.tsu.crimelab.dao.api.repository.BaseRepository;
import ru.tsu.crimelab.dao.domain.Chat;

public interface ChatRepository extends BaseRepository<Chat, Long> {

    /**
     * Поиск чата экспертизы по идентификатору экспертизы
     *
     * @param expertiseApplicationId идентификатор заявки на проведение экспертизы
     * @return чат
     */
    Chat findFirstByExpertiseApplication_Id(Long expertiseApplicationId);
}
