package ru.tsu.crimelab.dao.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.tsu.crimelab.dao.api.repository.BaseRepository;
import ru.tsu.crimelab.dao.domain.ExpertiseApplication;
import ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationStatus;

import java.util.List;
import java.util.Optional;

public interface ExpertiseApplicationRepository extends BaseRepository<ExpertiseApplication, Long> {

    /**
     * Метод для выгрузки заявок, созданных пользователем
     *
     * @param id     идентификатор пользователя, который является создателем заявки
     * @param status статус выгружаемых заявок
     * @return перечень заявок
     */
    List<ExpertiseApplication> findExpertiseApplicationsByCreationUserIdAndStatusEquals(Long id,
                                                                                        ExpertiseApplicationStatus status);

    /**
     * Метод для выгрузки заявок, созданных пользователем
     *
     * @param id идентификатор пользователя, который является создателем заявки
     * @return перечень заявок
     */
    List<ExpertiseApplication> findExpertiseApplicationsByCreationUserId(Long id);

    /**
     * Метод для выгрузки заявки по идентификатору подзадачи
     *
     * @param subTaskId идентификатор подзадачи
     * @return перечень заявок
     */
    @Query(value = "select ea from ExpertiseApplication ea " +
            "inner join Expertise e ON e.expertiseApplication.id = ea.id " +
            "inner join Task t ON t.expertise.id = e.id " +
            "inner join t.subTasks st " +
            "where st.id = :subTaskId")
    Optional<ExpertiseApplication> findBySubTaskId(@Param("subTaskId") Long subTaskId);
}
