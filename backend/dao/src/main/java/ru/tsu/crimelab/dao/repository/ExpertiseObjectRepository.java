package ru.tsu.crimelab.dao.repository;

import ru.tsu.crimelab.dao.api.repository.BaseRepository;
import ru.tsu.crimelab.dao.domain.ExpertiseObject;

public interface ExpertiseObjectRepository extends BaseRepository<ExpertiseObject,Long> {
}
