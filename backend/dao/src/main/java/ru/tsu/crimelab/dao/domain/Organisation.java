package ru.tsu.crimelab.dao.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "organisation")
public class Organisation implements BaseEntity<Long>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @NotNull
    @Column(name="organisation_name")
    private String name;

    @NotNull
    @Column(name="organisation_inn", length = 15, unique = true)
    private String inn;

    @NotNull
    @Column(name = "organisation_ogrn", length = 15, unique = true)
    private String ogrn;

    @NotNull
    @Column(name="organisation_phone")
    private String phone;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    Set<User> users = new HashSet<>();
}
