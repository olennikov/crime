package ru.tsu.crimelab.dao.domain.enums;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Role {
    DIRECTOR(RoleConstantName.directorRoleName),
    EMPLOYEE(RoleConstantName.employeeRoleName),
    CUSTOMER(RoleConstantName.customerRoleName);

    private final String roleName;

}
