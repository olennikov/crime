package ru.tsu.crimelab.dao.api.filter.filterparse;

public class LongValueParser implements ValueParser<Long> {
    @Override
    public Long parseValue(String value) {
        return Long.parseLong(value);
    }
}
