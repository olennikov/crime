package ru.tsu.crimelab.dao.repository;

import org.springframework.data.repository.query.Param;
import ru.tsu.crimelab.dao.api.repository.BaseRepository;
import ru.tsu.crimelab.dao.domain.ExpertiseApplication;
import ru.tsu.crimelab.dao.domain.ExpertiseApplicationFile;
import ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationStatus;

import java.util.List;
import java.util.Optional;

public interface ExpertiseApplicationFileRepository extends BaseRepository<ExpertiseApplicationFile,Long> {
    Optional<ExpertiseApplicationFile> findByFile_Id(@Param("file_id") Long fileId);
}
