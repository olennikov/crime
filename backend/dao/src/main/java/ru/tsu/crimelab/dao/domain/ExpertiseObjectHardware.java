package ru.tsu.crimelab.dao.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "hardware_object")
@DiscriminatorValue("HARDWARE")
public class ExpertiseObjectHardware extends ExpertiseObject {
    @Column(name = "inventarisation_code")
    private String inventarisationCode;

    @Column(name = "received")
    private Boolean received;

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinColumn(name = "hardware_type")
    private HardwareType hardwareType;

    @Column(name = "hardware_model")
    private String hardwareModel;
}
