package ru.tsu.crimelab.dao.api.filter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.tsu.crimelab.dao.api.filter.operatorpredicates.*;


@AllArgsConstructor
@Getter
public enum Operator {
    EQUALS(new EqualPredicate()),
    GREATE(new GreatPredicate()),
    IS_NULL(new NullPredicate()),
    NOT_NULL(new NotNullPredicate()),
    LESS(new LessPredicate()),
    GREAT_OR_EQUALS(new GreatOrEqualsPredicate()),
    LESS_OR_EQUALS(new LessOrEqualsPredicate());

    private PredicateParser predicateParser;
}
