package ru.tsu.crimelab.dao.repository;

import ru.tsu.crimelab.dao.api.repository.BaseRepository;
import ru.tsu.crimelab.dao.domain.SubTask;

import java.util.List;

public interface SubTaskRepository extends BaseRepository<SubTask, Long> {
    List<SubTask> findByTask_Id(Long taskId);

    /**
     * Подсчет кол-ва незакрытых подзадач для задачи
     *
     * @param taskId идентификатор задачи
     * @return кол-во не закрытых задач
     */
    Long countByTaskIdAndIsClosedFalse(Long taskId);
}
