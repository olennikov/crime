package ru.tsu.crimelab.dao.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "expertise_file")
@DiscriminatorValue("FILE")
public class ExpertiseObjectFile extends ExpertiseObject {
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "file_id")
    private File file;
}
