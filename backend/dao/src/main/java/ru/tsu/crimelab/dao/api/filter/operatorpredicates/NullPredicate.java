package ru.tsu.crimelab.dao.api.filter.operatorpredicates;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;

public class NullPredicate<Type> implements PredicateParser {
    @Override
    public Predicate generatePredicate(Object object, Expression expression, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.isNull(expression);
    }
}
