package ru.tsu.crimelab.dao.api.filter.operatorpredicates;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;

public class NotNullPredicate<Type> implements PredicateParser<Type> {
    @Override
    public Predicate generatePredicate(Type object, Expression<? extends Type> expression, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.isNotNull(expression);
    }
}
