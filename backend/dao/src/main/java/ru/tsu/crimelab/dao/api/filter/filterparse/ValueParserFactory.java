package ru.tsu.crimelab.dao.api.filter.filterparse;

import java.util.Date;

public class ValueParserFactory {

    private static final ValueParserFactory instance = new ValueParserFactory();

    public static ValueParserFactory getInstance (){
        return instance;
    }

    private ValueParserFactory (){

    }

    public  ValueParser getValueParser (Class cls){
        if (cls.equals(Long.class)){
            return new LongValueParser();
        } else if (cls.equals(Integer.class)){
            return new IntegerValueParser();
        } else if (cls.equals(Date.class)){
            return new DateValueParser();
        } else if (cls.equals(Boolean.class)){
            return new BooleanValueParser();
        } else if (cls.isEnum()){
            return new EnumValueParser(cls);
        } else {
            throw new RuntimeException(String.format("Не удалось найти парсер для значения фильтра с типом %s",cls.getSimpleName()));
        }
    }


}
