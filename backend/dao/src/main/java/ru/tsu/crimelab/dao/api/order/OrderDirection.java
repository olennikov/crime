package ru.tsu.crimelab.dao.api.order;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.tsu.crimelab.dao.api.order.orders.AscOrderDirection;
import ru.tsu.crimelab.dao.api.order.orders.DescOrderDirection;
import ru.tsu.crimelab.dao.api.order.orders.OrderBuilder;

@AllArgsConstructor
@Getter
public enum OrderDirection {
    ASC(new AscOrderDirection()),
    DESC(new DescOrderDirection());

    private OrderBuilder direction;
}
