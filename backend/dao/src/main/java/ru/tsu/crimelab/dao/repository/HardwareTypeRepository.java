package ru.tsu.crimelab.dao.repository;

import ru.tsu.crimelab.dao.api.repository.BaseRepository;
import ru.tsu.crimelab.dao.domain.HardwareType;

public interface HardwareTypeRepository extends BaseRepository<HardwareType,Long> {
}
