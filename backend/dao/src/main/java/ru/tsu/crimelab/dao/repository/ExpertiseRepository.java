package ru.tsu.crimelab.dao.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.tsu.crimelab.dao.api.repository.BaseRepository;
import ru.tsu.crimelab.dao.domain.Expertise;
import ru.tsu.crimelab.dao.domain.enums.ExpertiseStatus;

import java.util.Optional;

public interface ExpertiseRepository extends BaseRepository<Expertise, Long> {

    @Query(value = "SELECT DISTINCT e FROM Expertise e " +
            "INNER JOIN Task t ON t.expertise.id = e.id " +
            "WHERE t.responsibleUser.id = :userId AND e.status = :status")
    Page<Expertise> findByResponsibleUserAndStatus(Pageable pageable, @Param("userId") Long userId, @Param("status") ExpertiseStatus status);

    @Query(value = "SELECT DISTINCT e FROM Expertise e " +
            "INNER JOIN Task t ON t.expertise.id = e.id " +
            "WHERE t.responsibleUser.id = :userId AND e.status <> 'NEW'")
    Page<Expertise> findAllWithoutNEWByResponsibleUser(Pageable pageable, @Param("userId") Long userId);

    @Query("SELECT e FROM Expertise e INNER JOIN e.expertiseApplication a WHERE a.id = :expertiseAppId")
    Optional<Expertise> findByExpertiseApplicationId(@Param("expertiseAppId") Long expertiseAppId);
}
