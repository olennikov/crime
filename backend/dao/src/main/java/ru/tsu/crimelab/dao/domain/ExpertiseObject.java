package ru.tsu.crimelab.dao.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Entity
@Table(name = "expertise_object")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "object_type")
public abstract class ExpertiseObject implements BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "receive_date")
    private Date receiveDate;

    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "object_type")
    private String objectType;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "expertise_id")
    private Expertise expertise;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "sub_task_id")
    private SubTask subTask;
}
