package ru.tsu.crimelab.dao.api.filter.fields;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import ru.tsu.crimelab.dao.api.filter.Operator;

@Data
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", defaultImpl = StringField.class, visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value=IntegerField.class, name="IntegerField"),
        @JsonSubTypes.Type(value=StringField.class, name="StringField"),
        @JsonSubTypes.Type(value=DateField.class, name="DateField"),
        @JsonSubTypes.Type(value=DoubleField.class, name="DoubleField"),
        @JsonSubTypes.Type(value=LongField.class, name="LongField")
})
public abstract class BaseField<ValueType> {
    private String fieldName;
    private Operator operator;
    private ValueType value;
    private String type;
}
