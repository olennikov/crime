package ru.tsu.crimelab.dao.repository;

import ru.tsu.crimelab.dao.api.repository.BaseRepository;
import ru.tsu.crimelab.dao.domain.ExpertiseObjectFile;

public interface ExpertiseObjectFileRepository extends BaseRepository<ExpertiseObjectFile,Long> {
}
