package ru.tsu.crimelab.dao.domain.enums;

public enum RegistrationApplicationStatus {
    /**
     * Только что поступившая заявка на решистрацию
     */
    NEW,
    /**
     * Заявка на регистрацию одобрена, отправлена ссылка для завершения процесса регистрации пользователя
     */
    ACCEPTED,
    /**
     * Пользователь по заявке был создан
     */
    CREATED,
    DENIED;
}
