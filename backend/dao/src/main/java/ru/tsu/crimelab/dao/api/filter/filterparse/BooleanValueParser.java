package ru.tsu.crimelab.dao.api.filter.filterparse;

public class BooleanValueParser implements ValueParser<Boolean> {
    @Override
    public Boolean parseValue(String value) {
        return Boolean.parseBoolean(value);
    }
}
