package ru.tsu.crimelab.dao.repository;

import ru.tsu.crimelab.dao.api.repository.BaseRepository;
import ru.tsu.crimelab.dao.domain.User;
import ru.tsu.crimelab.dao.domain.enums.Role;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends BaseRepository<User, Long> {
    /**
     * Метод поиска пользователя по email
     *
     * @param email строка email пользователя
     * @return объект найденного пользователя
     */
    Optional<User> findOneByEmail(String email);

    /**
     * Поиск списка пользователей
     *
     * @param role роль пользователя
     * @return перечень пользователей, соответствующих переданной роли
     */
    List<User> findByRole(Role role);

    /**
     * Подсчет кол-ва пользователей с данной ролью
     *
     * @param role роль пользователей
     * @return количество пользователей с указанной ролью
     */
    long countByRoleEquals(Role role);

    /**
     * Поиск руководителей лаборатории, идентификаторы которых не входят в переданный массив идентификаторов
     *
     * @param role роли пользователей
     * @param ids  идентификаторы пользователей
     * @return руководители лаборатории, идентификаторы которых не входят в переданный массив идентификаторов
     */
    Collection<User> findByRoleAndIdNotIn(Role role, List<Long> ids);
}
