package ru.tsu.crimelab.dao.api.filter.filterparse;

public class EnumValueParser implements ValueParser<Enum> {

    private Class<? extends Enum> cls;

    public EnumValueParser(Class<? extends Enum> cls){
        this.cls = cls;
    }

    @Override
    public Enum parseValue(String value) {
        return Enum.valueOf(this.cls,value);
    }
}
