package ru.tsu.crimelab.dao.domain.enums;

/**
 * Статус заявки на эксперитзу
 */
public enum ExpertiseApplicationStatus {
    NEW,
    IN_WORK,
    ACCEPT,
    REJECT,
    CLOSED;
}
