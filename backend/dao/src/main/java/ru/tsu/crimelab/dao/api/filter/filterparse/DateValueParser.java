package ru.tsu.crimelab.dao.api.filter.filterparse;

import org.apache.commons.lang3.time.DateUtils;

import java.text.ParseException;
import java.util.Date;

public class DateValueParser implements ValueParser<Date> {
    @Override
    public Date parseValue(String value) {
        try {
            return DateUtils.parseDate(value,new String [] {"dd.MM.yyyy", "dd.MM.yyyy HH:mm:ss"});
        } catch (ParseException e) {
            e.printStackTrace();
            throw new RuntimeException(String.format("Ошибка при парсинге даты %s",value));
        }
    }
}
