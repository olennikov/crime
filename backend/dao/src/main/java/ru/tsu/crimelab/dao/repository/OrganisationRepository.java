package ru.tsu.crimelab.dao.repository;

import ru.tsu.crimelab.dao.api.repository.BaseRepository;
import ru.tsu.crimelab.dao.domain.Organisation;

public interface OrganisationRepository extends BaseRepository<Organisation, Long> {
    /**
     * Поиск организации по ИНН и ОГРН
     * @param inn
     * @param ogrn
     * @return
     */
    Organisation findOneByInnOrOgrn (String inn, String ogrn);
}
