package ru.tsu.crimelab.dao.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "hardware_type")
public class HardwareType implements BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;
}
