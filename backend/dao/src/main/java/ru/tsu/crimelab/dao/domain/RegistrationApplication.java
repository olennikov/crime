package ru.tsu.crimelab.dao.domain;


import lombok.*;
import ru.tsu.crimelab.dao.domain.enums.RegistrationApplicationStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.util.Date;

import static javax.persistence.FetchType.LAZY;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Table(name = "registration_application")
public class RegistrationApplication implements BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "receive_date")
    private Date receiveDate;

    @NotNull
    @Column(name = "last_name")
    private String lastName;

    @NotNull
    @Column(name = "first_name")
    private String firstName;

    @Column(name = "second_name")
    private String secondName;

    @Column(name = "phone")
    @NotNull
    private String phone;

    @Column(name = "email", unique = true)
    @NotNull
    private String email;

    @Column(name = "organisation_name")
    @NotNull
    private String organisationName;

    @Column(name = "organisation_inn", length = 13)
    @NotNull
    private String organisationInn;

    @Column(name = "organisation_phone", length = 15)
    @NotNull
    private String organisationPhone;

    @Column(name = "organisation_ogrn", length = 15)
    private String organisationOgrn;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private RegistrationApplicationStatus status;

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = LAZY)
    @JoinColumn(name = "created_user")
    private User createdUser;

    @Column(name = "reject_comment")
    private String rejectComment;

    @Column(name="change_password_url")
    private String changePasswordUrl;

    @Column(name="change_password_receive_date")
    private Date changePasswordReceiveDate;
}
