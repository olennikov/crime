package ru.tsu.crimelab.dao.repository;

import ru.tsu.crimelab.dao.api.repository.BaseRepository;
import ru.tsu.crimelab.dao.domain.ExpertiseObjectHardware;

public interface ExpertiseObjectHardwareRepository extends BaseRepository<ExpertiseObjectHardware,Long> {
}
