package ru.tsu.crimelab.dao.domain;


import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@Data
@Entity
@Table(name = "message")
public class Message implements BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "chat_id")
    private Chat chat;

    @NotNull
    @Column(name = "timestamp")
    private ZonedDateTime timestamp;

    @NotNull
    @Column(name = "text", columnDefinition = "text")
    private String text;

    @OneToOne
    @JoinColumn(name = "writer_id")
    private User writer;
}
