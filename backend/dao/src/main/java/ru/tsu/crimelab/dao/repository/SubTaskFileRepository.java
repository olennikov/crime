package ru.tsu.crimelab.dao.repository;

import ru.tsu.crimelab.dao.api.repository.BaseRepository;
import ru.tsu.crimelab.dao.domain.SubTaskFile;

import java.util.Optional;

public interface SubTaskFileRepository extends BaseRepository<SubTaskFile,Long> {
    Optional<SubTaskFile> findByFile_Id(Long fileId);
}
