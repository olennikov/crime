package ru.tsu.crimelab.dao.repository;

import ru.tsu.crimelab.dao.api.repository.BaseRepository;
import ru.tsu.crimelab.dao.domain.RegistrationApplication;
import ru.tsu.crimelab.dao.domain.enums.RegistrationApplicationStatus;

public interface RegistrationApplicationRepository extends BaseRepository<RegistrationApplication, Long> {
    /**
     * Метод для поиска заявки на регистрацию по токену одноразовой ссылки и статусу заявки
     * @param changePasswordUrl токен одноразовой ссылки для регистрации пользователя
     * @param status статус заявки о регистрации
     * @return объект заявки регистрации
     */
    RegistrationApplication findOneByChangePasswordUrlAndStatus(String changePasswordUrl, RegistrationApplicationStatus status);
}
