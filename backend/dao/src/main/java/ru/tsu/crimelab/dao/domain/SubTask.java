package ru.tsu.crimelab.dao.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@Entity
@Table(name = "subtask")
public class SubTask implements BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "description", length = 512)
    private String description;

    @Column(name = "comment", length = 512)
    private String comment;

    @Column(name = "deadline_date")
    private Date deadlineDate;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "task_id")
    private Task task;

    @OneToMany
    private Set<ExpertiseObject> expertiseObjects;

    @OneToMany(mappedBy = "subTask", fetch = FetchType.LAZY)
    private Set<SubTaskFile> subTaskFiles = new HashSet<>();

    @Column(name = "is_closed", columnDefinition = "boolean default false")
    private Boolean isClosed;
}
