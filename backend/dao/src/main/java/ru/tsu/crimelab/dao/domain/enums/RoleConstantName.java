package ru.tsu.crimelab.dao.domain.enums;

public class RoleConstantName {
    public static final String directorRoleName = "DIRECTOR";
    public static final String employeeRoleName = "EMPLOYEE";
    public static final String customerRoleName = "CUSTOMER";

    private RoleConstantName() {

    }
}
