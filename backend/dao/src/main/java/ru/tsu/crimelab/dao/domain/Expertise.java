package ru.tsu.crimelab.dao.domain;

import lombok.*;
import ru.tsu.crimelab.dao.domain.enums.ExpertiseStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "expertise")
public class Expertise implements BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "receive_date")
    private Date receiveDate;

    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "description", columnDefinition = "text")
    private String description;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "expertise")
    private Set<Task> expertiseTasks = new HashSet<>();

    @NotNull
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private ExpertiseStatus status;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinColumn(name = "application_id")
    private ExpertiseApplication expertiseApplication;

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinColumn(name = "leader_user")
    private User leaderUser;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "expertise")
    private Set<ExpertiseObject> expertiseObjects = new HashSet<>();

}
