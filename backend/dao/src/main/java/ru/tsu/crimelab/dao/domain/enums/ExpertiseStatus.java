package ru.tsu.crimelab.dao.domain.enums;

/**
 * Created by PetrushinDA on 17.11.2019
 * Статус экспертизы
 */
public enum ExpertiseStatus {
    NEW,
    IN_WORK,
    DONE
}
