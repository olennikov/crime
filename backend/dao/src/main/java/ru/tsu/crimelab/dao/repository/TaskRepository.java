package ru.tsu.crimelab.dao.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.tsu.crimelab.dao.api.repository.BaseRepository;
import ru.tsu.crimelab.dao.domain.Task;

import java.util.List;
import java.util.Optional;

public interface TaskRepository extends BaseRepository<Task, Long> {
    /**
     * Получение задач по идентификатору экспертизы
     *
     * @param expertiseId - идентификатор экспертизы
     * @return
     */
    @Query("SELECT t FROM Task t JOIN t.expertise WHERE t.expertise.id = :expertiseId")
    List<Task> getTasksByExpertise(@Param("expertiseId") Long expertiseId);

    Optional<Task> getByIdAndResponsibleUser_Id(Long id, Long responsibleUserId);
}
