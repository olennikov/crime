package ru.tsu.crimelab.dao.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.tsu.crimelab.dao.api.Query;
import ru.tsu.crimelab.dao.api.order.Order;
import ru.tsu.crimelab.dao.api.repository.response.QueryResponse;
import ru.tsu.crimelab.dao.domain.BaseEntity;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.List;

@NoRepositoryBean
public interface BaseRepository<Entity extends BaseEntity, ID extends Serializable> extends JpaRepository<Entity, ID> {

    /**
     * геттер для возрвтат типа Entity
     *
     * @return возвращает класс Entity
     */
    Class<Entity> getEntityClass();

    /**
     * Метод для генерации кастомного запроса
     *
     * @param query объект запроса сущностей для фронта
     * @return список сущностей (объекты Entity или DTO)
     */
    List<Entity> query(Query query);

    /**
     * Подсчёт количества для пагинации через объект query
     *
     * @param query - объект запроса сущностей
     * @return - общее количество сущностей, соответствующих данному обхекту query
     */
    Long count(Query query);

    /**
     * Метод для генерации полного ответа а запрос реестра
     *
     * @param query - объект запроса сущностей
     * @return список сущностей и их общее количество
     */
    QueryResponse<Entity> pagingQuery(Query query);

    /**
     * Генерация условий для запроса (преобразование объекта Query в условия CriteriaBuilder)
     *
     * @param query           - объект запроса сущностей с фронта
     * @param root            - объект CriteriaBuilder для обхода metamodel сущности
     * @param criteriaBuilder сущность для генерации условий запроса
     * @return список фильтров для метода Where при генерации CritereaQuery
     */
    List<Predicate> createWhereList(Query query, Root<Entity> root, CriteriaBuilder criteriaBuilder);

    /**
     * Метод для генерации объектов сортировки CriteriaBuilder из объекта запроса
     *
     * @param query           - объект запроса сущностей с фронта
     * @param root            - объект обхода сущности по metamodel
     * @param criteriaBuilder - сущность с помощью которой генерируются объекты сортировки
     * @return список сортировок CriteriaBuilder
     */
    List<javax.persistence.criteria.Order> createOrderList(Query query, Root<Entity> root, CriteriaBuilder criteriaBuilder);

    /**
     * Метод для парсинга в объект сущности из Json
     *
     * @param json - строка сущности в формате Json
     * @return объект entity
     */
    Entity parseJsonEntity(String json);

    /**
     * Метод для получения объекта по его идентификатору
     *
     * @param id - идентификатор объекта
     * @return объект сущности или dto
     */
    Entity getById(ID id);
}
