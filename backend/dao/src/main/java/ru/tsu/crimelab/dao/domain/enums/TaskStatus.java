package ru.tsu.crimelab.dao.domain.enums;

public enum TaskStatus {
    NEW,
    IN_WORK,
    CLOSED
}
