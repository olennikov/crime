package ru.tsu.crimelab.dao.api.order.orders;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;

public class AscOrderDirection implements OrderBuilder {
    @Override
    public Order createOrder(CriteriaBuilder criteriaBuilder, Expression expression) {
        return criteriaBuilder.asc(expression);
    }
}
