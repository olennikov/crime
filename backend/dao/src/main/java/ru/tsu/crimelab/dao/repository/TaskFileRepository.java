package ru.tsu.crimelab.dao.repository;

import ru.tsu.crimelab.dao.api.repository.BaseRepository;
import ru.tsu.crimelab.dao.domain.TaskFile;

import java.util.Optional;

public interface TaskFileRepository extends BaseRepository<TaskFile, Long> {
    Optional<TaskFile> findByFile_Id(Long fileId);
}
