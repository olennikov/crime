package ru.tsu.crimelab.dao.api.filter.filterparse;

import java.text.ParseException;

public interface ValueParser<ValueType> {
    /**
     * Метод для парсинга значения запроса из String
     * @param value - значение из фильтра
     * @return - значение для Predicate CriteriaBuilder
     */
    ValueType parseValue (String value);
}
