package ru.tsu.crimelab.dao.api.order.orders;


import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;

public interface OrderBuilder {
    Order createOrder (CriteriaBuilder criteriaBuilder, Expression expression);
}
