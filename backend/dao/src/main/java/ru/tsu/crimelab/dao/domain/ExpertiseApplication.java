package ru.tsu.crimelab.dao.domain;

import lombok.*;
import ru.tsu.crimelab.dao.domain.enums.ExpertiseApplicationStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "expertise_application")
public class ExpertiseApplication implements BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "receive_date")
    private Date receiveDate;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "creation_user")
    private User creationUser;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "leader_user")
    private User leaderUser;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private ExpertiseApplicationStatus status;

    @Column(name="reject_comment", columnDefinition = "text")
    private String rejectComment;

    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "description", columnDefinition = "text")
    private String description;

    @Column(name="result_description", columnDefinition = "text")
    private String resultDescription;

    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, mappedBy = "expertiseApplication")
    private Set<ExpertiseApplicationFile> expertiseFiles = new HashSet<>();
}
