package ru.tsu.crimelab.dao.api;

import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.tsu.crimelab.dao.api.order.Order;
import ru.tsu.crimelab.dao.api.filter.fields.BaseField;

import java.util.ArrayList;
import java.util.List;

/**
 * Объект для построения кастомного запроса
 * Используется в репозиториях в методах query, count и pagingQuery
 * Содержит в себе список фильтров, сортировок и данные для пагинации
 */

@Getter
@NoArgsConstructor
public class Query {
    /**
     * строка для поиска полей
     */
    private String findString;
    /**
     * Имя преобразования сущности в transfer object
     */
    private String dtoName;
    /**
     * Поля для фильтрации
     */
    private List<BaseField> filter = new ArrayList<>();
    /**
     * Сортировка
     */
    private List<Order> orders = new ArrayList<>();
    /**
     * Лимит
     */
    private Integer limit = 25;
    /**
     * Отступ
     */
    private Integer offset = 0;

}
