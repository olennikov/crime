package ru.tsu.crimelab.dao.api.filter.operatorpredicates;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;

public class LessPredicate implements PredicateParser<Number> {
    @Override
    public Predicate generatePredicate(Number object, Expression<? extends Number> expression, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.lt(expression,object);
    }
}
