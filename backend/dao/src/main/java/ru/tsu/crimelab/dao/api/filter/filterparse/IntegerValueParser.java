package ru.tsu.crimelab.dao.api.filter.filterparse;

public class IntegerValueParser implements ValueParser<Integer> {
    @Override
    public Integer parseValue(String value) {
        return Integer.parseInt(value);
    }
}
