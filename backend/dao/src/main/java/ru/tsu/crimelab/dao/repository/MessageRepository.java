package ru.tsu.crimelab.dao.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.tsu.crimelab.dao.api.repository.BaseRepository;
import ru.tsu.crimelab.dao.domain.Message;

import java.util.List;

public interface MessageRepository extends BaseRepository<Message, Long> {

    //ToDo: Возможно стоит переделать на Pageable
    @Query(value =
            "select * from ( " +
                    "select * from message m " +
                    "join chat c on m.chat_id = c.id " +
                    "where c.expertise_application_id = ?1 " +
                    "order by m.timestamp desc offset ?2 limit ?3) as s " +
                    "order by s.timestamp",
            nativeQuery = true)
    List<Message> getMessagesByExpertiseApplication(@Param("expertise_id") Long expertiseId,
                                                    @Param("skip") Long skip,
                                                    @Param("take") Long take);
}
