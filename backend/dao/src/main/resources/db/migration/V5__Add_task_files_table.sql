CREATE TABLE task_files(
    id bigserial PRIMARY KEY,
    task_id integer NOT NULL REFERENCES task (id),
    file_id integer NOT NULL REFERENCES "file" (id)
)