create sequence hibernate_sequence;

alter sequence hibernate_sequence owner to crimelab;

create table if not exists hardware_type
(
	id bigserial not null
		constraint hardware_type_pkey
			primary key,
	name varchar(255)
);

alter table hardware_type owner to crimelab;

create table if not exists organisation
(
	id bigserial not null
		constraint organisation_pkey
			primary key,
	organisation_inn varchar(15) not null
		constraint uk_5witolgkt2vrhqsp88wor56eg
			unique,
	organisation_name varchar(255) not null,
	organisation_ogrn varchar(15) not null
		constraint uk_gpvwvomhdnwpbqub9cpdu8sjb
			unique,
	organisation_phone varchar(255) not null
);

alter table organisation owner to crimelab;

create table if not exists users
(
	id bigserial not null
		constraint users_pkey
			primary key,
	email varchar(255)
		constraint uk_6dotkott2kjsp8vw4d0m25fb7
			unique,
	first_name varchar(255) not null,
	last_name varchar(255) not null,
	user_password varchar(256),
	phone varchar(255) not null,
	role varchar(255),
	salt varchar(255) not null,
	second_name varchar(255),
	organisation_id bigint
		constraint fkhb3nv5nv0xr3wpuxfsb2jft1s
			references organisation
);

alter table users owner to crimelab;

create table if not exists expertise_application
(
	id bigserial not null
		constraint expertise_application_pkey
			primary key,
	description text not null,
	name varchar(255) not null,
	receive_date timestamp not null,
	reject_comment text,
	status varchar(255),
	creation_user bigint
		constraint fkj4twlknyau3f8ffilhgrvbwwe
			references users,
	leader_user bigint
		constraint fk9gv5dy6goamiosk0uagw044jw
			references users,
	result_description text
);

alter table expertise_application owner to crimelab;

create table if not exists chat
(
	id bigserial not null
		constraint chat_pkey
			primary key,
	expertise_application_id bigint
		constraint fkcp0rx1au1dxfo8x2dlxc5iv1i
			references expertise_application
);

alter table chat owner to crimelab;

create table if not exists chat_participants
(
	chat_id bigint not null
		constraint fk44t5cpu2fejyqk36kb8pico4u
			references chat,
	participant_id bigint not null
		constraint fkniojp089hlg00mujvo3vf5hum
			references users,
	constraint chat_participants_pkey
		primary key (chat_id, participant_id)
);

alter table chat_participants owner to crimelab;

create table if not exists expertise
(
	id bigserial not null
		constraint expertise_pkey
			primary key,
	description text not null,
	name varchar(255) not null,
	receive_date timestamp not null,
	status varchar(255) not null,
	application_id bigint
		constraint fkcd8ol8muav8wahxkatkdyhhgw
			references expertise_application,
	leader_user bigint
		constraint fkk27vp0hvy2w1tse0afrvqhe6h
			references users
);

alter table expertise owner to crimelab;

create table if not exists file
(
	id bigserial not null
		constraint file_pkey
			primary key,
	date_create timestamp not null,
	file_name varchar(255),
	file_path varchar(255),
	file_size bigint,
	mime_type varchar(255),
	creator_id bigint
		constraint fk1stii8kh0o7tm0ts5nfm7qldr
			references users
);

alter table file owner to crimelab;

create table if not exists expertise_application_file
(
	id bigint not null,
	expertise_application_id bigint not null
		constraint fk2hjqib99hp2osbs5kocyoct5f
			references expertise_application,
	file_id bigint not null
		constraint uk_fadonbuky5ho6agvgup9q3hkw
			unique
		constraint fkessqjpr5na063cw7l2ju9ee70
			references file,
	file_type varchar(255),
	constraint expertise_application_file_pkey
		primary key (expertise_application_id, file_id)
);

alter table expertise_application_file owner to crimelab;

create table if not exists message
(
	id bigserial not null
		constraint message_pkey
			primary key,
	text text not null,
	timestamp timestamp not null,
	chat_id bigint
		constraint fkmejd0ykokrbuekwwgd5a5xt8a
			references chat,
	writer_id bigint
		constraint fkpgdddk6p4tp14m38he49xydnq
			references users
);

alter table message owner to crimelab;

create table if not exists organisation_users
(
	organisation_id bigint not null
		constraint fk3jrw8nnc74w295ufq16lx1dxk
			references organisation,
	users_id bigint not null
		constraint uk_cy0915mnmnpeykc6lij99otrg
			unique
		constraint fkjwqwxcfnb470nlo4tcl98x5pe
			references users,
	constraint organisation_users_pkey
		primary key (organisation_id, users_id)
);

alter table organisation_users owner to crimelab;

create table if not exists registration_application
(
	id bigserial not null
		constraint registration_application_pkey
			primary key,
	change_password_receive_date timestamp,
	change_password_url varchar(255),
	email varchar(255) not null
		constraint uk_phce1rjv4rqoaoid5lc5trlxt
			unique,
	first_name varchar(255) not null,
	last_name varchar(255) not null,
	organisation_inn varchar(13) not null,
	organisation_name varchar(255) not null,
	organisation_ogrn varchar(15),
	organisation_phone varchar(15) not null,
	phone varchar(255) not null,
	receive_date timestamp,
	reject_comment varchar(255),
	second_name varchar(255),
	status varchar(255),
	created_user bigint
		constraint fk87i7fhg47t7d2lga83hp6m6q2
			references users
);

alter table registration_application owner to crimelab;

create table if not exists task
(
	id bigserial not null
		constraint task_pkey
			primary key,
	deadline_date timestamp,
	expired_date timestamp,
	name varchar(255),
	task_status varchar(255),
	expertise_id bigint
		constraint fk97s945ct1pusnd429r6ol02t5
			references expertise,
	responsible_user bigint
		constraint fk5590rfro5xyo2urgb5feiny5m
			references users,
	description text
);

alter table task owner to crimelab;

create table if not exists subtask
(
	id bigserial not null
		constraint subtask_pkey
			primary key,
	comment varchar(512) not null,
	deadline_date timestamp not null,
	description varchar(512) not null,
	name varchar(255) not null,
	task_id bigint
		constraint fkqogp98iwmph591yu58frt48ud
			references task
);

alter table subtask owner to crimelab;

create table if not exists expertise_object
(
	object_type varchar(31) not null,
	id bigserial not null
		constraint expertise_object_pkey
			primary key,
	name varchar(255) not null,
	receive_date timestamp not null,
	expertise_id bigint
		constraint fkr3b9giey6f5tl0jo0tiorhfk9
			references expertise,
	sub_task_id bigint
		constraint fkhyumqce3gv5ygbokoycomchyg
			references subtask
);

alter table expertise_object owner to crimelab;

create table if not exists expertise_expertise_objects
(
	expertise_id bigint not null
		constraint fksdox0q5u3a43e5etco05qyrsf
			references expertise,
	expertise_objects_id bigint not null
		constraint uk_gx63dcyh9n94kp4m8voomrpty
			unique
		constraint fkhr2o9rlgr14lwd58bw3p6r7xi
			references expertise_object,
	constraint expertise_expertise_objects_pkey
		primary key (expertise_id, expertise_objects_id)
);

alter table expertise_expertise_objects owner to crimelab;

create table if not exists expertise_file
(
	id bigint not null
		constraint expertise_file_pkey
			primary key
		constraint fkhscni8apg9twu1mcvwbkts08c
			references expertise_object,
	file_id bigint
		constraint fks4i9v929eltijlk701kqy2ycv
			references file
);

alter table expertise_file owner to crimelab;

create table if not exists hardware_object
(
	hardware_model varchar(255),
	inventarisation_code varchar(255),
	received boolean,
	id bigint not null
		constraint hardware_object_pkey
			primary key
		constraint fkdfi9s4omr31vop4b4wrmctvgp
			references expertise_object,
	hardware_type bigint
		constraint fkig982jvntkgi9cswwx8aixivp
			references hardware_type
);

alter table hardware_object owner to crimelab;

create table if not exists subtask_expertise_objects
(
	sub_task_id bigint not null
		constraint fkst7hgx2yd7cn97aijh6t2vj07
			references subtask,
	expertise_objects_id bigint not null
		constraint uk_ey5uc8ouw9lwu4xql84t5g1s4
			unique
		constraint fk7kms8bgrspodxrtoyaunxqsip
			references expertise_object,
	constraint subtask_expertise_objects_pkey
		primary key (sub_task_id, expertise_objects_id)
);

alter table subtask_expertise_objects owner to crimelab;

create table if not exists subtask_files
(
	id bigserial not null
		constraint subtask_files_pkey
			primary key,
	file_id bigint
		constraint fkgnl1dh9h8ierp3qghjeh07k0y
			references file,
	subtask_id bigint
		constraint fkh8mpjwn3linm44f4e2vd11vui
			references subtask
);

alter table subtask_files owner to crimelab;

create table if not exists task_sub_tasks
(
	task_id bigint not null
		constraint fk1mgcpo988t6fnenr138icxi78
			references task,
	sub_tasks_id bigint not null
		constraint uk_5xixqs4npgerro5d0g3p00hgv
			unique
		constraint fkqt7835ebscsibw2nh60rxivb3
			references subtask,
	constraint task_sub_tasks_pkey
		primary key (task_id, sub_tasks_id)
);

alter table task_sub_tasks owner to crimelab;

