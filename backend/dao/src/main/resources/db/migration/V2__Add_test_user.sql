INSERT INTO users (email, first_name, last_name, user_password, phone, role, salt, second_name)
VALUES ('admin@mail.ru', 'Администратор', 'Администраторов',
        '254abe94a28633bb86c1a808d0bdbd2b74646de19fecfa3ce84a8807f623d390', '111111111', 'DIRECTOR',
        '36521023-3fdd-465a-8f5c-7afdb6444793', 'Администраторович'),
       ('employee@mail.ru', 'Сотрудник', 'Сотрудников', '4dde306aa159ed79f6946a44bcfd8cd16df826eb9967e058aec6f1c200f08d81',
        '111111111', 'EMPLOYEE', '8d4819c0-8e57-4e7b-b7fc-ff6e4a7fd227', 'Сотрудникович');
