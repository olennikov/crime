CREATE OR REPLACE FUNCTION employee_statistics(IN limit_rows bigint, IN offset_rows bigint)
    RETURNS REFCURSOR
    LANGUAGE plpgsql
AS
$$
DECLARE
    statistics REFCURSOR;
BEGIN
    OPEN statistics FOR
        with temp as (select t.id          as task,
                                      t.task_status,
                                      s.id          as subtask,
                                      s.is_closed   as subtask_is_closed,
                                      u.id          as user_id,
                                      u.first_name  as user_first_name,
                                      u.last_name   as user_last_name,
                                      u.second_name as user_second_name,
                                      u.phone       as user_phone,
                                      u.email       as user_email,
                                      u.is_block    as user_is_block
                               from users u
                                        left join task t on u.id = t.responsible_user
                                        left join subtask s on t.id = s.task_id
                               where u.role = 'EMPLOYEE'
                               limit limit_rows offset offset_rows
    )
                 select user_id                                                    as id,
                        user_first_name                                            as first_name,
                        user_last_name                                             as last_name,
                        user_second_name                                           as second_name,
                        user_phone                                                 as phone,
                        user_email                                                 as email,
                        user_is_block                                              as is_block,
                        count(distinct task)                                       as task_count,
                        count(distinct (select task where task_status = 'CLOSED')) as closed_task_count,
                        count(distinct subtask)                                    as subtask_count,
                        count(distinct (select subtask where subtask_is_closed))   as closed_subtask_count
                 from temp
                 group by temp.user_id, temp.user_first_name, temp.user_last_name, temp.user_second_name,
                          temp.user_phone, temp.user_email, temp.user_is_block;
    RETURN statistics;
END;
$$;

CREATE OR REPLACE FUNCTION director_statistics(IN limit_rows bigint, IN offset_rows bigint)
    RETURNS REFCURSOR
    LANGUAGE plpgsql
AS
$$
DECLARE
    statistics REFCURSOR;
BEGIN
    OPEN statistics FOR
        select u.id                                                      as id,
               u.first_name                                              as first_name,
               u.last_name                                               as last_name,
               u.second_name                                             as second_name,
               u.phone                                                   as phone,
               u.email                                                   as email,
               u.is_block                                                as is_block,
               count(e.id)                                               as expertise_count,
               coalesce((select count(e.id) where e.status = 'DONE'), 0) as closed_expertise_count
        from users u
                 left join expertise e on u.id = e.leader_user
        where u.role = 'DIRECTOR'
        group by u.id, u.first_name, u.last_name, u.second_name, u.phone, u.email, u.is_block, e.status
        limit limit_rows offset offset_rows;
    RETURN statistics;
END;
$$;

CREATE OR REPLACE FUNCTION client_statistics(IN limit_rows bigint, IN offset_rows bigint)
    RETURNS REFCURSOR
    LANGUAGE plpgsql
AS
$$
DECLARE
    statistics REFCURSOR;
BEGIN
    OPEN statistics FOR select u.id,
                        u.first_name,
                        u.last_name,
                        u.second_name,
                        u.phone,
                        u.email,
                        u.is_block   as is_block,
                        count(ea.id) as application_count
                 from users u
                          left join expertise_application ea on u.id = ea.creation_user
                 where u.role = 'CUSTOMER'
                 group by u.id, u.first_name, u.last_name, u.second_name, u.phone, u.email, u.is_block
                 limit limit_rows offset offset_rows;
    RETURN statistics;
END;
$$;
