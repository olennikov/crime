package parsing_test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import ru.tsu.crimelab.dao.api.filter.Operator;
import ru.tsu.crimelab.dao.api.filter.fields.BaseField;
import ru.tsu.crimelab.dao.api.filter.fields.IntegerField;

import java.io.IOException;

public class QueryParseTest {

    @Test
    public void deserializeAbstractField () throws IOException {
        String json = "{" +
                "\"fieldName\":\"id\"," +
                "\"operator\":\"EQUALS\","+
                "\"value\":2,"+
                "\"type\":\"IntegerField\""+
                "}";
        ObjectMapper objectMapper = new ObjectMapper();
        BaseField baseField = objectMapper.readValue(json, BaseField.class);
        return;
    }

    @Test
    public void serializeAbstractField () throws JsonProcessingException {
        IntegerField integerField = new IntegerField();
        integerField.setFieldName("id");
        integerField.setOperator(Operator.EQUALS);
        integerField.setValue(1);
        ObjectMapper objectMapper = new ObjectMapper();
        String str = objectMapper.writeValueAsString(integerField);
        return;
    }
}
