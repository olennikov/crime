package ru.tsu.crimelab.email.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by PetrushinDA on 26.10.2019
 */
@Data
@Component
public class EmailConfig {
    @Value("${spring.mail.sentfrom}")
    private String sentfrom;
}
