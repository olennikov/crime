package ru.tsu.crimelab.email.services;

import ru.tsu.crimelab.dto.model.EmailMessageDto;

/**
 * Created by PetrushinDA on 26.10.2019
 */
public interface EmailService {
    void sendMessage(EmailMessageDto message);
}
