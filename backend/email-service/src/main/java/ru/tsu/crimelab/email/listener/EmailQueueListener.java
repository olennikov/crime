package ru.tsu.crimelab.email.listener;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import ru.tsu.crimelab.appstarter.rabbitmq.consts.notification.Queues;
import ru.tsu.crimelab.dto.model.EmailMessageDto;
import ru.tsu.crimelab.email.services.EmailService;

/**
 * Created by PetrushinDA on 26.10.2019
 * Слушатель email очереди RabbitMQ
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class EmailQueueListener {

    private final EmailService emailService;

    /**
     * Обработчки сообщений
     */
    @RabbitListener(queues = Queues.EMAIL)
    public void processNewNotificationEvent(@Payload EmailMessageDto emailMessageDto) {
        log.info("Получено сообщение из очереди {} контент: {}", Queues.EMAIL, emailMessageDto.toString());
        emailService.sendMessage(emailMessageDto);
    }
}
