package ru.tsu.crimelab.email.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import ru.tsu.crimelab.dto.model.EmailMessageDto;
import ru.tsu.crimelab.email.config.EmailConfig;
import ru.tsu.crimelab.email.services.EmailService;

import java.util.Date;

import static java.lang.String.format;

/**
 * Created by PetrushinDA on 26.10.2019
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender javaMailSender;

    private final EmailConfig emailConfig;

    @Override
    public void sendMessage(EmailMessageDto messageDto) {
        try {
            log.info("Отправляем email на адрес {}", messageDto.getEmail());
            javaMailSender.send(buildSimpleMailMessage(messageDto));
        } catch (Exception e) {
            log.error(format("Возникла ошибка при отправке сообщения на адрес {}", messageDto.getEmail()), e);
            throw new RuntimeException(e);
        }
    }

    private SimpleMailMessage buildSimpleMailMessage(EmailMessageDto messageDto) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setReplyTo(emailConfig.getSentfrom());
        simpleMailMessage.setText(messageDto.getText());
        simpleMailMessage.setSentDate(new Date());
        simpleMailMessage.setSubject(messageDto.getSubject());
        simpleMailMessage.setTo(messageDto.getEmail());
        simpleMailMessage.setFrom(emailConfig.getSentfrom());
        return simpleMailMessage;
    }
}
