package ru.tsu.crimelab.filestorage.config;

import io.minio.MinioClient;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidPortException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by PetrushinDA on 27.10.2019
 * Конфигурация файлового хранилища
 */
@Configuration
public class StoreConfig {

    @Value("${crimelab.storage.endpoint}")
    private String endpoint;

    @Value("${crimelab.storage.port}")
    private int port;

    @Value("${crimelab.storage.username}")
    private String username;

    @Value("${crimelab.storage.password}")
    private String password;

    @Bean
    @ConditionalOnProperty(prefix = "crimelab.storage", name = "type", havingValue = "minio")
    public MinioClient minioClient() {
        try {
            return new MinioClient(endpoint, port, username, password);
        } catch (InvalidEndpointException | InvalidPortException e) {
            e.printStackTrace();
        }
        return null;
    }
}
