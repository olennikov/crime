package ru.tsu.crimelab.filestorage.service.storage.impl;

import io.minio.MinioClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import ru.tsu.crimelab.filestorage.BlobObjectWrapper;
import ru.tsu.crimelab.filestorage.service.storage.BlobStorageService;

import java.io.InputStream;
import java.util.Objects;

import static java.lang.String.format;

/**
 * Created by PetrushinDA on 27.10.2019
 * Класс для работы с хранилищем MinIO
 */
@Slf4j
@Service
@RequiredArgsConstructor
@ConditionalOnProperty(prefix = "crimelab.storage", name = "type", havingValue = "minio")
public class MinioBlobStorageServiceImpl implements BlobStorageService {

    private final MinioClient minioClient;

    @Value("${crimelab.storage.container}")
    private String bucketName;

    /**
     * {@inheritDoc}
     */
    @Override
    public InputStream getObject(String objectName) {
        try {
            return minioClient.getObject(bucketName, objectName);
        } catch (Exception e) {
            log.error(format("Возникла ошибка при получении файла с именем %s из хранилища", objectName), e);
            throw new RuntimeException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void putObject(BlobObjectWrapper blobObjectWrapper) {
        try {
            this.createBucketIfNotExists();
            minioClient.putObject(bucketName, blobObjectWrapper.getObjectName(), blobObjectWrapper.getStream(), blobObjectWrapper.getContentType());
        } catch (Exception e) {
            log.error(format("При попытке создать файл с именем %s возникла ошибка", blobObjectWrapper.getObjectName()), e);
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeObject(String objectName) {
        try {
            minioClient.removeObject(bucketName, objectName);
        } catch (Exception e) {
            log.error(format("Возникла ошибка при попытке удалить файла с именем %s из хранилища", objectName), e);
            throw new RuntimeException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean checkExists(String objectName) {
        try {
            return !Objects.isNull(minioClient.statObject(bucketName, objectName));
        } catch (Exception e) {
            log.error(format("Возникла ошибка при попытке определить существование файла с именем %s", objectName), e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Метод для создания объекта bucket, если его нет
     */
    private void createBucketIfNotExists() {
        try {
            if (!minioClient.bucketExists(bucketName)) {
                minioClient.makeBucket(bucketName);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
