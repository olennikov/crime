package ru.tsu.crimelab.filestorage;

import lombok.Builder;
import lombok.Data;

import java.io.InputStream;

/**
 * Created by PetrushinDA on 27.10.2019
 * Класс-обёртка над объектом, который загружается в BLOB-хранилище
 */
@Data
@Builder
public class BlobObjectWrapper {

    /**
     * Название, которое будет дано загружаемому объекту в хранилище
     */
    String objectName;

    /**
     * Поток байт
     */
    InputStream stream;

    /**
     * MIME-тип объекта
     */
    String contentType;
}
