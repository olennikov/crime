package ru.tsu.crimelab.filestorage.service.storage;

import ru.tsu.crimelab.filestorage.BlobObjectWrapper;

import java.io.InputStream;

/**
 * Created by PetrushinDA on 27.10.2019
 * Интерфейс работы с хранилищем бинарных файлов
 */
public interface BlobStorageService {

    /**
     * Скачивание объекта из хранилища
     *
     * @param objectName имя объекта
     * @return поток байт
     */
    InputStream getObject(String objectName);

    /**
     * Сохранение объекта в хранилище
     *
     * @param blobObjectWrapper загружаемый файл
     */
    void putObject(BlobObjectWrapper blobObjectWrapper);

    /**
     * Удаление объекта из хранилища
     *
     * @param objectName имя удаляемого объекта
     */
    void removeObject(String objectName);

    /**
     * Проверка на существование объекта в хранилище
     *
     * @param objectName имя объекта
     * @return признак наличия фпйла в хранилище
     */
    Boolean checkExists(String objectName);
}
