package ru.tsu.crimelab.appstarter.rabbitmq.consts.notification;

/**
 * Created by PetrushinDA on 26.10.2019
 */
public class NotificationExchange {

    public static final String NAME = "notification";

    private NotificationExchange() {
    }
}