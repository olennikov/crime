package ru.tsu.crimelab.appstarter.rabbitmq.declarations;

import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by PetrushinDA on 26.10.2019
 * Класс для регистрации очередей
 */
@Configuration
@Import({NotificationAmqpDeclaration.class})
@AutoConfigureBefore(RabbitAutoConfiguration.class)
public class AmqpDeclaration {

}
