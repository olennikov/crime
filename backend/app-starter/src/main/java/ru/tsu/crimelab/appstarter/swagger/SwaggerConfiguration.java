package ru.tsu.crimelab.appstarter.swagger;

import io.swagger.annotations.Api;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.tsu.crimelab.security.jwt.config.JwtAuthenticationConfig;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;


@Configuration
@EnableSwagger2
public class SwaggerConfiguration {


    @Bean
    public Docket api(JwtAuthenticationConfig jwtAuthenticationConfig) {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                .paths(PathSelectors.any())
                .build()
                .pathMapping("")
                .securitySchemes(Collections.singletonList(new ApiKey(jwtAuthenticationConfig.getName(), jwtAuthenticationConfig.getName(), "header")))
                .securityContexts(Collections.singletonList(securityContext(jwtAuthenticationConfig.getName())));
    }

    private SecurityContext securityContext(String headerName) {
        return SecurityContext.builder()
                .securityReferences(defaultAuth(headerName))
                .forPaths(PathSelectors.any())
                .build();
    }

    private List<SecurityReference> defaultAuth(String headerName) {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Collections.singletonList(new SecurityReference(headerName, authorizationScopes));
    }
}
