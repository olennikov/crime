package ru.tsu.crimelab.appstarter.rabbitmq.internal;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by PetrushinDA on 26.10.2019
 * Свойства для RabbitMQ
 */
@Data
@ConfigurationProperties(prefix = "crimelab.rabbitmq")
public class RabbitMqProperties {

    private Boolean enabled = true;

    private String host = "localhost";

    private Integer port = 5672;

    private String username = "guest";

    private String password = "guest";

    private boolean transacted = false;

    /**
     * Максимальное количество попыток обработать ошибочное сообщение
     */
    private int dlqMaxAttempts = 3;

    /**
     * Время задержки в секундах перед повторной опопыткой отправки
     */
    private int dlqDelay = 5 * 1000;

    /**
     * Включить слушателя очереди DLQ (очередь для повторной обработки ошибочных сообщений)
     */
    private boolean dlqListening = false;

    public void setDlqDelay(int dlqDelay) {
        this.dlqDelay = dlqDelay * 1000;
    }
}
