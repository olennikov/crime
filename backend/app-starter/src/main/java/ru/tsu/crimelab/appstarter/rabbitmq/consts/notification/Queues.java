package ru.tsu.crimelab.appstarter.rabbitmq.consts.notification;

/**
 * Created by PetrushinDA on 26.10.2019
 */
public class Queues {
    public static final String SOCKET = "notification.socket";
    public static final String EMAIL = "notification.email";
    public static final String ERRORS = "notification.errors";
    public static final String DLQ = "notification.dlq";

    private Queues() {
    }
}