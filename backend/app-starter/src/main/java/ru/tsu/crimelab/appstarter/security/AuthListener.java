package ru.tsu.crimelab.appstarter.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.authentication.event.LogoutSuccessEvent;
import org.springframework.util.Assert;
import ru.tsu.crimelab.security.jwt.JwtAuthenticationToken;
import ru.tsu.crimelab.security.jwt.JwtPreAuthenticationToken;
import ru.tsu.crimelab.security.jwt.JwtService;
import ru.tsu.crimelab.security.jwt.session.SessionStorage;

import static ru.tsu.crimelab.security.jwt.utils.Constants.SESSION_ID;

/**
 * Created by PetrushinDA on 08.01.2020
 */
@Slf4j
@RequiredArgsConstructor
public class AuthListener {

    private final SessionStorage sessionStorage;

    private final JwtService jwtService;

    /**
     * Слушатель успешной аутентификации
     */
    @EventListener(AuthenticationSuccessEvent.class)
    public void authSuccess(AuthenticationSuccessEvent authenticationSuccessEvent) {
        var authentication = (JwtPreAuthenticationToken) authenticationSuccessEvent.getAuthentication();
        Assert.notNull(authentication, "Получен пустой объект Authentication. Запись сессии в Redis невозможна");
        var jwtPayload = jwtService.decodeJwt(authentication.getJwt());
        var userId = jwtPayload.getId();
        var sessionId = jwtPayload.getDetails().get(SESSION_ID);
        sessionStorage.addRecord(userId, sessionId);
    }

    /**
     * Слушатель успешного выхода из системы
     */
    @EventListener(LogoutSuccessEvent.class)
    public void logoutSuccess(LogoutSuccessEvent logoutSuccessEvent) {
        var authentication = (JwtAuthenticationToken) logoutSuccessEvent.getAuthentication();
        Assert.notNull(authentication, "Получен пустой объект Authentication. Удаление сессии из Redis невозможна");
        var details = authentication.getDetails();
        var userId = details.getId();
        var sessionId = details.getSessionId();
        sessionStorage.removeRecord(userId, sessionId);
    }

    /**
     * Слушатель блокировки пользователя
     */
    @EventListener(BlockUserEvent.class)
    public void blockUser(BlockUserEvent blockUserEvent) {
        var userId = blockUserEvent.getBlockingUserId();
        sessionStorage.removeAllForUser(userId.toString());
    }
}
