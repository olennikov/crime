package ru.tsu.crimelab.appstarter.rabbitmq.consts.headers;

/**
 * Created by PetrushinDA on 26.10.2019
 * Константы для DeadLetter
 */
public class DeadLetter {
    public static final String EXCHANGE_NAME = "x-dead-letter-exchange";
    public static final String ROUTING_KEY = "x-dead-letter-routing-key";

    private DeadLetter() {
    }
}