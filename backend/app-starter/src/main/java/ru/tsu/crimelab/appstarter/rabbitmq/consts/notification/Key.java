package ru.tsu.crimelab.appstarter.rabbitmq.consts.notification;

/**
 * Created by PetrushinDA on 26.10.2019
 */
public class Key {

    /**
     * Отправка уведомления по email
     */
    public static final String EMAIL = "email";

    /**
     * Отправка уведомления по web socket на ui
     */
    public static final String SOCKET = "socket";

    /**
     * Отправка уведомления error
     */
    public static final String ERRORS = "errors";

    /**
     * Отправка уведомления dlq
     */
    public static final String DLQ = "dlq";

    private Key() {
    }
}