package ru.tsu.crimelab.appstarter.rabbitmq.internal;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import ru.tsu.crimelab.appstarter.rabbitmq.consts.headers.Header;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static java.util.Optional.ofNullable;
import static ru.tsu.crimelab.appstarter.rabbitmq.consts.headers.DeadMeta.*;
import static ru.tsu.crimelab.appstarter.rabbitmq.consts.headers.Header.DELAY;
import static ru.tsu.crimelab.appstarter.rabbitmq.consts.headers.Header.RETRIES_COUNT;

/**
 * Created by PetrushinDA on 26.10.2019
 * Базовый класс с инструментами для распределения ошибочных сообщений очереди
 */
@SuppressWarnings("unchecked")
@Slf4j
public abstract class BaseDeadLetterQueueListener {

    private final AmqpTemplate template;
    private final RabbitMqProperties rabbitMqProperties;

    protected BaseDeadLetterQueueListener(AmqpTemplate template, RabbitMqProperties rabbitMqProperties) {
        this.template = template;
        this.rabbitMqProperties = rabbitMqProperties;
    }

    protected void handle(Message msg, String exchangeName, String errorsKey) {
        Map<String, Object> headers = msg.getMessageProperties().getHeaders();
        Integer attempts = getAttemptsCount(headers);

        createId(headers);

        retrieveOriginRoutingKey(headers)
                .ifPresentOrElse(
                        originKey -> check(headers, attempts, exchangeName, originKey, errorsKey, msg),
                        () -> sendToErrorsStorage(exchangeName, errorsKey, msg)
                );
    }

    /**
     * Разрешает отправку сообщения или повторно в оригинальный обменник или в хранилище в фатальными ошибками
     *
     * @param headers   заголовки
     * @param attempts  количество произведенных попыток
     * @param exchange  обменик с которым идет работа
     * @param originKey ключ маршрутизации оригинального сообщения
     * @param errorsKey ключ маршрутизации хранилища фатальных ошибок
     * @param msg       сообщение
     */
    private void check(Map<String, Object> headers, Integer attempts, String exchange, String originKey, String errorsKey, Message msg) {
        if (attempts <= rabbitMqProperties.getDlqMaxAttempts()) {

            increment(headers, attempts);

            log.warn("Попытка № {} повторно отправить сообщение c id={} в exchange {} по ключу {}.", attempts, getId(headers), exchange, originKey);
            retryToOrigin(exchange, originKey, msg);
            return;
        }
        log.warn("Сообщение c id={} исчерпало количество попыток и было отброшено в очередь ошибок", getId(headers));
        sendToErrorsStorage(exchange, errorsKey, msg);
    }

    /**
     * Инкрементирует счетчик повторных попыток
     *
     * @param headers  заголовки сообщения
     * @param attempts количество попыток
     */
    private void increment(Map<String, Object> headers, Integer attempts) {
        headers.put(RETRIES_COUNT, ++attempts);
        headers.put(DELAY, rabbitMqProperties.getDlqDelay());
    }

    /**
     * Повторно отправляет в оригинальную очередь
     *
     * @param exchangeName     имя оригинального обменника
     * @param originRoutingKey имя оригинального ключа маршрутизации
     * @param message          сообщение
     */
    private void retryToOrigin(String exchangeName, String originRoutingKey, Message message) {
        template.convertAndSend(exchangeName, originRoutingKey, message);
    }

    /**
     * Отправляет сообщение в очередь ошибок
     *
     * @param exchangeName    имя обменника
     * @param errorRoutingKey имя ключа маршрутизации для очереди ошибок
     * @param message         сообщение
     */
    private void sendToErrorsStorage(String exchangeName, String errorRoutingKey, Message message) {
        template.convertAndSend(exchangeName, errorRoutingKey, message);
    }

    /**
     * Получить количество попыток обработать сообщение из заголовков
     *
     * @param headers заголовки
     * @return число попыток
     */
    private Integer getAttemptsCount(Map<String, Object> headers) {
        return ofNullable(headers.get(RETRIES_COUNT))
                .map(c -> (Integer) c)
                .orElse(0);
    }

    /**
     * Извлекает оригинальный ключ маршрутизации из заголовков
     *
     * @param headers заголовки
     */
    private Optional<String> retrieveOriginRoutingKey(Map<String, Object> headers) {
        return retrieveXDeadHeaders(headers)
                .flatMap(h -> ofNullable(h.get("routing-keys")))
                .map(keysRawHeaders -> (List<String>) keysRawHeaders)
                .flatMap(listKeys -> listKeys.stream().findFirst());
    }

    /**
     * Извлекает заголовки с мето информацией о DL
     *
     * @param headers заголовки
     */
    private Optional<Map<String, Object>> retrieveXDeadHeaders(Map<String, Object> headers) {
        return ofNullable(headers.get(NAME))
                .map(rawHeaders -> (List<Map<String, Object>>) rawHeaders)
                .flatMap(listHeaders -> listHeaders.stream().findFirst());
    }

    /**
     * Создать рандомный кастомный  UUID для сообщения
     *
     * @param headers заголовки
     */
    private void createId(Map<String, Object> headers) {
        if (!headers.containsKey(Header.ID)) {
            headers.put(Header.ID, UUID.randomUUID().toString());
        }
    }

    /**
     * Получить кастомный UUID сообщения
     *
     * @param headers заголовки
     * @return UUID сообщения
     */
    private String getId(Map<String, Object> headers) {
        return ofNullable(headers.get(Header.ID).toString()).orElse("");
    }
}
