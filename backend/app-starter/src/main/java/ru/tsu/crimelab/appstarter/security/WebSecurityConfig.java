package ru.tsu.crimelab.appstarter.security;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.tsu.crimelab.appstarter.redis.RedisConfig;
import ru.tsu.crimelab.appstarter.redis.RedisSessionStorage;
import ru.tsu.crimelab.security.jwt.JwtAuthenticationProvider;
import ru.tsu.crimelab.security.jwt.JwtService;
import ru.tsu.crimelab.security.jwt.session.SessionStorage;

import java.time.Duration;
import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
@Configuration
public class WebSecurityConfig {

    private final RedisConfig redisConfig;

    @Bean
    public SessionStorage sessionStorage(@Qualifier("session-template") RedisTemplate<String, List<String>> template) {
        return new RedisSessionStorage(template);
    }

    @Bean
    public AuthListener authListener(SessionStorage sessionStorage, JwtService jwtService) {
        return new AuthListener(sessionStorage, jwtService);
    }

    @Bean
    JedisConnectionFactory jedisConnectionFactory() {
        JedisClientConfiguration poolConfiguration = JedisClientConfiguration.builder()
                .clientName("redis-client").usePooling().build();

        RedisStandaloneConfiguration conf = new RedisStandaloneConfiguration(redisConfig.getHost(), redisConfig.getPort());
        conf.setDatabase(redisConfig.getDatabase());

        return new JedisConnectionFactory(conf, poolConfiguration);
    }

    @Bean(name = "session-template")
    public RedisTemplate<String, List<String>> redisTemplate() {
        final RedisTemplate<String, List<String>> template = new RedisTemplate<>();
        template.setConnectionFactory(jedisConnectionFactory());
        template.setValueSerializer(new JdkSerializationRedisSerializer());
        return template;
    }

    @Bean
    public JwtAuthenticationProvider jwtAuthenticationProvider(JwtService jwtService, SessionStorage sessionStorage) {
        return new JwtAuthenticationProvider(Collections.emptyMap(), Duration.ofHours(1), jwtService, sessionStorage);
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }
}
