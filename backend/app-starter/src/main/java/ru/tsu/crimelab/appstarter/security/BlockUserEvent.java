package ru.tsu.crimelab.appstarter.security;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * Created by PetrushinDA on 08.01.2020
 * Событие блокировки пользователя
 */
@Getter
public class BlockUserEvent extends ApplicationEvent {

    /**
     * Идентификатор блокируемого пользователя
     */
    private Long blockingUserId;

    public BlockUserEvent(Object source, Long blockingUserId) {
        super(source);
        this.blockingUserId = blockingUserId;
    }
}
