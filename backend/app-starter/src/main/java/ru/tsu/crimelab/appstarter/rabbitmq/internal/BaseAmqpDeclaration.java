package ru.tsu.crimelab.appstarter.rabbitmq.internal;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;

import static ru.tsu.crimelab.appstarter.rabbitmq.consts.headers.DeadLetter.EXCHANGE_NAME;
import static ru.tsu.crimelab.appstarter.rabbitmq.consts.headers.DeadLetter.ROUTING_KEY;
import static ru.tsu.crimelab.appstarter.rabbitmq.consts.headers.DeadMeta.EXCHANGE_TYPE;

/**
 * Created by PetrushinDA on 26.10.2019
 * Класс для базовых методов декларации очередей, обменников и связи между ними
 */
@SuppressWarnings({"SameParameterValue", "unused", "WeakerAccess"})
@Slf4j
public class BaseAmqpDeclaration {

    protected BaseAmqpDeclaration() {

    }

    /**
     * Создает обменник с указанным именем и дефолтным типом Topic
     *
     * @param name имя обменника
     */
    protected Exchange makeExchange(String name) {
        return makeExchange(name, ExchangeType.TOPIC);
    }

    /**
     * Создает обменник с указанным именем и типом из ExchangeTypes
     * например ExchangeTypes.TOPIC
     *
     * @param name имя обменника
     * @param type тип обменника
     */
    protected Exchange makeExchange(String name, ExchangeType type) {
        log.debug("Подключается exchange {}", name);
        return new ExchangeBuilder(name, type.name)
                .durable(true)
                .withArgument(EXCHANGE_TYPE, type.name)
                .delayed()
                .build();
    }

    /**
     * Создает очередь с указанным именем
     *
     * @param name имя очереди
     */
    protected Queue makeQueue(String name) {
        log.debug("Подключается очередь {}", name);
        return QueueBuilder.durable(name).build();
    }

    /**
     * Создает очередь поддерживающую особую обработку сообщений с ошибками
     *
     * @param name                   имя
     * @param deadLetterExchangeName имя обменника в котором находится dead letter очередь
     * @param deadLetterRoutingKey   имя ключа маршрутизации для dead letter очереди
     */
    protected Queue makeQueue(String name, String deadLetterExchangeName, String deadLetterRoutingKey) {
        log.debug("Подключается очередь {}", name);
        return QueueBuilder
                .durable(name)
                .withArgument(EXCHANGE_NAME, deadLetterExchangeName)
                .withArgument(ROUTING_KEY, deadLetterRoutingKey)
                .build();
    }

    /**
     * Связывает очередь с обменником по ключу маршрутизации
     *
     * @param queue      очередь
     * @param exchange   обменник
     * @param bindingKey ключ
     */
    protected Binding bind(Queue queue, Exchange exchange, String bindingKey) {
        log.debug("Очередь {} привязана к обменнику {} по ключу маршрутизации {}", queue.getName(), exchange.getName(), bindingKey);
        return BindingBuilder.bind(queue).to(exchange).with(bindingKey).noargs();
    }

}
