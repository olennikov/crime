package ru.tsu.crimelab.appstarter.redis;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import ru.tsu.crimelab.security.jwt.session.SessionStorage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PetrushinDA on 08.01.2020
 * Redis реализация SessionStorage
 */
public class RedisSessionStorage implements SessionStorage {

    @Qualifier("session-template")
    private final RedisTemplate<String, List<String>> redisTemplate;

    public RedisSessionStorage(RedisTemplate<String, List<String>> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public boolean nonExpired(String userId, String session) {
        if (!redisTemplate.hasKey(userId))
            return false;

        var sessionsForUser = redisTemplate.opsForValue().get(userId);
        return sessionsForUser.contains(session);
    }

    @Override
    public boolean addRecord(String userId, String session) {
        var userSessions = redisTemplate.opsForValue().get(userId);
        if (userSessions == null)
            userSessions = new ArrayList<>();
        userSessions.add(session);
        redisTemplate.opsForValue().getAndSet(userId, userSessions);
        return true;
    }

    @Override
    public boolean removeRecord(String userId, String session) {
        var userSessions = redisTemplate.opsForValue().get(userId);
        if (userSessions == null)
            return true;

        userSessions.remove(session);
        redisTemplate.opsForValue().getAndSet(userId, userSessions);
        return true;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public boolean removeAllForUser(String userId) {
        return redisTemplate.delete(userId);
    }
}
