package ru.tsu.crimelab.appstarter.rabbitmq;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import ru.tsu.crimelab.appstarter.rabbitmq.internal.BaseDeadLetterQueueListener;
import ru.tsu.crimelab.appstarter.rabbitmq.internal.RabbitMqProperties;

import static ru.tsu.crimelab.appstarter.rabbitmq.consts.notification.Key.DLQ;
import static ru.tsu.crimelab.appstarter.rabbitmq.consts.notification.Key.ERRORS;
import static ru.tsu.crimelab.appstarter.rabbitmq.consts.notification.NotificationExchange.NAME;

/**
 * Created by PetrushinDA on 26.10.2019
 * Слушатель DLQ очередей
 * Получает сообщения которые которые не смогли быть обработаны из-за ошибок
 * и решает отправить ли их повторно по таймауту или после исчерпания числа попыток
 * отбросить в мертвую очередь errors
 */
@Slf4j
public class DeadLetterQueueListener extends BaseDeadLetterQueueListener {

    public DeadLetterQueueListener(AmqpTemplate template, RabbitMqProperties rabbitMqProperties) {
        super(template, rabbitMqProperties);
    }

    @RabbitListener(queues = {DLQ})
    public void notificationDeadLetterQueue(Message msg) {
        handle(msg, NAME, ERRORS);
    }
}
