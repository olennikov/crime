package ru.tsu.crimelab.appstarter.rabbitmq.internal;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;
import ru.tsu.crimelab.appstarter.rabbitmq.DeadLetterQueueListener;
import ru.tsu.crimelab.appstarter.rabbitmq.declarations.AmqpDeclaration;

/**
 * Created by PetrushinDA on 26.10.2019
 * Общая настройка RabbitTemplate а также сериализации и десериализации сообщений в json,
 */
@Configuration
@EnableConfigurationProperties(RabbitMqProperties.class)
@AutoConfigureBefore(RabbitAutoConfiguration.class)
@RequiredArgsConstructor
@Import({AmqpDeclaration.class})
@Slf4j
public class CrimelabRabbitMqAutoConfiguration implements RabbitListenerConfigurer {

    // Интервал между попытками восстановления
    private static final long retryConnectingInterval = 60000L;
    private final RabbitMqProperties rabbitMqProperties;

    /**
     * Настройка подключения к RabbitMQ
     */
    @Bean
    @ConditionalOnMissingBean(name = "connectionFactory")
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory(rabbitMqProperties.getHost());
        connectionFactory.setPort(rabbitMqProperties.getPort());
        connectionFactory.setUsername(rabbitMqProperties.getUsername());
        connectionFactory.setPassword(rabbitMqProperties.getPassword());

        return connectionFactory;
    }

    /**
     * Устанавливаем messageConverter для конвертации сообщений в json при записи в очередь
     */
    @Bean
    @ConditionalOnMissingBean(name = "rabbitTemplate")
    public RabbitTemplate rabbitTemplate() {

        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory());

        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter(objectMapper()));
        //Подключение транзакционной модели для данного канала
        rabbitTemplate.setChannelTransacted(rabbitMqProperties.isTransacted());
        log.debug("RabbitMQ запускается с конфигурацией транзакционной модели {}", rabbitMqProperties.isTransacted());

        return rabbitTemplate;
    }

    /**
     * Переопределяем бин, создающий RabbitListener'ы для добавления имени активного профиля к названию очереди
     */
    @Bean
    @ConditionalOnMissingBean(name = "rabbitListenerContainerFactory")
    public RabbitListenerContainerFactory rabbitListenerContainerFactory() {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory());
        factory.setDefaultRequeueRejected(false);
        factory.setRecoveryInterval(retryConnectingInterval);
        return factory;
    }

    /**
     * Устанавливаем messageHandler для чтения из json
     */
    @Override
    public void configureRabbitListeners(RabbitListenerEndpointRegistrar rabbitListenerEndpointRegistrar) {
        rabbitListenerEndpointRegistrar.setMessageHandlerMethodFactory(messageHandlerMethodFactory());
    }

    /**
     * Создаем обработчик сообщений с созданным ранее конвертером из json
     */
    @Bean
    public DefaultMessageHandlerMethodFactory messageHandlerMethodFactory() {
        DefaultMessageHandlerMethodFactory factory = new DefaultMessageHandlerMethodFactory();
        factory.setMessageConverter(consumerJackson2MessageConverter());
        return factory;
    }

    /**
     * Конвертер для чтения сообщений в формате json
     */
    private MappingJackson2MessageConverter consumerJackson2MessageConverter() {
        MappingJackson2MessageConverter mappingJackson2MessageConverter = new MappingJackson2MessageConverter();
        mappingJackson2MessageConverter.setObjectMapper(objectMapper());
        return mappingJackson2MessageConverter;
    }

    /**
     * Создаем objectMapper и настраиваем сериализацию даты и времени.
     */
    private ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper()
                .registerModule(new Jdk8Module())
                .registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        return objectMapper;
    }

    @Bean
    @ConditionalOnProperty(prefix = "crimelab.rabbitmq", name = "dlq-listening", havingValue = "true")
    public DeadLetterQueueListener deadLetterQueueListener() {
        return new DeadLetterQueueListener(rabbitTemplate(), rabbitMqProperties);
    }
}