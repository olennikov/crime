package ru.tsu.crimelab.appstarter.rabbitmq.consts.headers;

/**
 * Created by PetrushinDA on 26.10.2019
 * Константы хедеров сообщения
 */
public class Header {

    /**
     * Идентификатор сообщения
     */
    public static final String ID = "x-message-inner-uuid";

    /**
     * Количество попыток
     */
    public static final String RETRIES_COUNT = "x-retries";

    /**
     * Задержка в милисекундах между попытками
     */
    public static final String DELAY = "x-delay";

    private Header() {
    }
}