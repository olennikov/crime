package ru.tsu.crimelab.appstarter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.lang.NonNull;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.springframework.core.Ordered.LOWEST_PRECEDENCE;

/**
 * Created by PetrushinDA on 19.10.2019
 * Подгружает дефолтные конфиги из файлов конфигурации
 * В зависимости от профилья загружает файл с общими настройками и специфичный для профиля файл настроек
 * <p>
 * ОСОБОЕ ВНИМАНИЕ!!!
 * Настройки из подгружаемых файлов перекрывают дефолтные настроки заданные через @{@link org.springframework.boot.context.properties.ConfigurationProperties}
 * Поэтому подсказки в IDE о использованных дефолтных параметрах могут быть не верными!!!
 */
@Slf4j
@Order(LOWEST_PRECEDENCE - 1)
public class AutoConfigurationPropertyLoader implements EnvironmentPostProcessor {

    private static final String PROPERTIES_FILE_PATH = "ru/tsu/crimelab/appstarter/";
    private static final String PROPERTIES_FILE_PREFIX = "app-";
    private static final String PROPERTIES_FILE_POSTFIX = ".properties";

    private static final String PROPERTIES_FILE_DEFAULT = "app.properties";

    @Override
    public void postProcessEnvironment(ConfigurableEnvironment env, SpringApplication application) {

         String profile = checkActiveProfile(env);

        // Грузим общие настройки
        Resource commonResource = new ClassPathResource(PROPERTIES_FILE_PATH + PROPERTIES_FILE_DEFAULT);
        PropertySource<?> commonPropertySource = loadSource(commonResource, PROPERTIES_FILE_DEFAULT);
        env.getPropertySources().addLast(commonPropertySource);

        // Грузим настройки для профиля
        String resourceName = PROPERTIES_FILE_PREFIX + profile + PROPERTIES_FILE_POSTFIX;
        Resource profileResource = new ClassPathResource(PROPERTIES_FILE_PATH + resourceName);
        PropertySource<?> profilePropertySource = loadSource(profileResource, resourceName);
        env.getPropertySources().addBefore(PROPERTIES_FILE_DEFAULT, profilePropertySource);

        log.info("Загружены дефолтные настройки для профиля " + profile);
    }

    private PropertySource<?> loadSource(@NonNull Resource resource, @NonNull String name) {
        try {
            Properties properties = new Properties();
            properties.load(new InputStreamReader(resource.getInputStream(), UTF_8));
            return new PropertiesPropertySource(name, properties);
        } catch (IOException ex) {
            throw new IllegalStateException("Failed to load properties configuration from " + resource, ex);
        }
    }

    /**
     * Проверяет текущий активный профиль
     *
     * @return текущий активный профиль
     */
    private String checkActiveProfile(Environment env) {

        var activeProfiles = env.getActiveProfiles();
        if (activeProfiles.length == 0)
            throw new RuntimeException("Запуск приложения без указания профиля запрещен");

        if (activeProfiles.length > 1)
            throw new RuntimeException("Приложение не поддерживает работу с несколькими активными профилями");

        String profile = activeProfiles[0];

        if (!Profiles.list().contains(profile)) {
            throw new RuntimeException("Приложение не поддерживает работбу с профилем " + profile);
        }

        return profile;
    }

    @Getter
    @AllArgsConstructor
    private enum Profiles {
        LOCAL("local"),
        PROD("prod");
        private String name;

        public static List<String> list(){
            return Arrays.stream(values())
                    .map(Profiles::getName)
                    .collect(Collectors.toList());
        }
    }
}
