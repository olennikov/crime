package ru.tsu.crimelab.appstarter.rabbitmq.internal;

import org.springframework.amqp.core.ExchangeTypes;

/**
 * Created by PetrushinDA on 26.10.2019
 * Типы обменников
 */
public enum ExchangeType {

    /**
     * Направляет сообщение напрямую в определеную очередь
     */
    DIRECT(ExchangeTypes.DIRECT),

    /**
     * Направляет сообщения во все очереди обменника
     */
    FANOUT(ExchangeTypes.FANOUT),

    /**
     * Направляет в соответствующие топики
     */
    TOPIC(ExchangeTypes.TOPIC),

    /**
     * Направляет согласно заголовкам
     */
    HEADERS(ExchangeTypes.HEADERS);

    /**
     * Имя обменника
     */
    public final String name;

    ExchangeType(String name) {
        this.name = name;
    }
}
