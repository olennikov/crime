package ru.tsu.crimelab.appstarter.redis;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration()
@ConfigurationProperties(prefix = "spring.redis")
public class RedisConfig {
    private Boolean enabled = true;
    private String host = "localhost";
    private Integer port = 6379;
    private Integer database = 0;
}