package ru.tsu.crimelab.appstarter.rabbitmq.declarations;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.tsu.crimelab.appstarter.rabbitmq.consts.notification.Key;
import ru.tsu.crimelab.appstarter.rabbitmq.consts.notification.NotificationExchange;
import ru.tsu.crimelab.appstarter.rabbitmq.consts.notification.Queues;
import ru.tsu.crimelab.appstarter.rabbitmq.internal.BaseAmqpDeclaration;

/**
 * Created by PetrushinDA on 26.10.2019
 * Notification очереди
 */
@Configuration
public class NotificationAmqpDeclaration extends BaseAmqpDeclaration {

    protected NotificationAmqpDeclaration() {
        super();
    }

    @Bean
    @ConditionalOnMissingBean(name = "notificationExchange")
    public Exchange notificationExchange() {
        return makeExchange(NotificationExchange.NAME);
    }

    /**
     * Очередь для уведомлений модуля нотификаций UI по вебсокетам
     */
    @Bean
    @ConditionalOnMissingBean(name = "notificationsSocketQueue")
    public Queue notificationsSocketQueue() {
        return makeQueue(Queues.SOCKET, NotificationExchange.NAME, Key.DLQ);
    }

    @Bean
    @ConditionalOnMissingBean(name = "notificationsSocketBinding")
    public Binding notificationsSocketBinding() {
        return bind(notificationsSocketQueue(), notificationExchange(), Key.SOCKET);
    }

    /**
     * Очередь для уведомлений модуля рассылки писем
     */
    @Bean
    @ConditionalOnMissingBean(name = "notificationsEmailQueue")
    public Queue notificationEmailQueue() {
        return makeQueue(Queues.EMAIL, NotificationExchange.NAME, Key.DLQ);
    }

    @Bean
    @ConditionalOnMissingBean(name = "notificationsEmailBinding")
    public Binding notificationEmailBinding() {
        return bind(notificationEmailQueue(), notificationExchange(), Key.EMAIL);
    }

    @Bean
    @ConditionalOnMissingBean(name = "notificationDeadLetterQueue")
    public Queue notificationDeadLetterQueue() {
        return makeQueue(Queues.DLQ);
    }

    @Bean
    @ConditionalOnMissingBean(name = "notificationDeadLetterBinding")
    public Binding notificationDeadLetterBinding() {
        return bind(notificationDeadLetterQueue(), notificationExchange(), Key.DLQ);
    }

    /**
     * Очередь для фатальных ошибок
     */
    @Bean
    @ConditionalOnMissingBean(name = "notificationErrorQueue")
    public Queue notificationErrorQueue() {
        return makeQueue(Queues.ERRORS);
    }

    @Bean
    @ConditionalOnMissingBean(name = "notificationErrorsBinding")
    public Binding notificationErrorsBinding() {
        return bind(notificationErrorQueue(), notificationExchange(), Key.ERRORS);
    }
}
