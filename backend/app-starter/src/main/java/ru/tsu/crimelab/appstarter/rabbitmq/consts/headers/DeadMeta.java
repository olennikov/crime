package ru.tsu.crimelab.appstarter.rabbitmq.consts.headers;

/**
 * Created by PetrushinDA on 26.10.2019
 * Константы для DeadMeta
 */
public class DeadMeta {
    public static final String NAME = "x-death";
    public static final String EXCHANGE_TYPE = "x-delayed-type";

    private DeadMeta() {
    }
}