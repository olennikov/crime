package ru.tsu.crimelab.appstarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;
import ru.tsu.crimelab.appstarter.rabbitmq.internal.CrimelabRabbitMqAutoConfiguration;
import ru.tsu.crimelab.appstarter.security.WebSecurityAdapterConfig;
import ru.tsu.crimelab.appstarter.security.WebSecurityConfig;
import ru.tsu.crimelab.appstarter.swagger.SwaggerConfiguration;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Collections.emptySet;
import static java.util.Collections.singletonMap;
import static java.util.Optional.ofNullable;
import static org.springframework.core.Ordered.LOWEST_PRECEDENCE;

/**
 * Created by PetrushinDA on 30.10.2019
 * Отвечает за отключение различных автоконфигов.
 * Для отключения тех или иных конфигов необходимо в файле параметров указать crimelab.[имя конфига].enable=false
 * Список доступных конфигов можно посмотреть ниже в коде
 */
@Order(LOWEST_PRECEDENCE)
public class AutoConfigurationExclude implements EnvironmentPostProcessor {

    private static final String CUSTOM_EXCLUSIONS_PROPERTY_SOURCE = "customExclusionsPropertySource";
    private static final String PROPERTY_NAME_AUTOCONFIGURE_EXCLUDE = "spring.autoconfigure.exclude";

    private static final String SECURITY_ENABLED_PARAMETER = "crimelab.security.enabled";
    private static final String SWAGGER_ENABLED_PARAMETER = "crimelab.swagger.enabled";
    private static final String RABBITMQ_ENABLED_PARAMETER = "crimelab.rabbitmq.enabled";

    static final Map<String, String> EXCLUSION_PARAMETER_MAP = Map.of(
            SECURITY_ENABLED_PARAMETER, "Авторизация",
            SWAGGER_ENABLED_PARAMETER, "Swagger API",
            RABBITMQ_ENABLED_PARAMETER, "RabbitMQ"
    );

    private static final Set<String> parameters = EXCLUSION_PARAMETER_MAP.keySet();

    @Override
    public void postProcessEnvironment(ConfigurableEnvironment env, SpringApplication application) {
        Set<String> exclusions = new HashSet<>();
        parameters.forEach(p -> {
            Boolean isEnabled = env.getProperty(p, Boolean.class, true);
            if (!isEnabled) {
                exclusions.addAll(getExclusions(p));
            }
        });

        addExclusions(env, exclusions);
    }

    /**
     * Добавляет список исключенных автоконфигураций в общий конфиг
     */
    private void addExclusions(ConfigurableEnvironment env, Set<String> exclusions) {
        String value = Stream.concat(
                ofNullable(env.getProperty(PROPERTY_NAME_AUTOCONFIGURE_EXCLUDE, String[].class))
                        .map(Arrays::asList)
                        .orElse(Collections.emptyList())
                        .stream(), exclusions.stream())
                .distinct()
                .collect(Collectors.joining(","));

        MutablePropertySources propertySources = env.getPropertySources();

        Map<String, Object> map = singletonMap(PROPERTY_NAME_AUTOCONFIGURE_EXCLUDE, value);
        MapPropertySource source = new MapPropertySource(CUSTOM_EXCLUSIONS_PROPERTY_SOURCE, map);

        if (propertySources.contains(CUSTOM_EXCLUSIONS_PROPERTY_SOURCE)) {
            propertySources.replace(CUSTOM_EXCLUSIONS_PROPERTY_SOURCE, source);
        } else {
            propertySources.addFirst(source);
        }
    }

    /**
     * Генерирует список исключений для каждого из автоконфигов
     * Если нужно чтото добполнительно исключить, то желательно перечислять это тут
     */
    private Set<String> getExclusions(String parameter) {
        switch (parameter) {

            case SECURITY_ENABLED_PARAMETER:
                return setOf(
                        WebSecurityConfig.class,
                        WebSecurityAdapterConfig.class,
                        SecurityAutoConfiguration.class);
            case SWAGGER_ENABLED_PARAMETER:
                return setOf(
                        SwaggerConfiguration.class);

            case RABBITMQ_ENABLED_PARAMETER:
                return setOf(
                        CrimelabRabbitMqAutoConfiguration.class,
                        RabbitAutoConfiguration.class);

            default:
                return emptySet();
        }
    }

    private Set<String> setOf(Class<?>... clazz) {
        return Stream.of(clazz)
                .map(Class::getName)
                .collect(Collectors.toSet());
    }
}
