chcp 65001

echo "Начинаем скачивание образов"
if exist ./docker-compose-gitlab.yml (
    call docker login -u olennikov@utmn.ru -p password registry.gitlab.com/olennikov/crime
    call docker-compose -f docker-compose-gitlab.yml pull && docker-compose -f docker-compose-gitlab.yml up --detach
    call docker logout registry.gitlab.com/petrushinda/criminalistics-laboratory
    echo  "Done..."
) else (
    echo "Не найден файл docker-compose-gitlab.yml. Разместите данный файл в одну директорию с файлом docker-compose-gitlab.yml"
)
